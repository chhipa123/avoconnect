package com.avoconnect.testingcard;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.FileProvider;

import android.Manifest;
import android.app.Dialog;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.hardware.Camera;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.StrictMode;
import android.provider.MediaStore;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.avoconnect.BuildConfig;
import com.avoconnect.Prefrence.UserPrefrence;
import com.avoconnect.R;
import com.avoconnect.Util.CameraUtils;
import com.avoconnect.Util.CameraUtils_Temp;
import com.avoconnect.Util.ProgressBarCustom;
import com.avoconnect.activity.AddEditCardActitivty;

import com.avoconnect.activity.DetialsCardActivity;
import com.avoconnect.api.RestClient2_Ocr;
import com.avoconnect.responsemodel.BusinessCardInfo;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.firebase.ml.vision.FirebaseVision;
import com.google.firebase.ml.vision.common.FirebaseVisionImage;
import com.google.firebase.ml.vision.text.FirebaseVisionText;
import com.google.firebase.ml.vision.text.FirebaseVisionTextRecognizer;
import com.karumi.dexter.Dexter;
import com.karumi.dexter.MultiplePermissionsReport;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.DexterError;
import com.karumi.dexter.listener.PermissionRequest;
import com.karumi.dexter.listener.PermissionRequestErrorListener;
import com.karumi.dexter.listener.multi.MultiplePermissionsListener;
import com.theartofdev.edmodo.cropper.CropImage;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URI;
import java.net.URL;
import java.util.List;

import retrofit.RetrofitError;
import retrofit.client.Response;

public class TextActivity extends AppCompatActivity {

    private static final int PIC_CROP = 125;

    public static final int MEDIA_TYPE_IMAGE = 1;
    String profilePath="";
    public static String cropfilepath="";
    private static final int CAMERA_REQUEST = 0;
    private static final int BITMAP_SAMPLE_SIZE = 8;
    public static Bitmap profileBitmap;
    Context mcontext;
    public File imageFile;
    ProgressBarCustom progressBarCustom;

    FirebaseVisionText imagescan_str_first,imagescan_str_second;
    Bitmap firstcard_bitmap,secondcard_bitmap;
    int count_card=1;
    EditText et_fullname, et_desingnation, et_mobile, et_email, et_company, et_website, et_address;
    TextView txt_login;
    ImageView imageView;
    LinearLayout llCardDetail;
    FloatingActionButton card_add;
    String mycard="",myfirstcard="";
    Uri fileUri;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        StrictMode.VmPolicy.Builder builder = new StrictMode.VmPolicy.Builder();
        StrictMode.setVmPolicy(builder.build());
        setContentView(R.layout.scan_business_card_layout);
        mcontext=TextActivity.this;

        card_add = (FloatingActionButton) findViewById(R.id.card_add);
        card_add.show();
        card_add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                requestStoragePermission();
            }
        });
        txt_login = (TextView) findViewById(R.id.txt_login);
        txt_login.setText("Next");
        et_fullname = (EditText) findViewById(R.id.et_fullname);
        et_address = (EditText) findViewById(R.id.et_address);
        et_desingnation = (EditText) findViewById(R.id.et_desingnation);
        et_mobile = (EditText) findViewById(R.id.et_mobile);
        et_email = (EditText) findViewById(R.id.et_email);
        et_company = (EditText) findViewById(R.id.et_company);
        et_website = (EditText) findViewById(R.id.et_website);
        imageView = (ImageView) findViewById(R.id.imageView);
        llCardDetail = (LinearLayout) findViewById(R.id.llCardDetail);

//        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
//        File file = CameraUtils.getOutputMediaFile(MEDIA_TYPE_IMAGE);
//        if (file != null) {
//            profilePath = file.getAbsolutePath();
//            ///  uploadPathMap.put("profile",file.getAbsolutePath());
//        }
//
//
//        Uri fileUri = CameraUtils.getOutputMediaFileUri(getApplicationContext(), file);
//        intent.putExtra(MediaStore.EXTRA_OUTPUT, fileUri);
//        // start the image capture Intent
//        startActivityForResult(intent, CAMERA_REQUEST);

        Bundle bundle=getIntent().getExtras();

        if(bundle!=null)
        {
            if(bundle.getString("mycard")!=null)
            {
                mycard=bundle.getString("mycard");

            }
            if(bundle.getString("myfirstcard")!=null)
            {
                myfirstcard=bundle.getString("myfirstcard");
            }
        }
        requestStoragePermission();
    }

    private void requestStoragePermission() {
        Dexter.withActivity(this)
                .withPermissions(
                        Manifest.permission.CAMERA,
                        Manifest.permission.READ_EXTERNAL_STORAGE,
                        Manifest.permission.WRITE_EXTERNAL_STORAGE)

                .withListener(new MultiplePermissionsListener() {
                    @Override
                    public void onPermissionsChecked(MultiplePermissionsReport report) {
                        if (report.areAllPermissionsGranted()) {
//                            //  openGallery();
                            startCameraIntentForResult();
                        }
                        if (report.isAnyPermissionPermanentlyDenied()) {

                            CameraUtils_Temp.showSettingsDialog(mcontext);
                        }
                    }

                    @Override
                    public void onPermissionRationaleShouldBeShown(List<PermissionRequest> permissions, PermissionToken token) {
                        token.continuePermissionRequest();
                    }
                }).
                withErrorListener(new PermissionRequestErrorListener() {
                    @Override
                    public void onError(DexterError error) {
                        Toast.makeText(mcontext, "Error occurred! ", Toast.LENGTH_SHORT).show();
                    }
                })
                .onSameThread()
                .check();
    }

    private void startCameraIntentForResult() {
        // Clean up last time's image

        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);

        File file = CameraUtils_Temp.getOutputMediaFile(MEDIA_TYPE_IMAGE);
        if (file != null) {
            profilePath = file.getAbsolutePath();
        }
         fileUri = CameraUtils_Temp.getOutputMediaFileUri(getApplicationContext(), file);
        intent.putExtra(MediaStore.EXTRA_OUTPUT, fileUri);
        intent.putExtra(
                "android.intent.extras.CAMERA_FACING",
                Camera.CameraInfo.CAMERA_FACING_BACK);
        // start the image capture Intent
        startActivityForResult(intent, CAMERA_REQUEST);
    }

    private void performCrop(String picUri) {
        Uri mCropImagedUri;
        try {

            //imageStoragePath = picUri;
            Intent cropIntent = new Intent("com.android.camera.action.CROP");

            File f = new File(picUri);
            Uri contentUri = FileProvider.getUriForFile(TextActivity.this, BuildConfig.APPLICATION_ID + ".provider",f);
            //  Uri contentUri = Uri.fromFile(f);
            cropIntent.setDataAndType(contentUri, "image/*");
            cropIntent.putExtra("crop", "true");

            cropIntent.putExtra("aspectX", 1);
            cropIntent.putExtra("aspectY", 1);
            cropIntent.putExtra("outputX", 500);
            cropIntent.putExtra("outputY", 500);
            cropIntent.putExtra("return-data", true);
            cropIntent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
            File fileN = createNewFile("CROP_");
            try {
                fileN.createNewFile();
            } catch (IOException ex) {
                Log.e("io", ex.getMessage());
            }
            mCropImagedUri = Uri.fromFile(fileN);
            cropfilepath=mCropImagedUri.getPath();
            cropIntent.putExtra(MediaStore.EXTRA_OUTPUT, mCropImagedUri);
            startActivityForResult(cropIntent, PIC_CROP);
        }

        catch (ActivityNotFoundException anfe) {
            String errorMessage = "your device doesn't support the crop action!";
            Toast toast = Toast.makeText(this, errorMessage, Toast.LENGTH_SHORT);
            toast.show();
        }
    }

    private File createNewFile(String prefix){



        if(prefix==null || "".equalsIgnoreCase(prefix)){
            prefix="IMG_";
        }
        File newDirectory = new File(Environment.getExternalStorageDirectory()+"/Avocard/");
        if(!newDirectory.exists()){
            if(newDirectory.mkdir()){
                Log.d(TextActivity.this.getClass().getName(), newDirectory.getAbsolutePath()+" directory created");
            }
        }
        File file = new File(newDirectory,(prefix+System.currentTimeMillis()+".jpg"));
        if(file.exists()){
            //this wont be executed
            file.delete();
            try {
                file.createNewFile();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        return file;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK && requestCode == CAMERA_REQUEST) {
            if (resultCode == RESULT_OK) {

                CameraUtils.refreshGallery(getApplicationContext(), profilePath);
                profileBitmap = CameraUtils.optimizeBitmap(BITMAP_SAMPLE_SIZE, profilePath);
                if (profilePath.length() == 0 && profilePath == null) {
                    Toast.makeText(TextActivity.this, "Something went wrong", Toast.LENGTH_SHORT).show();
                } else {

                    CropImage.activity(fileUri)
                            .start(this);

                //    performCrop(profilePath);
                }

            }


        }
//        else if(resultCode == RESULT_OK && requestCode == PIC_CROP)
//        {
//            Bundle extras = data.getExtras();
//            try {
//                if (extras != null) {
//                    if (extras.getParcelable("data") != null) {
//                        Log.e("=data", extras.getParcelable("data").toString());
//                        profileBitmap = extras.getParcelable("data");
//
//                        progressBarCustom = new ProgressBarCustom(mcontext);
//                        progressBarCustom.showProgress();
//                        runTextRecognization(profileBitmap);
//                    } else {
//                        Uri selectedImageUri = data.getData();
//                        if (selectedImageUri != null) {
//                            Log.e("=data", selectedImageUri.toString());
//                            profilePath = selectedImageUri.getPath();
//                            profileBitmap = BitmapFactory.decodeFile(cropfilepath);
//
//                            progressBarCustom = new ProgressBarCustom(mcontext);
//                            progressBarCustom.showProgress();
//                            runTextRecognization(profileBitmap);
//                        } else {
//                            profilePath = cropfilepath;
//                            profileBitmap = BitmapFactory.decodeFile(cropfilepath);
//
//                            progressBarCustom = new ProgressBarCustom(mcontext);
//                            progressBarCustom.showProgress();
//                            runTextRecognization(profileBitmap);
//                        }
//                    }
//                } else {
//                    Uri selectedImageUri = data.getData();
//                    if (selectedImageUri != null) {
//                        Log.e("=data", selectedImageUri.toString());
//                        profilePath = selectedImageUri.getPath();
//                        profileBitmap = BitmapFactory.decodeFile(cropfilepath);
//
//                        progressBarCustom = new ProgressBarCustom(mcontext);
//                        progressBarCustom.showProgress();
//
//                        runTextRecognization(profileBitmap);
//                    } else {
//                        profilePath = cropfilepath;
//                        profileBitmap = BitmapFactory.decodeFile(cropfilepath);
//
//                        progressBarCustom = new ProgressBarCustom(mcontext);
//                        progressBarCustom.showProgress();
//
//                        runTextRecognization(profileBitmap);
//                    }
//                }
//            } catch (Exception e) {
//                e.printStackTrace();
//            }
//        }
        else if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
            try {
                Log.i("nirmala 1111", "" + requestCode);
                CropImage.ActivityResult result = CropImage.getActivityResult(data);
                if (resultCode == RESULT_OK) {
                    Uri resultUri = result.getUri();
                    cropfilepath = resultUri.getPath(); // "file:///mnt/sdcard/FileName.mp3"

                    File file = new File(cropfilepath);
                    int file_size = Integer.parseInt(String.valueOf(file.length()/1024));

                    Log.i("nirmala uri", "" + resultUri+" "+file_size);
                    if (resultUri != null) {
                        Log.e("=data", resultUri.toString());
                        profilePath = resultUri.getPath();
                        profileBitmap = BitmapFactory.decodeFile(resultUri.getPath());

                        progressBarCustom = new ProgressBarCustom(mcontext);
                        progressBarCustom.showProgress();
                        runTextRecognization(profileBitmap);
                    } else {
                        profilePath = resultUri.getPath();
                        profileBitmap = BitmapFactory.decodeFile(resultUri.getPath());

                        progressBarCustom = new ProgressBarCustom(mcontext);
                        progressBarCustom.showProgress();
                        runTextRecognization(profileBitmap);
                    }

                } else if (resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {
                    Exception error = result.getError();
                }
            }catch (Exception e)
            {
                e.printStackTrace();
            }
        }
        else if (requestCode == 144 && resultCode == RESULT_OK) {

            Intent intent = new Intent();
            setResult(RESULT_OK, intent);
            finish();
        }
        else{
            finish();
        }
    }

    private void runTextRecognization(Bitmap bitmapForDetection) {
        FirebaseVisionImage firebaseVisionImage = FirebaseVisionImage.fromBitmap(bitmapForDetection);
        FirebaseVisionTextRecognizer FirebaseVisionTextDetector = FirebaseVision.getInstance().getOnDeviceTextRecognizer();
        FirebaseVisionTextDetector.processImage(firebaseVisionImage).addOnSuccessListener(new OnSuccessListener<FirebaseVisionText>() {
            @Override
            public void onSuccess(FirebaseVisionText firebaseVisionText) {

                if(count_card==1)
                {
                    /*First time*/
                    firstcard_bitmap=profileBitmap;
                    imagescan_str_first=firebaseVisionText;
                    generapop(firebaseVisionText);
                }else{
                    /*Second time*/
                    secondcard_bitmap=profileBitmap;
                    imagescan_str_second=firebaseVisionText;
                    getTextFromResult(firebaseVisionText);
                }

            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                Toast.makeText(mcontext,"Scan failed. Please scan again",Toast.LENGTH_LONG).show();

                progressBarCustom.cancelProgress();
                finish();
            }
        });
    }

    private void getTextFromResult(FirebaseVisionText firebaseVisionText) {
       final StringBuilder strtext = new StringBuilder();
        if(imagescan_str_first!=null) {
            List<FirebaseVisionText.TextBlock> blocks = imagescan_str_first.getTextBlocks();
            if (blocks.size() == 0) {
                Log.i("getTextFromResult", "no data found");
                Toast.makeText(mcontext, "Scan failed. Please scan again", Toast.LENGTH_LONG).show();
                progressBarCustom.cancelProgress();
                finish();
                return;
            }

            for (int i = 0; i < blocks.size(); i++) {
                List<FirebaseVisionText.Line> lineList = blocks.get(i).getLines();
                for (int j = 0; j < lineList.size(); j++) {
                    if (strtext.toString().isEmpty()) {
                        strtext.append(lineList.get(j).getText());
                    } else {
                        strtext.append("\n" + lineList.get(j).getText());
                    }
                }
            }
            Log.i("getTextFromResult", "" + strtext.toString());
        }

        /*Second time oick image*/
        if(imagescan_str_second!=null) {
            List<FirebaseVisionText.TextBlock> blocks_Second = imagescan_str_second.getTextBlocks();
            if (blocks_Second.size() == 0) {
                Log.i("getTextFromResult", "no data found");
                Toast.makeText(mcontext, "Scan failed. Please scan again", Toast.LENGTH_LONG).show();
                progressBarCustom.cancelProgress();
                finish();
                return;
            }
            for (int i = 0; i < blocks_Second.size(); i++) {
                List<FirebaseVisionText.Line> lineList = blocks_Second.get(i).getLines();
                for (int j = 0; j < lineList.size(); j++) {
                    if (strtext.toString().isEmpty()) {
                        strtext.append(lineList.get(j).getText());
                    } else {
                        strtext.append("\n" + lineList.get(j).getText());
                    }
                }
            }

        }

        if(secondcard_bitmap!=null)
        {
            profileBitmap=ProcessingBitmap();
            File fileN = createNewFile("CROP_");
            FileOutputStream fOut;
            try {
                fOut = new FileOutputStream(fileN);
                profileBitmap.compress(Bitmap.CompressFormat.PNG, 85, fOut);
                fOut.flush();
                fOut.close();

                cropfilepath=fileN.getAbsolutePath();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        businessCardReader(strtext.toString());
    }


    public class GetImage extends AsyncTask<Void, Void, Void> {

        String strtext;
        public GetImage(String strtext) {
           this.strtext=strtext;
        }
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressBarCustom=new ProgressBarCustom(TextActivity.this);
            progressBarCustom.showProgress();;
        }


        @Override
        protected Void doInBackground(Void... voids) {
            try {

                profileBitmap=ProcessingBitmap();
             //   profileBitmap = getResizedBitmap(profileBitmap, 400);
                File fileN = createNewFile("CROP_");
                FileOutputStream fOut;
                try {
                    fOut = new FileOutputStream(fileN);
                    profileBitmap.compress(Bitmap.CompressFormat.PNG, 85, fOut);
                    fOut.flush();
                    fOut.close();

                    cropfilepath=fileN.getAbsolutePath();
                } catch (Exception e) {
                    e.printStackTrace();
                }


            } catch (Exception e) {

                e.printStackTrace();

            }

            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            if(progressBarCustom!=null)
            {
                progressBarCustom.cancelProgress();
            }
            businessCardReader(strtext.toString());

        }
    }

    public Bitmap getResizedBitmap(Bitmap image, int maxSize) {
        int width = image.getWidth();
        int height = image.getHeight();

        float bitmapRatio = (float)width / (float) height;
        if (bitmapRatio > 1) {
            width = maxSize;
            height = (int) (width / bitmapRatio);
        } else {
            height = maxSize;
            width = (int) (height * bitmapRatio);
        }
        return Bitmap.createScaledBitmap(image, width, height, true);
    }


    private Bitmap ProcessingBitmap(){
        Bitmap bm1 = firstcard_bitmap;
        Bitmap bm2 =  secondcard_bitmap;
        Bitmap newBitmap = null;


        int w = bm1.getWidth();
        int h = bm1.getHeight() + bm2.getHeight();
//        int h;
//        if(bm1.getHeight() >= bm2.getHeight()){
//            h = bm1.getHeight();
//        }else{
//            h = bm2.getHeight();
//        }

        Bitmap.Config config = bm1.getConfig();
        if(config == null){
            config = Bitmap.Config.ARGB_8888;
        }

        newBitmap = Bitmap.createBitmap(w, h, config);
        Canvas newCanvas = new Canvas(newBitmap);

        newCanvas.drawBitmap(bm1, 0, 0, null);
        newCanvas.drawBitmap(bm2, 0, bm1.getHeight(), null);


        int width = newBitmap.getWidth();
        int height = newBitmap.getHeight();

        float bitmapRatio = (float)width / (float) height;
        if (bitmapRatio > 1) {
            width = 450;
            height = (int) (width / bitmapRatio);
        } else {
            height = 450;
            width = (int) (height * bitmapRatio);
        }
        return Bitmap.createScaledBitmap(newBitmap, width, height, true);
    }


    public void businessCardReader(String strData) {
        try {
            RestClient2_Ocr.getInstance(mcontext).get().getBusinessCardData(UserPrefrence.getInstance(this).getAuthtoken(), strData, new retrofit.Callback<BusinessCardInfo>() {
                @Override
                public void success(final BusinessCardInfo res, Response response) {
                    progressBarCustom.cancelProgress();
                    if (res != null) {

                        et_fullname.setText(res.getName());
                        try {

                            if (res.getDesignation().contains("\\u0026")) {
                                String designation = res.getDesignation().replace("\\u0026", "&");
                                et_desingnation.setText(designation);
                            } else {
                                et_desingnation.setText(res.getDesignation());
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                            et_desingnation.setText(res.getDesignation());
                        }
                        if (res.getEmails().size() > 0) {
                            et_email.setText(res.getEmails().get(0));
                        }
                        if (res.getUrls().size() > 0) {
                            et_website.setText(res.getUrls().get(0));
                        }
                        et_company.setText(res.getCompany());
                        if (res.getPhoneNumbers().size() > 0) {
                            StringBuilder stringBuilder = new StringBuilder();
                            for (String ss : res.getPhoneNumbers()) {
                                if (stringBuilder.toString().isEmpty()) {
                                    stringBuilder.append(ss);
                                } else {
                                    stringBuilder.append("," + ss);
                                }
                            }
                            et_mobile.setText(stringBuilder.toString());
                        }
                        if(res.getAddress()!=null&&res.getAddress().length()>0)
                        {
                            et_address.setText(res.getAddress());
                        }



                        Intent intent=new Intent(TextActivity.this, AddEditCardActitivty.class);
                        intent.putExtra("ocrResult", "Scan");
                        if(mycard.length()>0)
                        {
                            intent.putExtra("mycard", "mycard");
                        }
                        if(myfirstcard.length()>0)
                        {
                            intent.putExtra("myfirstcard", "myfirstcard");
                        }
                        if(et_desingnation.getText().toString().length()>0)
                        {
                            intent.putExtra("position", et_desingnation.getText().toString().trim());
                        }
                        if(et_fullname.getText().toString().length()>0)
                        {
                            intent.putExtra("fullName", et_fullname.getText().toString().trim());
                        }
//                        if(profileBitmap!=null)
//                        {
//                            ByteArrayOutputStream bStream = new ByteArrayOutputStream();
//                            profileBitmap.compress(Bitmap.CompressFormat.PNG, 100, bStream);
//                            byte[] byteArray = bStream.toByteArray();
//                            intent.putExtra("profileBitmap",byteArray);
//                        }
                        if(et_mobile.getText().toString().length()>0)
                        {
                            intent.putExtra("mobileNumber", et_mobile.getText().toString().trim());
                        }
                        if(et_email.getText().toString().length()>0)
                        {
                            intent.putExtra("emailAddress", et_email.getText().toString().trim());
                        }
                        if(et_company.getText().toString().length()>0)
                        {
                            intent.putExtra("companyName", et_company.getText().toString().trim());
                        }
                        if(et_website.getText().toString().length()>0)
                        {
                            intent.putExtra("website", et_website.getText().toString().trim());
                        }
                        if(et_address.getText().toString().length()>0)
                        {
                            intent.putExtra("address", et_address.getText().toString().trim());
                        }

                        startActivityForResult(intent,144);

                    }
                }

                @Override
                public void failure(final RetrofitError error) {
                    progressBarCustom.cancelProgress();
                    error.printStackTrace();
                }
            });

        } catch (Exception e) {
            progressBarCustom.cancelProgress();
            //progress.cancelProgress();
            e.printStackTrace();
        }
    }

    private void generapop(final FirebaseVisionText firebaseVisionText) {
        AlertDialog.Builder builder=new AlertDialog.Builder(TextActivity.this);
        View view= LayoutInflater.from(this).inflate(R.layout.logout_lay,null);
        TextView txtDialogMessage=(TextView)view.findViewById(R.id.txtDialogMessage);
        ImageView center_line=(ImageView)view.findViewById(R.id.center_line);
        txtDialogMessage.setText(getResources().getString(R.string.second_image));
        Button btnDialogYes=(Button)view.findViewById(R.id.btnDialogYes);
        btnDialogYes.setText("YES");
        Button btnDialogNo=(Button)view.findViewById(R.id.btnDialogNo);
        btnDialogNo.setText("NO");
        builder.setView(view);
        final Dialog dialog=builder.create();
        dialog.setCancelable(false);
        dialog.setCanceledOnTouchOutside(false);

        btnDialogYes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
                count_card++;
                requestStoragePermission();
            }
        });
        btnDialogNo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
                getTextFromResult(firebaseVisionText);
            }
        });
        dialog.show();
    }

}
