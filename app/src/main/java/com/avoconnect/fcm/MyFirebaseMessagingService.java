package com.avoconnect.fcm;

import android.annotation.SuppressLint;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.media.RingtoneManager;
import android.net.Uri;

import androidx.annotation.NonNull;
import androidx.core.app.NotificationCompat;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;

import android.os.Build;
import android.provider.Settings;
import android.util.Log;

import com.avoconnect.Prefrence.UserPrefrence;
import com.avoconnect.R;
import com.avoconnect.activity.DetialsCardActivity;
import com.avoconnect.activity.EventsListActitivty;
import com.avoconnect.activity.HomeActivity;
import com.avoconnect.activity.Login_Activity;
import com.avoconnect.activity.RequestoView_Actitivty;
import com.avoconnect.activity.ShowOnlyProfile_Actitivty;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

public class MyFirebaseMessagingService extends FirebaseMessagingService {
    private static final String TAG = "MyAndroidFCMService";
    String clickaction="",title="",notificationId="",senderId="",cardId="",requestViewProfileId="";

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        //Log data to Log Cat
        Log.e(TAG, "From: " + remoteMessage.getFrom());
        Log.e(TAG, "Notification Message Body: " + remoteMessage.getNotification().getBody());
        Log.e(TAG, "Notification Data Body: " + remoteMessage.getData().toString());
        //create notification
        try {
            String title = remoteMessage.getNotification().getTitle();

            String message = remoteMessage.getNotification().getBody();


            if (remoteMessage.getData() != null) {

                if (remoteMessage.getData().get("clickAction") != null) {
                    clickaction = remoteMessage.getData().get("clickAction");

                }
                if (remoteMessage.getData().get("notificationId") != null) {
                    notificationId = remoteMessage.getData().get("notificationId").toString();
                }
                if (remoteMessage.getData().get("cardId") != null) {
                    cardId = remoteMessage.getData().get("cardId").toString();
                }
                if (remoteMessage.getData().get("senderId") != null) {
                    senderId = remoteMessage.getData().get("senderId").toString();
                }

                if (remoteMessage.getData().get("requestViewProfileId") != null) {
                    requestViewProfileId = remoteMessage.getData().get("requestViewProfileId").toString();
                }


                if (remoteMessage.getNotification().getBody() != null) {
                    title = remoteMessage.getNotification().getBody();
                }
            }
            Intent intentBroadcast1 = new Intent("notificattions");
            if (clickaction.equalsIgnoreCase("card_received")) {
                Intent intent = new Intent(this, DetialsCardActivity.class);
                intent.putExtra("card_id", cardId);
                intent.putExtra("additional_info","yes");
                //intent.putExtra("senderId", senderId);
                intentBroadcast1.putExtra("click_action", "card_received");
                LocalBroadcastManager.getInstance(MyFirebaseMessagingService.this).sendBroadcast(intentBroadcast1);
                sendNotification(title, message, intent);
            } else if (clickaction.equalsIgnoreCase("view_profile_request")) {
                Intent intent = new Intent(this, RequestoView_Actitivty.class);
                intent.putExtra("sender", senderId);
                intent.putExtra("request","res");
                intent.putExtra("card_id",cardId);
                intent.putExtra("notification_id",notificationId+"");
                intent.putExtra("requestViewProfileId",requestViewProfileId);
                intentBroadcast1.putExtra("click_action", "notificationflag");
                LocalBroadcastManager.getInstance(MyFirebaseMessagingService.this).sendBroadcast(intentBroadcast1);
                sendNotification(title, message, intent);
            }
            else if(clickaction.equalsIgnoreCase("accept_request"))
            {
                Intent intent = new Intent(this, ShowOnlyProfile_Actitivty.class);
                intent.putExtra("sender_id", senderId);
                intent.putExtra("request","res");
                intent.putExtra("requestViewProfileId",requestViewProfileId);
                intentBroadcast1.putExtra("click_action", "notificationflag");
                LocalBroadcastManager.getInstance(MyFirebaseMessagingService.this).sendBroadcast(intentBroadcast1);
                sendNotification(title, message, intent);
            }
            else if(clickaction.equalsIgnoreCase("broadcast"))
            {
                Intent intent = new Intent(this, HomeActivity.class);
                intentBroadcast1.putExtra("click_action", "notificationflag");
                LocalBroadcastManager.getInstance(MyFirebaseMessagingService.this).sendBroadcast(intentBroadcast1);
                sendNotification(title, message, intent);
            }else if(clickaction.equalsIgnoreCase("shared_my_userId"))
            {
                Intent intent = new Intent(this, HomeActivity.class);
                intentBroadcast1.putExtra("click_action", "notificationflag");
                LocalBroadcastManager.getInstance(MyFirebaseMessagingService.this).sendBroadcast(intentBroadcast1);
                sendNotification(title, message, intent);
            }else if(clickaction.equalsIgnoreCase("event_invitation"))
            {
                Intent intent = new Intent(this, EventsListActitivty.class);
                intentBroadcast1.putExtra("click_action", "notificationflag");
                LocalBroadcastManager.getInstance(MyFirebaseMessagingService.this).sendBroadcast(intentBroadcast1);
                sendNotification(title, message, intent);
            }else if(clickaction.equalsIgnoreCase("received_card_viewed_first_time"))
            {
                Intent intent = new Intent(this, HomeActivity.class);
                intentBroadcast1.putExtra("click_action", "notificationflag");
                LocalBroadcastManager.getInstance(MyFirebaseMessagingService.this).sendBroadcast(intentBroadcast1);
                sendNotification(title, message, intent);
            }



        }catch (Exception e)
        {
            e.printStackTrace();
        }

    }

    private void sendNotification(String title, String message, Intent intent) {
        String NOTIFICATION_CHANNEL_ID = "my_channel_id_01";
        NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0 /* Request code */, intent,
                PendingIntent.FLAG_UPDATE_CURRENT);
        Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            @SuppressLint("WrongConstant")
            NotificationChannel notificationChannel = new NotificationChannel(NOTIFICATION_CHANNEL_ID, "My Notifications", NotificationManager.IMPORTANCE_MAX);

            // Configure the notification channel.
            notificationChannel.setDescription("Channel description");
            notificationChannel.enableLights(true);
            notificationChannel.setLightColor(Color.RED);
            notificationChannel.setVibrationPattern(new long[]{0, 1000, 500, 1000});
            notificationChannel.enableVibration(true);
            notificationManager.createNotificationChannel(notificationChannel);
        }
        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this, NOTIFICATION_CHANNEL_ID);
        notificationBuilder.setSmallIcon(R.drawable.applogo).setContentTitle(title).setContentText(message).setAutoCancel(true).setSound(defaultSoundUri).setContentIntent(pendingIntent);
//        NotificationCompat.Builder notificationBuilder = new NotificationCompat
//                .Builder(this).setSmallIcon(R.drawable.applogo).setContentTitle(title).setContentText(message).setAutoCancel(true).setSound(defaultSoundUri).setContentIntent(pendingIntent);

        Notification notifi = notificationBuilder.build();

        notifi.defaults |= Notification.DEFAULT_LIGHTS;
        notifi.defaults |= Notification.DEFAULT_SOUND;
        //notifi.flags |= Notification.FLAG_INSISTENT;
        notifi.flags |= Notification.FLAG_AUTO_CANCEL;
        Date now = new Date();
        int id = Integer.parseInt(new SimpleDateFormat("ddHHmmss", Locale.US).format(now));
        notificationManager.notify(id /* ID of notification */, notifi);


    }



    @Override
    public void onNewToken(@NonNull String refreshedToken) {
        super.onNewToken(refreshedToken);
        Log.e("refreshedToken",refreshedToken);
        new UserPrefrence(this);
        UserPrefrence.getInstance(this).setFcmToken(refreshedToken);
        sendRegistrationToServer(refreshedToken);
    }

    private void sendRegistrationToServer(String token) {
        // Add custom implementation, as needed.
        Log.e(TAG, "sendRegistrationToServer: " + token);

    }
}
