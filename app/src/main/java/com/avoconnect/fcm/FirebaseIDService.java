//package com.avoconnect.fcm;
//
//import android.util.Log;
//
//import com.avoconnect.Prefrence.UserPrefrence;
//import com.google.firebase.iid.FirebaseInstanceId;
//import com.google.firebase.iid.FirebaseInstanceIdService;
//
//public class FirebaseIDService extends FirebaseInstanceIdService {
//    private static final String TAG = "FirebaseIDService";
//
//    @Override
//    public void onTokenRefresh() {
//        // Get updated InstanceID token.
//        String refreshedToken = FirebaseInstanceId.getInstance().getToken();
//        // TODO: Implement this method to send any registration to your app's servers.
//        new UserPrefrence(this);
//        UserPrefrence.getInstance(this).setFcmToken(refreshedToken);
//        sendRegistrationToServer(refreshedToken);
//    }
//    private void sendRegistrationToServer(String token) {
//        // Add custom implementation, as needed.
//        Log.e(TAG, "sendRegistrationToServer: " + token);
//    }
//}
