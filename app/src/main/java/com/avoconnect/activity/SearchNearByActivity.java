package com.avoconnect.activity;

import android.Manifest;
import android.content.DialogInterface;
import android.content.Intent;
import android.media.Image;
import android.os.Handler;
import android.provider.Settings;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.avoconnect.Constants.Constant;
import com.avoconnect.Prefrence.UserPrefrence;
import com.avoconnect.R;
import com.avoconnect.Util.GPS_TRACKER;
import com.avoconnect.Util.NonScrollListView;
import com.avoconnect.Util.ProgressBarCustom;
import com.avoconnect.Util.Utils;
import com.avoconnect.adapter.NearByUser_Adapter;
import com.avoconnect.api.RestClient;
import com.avoconnect.responsemodel.NearByUser_Model;
import com.avoconnect.responsemodel.ReadNotification_Model;
import com.avoconnect.responsemodel.ShareCard_Model;
import com.avoconnect.responsemodel.UpdateLatlng_Model;
import com.karumi.dexter.Dexter;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.PermissionDeniedResponse;
import com.karumi.dexter.listener.PermissionGrantedResponse;
import com.karumi.dexter.listener.PermissionRequest;
import com.karumi.dexter.listener.single.PermissionListener;
import com.skyfishjy.library.RippleBackground;

import org.json.JSONObject;

import java.util.List;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;
import retrofit.mime.TypedByteArray;
import retrofit.mime.TypedInput;

public class SearchNearByActivity extends AppCompatActivity {

    public static int REQUEST_CODE=145;
    ListView listview_searchuser;
    TextView result_found;
    ImageView backimage;
    boolean is_enable=true;
    String card_id="";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search_near_by);

        final RippleBackground rippleBackground=(RippleBackground)findViewById(R.id.content);
        ImageView imageView=(ImageView)findViewById(R.id.centerImage);

        result_found=(TextView)findViewById(R.id.result_found);
        backimage=(ImageView) findViewById(R.id.backimage);
        listview_searchuser=(ListView)findViewById(R.id.listview_searchuser);

        rippleBackground.startRippleAnimation();

        checkPermissionLatlng();

        backimage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                GPS_TRACKER gps_tracker=new GPS_TRACKER(SearchNearByActivity.this,false);
                GetNearByUser(0.0,0.0,true);

            }
        });
        
        Bundle bundle=getIntent().getExtras();
        if(bundle!=null)
        {
            if(bundle.getString("is_enable")!=null)
            {
                is_enable=false;
            }
            if(bundle.getString("card_id")!=null)
            {
                card_id=bundle.getString("card_id");
            }
        }
    }

    private void buildAlertMessageNoGps() {
        final AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage("Your GPS seems to be disabled, do you want to enable it?")
                .setCancelable(false)
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    public void onClick(@SuppressWarnings("unused") final DialogInterface dialog, @SuppressWarnings("unused") final int id) {
                        Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                        startActivityForResult(intent, REQUEST_CODE);
                    }
                })
                .setNegativeButton("No", new DialogInterface.OnClickListener() {
                    public void onClick(final DialogInterface dialog, @SuppressWarnings("unused") final int id) {
                        dialog.cancel();
                    }
                });
        final AlertDialog alert = builder.create();
        alert.show();
    }

    private void checkPermissionLatlng() {
        Dexter.withActivity(this)
                .withPermission(Manifest.permission.ACCESS_FINE_LOCATION)
                .withListener(new PermissionListener() {
                    @Override public void onPermissionGranted(PermissionGrantedResponse response) {

                        runOnUiThread(new Runnable() {
                            public void run() {
                                GPS_TRACKER gps_tracker=new GPS_TRACKER(SearchNearByActivity.this);
                                gps_tracker.onCallBackReturn(new GPS_TRACKER.Callback() {
                                    @Override
                                    public void clickaction(Double latitude, Double longitude) {
                                        Log.i("latLong","Lat: " + latitude + ", " +
                                                "Lng: " +longitude);
                                        GetNearByUser(latitude,longitude,false);
                                    }

                                });
                        }
                        });



                    }
                    @Override public void onPermissionDenied(PermissionDeniedResponse response) {

                    }
                    @Override public void onPermissionRationaleShouldBeShown(PermissionRequest permission, PermissionToken token) {

                    }
                }).check();
    }



    private void GetNearByUser(Double latitude, Double longitude, final boolean isfinish) {
      final  ProgressBarCustom progressBarCustom;
        try {
             progressBarCustom=new ProgressBarCustom(SearchNearByActivity.this);
            progressBarCustom.showProgress();
            String token = UserPrefrence.getInstance(this).getAuthtoken();
            token="bearer " + token;
            RestClient.getInstance(this).get().nearby( token,UserPrefrence.getInstance(this).getUserid(),latitude +"",longitude+"",new retrofit.Callback<NearByUser_Model>() {

                @Override
                public void success(final NearByUser_Model res, Response response) {
                    progressBarCustom.cancelProgress();

                    if(res.getResponseMessage().equalsIgnoreCase(Constant.SUCCESS))
                    {

                        Handler handler = new Handler();
                        handler.post(new Runnable() {
                            @Override
                            public void run() {
                              if(res.getResourceData()!=null&&res.getResourceData().size()>0)
                              {
                                  listview_searchuser.setVisibility(View.VISIBLE);
                                  result_found.setText(res.getResourceData().size()+" Results Found");
                                 setAdapter(res);
                              }else{
                                  result_found.setText("0"+" Results Found");
                                  listview_searchuser.setVisibility(View.GONE);
                              }


                              if(isfinish==true)
                              {
                                  finish();
                              }
                            }
                        });

                    }else{
                        result_found.setText("0"+" Results Found");
                        listview_searchuser.setVisibility(View.GONE);
                        Toast.makeText(SearchNearByActivity.this, res.getMessage(), Toast.LENGTH_SHORT).show();
                    }

                }
                @Override
                public void failure(final RetrofitError error) {
                    progressBarCustom.cancelProgress();
                    error.printStackTrace();
                    if (error != null) {
                        Utils utils=new Utils(SearchNearByActivity.this);
                        utils.showError(error);
                    }

                }
            });
        }catch (Exception e)
        {
            e.printStackTrace();
//            if(progressBarCustom!=null) {
//                progressBarCustom.cancelProgress();
//                //progress.cancelProgress();
//                e.printStackTrace();
//            }
        }
    }

    private void setAdapter(final NearByUser_Model res) {
        NearByUser_Adapter nearByUser_adapter=new NearByUser_Adapter(this,res);
        listview_searchuser.setAdapter(nearByUser_adapter);
        nearByUser_adapter.onCallBackReturn(new NearByUser_Adapter.Callback() {
            @Override
            public void clickaction(int position) {
                if(is_enable==true)
                {
                    callApiUserid(res,position);
                }
            }
        });


    }

    private void callApiUserid(final NearByUser_Model res,int position) {
        final ProgressBarCustom progressBarCustom=new ProgressBarCustom(SearchNearByActivity.this);
        progressBarCustom.showProgress();
        try {
            String token = UserPrefrence.getInstance(SearchNearByActivity.this).getAuthtoken();
            token="bearer " + token;
            RestClient.getInstance(SearchNearByActivity.this).get().sharecard_id(token,
                    UserPrefrence.getInstance(SearchNearByActivity.this).getUserid(),
                    res.getResourceData().get(position).getAvoId()+""
                    ,card_id,new Callback<ShareCard_Model>() {

                        @Override
                        public void success(final ShareCard_Model res, Response response) {
                            progressBarCustom.cancelProgress();

                            if(res.getResponseMessage().equalsIgnoreCase(Constant.SUCCESS))
                            {
                               ProgressBarCustom.customDialog(SearchNearByActivity.this,res.getMessage(),false);
                            }else{
                                Toast.makeText(SearchNearByActivity.this, res.getMessage(), Toast.LENGTH_SHORT).show();
                            }

                        }

                        @Override
                        public void failure(final RetrofitError error) {
                            progressBarCustom.cancelProgress();
                            error.printStackTrace();
                            if (error != null) {
                                Utils utils=new Utils(SearchNearByActivity.this);
                                utils.showError(error);
                            }
                        }
                    });

        }catch (Exception e)
        {
            progressBarCustom.cancelProgress();
            e.printStackTrace();
        }
    }

    protected void onActivityResult(int requestCode, int resultCode, Intent data){
        if(requestCode == REQUEST_CODE && resultCode == 0){
            String provider = Settings.Secure.getString(getContentResolver(), Settings.Secure.LOCATION_PROVIDERS_ALLOWED);
            if(provider != null){
                checkPermissionLatlng();
            }else{
                //Users did not switch on the GPS
            }
        }
    }


    @Override
    public void onBackPressed() {
        GPS_TRACKER gps_tracker=new GPS_TRACKER(this,false);
        GetNearByUser(0.0,0.0,true);

    }

    @Override
    public void onPause() {
        GPS_TRACKER gps_tracker = new GPS_TRACKER(this, false);
        GetNearByUser(0.0,0.0,true);
        super.onPause();
        Log.e("============","===onPause");

    }
}
