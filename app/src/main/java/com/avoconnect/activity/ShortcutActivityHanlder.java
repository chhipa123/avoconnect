package com.avoconnect.activity;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.avoconnect.BottomSheetFragments.ShareCardFragment;
import com.avoconnect.Constants.Constant;
import com.avoconnect.Prefrence.UserPrefrence;
import com.avoconnect.R;
import com.avoconnect.Util.ProgressBarCustom;
import com.avoconnect.Util.Utils;
import com.avoconnect.api.RestClient;
import com.avoconnect.responsemodel.CardModel;
import com.google.android.material.bottomsheet.BottomSheetBehavior;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

public class ShortcutActivityHanlder extends AppCompatActivity {

    private BottomSheetBehavior mBottomSheetBehavior;
    CardModel resourceData;
    boolean isfoundprimary=false;
    String card_id="";
    int selected_postion=0;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_shortcut_hanlder);

        
        callApigetPrimaryCard();
    }

    private void callApigetPrimaryCard() {

        final ProgressBarCustom progressBarCustom = new ProgressBarCustom(ShortcutActivityHanlder.this);
        progressBarCustom.showProgress();
        try {
            String token = UserPrefrence.getInstance(ShortcutActivityHanlder.this).getAuthtoken();
            token = "bearer " + token;
            RestClient.getInstance(ShortcutActivityHanlder.this).get().getCardList(token, UserPrefrence.getInstance(ShortcutActivityHanlder.this).getUserid(), new Callback<CardModel>() {

                @Override
                public void success(final CardModel res, Response response) {
                    progressBarCustom.cancelProgress();
                    if (response.getStatus() == 200) {
                        if (res.getResponseMessage().equalsIgnoreCase(Constant.SUCCESS)) {

                            if(res.getResourceData()!=null)
                            {
                                if(res.getResourceData().size()>0)
                                {
                                    resourceData=res;
                                    for(int a=0;a<resourceData.getResourceData().size();a++)
                                    {
                                       if(resourceData.getResourceData().get(a).getIsPrimary()==1)
                                       {
                                           isfoundprimary=true;
                                           card_id=resourceData.getResourceData().get(a).getId().toString();
                                           selected_postion=a;
                                           break;
                                       }
                                    }

                                    if(isfoundprimary=true)
                                    {
                                        LinearLayout bottomSheet = (LinearLayout) findViewById(R.id.bottom_sheet);
                                        mBottomSheetBehavior = BottomSheetBehavior.from(bottomSheet);
                                        mBottomSheetBehavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
                                        mBottomSheetBehavior.setPeekHeight(0);
                                        opendialogbar();
                                    }else{
                                        Handler handler = new Handler();

                                        handler.post(new Runnable() {
                                            @Override
                                            public void run() {
                                                Toast.makeText(ShortcutActivityHanlder.this, "No Primary Card Found", Toast.LENGTH_SHORT).show();
                                                finishAffinity();
                                            }
                                        });


                                    }

                                }
                            }
                        } else {

                        }
                    }
                }

                @Override
                public void failure(final RetrofitError error) {
                    progressBarCustom.cancelProgress();
                    error.printStackTrace();
                    if (error != null) {
                        Utils utils = new Utils(ShortcutActivityHanlder.this);
                        utils.showError(error);
                    }
                }
            });

        } catch (Exception e) {
            progressBarCustom.cancelProgress();
            e.printStackTrace();
        }
    }

    private void opendialogbar() {
        ShareCardFragment bottomSheetFragment = new ShareCardFragment();
        Bundle bundle=new Bundle();
        bundle.putString("name",resourceData.getResourceData().get(selected_postion).getFullName());
        bundle.putString("email",resourceData.getResourceData().get(selected_postion).getEmail());
        bundle.putString("mobile",resourceData.getResourceData().get(selected_postion).getMobile());
        bundle.putString("share_url",resourceData.getResourceData().get(selected_postion).getShareCardUrl());
        bundle.putString("card_id",card_id+"");
        bottomSheetFragment.setArguments(bundle);
        bottomSheetFragment.show(getSupportFragmentManager(), bottomSheetFragment.getTag());

        bottomSheetFragment.onCutCallBackReturn(new ShareCardFragment.CutCallback() {
            @Override
            public void cut_clickaction(int position) {
                finish();
            }
        });
        bottomSheetFragment.onCallBackReturn(new ShareCardFragment.Callback() {
            @Override
            public void clickaction(int position) {
                finish();
            }
        });
    }


}
