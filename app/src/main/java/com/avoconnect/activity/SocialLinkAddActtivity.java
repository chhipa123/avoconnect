package com.avoconnect.activity;

import androidx.annotation.Nullable;
import com.google.android.material.bottomsheet.BottomSheetBehavior;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.LinearLayout;

import com.avoconnect.BottomSheetFragments.AddtionalLinkFragment;
import com.avoconnect.Fragments.ImportantLinkfragment;

import com.avoconnect.Fragments.MediaLinkFragment;
import com.avoconnect.Fragments.SoacialLinkTitleFragment;
import com.avoconnect.R;

public class SocialLinkAddActtivity extends AppCompatActivity {

    private BottomSheetBehavior mBottomSheetBehavior;
    String isimp="";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_social_link_add_acttivity);
        LinearLayout bottomSheet =(LinearLayout) findViewById(R.id.bottom_sheet);
        mBottomSheetBehavior = BottomSheetBehavior.from(bottomSheet);
        mBottomSheetBehavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
        mBottomSheetBehavior.setPeekHeight(0);

        Bundle bundle=getIntent().getExtras();
        if(bundle!=null)
        {
            if(bundle.getString("type")!=null)
            {
                String type=bundle.getString("type");
                if(type.equalsIgnoreCase("important_link"))
                {
                    ImportantLinkfragment importantLinkfragment = new ImportantLinkfragment();
                    importantLinkfragment.setCancelable(false);
                    importantLinkfragment.show(getSupportFragmentManager(), importantLinkfragment.getTag());
                }
                else if(type.equalsIgnoreCase("additional_link"))
                {
                    AddtionalLinkFragment importantLinkfragment = new AddtionalLinkFragment();
                    importantLinkfragment.setCancelable(false);
                    importantLinkfragment.show(getSupportFragmentManager(), importantLinkfragment.getTag());
                }
                else if(type.equalsIgnoreCase("media"))
                {
                    MediaLinkFragment importantLinkfragment = new MediaLinkFragment();
                    importantLinkfragment.setCancelable(false);
                    importantLinkfragment.show(getSupportFragmentManager(), importantLinkfragment.getTag());
                }
            }

        }
        else{
            SoacialLinkTitleFragment linkTitleFragment = new SoacialLinkTitleFragment();
            linkTitleFragment.setCancelable(false);
            linkTitleFragment.show(getSupportFragmentManager(), linkTitleFragment.getTag());
        }


    }
}
