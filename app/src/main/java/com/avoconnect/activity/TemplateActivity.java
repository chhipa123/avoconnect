package com.avoconnect.activity;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.text.Html;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.avoconnect.Constants.Constant;
import com.avoconnect.Prefrence.UserPrefrence;
import com.avoconnect.R;
import com.avoconnect.Util.ProgressBarCustom;
import com.avoconnect.Util.Utils;
import com.avoconnect.api.RestClient;
import com.avoconnect.responsemodel.BlockSearchModal;
import com.avoconnect.responsemodel.ShareCardTemplate_Model;
import com.twitter.sdk.android.core.models.User;

import retrofit.RetrofitError;
import retrofit.client.Response;

public class TemplateActivity extends AppCompatActivity implements View.OnClickListener{

    EditText et_template;
    TextView txt_submit;
    ImageView backimage;
    public static boolean ischange=false;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_template);
        intis();
    }

    private void intis() {
        et_template=(EditText)findViewById(R.id.et_template);
        txt_submit=(TextView)findViewById(R.id.txt_submit);
        backimage=(ImageView)findViewById(R.id.backimage);

        backimage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
        if(!UserPrefrence.getInstance(this).getUsertype().equalsIgnoreCase(Constant.USERTYPE_FREE))
        {

        }else{
            et_template.setEnabled(false);
            et_template.setClickable(false);
        }
        txt_submit.setOnClickListener(this);
        callApigetTemplate("",false);
    }

    private void callApigetTemplate(final String template,final boolean isshowdialog) {
        final ProgressBarCustom progressBarCustom=new ProgressBarCustom(TemplateActivity.this);
        progressBarCustom.showProgress();
        try {
            String token = UserPrefrence.getInstance(TemplateActivity.this).getAuthtoken();
            token="bearer " + token;
            RestClient.getInstance(TemplateActivity.this).get().shareCardTemplate( token,UserPrefrence.getInstance(TemplateActivity.this).getUserid()+"",
                    template,
                    new retrofit.Callback<ShareCardTemplate_Model>() {

                        @Override
                        public void success(final ShareCardTemplate_Model res, Response response) {
                            progressBarCustom.cancelProgress();

                            if(res.getResponseMessage().equalsIgnoreCase(Constant.SUCCESS))
                            {
                                if(res.getResourceData()!=null) {

                                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {

                                        String testing=res.getResourceData().replaceAll("\\r", "").replaceAll("\\\n", "\n");
                                        et_template.setText(testing);

                                        if(!UserPrefrence.getInstance(TemplateActivity.this).getUsertype().equalsIgnoreCase(Constant.USERTYPE_FREE)) {
                                            et_template.requestFocus();
                                            et_template.setSelection(et_template.getText().toString().length());
                                        }
                                    } else {
                                        String testing=res.getResourceData().replaceAll("\\r", "").replaceAll("\\\n", "\n");
                                        et_template.setText(testing);

                                        if(!UserPrefrence.getInstance(TemplateActivity.this).getUsertype().equalsIgnoreCase(Constant.USERTYPE_FREE)) {
                                            et_template.requestFocus();
                                            et_template.setSelection(et_template.getText().toString().length());
                                        }
                                    }

                                    if(isshowdialog==true)
                                    {
                                        ischange=true;
                                        ProgressBarCustom.customDialog(TemplateActivity.this,res.getMessage(),true);
                                    }
                                }
                            }else{
                                ProgressBarCustom.customDialog(TemplateActivity.this,res.getMessage(),false);
                               // Toast.makeText(TemplateActivity.this, res.getMessage(), Toast.LENGTH_SHORT).show();
                            }

                        }
                        @Override
                        public void failure(final RetrofitError error) {
                            progressBarCustom.cancelProgress();
                            error.printStackTrace();
                            if (error != null) {
                                Utils utils=new Utils(TemplateActivity.this);
                                utils.showError(error);
                            }

                        }
                    });
        }catch (Exception e)
        {
            progressBarCustom.cancelProgress();
            //progress.cancelProgress();
            e.printStackTrace();
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId())
        {
            case R.id.txt_submit:
                if(!UserPrefrence.getInstance(this).getUsertype().equalsIgnoreCase(Constant.USERTYPE_FREE))
                {
                    if(et_template.getText().toString().length()!=0)
                    {
                        callApigetTemplate(et_template.getText().toString(),true);
                    }
                }else{
                    Utils.customYesDialog(this,TemplateActivity.this);
                }

                break;

            case R.id.txt_upgrade:
                if(Utils.yesNoDialog!=null&&Utils.yesNoDialog.isShowing()==true)
                {
                    Intent intent11=new Intent(TemplateActivity.this, PlanUserSubcription_Act.class);
                    startActivityForResult(intent11,155);
                    Utils.yesNoDialog.dismiss();
                }
                break;
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
           if(requestCode==155&&resultCode==RESULT_OK){
               intis();
        }
    }
}
