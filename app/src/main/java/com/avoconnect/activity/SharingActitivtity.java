package com.avoconnect.activity;

import android.Manifest;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Environment;
import android.os.StrictMode;
import android.provider.ContactsContract;
import android.provider.MediaStore;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;

import android.os.Bundle;
import android.telephony.TelephonyManager;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.avoconnect.Constants.Constant;
import com.avoconnect.Prefrence.UserPrefrence;
import com.avoconnect.R;
import com.avoconnect.Util.ProgressBarCustom;
import com.avoconnect.Util.Utils;
import com.avoconnect.api.RestClient;
import com.avoconnect.responsemodel.ShareCard_Model;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.google.zxing.BarcodeFormat;
import com.google.zxing.WriterException;
import com.google.zxing.common.BitMatrix;
import com.google.zxing.qrcode.QRCodeWriter;
import com.karumi.dexter.Dexter;
import com.karumi.dexter.MultiplePermissionsReport;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.DexterError;
import com.karumi.dexter.listener.PermissionRequest;
import com.karumi.dexter.listener.PermissionRequestErrorListener;
import com.karumi.dexter.listener.multi.MultiplePermissionsListener;
import com.twitter.sdk.android.core.models.Card;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

public class SharingActitivtity extends AppCompatActivity implements View.OnClickListener{

    private static final int SELECT_PHONE_NUMBER =100 ;
    String sharing_type="",card_id="",share_url="";
    RelativeLayout relay_phone,relay_email,relay_userid;
    EditText et_userid,et_email,et_phone;
    TextView heading,subheading,subheading_qrcode,txt_save,txt_user_placeholder;
    ImageView hintEmailIcon1,hintEmailIcon2,hintEmailIcon3;
    ImageView backimage;
    LinearLayout view_lin1;
    RelativeLayout qrcode_layout;
    ImageView qrcode_img;
    Bitmap bitmap;
    TextView username_txt,useremail,phonenumber_txt;
    TextView username_txt_qr,useremail_qr,phonenumber_txt_qr;
    ImageView hintEmailIcon_phone;
    TextView txt_share,txt_user_placeholder_qr;
    String logo_url="",backgroundcolor="";
    private CircleImageView img_userimg,img_userimg_qr;
    CardView maincard;

    Uri uri;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        StrictMode.VmPolicy.Builder builder = new StrictMode.VmPolicy.Builder();
        StrictMode.setVmPolicy(builder.build());
        setContentView(R.layout.activity_sharing_actitivtity);

        relay_phone=(RelativeLayout)findViewById(R.id.relay_phone);
        relay_email=(RelativeLayout)findViewById(R.id.relay_email);
        relay_userid=(RelativeLayout)findViewById(R.id.relay_userid);

        view_lin1=(LinearLayout)findViewById(R.id.view_lin1);
        qrcode_layout=(RelativeLayout)findViewById(R.id.qrcode_layout);

        hintEmailIcon_phone=(ImageView)findViewById(R.id.hintEmailIcon_phone);

        img_userimg=(CircleImageView) findViewById(R.id.img_userimg);
        img_userimg_qr=(CircleImageView) findViewById(R.id.img_userimg_qr);

        txt_user_placeholder_qr=(TextView) findViewById(R.id.txt_user_placeholder_qr);
        txt_user_placeholder=(TextView) findViewById(R.id.txt_user_placeholder);
        maincard=(CardView)findViewById(R.id.maincard);

        et_userid=(EditText)findViewById(R.id.et_userid);
        et_email=(EditText)findViewById(R.id.et_email);
        et_phone=(EditText)findViewById(R.id.et_phone);

        qrcode_img=(ImageView)findViewById(R.id.qrcode_img);
        hintEmailIcon1=(ImageView)findViewById(R.id.hintEmailIcon1);
        hintEmailIcon2=(ImageView)findViewById(R.id.hintEmailIcon2);
        hintEmailIcon3=(ImageView)findViewById(R.id.hintEmailIcon3);
        backimage=(ImageView)findViewById(R.id.backimage);
        heading=(TextView)findViewById(R.id.heading);
        txt_save=(TextView)findViewById(R.id.txt_save);
        subheading=(TextView)findViewById(R.id.subheading);
        subheading_qrcode=(TextView)findViewById(R.id.subheading_qrcode);

        txt_share=(TextView)findViewById(R.id.txt_share);
        hintEmailIcon1.setOnClickListener(this);
        hintEmailIcon2.setOnClickListener(this);
        hintEmailIcon3.setOnClickListener(this);
        hintEmailIcon_phone.setOnClickListener(this);

        username_txt=(TextView)findViewById(R.id.username_txt);
        useremail=(TextView)findViewById(R.id.useremail);
        phonenumber_txt=(TextView)findViewById(R.id.phonenumber_txt);

        username_txt_qr=(TextView)findViewById(R.id.username_txt_qr);
        useremail_qr=(TextView)findViewById(R.id.useremail_qr);
        phonenumber_txt_qr=(TextView)findViewById(R.id.phonenumber_txt_qr);



        txt_save.setOnClickListener(this);
        txt_share.setOnClickListener(this);
        Bundle bundle=getIntent().getExtras();
        {
            if(bundle!=null)
            {
                    if(bundle.getString("sharing_type")!=null)
                    {

                        if(bundle.getString("logo_url")!=null)
                        {
                            if(bundle.getString("logo_url").length()>0)
                            {
                                logo_url=bundle.getString("logo_url");
                            }
                        }

                        if(bundle.getString("backgroundcolor")!=null)
                        {
                            if(bundle.getString("backgroundcolor").length()>0)
                            {
                                backgroundcolor=bundle.getString("backgroundcolor");
                                maincard.setCardBackgroundColor(Color.parseColor(backgroundcolor));
                            }
                        }

                        sharing_type=bundle.getString("sharing_type");
                        share_url=bundle.getString("share_url");
                        if(sharing_type.equalsIgnoreCase("userid"))
                        {
                            relay_userid.setVisibility(View.VISIBLE);
                            heading.setText(getResources().getString(R.string.share_userid));
                            subheading.setText(getResources().getString(R.string.subheading_userid));
                        }
                       else if(sharing_type.equalsIgnoreCase("email"))
                        {
                            relay_email.setVisibility(View.VISIBLE);
                            heading.setText(getResources().getString(R.string.share_emai));
                            subheading.setText(getResources().getString(R.string.subheading_email));
                        }
                        else  if(sharing_type.equalsIgnoreCase("sms"))
                        {
                            relay_phone.setVisibility(View.VISIBLE);
                            heading.setText(getResources().getString(R.string.share_sms));
                            subheading.setText(getResources().getString(R.string.subheading_sms));
                        }

                        else if(sharing_type.equalsIgnoreCase("qrocode"))
                        {
                            view_lin1.setVisibility(View.GONE);
                            heading.setText(getResources().getString(R.string.share_qrcode));
                            subheading_qrcode.setText(getResources().getString(R.string.subheading_qrcode));
                            if(bundle.getString("card_id")!=null)
                            {
                                card_id=bundle.getString("card_id");
                            }


                    }

                    if(bundle.getString("name")!=null)
                    {
                        username_txt.setText(bundle.getString("name"));
                        username_txt_qr.setText(bundle.getString("name"));
                    }
                    if(bundle.getString("email")!=null)
                    {
                        useremail.setText(bundle.getString("email"));
                        useremail_qr.setText(bundle.getString("email"));
                    }
                    if(bundle.getString("mobile")!=null)
                    {
                        phonenumber_txt_qr.setText(bundle.getString("mobile"));
                        phonenumber_txt.setText(bundle.getString("mobile"));
                    }
                    if(bundle.getString("card_id")!=null)
                    {
                        card_id=bundle.getString("card_id");
                    }

                        if (logo_url != null && logo_url.length() > 0) {
                            img_userimg_qr.setVisibility(View.VISIBLE);
                            txt_user_placeholder_qr.setVisibility(View.GONE);
                            Glide.with(this)
                                    .load(logo_url)
                                    .apply(RequestOptions.circleCropTransform())
                                    .into(img_userimg_qr);
                        } else if (Utils.checkNull(username_txt_qr.getText().toString())) {
                            img_userimg_qr.setVisibility(View.GONE);
                            txt_user_placeholder_qr.setVisibility(View.VISIBLE);
                            txt_user_placeholder_qr.setText(Utils.nameInital(username_txt_qr.getText().toString().trim()) + "");
                            //  holder. txt_user_placeholder.setText(new Utils(mContext).getFirstLastCharacter(resourceData.get(position).getFullName()));
                        }


                        geraQR(Constant.QRSACN_URL+""+card_id+","+new UserPrefrence(this).getUserid());
                    }

                if (logo_url != null && logo_url.length() > 0) {
                    img_userimg.setVisibility(View.VISIBLE);
                    txt_user_placeholder.setVisibility(View.GONE);
                    Glide.with(this)
                            .load(logo_url)
                            .apply(RequestOptions.circleCropTransform())
                            .into(img_userimg);
                } else if (Utils.checkNull(username_txt.getText().toString())) {
                   img_userimg.setVisibility(View.GONE);
                    txt_user_placeholder.setVisibility(View.VISIBLE);
                    txt_user_placeholder.setText(Utils.nameInital(username_txt.getText().toString().trim()) + "");
                    //  holder. txt_user_placeholder.setText(new Utils(mContext).getFirstLastCharacter(resourceData.get(position).getFullName()));
                }

            }
        }

        backimage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        txt_save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Dexter.withActivity(SharingActitivtity.this)
                        .withPermissions(
                                Manifest.permission.WRITE_EXTERNAL_STORAGE,
                                Manifest.permission.READ_EXTERNAL_STORAGE
                        ).withListener(new MultiplePermissionsListener() {
                    @Override
                    public void onPermissionsChecked(MultiplePermissionsReport report) {
                        String path = MediaStore.Images.Media.insertImage(getContentResolver(), bitmap,"title", null);
                        Uri screenshotUri = Uri.parse(path);
                        final Intent emailIntent1 = new Intent(     android.content.Intent.ACTION_SEND);
                        emailIntent1.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        emailIntent1.putExtra(Intent.EXTRA_STREAM, screenshotUri);
                        emailIntent1.putExtra(Intent.EXTRA_SUBJECT, "Avoconnect");
                        emailIntent1.putExtra(Intent.EXTRA_TEXT, "Scan and connect with First Card on Avocconnect");
                        emailIntent1.setType("image/png");
                        startActivity(Intent.createChooser(emailIntent1, "Send email using"));
                    }

                    @Override
                    public void onPermissionRationaleShouldBeShown(List<PermissionRequest> permissions, PermissionToken token) {
                    }
                }).withErrorListener(new PermissionRequestErrorListener() {

                    @Override
                    public void onError(DexterError error) {
                        Log.e("=eroor", error.toString());
                    }

                }).check();




            }
        });

        Dexter.withActivity(SharingActitivtity.this)
                .withPermissions(
                        Manifest.permission.WRITE_EXTERNAL_STORAGE,
                        Manifest.permission.READ_EXTERNAL_STORAGE
                ).withListener(new MultiplePermissionsListener() {
            @Override
            public void onPermissionsChecked(MultiplePermissionsReport report) {
                getUriToResource(SharingActitivtity.this,R.drawable.applogo);
            }

            @Override
            public void onPermissionRationaleShouldBeShown(List<PermissionRequest> permissions, PermissionToken token) {
            }
        }).withErrorListener(new PermissionRequestErrorListener() {

            @Override
            public void onError(DexterError error) {
                Log.e("=eroor", error.toString());
            }

        }).check();





    }

    private void geraQR(String texto){
        QRCodeWriter writer = new QRCodeWriter();
        try {
            BitMatrix bitMatrix = writer.encode(texto, BarcodeFormat.QR_CODE, 512, 512);
            int width = 512;
            int height = 512;
            Bitmap bmp = Bitmap.createBitmap(width, height, Bitmap.Config.RGB_565);
            for (int x = 0; x < width; x++) {
                for (int y = 0; y < height; y++) {
                    if (bitMatrix.get(x,y))
                        bmp.setPixel(x, y, Color.BLACK);
                    else
                        bmp.setPixel(x, y, Color.WHITE);
                }
            }
            qrcode_img.setImageBitmap(bmp);
            bitmap=bmp;
        } catch (WriterException e) {
            Log.e("QR ERROR", e.toString());
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId())
        {
            case R.id.hintEmailIcon1:
               // validation(0);
                break;
            case R.id.hintEmailIcon2:
                break;
            case R.id.hintEmailIcon3:
                break;

            case R.id.hintEmailIcon_phone:
                Intent i=new Intent(Intent.ACTION_PICK);
                i.setType(ContactsContract.CommonDataKinds.Phone.CONTENT_TYPE);
                startActivityForResult(i, SELECT_PHONE_NUMBER);
                break;
            case R.id.txt_share:

                    if(sharing_type.equalsIgnoreCase("userid"))
                    {
                        validation(0);
                    }
                    else if(sharing_type.equalsIgnoreCase("email"))
                    {
                        validation(1);
                    }
                    else  if(sharing_type.equalsIgnoreCase("sms"))
                    {
                        validation(2);
                    }

                break;
        }
    }

    private void validation(int cases) {
        Utils utils=new Utils(this);
        String getEmial=et_email.getText().toString();
        if(cases==0)
        {
            if(et_userid.getText().toString().length()==0)
            {
                ProgressBarCustom.customDialog(this,getResources().getString(R.string.enter_userid_msg),false);
            }else{
                callApiUserid();
            }
        }else if(cases==1)
        {
            if(et_email.getText().toString().length()==0)

            {
                ProgressBarCustom.customDialog(this,getResources().getString(R.string.enter_email),false);
            }else if(utils.isvalidaEmail(getEmial)==false)
            {
                ProgressBarCustom.customDialog(this,getResources().getString(R.string.enter_valid_email),false);
            }else{
                openMailComoper();
            }
        }else if(cases==2)
        {
            if(et_phone.getText().toString().length()==0)

            {
                ProgressBarCustom.customDialog(this,getResources().getString(R.string.enter_mobile_number),false);
            }
            else if(et_phone.getText().toString().length()<10)
            {
                ProgressBarCustom.customDialog(this,getResources().getString(R.string.enter_mobile),false);
            }else{
                openSendMEssageComposer();
            }
        }
    }

    private void openSendMEssageComposer() {

        Dexter.withActivity(SharingActitivtity.this)
                .withPermissions(
                        Manifest.permission.WRITE_EXTERNAL_STORAGE,
                        Manifest.permission.READ_EXTERNAL_STORAGE
                ).withListener(new MultiplePermissionsListener() {
            @Override
            public void onPermissionsChecked(MultiplePermissionsReport report) {

                //String status = checkInternetConnection();
//                TelephonyManager manager = (TelephonyManager)getSystemService(Context.TELEPHONY_SERVICE);
//                if(manager!=null) {
//                    if (manager.getPhoneCount() == 2) {
//                        // Dual sim
//                        try {
//                            Intent sendIntent = new Intent(Intent.ACTION_SEND);
//                            sendIntent.setClassName("com.android.mms", "com.android.mms.ui.ComposeMessageActivity");
//                            sendIntent.putExtra("address", Uri.parse("sms:"+et_phone.getText().toString()));
//                            sendIntent.putExtra("sms_body", share_url);
//                            sendIntent.putExtra(Intent.EXTRA_STREAM, uri);
//                            sendIntent.setType("image/png");
//                            startActivity(sendIntent);
//                        } catch (ActivityNotFoundException activityNotFoundException) {
//
////                            Intent intentsms = new Intent(Intent.ACTION_SEND, Uri.parse("sms:" + et_phone.getText().toString()));
////                            intentsms.putExtra("address", Uri.parse("sms:"+et_phone.getText().toString()));
////                            intentsms.putExtra("sms_body", share_url);
////                            intentsms.putExtra(Intent.EXTRA_STREAM, uri);
////                            intentsms.setType("image/png");
////                            startActivity(intentsms);
//
//                            Intent sendIntent = new Intent(Intent.ACTION_SEND);
//                            sendIntent.putExtra("address", Uri.parse("sms:"+et_phone.getText().toString()));
//                            sendIntent.putExtra("sms_body", share_url);
//                            sendIntent.putExtra(Intent.EXTRA_STREAM, uri);
//                            sendIntent.setType("image/png");
//                            startActivity(sendIntent);
//                        }
//                    }else{
//                        Toast.makeText(SharingActitivtity.this, "For sending MMS need to sim card", Toast.LENGTH_SHORT).show();
//                    }



//                if (status.equalsIgnoreCase("MOBILE_DATA")) {
//
//                } else if (status.equalsIgnoreCase("NotAvailable")) {
//                    Toast.makeText(SharingActitivtity.this, "no internet connection", Toast.LENGTH_SHORT).show();
//                } else if (status.equalsIgnoreCase("WIFI")) {
//                    Toast.makeText(SharingActitivtity.this, "For sending MMS need to mobile data", Toast.LENGTH_SHORT).show();
//                } else if (status.equalsIgnoreCase("WifiUnAvailable")) {
//                    Toast.makeText(SharingActitivtity.this, "no internet connection", Toast.LENGTH_SHORT).show();
//                }else if (status.equalsIgnoreCase("DataUnAvailable")) {
//                    Toast.makeText(SharingActitivtity.this, "no internet connection", Toast.LENGTH_SHORT).show();
//                }
            }

            @Override
            public void onPermissionRationaleShouldBeShown(List<PermissionRequest> permissions, PermissionToken token) {
            }
        }).withErrorListener(new PermissionRequestErrorListener() {

            @Override
            public void onError(DexterError error) {
                Log.e("=eroor", error.toString());
            }

        }).check();

//        Intent intentsms = new Intent(Intent.ACTION_VIEW, Uri.parse("sms:"+et_phone.getText().toString()));
//        intentsms.putExtra("sms_body",  share_url);
//        startActivity(intentsms);
    }



    protected boolean sendSMS() {
        ArrayList<String> nums = new ArrayList<String>();
        nums.add("9806503950");
        nums.add("9806373797");
        Log.i("Send SMS", "");
        Intent smsIntent = new Intent(Intent.ACTION_VIEW);

        smsIntent.setData(Uri.parse("smsto:"));
        smsIntent.setType("vnd.android-dir/mms-sms");
        smsIntent.putExtra(Intent.EXTRA_STREAM, uri);
        smsIntent.putExtra("address"  ,nums);
        smsIntent.putExtra("sms_body"  , "Test ");

        try {
            startActivity(smsIntent);
            finish();
            return true;
        }
        catch (android.content.ActivityNotFoundException ex) {
            Toast.makeText(SharingActitivtity.this,
                    "SMS faild, please try again later.", Toast.LENGTH_SHORT).show();
            return false;
        }


    }

    private void openMailComoper() {


//        Uri imageUri = Uri.parse("android.resource://" + getPackageName()
//                + "/drawable/" + "ic_launcher");
//        Intent shareIntent = new Intent();
//        shareIntent.setAction(Intent.ACTION_SEND);
//        shareIntent.putExtra(Intent.EXTRA_TEXT, "Hello");
//        shareIntent.putExtra(Intent.EXTRA_STREAM, imageUri);
//        shareIntent.setType("image/jpeg");
//        shareIntent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
//        startActivity(Intent.createChooser(shareIntent, "send"));

        Intent intent = new Intent(Intent.ACTION_SENDTO);
        intent.setData(Uri.parse("mailto:"+et_email.getText().toString())); // only email apps should handle this
        intent.putExtra(Intent.EXTRA_EMAIL, et_email.getText().toString());
        intent.putExtra(Intent.EXTRA_SUBJECT, "Avoconnect");
        intent.putExtra(Intent.EXTRA_TEXT, share_url);
//        intent.putExtra(Intent.EXTRA_TEXT,
//                Constant.QRSACN_URL+card_id+","+UserPrefrence.getInstance(this).getUserid());
        if (intent.resolveActivity(getPackageManager()) != null) {
            startActivity(intent);
        }

    }

    private void callApiUserid() {
        final ProgressBarCustom progressBarCustom=new ProgressBarCustom(SharingActitivtity.this);
        progressBarCustom.showProgress();
        try {
            String token = UserPrefrence.getInstance(SharingActitivtity.this).getAuthtoken();
            token="bearer " + token;
            RestClient.getInstance(SharingActitivtity.this).get().sharecard_id(token,UserPrefrence.getInstance(SharingActitivtity.this).getUserid(),
                    et_userid.getText().toString()
                    ,card_id,new Callback<ShareCard_Model>() {

                @Override
                public void success(final ShareCard_Model res, Response response) {
                    progressBarCustom.cancelProgress();

                    if(res.getResponseMessage().equalsIgnoreCase(Constant.SUCCESS))
                    {
                        Toast.makeText(SharingActitivtity.this, res.getMessage(), Toast.LENGTH_SHORT).show();
                        Intent intent=new Intent();
                        setResult(RESULT_OK,intent);
                        finish();
                    }else{
                        ProgressBarCustom.customDialog(SharingActitivtity.this,res.getMessage(),false);
                    }

                }

                @Override
                public void failure(final RetrofitError error) {
                    progressBarCustom.cancelProgress();
                    error.printStackTrace();
                    if (error != null) {
                        Utils utils=new Utils(SharingActitivtity.this);
                        utils.showError(error);
                    }
                }
            });

        }catch (Exception e)
        {
            progressBarCustom.cancelProgress();
            e.printStackTrace();
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        if (requestCode == SELECT_PHONE_NUMBER && resultCode == RESULT_OK) {
            // Get the URI and query the content provider for the phone number
            Uri contactUri = data.getData();
            String[] projection = new String[]{ContactsContract.CommonDataKinds.Phone.NUMBER};
            Cursor cursor = this.getContentResolver().query(contactUri, projection,
                    null, null, null);

            // If the cursor returned is valid, get the phone number
            if (cursor != null && cursor.moveToFirst()) {
                int numberIndex = cursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER);
                String number = cursor.getString(numberIndex);
               et_phone.setText(number+"");

            }

            cursor.close();
        }
    }


    public   void getUriToResource(Context context, int resId){
        try {
            Bitmap icon = BitmapFactory.decodeResource(context.getResources(),
                    R.drawable.applogo);

            ByteArrayOutputStream bytes = new ByteArrayOutputStream();
            icon.compress(Bitmap.CompressFormat.JPEG, 60, bytes);
            File f = new File(Environment.getExternalStorageDirectory()
                    + File.separator + "testimage.jpg");
            f.createNewFile();
            FileOutputStream fo = new FileOutputStream(f);
            fo.write(bytes.toByteArray());
            fo.close();

            uri = Uri.fromFile(f);


            /** return uri */

        }catch (Exception e)
        {
            e.printStackTrace();
        }
    }
}
