package com.avoconnect.activity;

import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.os.Build;
import android.os.Handler;
import android.os.Message;
import android.os.SystemClock;

import androidx.annotation.RequiresApi;
import androidx.core.app.ActivityCompat;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Base64;
import android.util.DisplayMetrics;
import android.util.Log;
import android.widget.TextView;

import com.avoconnect.ApplicationClass.MyApplication;
import com.avoconnect.Constants.Constant;
import com.avoconnect.Prefrence.UserPrefrence;
import com.avoconnect.R;
import com.avoconnect.Util.Utils;

import org.json.JSONArray;
import org.json.JSONObject;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.Locale;




public class SplashSreen_Acitivity extends AppCompatActivity {

    private Thread mThread;
    private boolean isFinish = false;
    Context appContext;
    boolean islLogin;
    TextView txt_copyrite;
    Utils utils;

    @RequiresApi(api = Build.VERSION_CODES.O)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_sreen__acitivity);
        new UserPrefrence(SplashSreen_Acitivity.this);
        mThread = new Thread(mRunnable);
        mThread.start();
        utils=new Utils(this);
        Log.e("===mytoken",UserPrefrence.getInstance(this).getFcmToken());

        if(UserPrefrence.getInstance(SplashSreen_Acitivity.this).getCurrentLanguage().length()==0)
        {
            UserPrefrence.getInstance(this).setCurrentLanguage(Constant.ENGLISH_LANG);
        }
        else if(UserPrefrence.getInstance(SplashSreen_Acitivity.this)
                .getCurrentLanguage().equalsIgnoreCase(Constant.HINDI_LANG))
        {
            UserPrefrence.getInstance(this).setCurrentLanguage(Constant.HINDI_LANG);
            MyApplication.setLocalHinidi(SplashSreen_Acitivity.this);
        }
        txt_copyrite=(TextView)findViewById(R.id.txt_copyrite);
        txt_copyrite.setText(getResources().getString(R.string.copyrite));


        try {



            String chcking = getDeviceDensityString(this);
            Log.e("==checking", chcking);

          //  generateHashkey();
        }catch (Exception e)
        {
            e.printStackTrace();
        }

        changedate();
    }



    private void changedate() {

        try {


        }catch (Exception e)
        {
            e.printStackTrace();
        }

    }

    private Runnable mRunnable = new Runnable() {

        @Override
        public void run() {
            SystemClock.sleep(3500);
            mHandler.sendEmptyMessage(0);
        }
    };

    private Handler mHandler = new Handler() {

        @Override
        public void handleMessage(Message msg) {
            if (msg.what == 0 && (!isFinish)) {
                stop();
            }
            super.handleMessage(msg);
        }

    };

    @Override
    protected void onDestroy() {
        try {
            mThread.interrupt();
            mThread = null;
        } catch (Exception e) {
        }
        super.onDestroy();
    }

    private void stop() {
        isFinish = true;

//        Intent intent=new Intent(SplashSreen_Acitivity.this,Login_Activity.class);
//        startActivity(intent);
//        finish();

        if(UserPrefrence.getInstance(this).isLogin()==true)
        {
            Intent intent=new Intent(SplashSreen_Acitivity.this,HomeActivity.class);
            startActivity(intent);
            finish();
        }else{

            Intent intent=new Intent(SplashSreen_Acitivity.this,Login_Activity.class);
            startActivity(intent);
            finish();
        }
    }





    @Override
    public void onBackPressed() {
        super.onBackPressed();
        isFinish = true;
        try {
            mThread.interrupt();
            mThread = null;
        } catch (Exception e) {
        }
        finish();
    }

    @Override
    public void finish() {
        super.finish();

    }

//    public void generateHashkey(){
//        try {
//            PackageInfo info = getPackageManager().getPackageInfo(
//                    PACKAGE,
//                    PackageManager.GET_SIGNATURES);
//            for (Signature signature : info.signatures) {
//                MessageDigest md = MessageDigest.getInstance("SHA");
//                md.update(signature.toByteArray());
//
//                Log.d("KeyHash:", Base64.encodeToString(md.digest(), Base64.DEFAULT));
//            }
//        } catch (PackageManager.NameNotFoundException e) {
//            Log.d("=====", e.getMessage(), e);
//        } catch (NoSuchAlgorithmException e) {
//            Log.d("=====", e.getMessage(), e);
//        }
//    }


    public  String getDeviceDensityString(Context context) {
        String desenty="";
        switch (context.getResources().getDisplayMetrics().densityDpi) {
            case DisplayMetrics.DENSITY_LOW:
                return desenty="ldpi";
            case DisplayMetrics.DENSITY_MEDIUM:
                return desenty="mdpi";
            case DisplayMetrics.DENSITY_TV:
            case DisplayMetrics.DENSITY_HIGH:
                return desenty="hdpi";
            case DisplayMetrics.DENSITY_260:
            case DisplayMetrics.DENSITY_280:
            case DisplayMetrics.DENSITY_300:
            case DisplayMetrics.DENSITY_XHIGH:
                return desenty="xhdpi";
            case DisplayMetrics.DENSITY_340:
            case DisplayMetrics.DENSITY_360:
            case DisplayMetrics.DENSITY_400:
            case DisplayMetrics.DENSITY_420:
            case DisplayMetrics.DENSITY_440:
            case DisplayMetrics.DENSITY_XXHIGH:
                return desenty="xxhdpi";
            case DisplayMetrics.DENSITY_560:
            case DisplayMetrics.DENSITY_XXXHIGH:
                return desenty="xxxhdpi";
        }
        return desenty;
    }
}
