package com.avoconnect.activity;

import android.Manifest;
import android.app.Activity;
import android.app.Dialog;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.hardware.Camera;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.StrictMode;
import android.provider.MediaStore;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import com.avoconnect.Prefrence.UserPrefrence;
import com.avoconnect.R;
import com.avoconnect.Util.CameraUtils_Temp;
import com.avoconnect.Util.ProgressBarCustom;
import com.avoconnect.api.RestClient2_Ocr;
import com.avoconnect.responsemodel.BusinessCardInfo;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.firebase.ml.vision.FirebaseVision;
import com.google.firebase.ml.vision.common.FirebaseVisionImage;
import com.google.firebase.ml.vision.text.FirebaseVisionText;
import com.google.firebase.ml.vision.text.FirebaseVisionTextRecognizer;
import com.karumi.dexter.Dexter;
import com.karumi.dexter.MultiplePermissionsReport;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.DexterError;
import com.karumi.dexter.listener.PermissionRequest;
import com.karumi.dexter.listener.PermissionRequestErrorListener;
import com.karumi.dexter.listener.multi.MultiplePermissionsListener;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.List;

import retrofit.RetrofitError;
import retrofit.client.Response;

public class ScanBusinessCardActivity extends AppCompatActivity {
    private static final String TAG = "ScanBusinessCard";
    private static final int REQUEST_IMAGE_CAPTURE = 1001;
    private final int RESULT_CROP = 400;
    EditText et_fullname, et_desingnation, et_mobile, et_email, et_company, et_website, et_address;
    TextView txt_login;
    Context mcontext;
    Activity mActivity;
    ImageView imageView;
    LinearLayout llCardDetail;
    FloatingActionButton card_add;
    ProgressBarCustom progressBarCustom;

    public static final int MEDIA_TYPE_IMAGE = 1;
    String profilePath = "";
    private static final int BITMAP_SAMPLE_SIZE = 8;
    public  static Bitmap profileBitmap = null;
   public static String cropfilepath="";
   int count_card=1;
   FirebaseVisionText imagescan_str_first,imagescan_str_second;
   Bitmap firstcard_bitmap,secondcard_bitmap;

    public static final String GALLERY_DIRECTORY_NAME = "AvoCard";
    public static final String IMAGE_EXTENSION = "jpg";
   
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        StrictMode.VmPolicy.Builder builder = new StrictMode.VmPolicy.Builder();
        StrictMode.setVmPolicy(builder.build());

        setContentView(R.layout.scan_business_card_layout);
        mcontext = ScanBusinessCardActivity.this;
        mActivity = this;
        inits();

    }
    private void inits() {
        card_add = (FloatingActionButton) findViewById(R.id.card_add);
        card_add.show();
        card_add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                requestStoragePermission();
            }
        });
        txt_login = (TextView) findViewById(R.id.txt_login);
        txt_login.setText("Next");
        et_fullname = (EditText) findViewById(R.id.et_fullname);
        et_address = (EditText) findViewById(R.id.et_address);
        et_desingnation = (EditText) findViewById(R.id.et_desingnation);
        et_mobile = (EditText) findViewById(R.id.et_mobile);
        et_email = (EditText) findViewById(R.id.et_email);
        et_company = (EditText) findViewById(R.id.et_company);
        et_website = (EditText) findViewById(R.id.et_website);
        imageView = (ImageView) findViewById(R.id.imageView);
        llCardDetail = (LinearLayout) findViewById(R.id.llCardDetail);
        llCardDetail.setVisibility(View.GONE);
        txt_login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(mcontext, AddEditCardActitivty.class);
                intent.putExtra("ocrResult", "Scan");
                intent.putExtra("fullName", et_fullname.getText().toString().trim());
                intent.putExtra("position", et_desingnation.getText().toString().trim());
                intent.putExtra("mobileNumber", et_mobile.getText().toString().trim());
                intent.putExtra("emailAddress", et_email.getText().toString().trim());
                intent.putExtra("companyName", et_company.getText().toString().trim());
                intent.putExtra("website", et_website.getText().toString().trim());
                intent.putExtra("address", et_address.getText().toString().trim());
                startActivity(intent);
                finish();
            }
        });

        requestStoragePermission();
    }

    private void startCameraIntentForResult() {
        // Clean up last time's image

        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);

        File file = CameraUtils_Temp.getOutputMediaFile(MEDIA_TYPE_IMAGE);
        if (file != null) {
            profilePath = file.getAbsolutePath();
        }
        Uri fileUri = CameraUtils_Temp.getOutputMediaFileUri(getApplicationContext(), file);
        intent.putExtra(MediaStore.EXTRA_OUTPUT, fileUri);
        intent.putExtra(
                "android.intent.extras.CAMERA_FACING",
                Camera.CameraInfo.CAMERA_FACING_BACK);
        // start the image capture Intent
        startActivityForResult(intent, REQUEST_IMAGE_CAPTURE);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        try {
            if (requestCode == REQUEST_IMAGE_CAPTURE&&resultCode == RESULT_OK ) {
                if (resultCode == RESULT_OK) {
                    CameraUtils_Temp.refreshGallery(getApplicationContext(), profilePath);
                    profileBitmap = CameraUtils_Temp.optimizeBitmap(BITMAP_SAMPLE_SIZE, profilePath);
                    Log.e("=profilePath", profilePath);
                    if (profilePath.length() == 0 && profilePath == null) {
                        Toast.makeText(mcontext, "Something went wrong", Toast.LENGTH_SHORT).show();
                    } else {
                        performCrop(profilePath);
                    }

                }
            } else if (requestCode == RESULT_CROP) {
                if (resultCode == RESULT_OK) {
                    Log.e("=after crop", "=crop");
                    card_add.hide();
                    llCardDetail.setVisibility(View.GONE);
                    Bundle extras = data.getExtras();
                    try {
                        if (extras != null) {
                            if (extras.getParcelable("data") != null) {
                                Log.e("=data", extras.getParcelable("data").toString());
                                profileBitmap = extras.getParcelable("data");
                                imageView.setImageBitmap(profileBitmap);
                                progressBarCustom = new ProgressBarCustom(mcontext);
                                progressBarCustom.showProgress();
                                runTextRecognization(profileBitmap);
                            } else {
                                Uri selectedImageUri = data.getData();
                                if (selectedImageUri != null) {
                                    Log.e("=data", selectedImageUri.toString());
                                    profilePath = selectedImageUri.getPath();
                                    profileBitmap = BitmapFactory.decodeFile(cropfilepath);
                                    imageView.setImageBitmap(profileBitmap);
                                    progressBarCustom = new ProgressBarCustom(mcontext);
                                    progressBarCustom.showProgress();
                                    runTextRecognization(profileBitmap);
                                } else {
                                    Log.e("=null", "null");
                                }
                            }
                        } else {
                            Uri selectedImageUri = data.getData();
                            if (selectedImageUri != null) {
                                Log.e("=data", selectedImageUri.toString());
                                profilePath = selectedImageUri.getPath();
                                profileBitmap = BitmapFactory.decodeFile(cropfilepath);
                                imageView.setImageBitmap(profileBitmap);
                                progressBarCustom = new ProgressBarCustom(mcontext);
                                progressBarCustom.showProgress();



                                runTextRecognization(profileBitmap);
                            } else {
                                Log.e("=null", "null");
                            }
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }else{
                    finish();
                }
            } else if (requestCode == 144 && resultCode == RESULT_OK) {

                Intent intent = new Intent();
                setResult(RESULT_OK, intent);
                finish();
            } else {
                finish();
            }
        }catch (Exception e)
        {
            e.printStackTrace();
        }

    }

    private void runTextRecognization(Bitmap bitmapForDetection) {
        FirebaseVisionImage firebaseVisionImage = FirebaseVisionImage.fromBitmap(bitmapForDetection);
        FirebaseVisionTextRecognizer FirebaseVisionTextDetector = FirebaseVision.getInstance().getOnDeviceTextRecognizer();
        FirebaseVisionTextDetector.processImage(firebaseVisionImage).addOnSuccessListener(new OnSuccessListener<FirebaseVisionText>() {
            @Override
            public void onSuccess(FirebaseVisionText firebaseVisionText) {

                if(count_card==1)
                {
                    /*First time*/
                    firstcard_bitmap=profileBitmap;
                    imagescan_str_first=firebaseVisionText;
                    generapop(firebaseVisionText);
                }else{
                    /*Second time*/
                    secondcard_bitmap=profileBitmap;
                    imagescan_str_second=firebaseVisionText;
                    getTextFromResult(firebaseVisionText);
                }

            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                Toast.makeText(mcontext,"Scan failed. Please scan again",Toast.LENGTH_LONG).show();
                card_add.show();
                llCardDetail.setVisibility(View.GONE);
                progressBarCustom.cancelProgress();
            }
        });
    }

    private void generapop(final FirebaseVisionText firebaseVisionText) {
        AlertDialog.Builder builder=new AlertDialog.Builder(ScanBusinessCardActivity.this);
        View view= LayoutInflater.from(this).inflate(R.layout.logout_lay,null);
        TextView txtDialogMessage=(TextView)view.findViewById(R.id.txtDialogMessage);
        ImageView center_line=(ImageView)view.findViewById(R.id.center_line);
        txtDialogMessage.setText(getResources().getString(R.string.second_image));
        Button btnDialogYes=(Button)view.findViewById(R.id.btnDialogYes);
        btnDialogYes.setText("YES");
        Button btnDialogNo=(Button)view.findViewById(R.id.btnDialogNo);
        btnDialogNo.setText("NO");
        builder.setView(view);
        final Dialog dialog=builder.create();
        dialog.setCancelable(false);
        dialog.setCanceledOnTouchOutside(false);

        btnDialogYes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
                count_card++;
                requestStoragePermission();
            }
        });
        btnDialogNo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
                getTextFromResult(firebaseVisionText);
            }
        });
        dialog.show();
    }

    private void getTextFromResult(FirebaseVisionText firebaseVisionText) {
        StringBuilder strtext = new StringBuilder();
        if(imagescan_str_first!=null) {
            List<FirebaseVisionText.TextBlock> blocks = imagescan_str_first.getTextBlocks();
            if (blocks.size() == 0) {
                Log.i("getTextFromResult", "no data found");
                Toast.makeText(mcontext, "Scan failed. Please scan again", Toast.LENGTH_LONG).show();
                card_add.show();
                llCardDetail.setVisibility(View.GONE);
                progressBarCustom.cancelProgress();
                return;
            }

            for (int i = 0; i < blocks.size(); i++) {
                List<FirebaseVisionText.Line> lineList = blocks.get(i).getLines();
                for (int j = 0; j < lineList.size(); j++) {
                    if (strtext.toString().isEmpty()) {
                        strtext.append(lineList.get(j).getText());
                    } else {
                        strtext.append("\n" + lineList.get(j).getText());
                    }
                }
            }
            Log.i("getTextFromResult", "" + strtext.toString());
        }

        /*Second time oick image*/
        if(imagescan_str_second!=null) {
            List<FirebaseVisionText.TextBlock> blocks_Second = imagescan_str_second.getTextBlocks();
            if (blocks_Second.size() == 0) {
                Log.i("getTextFromResult", "no data found");
                Toast.makeText(mcontext, "Scan failed. Please scan again", Toast.LENGTH_LONG).show();
                card_add.show();
                llCardDetail.setVisibility(View.GONE);
                progressBarCustom.cancelProgress();
                return;
            }
            for (int i = 0; i < blocks_Second.size(); i++) {
                List<FirebaseVisionText.Line> lineList = blocks_Second.get(i).getLines();
                for (int j = 0; j < lineList.size(); j++) {
                    if (strtext.toString().isEmpty()) {
                        strtext.append(lineList.get(j).getText());
                    } else {
                        strtext.append("\n" + lineList.get(j).getText());
                    }
                }
            }

        }

        if(secondcard_bitmap!=null)
        {

            profileBitmap=ProcessingBitmap();

            File fileN = createNewFile("CROP_");
            FileOutputStream fOut;
            try {
                fOut = new FileOutputStream(fileN);
                profileBitmap.compress(Bitmap.CompressFormat.PNG, 100, fOut);
                fOut.flush();
                fOut.close();

                cropfilepath=fileN.getAbsolutePath();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }


       businessCardReader(strtext.toString());
    }

    private Bitmap ProcessingBitmap(){
        Bitmap bm1 = firstcard_bitmap;
        Bitmap bm2 =  secondcard_bitmap;
        Bitmap newBitmap = null;


        int w = bm1.getWidth();
        int h = bm1.getHeight() + bm2.getHeight();
//        int h;
//        if(bm1.getHeight() >= bm2.getHeight()){
//            h = bm1.getHeight();
//        }else{
//            h = bm2.getHeight();
//        }

        Bitmap.Config config = bm1.getConfig();
        if(config == null){
            config = Bitmap.Config.ARGB_8888;
        }

        newBitmap = Bitmap.createBitmap(w, h, config);
        Canvas newCanvas = new Canvas(newBitmap);

        newCanvas.drawBitmap(bm1, 0, 0, null);
        newCanvas.drawBitmap(bm2, 0, bm1.getWidth(), null);

        return newBitmap;
    }

    public void businessCardReader(String strData) {
        try {
            RestClient2_Ocr.getInstance(mcontext).get().getBusinessCardData(UserPrefrence.getInstance(this).getAuthtoken(), strData, new retrofit.Callback<BusinessCardInfo>() {
                @Override
                public void success(final BusinessCardInfo res, Response response) {
                    progressBarCustom.cancelProgress();
                    if (res != null) {
                        llCardDetail.setVisibility(View.GONE);
                        et_fullname.setText(res.getName());
                        try {

                            if (res.getDesignation().contains("\\u0026")) {
                                String designation = res.getDesignation().replace("\\u0026", "&");
                                et_desingnation.setText(designation);
                            } else {
                                et_desingnation.setText(res.getDesignation());
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                            et_desingnation.setText(res.getDesignation());
                        }
                        if (res.getEmails().size() > 0) {
                            et_email.setText(res.getEmails().get(0));
                        }
                        if (res.getUrls().size() > 0) {
                            et_website.setText(res.getUrls().get(0));
                        }
                        et_company.setText(res.getCompany());
                        if (res.getPhoneNumbers().size() > 0) {
                            StringBuilder stringBuilder = new StringBuilder();
                            for (String ss : res.getPhoneNumbers()) {
                                if (stringBuilder.toString().isEmpty()) {
                                    stringBuilder.append(ss);
                                } else {
                                    stringBuilder.append("," + ss);
                                }
                            }
                            et_mobile.setText(stringBuilder.toString());
                        }
                        if(res.getAddress()!=null&&res.getAddress().length()>0)
                        {
                            et_address.setText(res.getAddress());
                        }



                        Intent intent=new Intent(ScanBusinessCardActivity.this,AddEditCardActitivty.class);
                        intent.putExtra("ocrResult", "Scan");
                        if(et_desingnation.getText().toString().length()>0)
                        {
                            intent.putExtra("position", et_desingnation.getText().toString().trim());
                        }
                        if(et_fullname.getText().toString().length()>0)
                        {
                            intent.putExtra("fullName", et_fullname.getText().toString().trim());
                        }
//                        if(profileBitmap!=null)
//                        {
//                            ByteArrayOutputStream bStream = new ByteArrayOutputStream();
//                            profileBitmap.compress(Bitmap.CompressFormat.PNG, 100, bStream);
//                            byte[] byteArray = bStream.toByteArray();
//                            intent.putExtra("profileBitmap",byteArray);
//                        }
                        if(et_mobile.getText().toString().length()>0)
                        {
                            intent.putExtra("mobileNumber", et_mobile.getText().toString().trim());
                        }
                        if(et_email.getText().toString().length()>0)
                        {
                            intent.putExtra("emailAddress", et_email.getText().toString().trim());
                        }
                        if(et_company.getText().toString().length()>0)
                        {
                            intent.putExtra("companyName", et_company.getText().toString().trim());
                        }
                        if(et_website.getText().toString().length()>0)
                        {
                            intent.putExtra("website", et_website.getText().toString().trim());
                        }
                        if(et_address.getText().toString().length()>0)
                        {
                            intent.putExtra("address", et_address.getText().toString().trim());
                        }

                        startActivityForResult(intent,144);

                    }
                }

                @Override
                public void failure(final RetrofitError error) {
                    progressBarCustom.cancelProgress();
                    error.printStackTrace();
                }
            });

        } catch (Exception e) {
            progressBarCustom.cancelProgress();
            //progress.cancelProgress();
            e.printStackTrace();
        }
    }

    private void requestStoragePermission() {
        Dexter.withActivity(this)
                .withPermissions(
                        Manifest.permission.CAMERA,
                        Manifest.permission.READ_EXTERNAL_STORAGE,
                        Manifest.permission.WRITE_EXTERNAL_STORAGE)

                .withListener(new MultiplePermissionsListener() {
                    @Override
                    public void onPermissionsChecked(MultiplePermissionsReport report) {
                        if (report.areAllPermissionsGranted()) {
//                            //  openGallery();
                            startCameraIntentForResult();
                        }
                        if (report.isAnyPermissionPermanentlyDenied()) {

                            CameraUtils_Temp.showSettingsDialog(mcontext);
                        }
                    }

                    @Override
                    public void onPermissionRationaleShouldBeShown(List<PermissionRequest> permissions, PermissionToken token) {
                        token.continuePermissionRequest();
                    }
                }).
                withErrorListener(new PermissionRequestErrorListener() {
                    @Override
                    public void onError(DexterError error) {
                        Toast.makeText(mcontext, "Error occurred! ", Toast.LENGTH_SHORT).show();
                    }
                })
                .onSameThread()
                .check();
    }


    private void performCrop(String picUri) {
        Uri mCropImagedUri;
        try {
            //imageStoragePath = picUri;
            Intent cropIntent = new Intent("com.android.camera.action.CROP");
            File f = new File(picUri);
            Uri contentUri = Uri.fromFile(f);
            cropIntent.setDataAndType(contentUri, "image/*");
          //  cropIntent.putExtra("crop", "true");
            cropIntent.putExtra("aspectX", 1);
            cropIntent.putExtra("aspectY", 1);
            cropIntent.putExtra("outputX", 600);
            cropIntent.putExtra("outputY", 600);
            cropIntent.putExtra("return-data", true);
            File fileN = createNewFile("CROP_");
            try {
                f.createNewFile();
            } catch (IOException ex) {
                Log.e("io", ex.getMessage());
            }
            mCropImagedUri = Uri.fromFile(fileN);
            cropfilepath=mCropImagedUri.getPath();
            cropIntent.putExtra(MediaStore.EXTRA_OUTPUT, mCropImagedUri);
            startActivityForResult(cropIntent, RESULT_CROP);
        } catch (ActivityNotFoundException anfe) {
            String errorMessage = "your device doesn't support the crop action!";
            Toast toast = Toast.makeText(this, errorMessage, Toast.LENGTH_SHORT);
            toast.show();
        }
    }

    private File createNewFile(String prefix) {
        if (prefix == null || "".equalsIgnoreCase(prefix)) {
            prefix = "IMG_";
        }
        File newDirectory = new File(Environment.getExternalStorageDirectory() + "/Avocard/");
        if (!newDirectory.exists()) {
            if (newDirectory.mkdir()) {
                Log.d(mcontext.getClass().getName(), newDirectory.getAbsolutePath() + " directory created");
            }
        }
        File file = new File(newDirectory, (prefix + System.currentTimeMillis() + ".jpg"));
        if (file.exists()) {
            //this wont be executed
            file.delete();
            try {
                file.createNewFile();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        return file;
    }



}
