package com.avoconnect.activity;

import android.content.Intent;
import android.net.Uri;

import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.text.InputType;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.avoconnect.Constants.Constant;
import com.avoconnect.R;
import com.avoconnect.Util.Utils;

import androidx.core.content.ContextCompat;

import android.util.Log;
import android.widget.EditText;
import android.widget.Toast;

import com.avoconnect.Prefrence.UserPrefrence;
import com.avoconnect.Util.ProgressBarCustom;
import com.avoconnect.api.RestClient;
import com.avoconnect.responsemodel.ChangePass_Model;
import com.avoconnect.responsemodel.ForgetPass_Model;

import org.json.JSONObject;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;
import retrofit.mime.TypedByteArray;
import retrofit.mime.TypedInput;

public class ForgetPassword_Actitivty extends AppCompatActivity {

    ImageView img_back,hintEmailIcon,hintpassIcon;
    TextView txt_heading,txt_forget_msg,txt_resend;
    EditText et_email,et_password;
    Utils utils;
    boolean isreset=false;
    String uri;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forget_password__actitivty);
        txt_heading=(TextView)findViewById(R.id.txt_heading);
        txt_resend=(TextView)findViewById(R.id.txt_resend);
        et_email=(EditText) findViewById(R.id.et_email);
        hintEmailIcon=(ImageView)findViewById(R.id.hintEmailIcon);
        hintpassIcon=(ImageView)findViewById(R.id.hintpassIcon);
        et_password=(EditText) findViewById(R.id.et_password);
        txt_forget_msg=(TextView)findViewById(R.id.txt_forget_msg);
        Uri data = this.getIntent().getData();
        if (data != null && data.isHierarchical()) {
            isreset=true;
             uri = this.getIntent().getDataString();
            txt_heading.setText(getResources().getString(R.string.reset_pass));
            txt_resend.setText(getResources().getString(R.string.submit));
            txt_forget_msg.setText(getResources().getString(R.string.newpass_txt));
            et_email.setVisibility(View.GONE);
            hintEmailIcon.setVisibility(View.GONE);
            hintpassIcon.setVisibility(View.VISIBLE);
            et_password.setVisibility(View.VISIBLE);

            et_password.setInputType(InputType.TYPE_CLASS_TEXT |
                    InputType.TYPE_TEXT_VARIATION_PASSWORD);
            uri= uri.substring(uri.lastIndexOf('/') + 1).trim();
            Log.e("MyApp", "Deep link clicked " + uri);
        }

        utils=new Utils(ForgetPassword_Actitivty.this);
        img_back=(ImageView)findViewById(R.id.img_back);





        img_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(isreset==true)
                {
                    Intent intent=new Intent(ForgetPassword_Actitivty.this,Login_Activity.class);
                    startActivity(intent);
                    finish();
                }else {
                    finish();
                }
            }
        });

        txt_resend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                validation();
            }
        });

        et_email.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View mView, boolean hasFocus) {
                if (hasFocus)
                {
                    addOrRemoveProperty(hintEmailIcon, RelativeLayout.CENTER_VERTICAL, false);
                }else{
                    if (et_email.getText().toString().trim().length()>0)
                    {
                        addOrRemoveProperty(hintEmailIcon, RelativeLayout.CENTER_VERTICAL, false);
                    }else{
                        addOrRemoveProperty(hintEmailIcon, RelativeLayout.CENTER_VERTICAL, true);
                    }
                }
            }
        });

        et_password.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View mView, boolean hasFocus) {
                if (hasFocus)
                {
                    addOrRemoveProperty(hintpassIcon, RelativeLayout.CENTER_VERTICAL, false);
                }else{
                    if (et_password.getText().toString().trim().length()>0)
                    {
                        addOrRemoveProperty(hintpassIcon, RelativeLayout.CENTER_VERTICAL, false);
                    }else{
                        addOrRemoveProperty(hintpassIcon, RelativeLayout.CENTER_VERTICAL, true);
                    }
                }
            }
        });

        setFonts();
    }

    private void addOrRemoveProperty(ImageView view ,int property, boolean flag){
        RelativeLayout.LayoutParams layoutParams = (RelativeLayout.LayoutParams) view.getLayoutParams();
        if(flag){
            view.setColorFilter(ContextCompat.getColor(ForgetPassword_Actitivty.this, R.color.light_grey));
            layoutParams.addRule(property);
        }else {
            view.setColorFilter(ContextCompat.getColor(ForgetPassword_Actitivty.this, R.color.background_color));
            layoutParams.removeRule(property);
        }
        view.setLayoutParams(layoutParams);
    }

    private void setFonts() {
        utils.setTextview(txt_heading,1);
        utils.setTextview(txt_forget_msg,1);
        utils.setTextview(txt_resend,2);
        utils.setEditview(et_email,1);

    }

    private void validation() {
        String getemail=et_email.getText().toString();
        if(isreset==true)
        {
            if(et_password.getText().toString().length()==0)
            {
                ProgressBarCustom.customDialog(this,getResources().getString(R.string.enter_password),false);
            }else if(et_password.getText().toString().length()<8)
            {
                ProgressBarCustom.customDialog(this,getResources().getString(R.string.password_length),false);
            }else{
                if(utils.isNetworkAvailable(this)==true)
                {
                    callApiResetPassword();
                }else {
                    ProgressBarCustom.customDialog(this,getResources().getString(R.string.internet_connection),false);
                }
            }
        }else{
            if(et_email.getText().toString().length()==0)
            {
                ProgressBarCustom.customDialog(this,getResources().getString(R.string.enter_email),false);
            }else if(new Utils(this).isvalidaEmail(getemail)==false)
            {
                ProgressBarCustom.customDialog(this,getResources().getString(R.string.enter_valid_email),false);
            }else{
                if(utils.isNetworkAvailable(this)==true)
                {
                    callApi();
                }else {
                    ProgressBarCustom.customDialog(this,getResources().getString(R.string.internet_connection),false);
                }
            }
        }

    }

    private void callApi() {
        final ProgressBarCustom progressBarCustom=new ProgressBarCustom(ForgetPassword_Actitivty.this);
        progressBarCustom.showProgress();
        try {
            String getemail=et_email.getText().toString();
            JSONObject jsonObject3 = new JSONObject();
            jsonObject3.put("email", et_email.getText().toString());

            TypedInput in3 = new TypedByteArray("application/json", jsonObject3.toString().getBytes("UTF-8"));

            RestClient.getInstance(this).get().forgetpasswrod(getemail, in3, new Callback<ForgetPass_Model>() {

                @Override
                public void success(final ForgetPass_Model res, Response response) {
                    progressBarCustom.cancelProgress();
                    if(res.getResponseMessage().equalsIgnoreCase(Constant.SUCCESS))
                    {
                        Toast.makeText(ForgetPassword_Actitivty.this, res.getMessage(), Toast.LENGTH_SHORT).show();
                    }else{
                        Toast.makeText(ForgetPassword_Actitivty.this, res.getMessage(), Toast.LENGTH_SHORT).show();
                    }

                }

                @Override
                public void failure(final RetrofitError error) {
                    progressBarCustom.cancelProgress();
                    error.printStackTrace();
                    if (error != null) {
                        utils.showError(error);
                    }
                }
            });

        }catch (Exception e)
        {
            progressBarCustom.cancelProgress();
            //progress.cancelProgress();
            e.printStackTrace();
        }


    }

    @Override
    public void onBackPressed() {
        if(isreset==true)
        {
            Intent intent=new Intent(ForgetPassword_Actitivty.this,Login_Activity.class);
            startActivity(intent);
            finish();
        }else {
            finish();
        }
    }



    public void callApiResetPassword()
    {
        final ProgressBarCustom progressBarCustom=new ProgressBarCustom(ForgetPassword_Actitivty.this);
        progressBarCustom.showProgress();
        try {

            JSONObject jsonObject3 = new JSONObject();
            jsonObject3.put("id", new UserPrefrence(this).getUserid());
            jsonObject3.put("password", et_password.getText().toString());
            jsonObject3.put("token", uri.toString());
            TypedInput in3 = new TypedByteArray("application/json", jsonObject3.toString().getBytes("UTF-8"));

            RestClient.getInstance(this).get().changepassword(in3, new Callback<ChangePass_Model>() {

                @Override
                public void success(final ChangePass_Model res, Response response) {
                    progressBarCustom.cancelProgress();
                    if(res.getResponseMessage().equalsIgnoreCase(Constant.SUCCESS))
                    {
                        Toast.makeText(ForgetPassword_Actitivty.this, res.getMessage(), Toast.LENGTH_SHORT).show();
                        Intent intent=new Intent(ForgetPassword_Actitivty.this,Login_Activity.class);
                        startActivity(intent);
                        finish();
                    }else{
                        Toast.makeText(ForgetPassword_Actitivty.this, res.getMessage(), Toast.LENGTH_SHORT).show();
                    }

                }

                @Override
                public void failure(final RetrofitError error) {
                    progressBarCustom.cancelProgress();
                    error.printStackTrace();
                    if (error != null) {
                        utils.showError(error);
                    }
                }
            });

        }catch (Exception e)
        {
            progressBarCustom.cancelProgress();
            //progress.cancelProgress();
            e.printStackTrace();
        }

    }
}
