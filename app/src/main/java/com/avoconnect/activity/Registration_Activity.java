package com.avoconnect.activity;

import android.content.Context;

import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.InputType;
import android.text.TextWatcher;
import android.text.method.PasswordTransformationMethod;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.avoconnect.Constants.Constant;
import com.avoconnect.R;
import com.avoconnect.Util.ProgressBarCustom;
import com.avoconnect.Util.Utils;
import com.avoconnect.api.RestClient;
import com.avoconnect.responsemodel.Signup_Model;

import org.json.JSONObject;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;
import retrofit.mime.TypedByteArray;
import retrofit.mime.TypedInput;


public class Registration_Activity extends AppCompatActivity implements View.OnClickListener{

    EditText et_fullname,et_email,et_password,et_conf_password;
    TextView txt_reg,txt_signin,txt_already;
    ImageView hint_img_fullname,hint_img_email,hint_img_password,hint_img_conimpass;
    Utils utils;
    Context context;
    private final String PASSWORD_PATTERN =
            "((?=.*\\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[@#$%]).{6,20})";
    private Pattern pattern;
    private Matcher matcher;

    boolean ispassword_visisble=true,isconfimepassword_visisble=true;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.registration_screen);
         context=Registration_Activity.this;
        pattern = Pattern.compile(PASSWORD_PATTERN);
        inits();
    }

    private void inits() {
        et_fullname=(EditText)findViewById(R.id.et_fullname);
        et_email=(EditText)findViewById(R.id.et_email);
        et_password=(EditText)findViewById(R.id.et_password);
        et_conf_password=(EditText)findViewById(R.id.et_conf_password);
        txt_reg=(TextView) findViewById(R.id.txt_reg);
        txt_signin=(TextView)findViewById(R.id.txt_signin);
        txt_already=(TextView)findViewById(R.id.txt_already);

        hint_img_fullname=(ImageView)findViewById(R.id.hint_img_fullname);
        hint_img_email=(ImageView)findViewById(R.id.hint_img_email);
        hint_img_password=(ImageView)findViewById(R.id.hint_img_password);
        hint_img_conimpass=(ImageView)findViewById(R.id.hint_img_conimpass);

        setCapitalizeTextWatcher(et_fullname);


        utils=new Utils(this);
        txt_reg.setOnClickListener(this);
        txt_signin.setOnClickListener(this);


        setFonts();
    }

    private void setFonts() {

        et_password.setInputType(InputType.TYPE_CLASS_TEXT |
                InputType.TYPE_TEXT_VARIATION_PASSWORD);

        et_conf_password.setInputType(InputType.TYPE_CLASS_TEXT |
                InputType.TYPE_TEXT_VARIATION_PASSWORD);


        et_fullname.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View mView, boolean hasFocus) {
                if (hasFocus)
                {
                    addOrRemoveProperty(hint_img_fullname, RelativeLayout.CENTER_VERTICAL, false);
                }else{
                    if (et_fullname.getText().toString().trim().length()>0)
                    {
                        addOrRemoveProperty(hint_img_fullname, RelativeLayout.CENTER_VERTICAL, false);
                    }else{
                        addOrRemoveProperty(hint_img_fullname, RelativeLayout.CENTER_VERTICAL, true);
                    }
                }
            }
        });

        et_email.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View mView, boolean hasFocus) {
                if (hasFocus)
                {
                    addOrRemoveProperty(hint_img_email, RelativeLayout.CENTER_VERTICAL, false);
                }else{
                    if (et_email.getText().toString().trim().length()>0)
                    {
                        addOrRemoveProperty(hint_img_email, RelativeLayout.CENTER_VERTICAL, false);
                    }else{
                        addOrRemoveProperty(hint_img_email, RelativeLayout.CENTER_VERTICAL, true);
                    }
                }
            }
        });

        et_password.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View mView, boolean hasFocus) {
                if (hasFocus)
                {
                    addOrRemoveProperty(hint_img_password, RelativeLayout.CENTER_VERTICAL, false);
                }else{
                    if (et_password.getText().toString().trim().length()>0)
                    {
                        addOrRemoveProperty(hint_img_password, RelativeLayout.CENTER_VERTICAL, false);
                    }else{
                        addOrRemoveProperty(hint_img_password, RelativeLayout.CENTER_VERTICAL, true);
                    }
                }
            }
        });

        et_conf_password.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View mView, boolean hasFocus) {
                if (hasFocus)
                {
                    addOrRemoveProperty(hint_img_conimpass, RelativeLayout.CENTER_VERTICAL, false);
                }else{
                    if (et_conf_password.getText().toString().trim().length()>0)
                    {
                        addOrRemoveProperty(hint_img_conimpass, RelativeLayout.CENTER_VERTICAL, false);
                    }else{
                        addOrRemoveProperty(hint_img_conimpass, RelativeLayout.CENTER_VERTICAL, true);
                    }
                }
            }
        });

        hint_img_password.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(ispassword_visisble==true)
                {
                    ispassword_visisble=false;
                    et_password.setTransformationMethod(null);
                    et_password.setSelection(et_password.getText().toString().length());
                }else{
                    ispassword_visisble=true;
                    et_password.setTransformationMethod(new PasswordTransformationMethod());
                    et_password.setSelection(et_password.getText().toString().length());
                }
            }
        });

        hint_img_conimpass.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(isconfimepassword_visisble==true)
                {
                    isconfimepassword_visisble=false;
                    et_conf_password.setTransformationMethod(null);
                    et_conf_password.setSelection(et_conf_password.getText().toString().length());
                }else{
                    isconfimepassword_visisble=true;
                    et_conf_password.setTransformationMethod(new PasswordTransformationMethod());
                    et_conf_password.setSelection(et_conf_password.getText().toString().length());
                }
            }
        });

    }

    private void addOrRemoveProperty(ImageView view ,int property, boolean flag){
        RelativeLayout.LayoutParams layoutParams = (RelativeLayout.LayoutParams) view.getLayoutParams();
        if(flag){
            view.setColorFilter(ContextCompat.getColor(Registration_Activity.this, R.color.light_grey));
            layoutParams.addRule(property);
        }else {
            view.setColorFilter(ContextCompat.getColor(Registration_Activity.this, R.color.background_color));
            layoutParams.removeRule(property);
        }
        view.setLayoutParams(layoutParams);
    }
    @Override
    public void onClick(View view) {
        switch (view.getId())
        {
            case R.id.txt_signin:
                finish();
                break;
            case R.id.txt_reg:
                validation();
                break;
        }
    }

    private void validation() {
        String getemail=et_email.getText().toString();
        String getpassword=et_password.getText().toString();
        String get_confirmpass=et_conf_password.getText().toString();
        String getfullname=et_fullname.getText().toString();
        if(et_fullname.getText().toString().length()==0)
        {
            ProgressBarCustom.customDialog(this,getResources().getString(R.string.enter_fulllname),false);
        }
        else if(utils.validateFirstName(getfullname)==false)
        {
            ProgressBarCustom.customDialog(this,getResources().getString(R.string.valid_firstname),false);
        }
        else if(et_email.getText().toString().length()==0)
        {
            ProgressBarCustom.customDialog(this,getResources().getString(R.string.enter_email),false);
        }
        else if(new Utils(this).isvalidaEmail(getemail)==false)
        {
            ProgressBarCustom.customDialog(this,getResources().getString(R.string.enter_valid_email),false);
        }
        else if(getpassword.length()==0)
        {
            ProgressBarCustom.customDialog(this,getResources().getString(R.string.enter_password),false);
        }
        else if(getpassword.length()<8)
        {
            ProgressBarCustom.customDialog(this,getResources().getString(R.string.password_length),false);
        }
        else if(get_confirmpass.length()==0){

            ProgressBarCustom.customDialog(this,getResources().getString(R.string.enter_con_password),false);

        }
        else if(!getpassword.equalsIgnoreCase(get_confirmpass))
        {
            ProgressBarCustom.customDialog(this,getResources().getString(R.string.password_not_match),false);
        }
        else{
            if(utils.isNetworkAvailable(this)==true)
            {
                Intent intent=new Intent(Registration_Activity.this,TermsCondition_Actitivity.class);
                intent.putExtra("name",et_fullname.getText().toString());
                intent.putExtra("email",et_email.getText().toString());
                intent.putExtra("password",et_password.getText().toString());
                startActivityForResult(intent,124);

          //      callApi();
            }else{
                ProgressBarCustom.customDialog(this,getResources().getString(R.string.internet_connection),false);
            }

        }
    }

    private void callApi() {
           final ProgressBarCustom progressBarCustom=new ProgressBarCustom(Registration_Activity.this);
            progressBarCustom.showProgress();
            try {
                String getpassword=et_password.getText().toString();
                String checking=utils.convertpasswordtobase64(getpassword);

                JSONObject jsonObject3 = new JSONObject();
                jsonObject3.put("name", et_fullname.getText().toString());
                jsonObject3.put("email", et_email.getText().toString());
                jsonObject3.put("password",checking.trim());
                TypedInput in3 = new TypedByteArray("application/json", jsonObject3.toString().getBytes("UTF-8"));

                RestClient.getInstance(this).get().signup( in3, new Callback<Signup_Model>() {

                    @Override
                    public void success(final Signup_Model res, Response response) {
                        progressBarCustom.cancelProgress();

                            if(res.getResponseMessage().equalsIgnoreCase(Constant.SUCCESS))
                            {
//                            UserPrefrence.getInstance(context)
//                                    .setUserid(res.getUserId().toString());
//
//                            UserPrefrence.getInstance(context)
//                                    .setIsLogin(res.getUserId().toString());
//
//                            UserPrefrence.getInstance(context)
//                                    .setUsername(res.getName().toString());
//
//                            UserPrefrence.getInstance(context)
//                                    .setEmail(res.getEmail().toString());

//                                UserPrefrence.getInstance(Registration_Activity.this)
//                                        .setAuthtoken(res.ge().toString());

                            ProgressBarCustom.customDialog(Registration_Activity.this,res.getMessage(),true);
                            }else{
                                Toast.makeText(context, res.getMessage(), Toast.LENGTH_SHORT).show();
                            }
                    }

                    @Override
                    public void failure(final RetrofitError error) {
                        progressBarCustom.cancelProgress();
                        error.printStackTrace();
                        if (error != null) {
                            utils.showError(error);
                        }

                    }
                });

            }catch (Exception e)
            {
                progressBarCustom.cancelProgress();
                //progress.cancelProgress();
                e.printStackTrace();
            }


    }


    public  void setCapitalizeTextWatcher(final EditText editText) {
        try {
            final TextWatcher textWatcher = new TextWatcher() {

                int mStart = 0;

                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                }

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {
                    mStart = start + count;
                }

                @Override
                public void afterTextChanged(Editable s) {
                    String input = s.toString();
                    String capitalizedText;
                    if (input.length() < 1)
                        capitalizedText = input;
                    else
                        capitalizedText = input.substring(0, 1).toUpperCase() + input.substring(1);
                    if (!capitalizedText.equals(editText.getText().toString())) {
                        editText.addTextChangedListener(new TextWatcher() {
                            @Override
                            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                            }

                            @Override
                            public void onTextChanged(CharSequence s, int start, int before, int count) {

                            }

                            @Override
                            public void afterTextChanged(Editable s) {
                                editText.setSelection(mStart);
                                editText.removeTextChangedListener(this);
                            }
                        });
                        editText.setText(capitalizedText);
                    }
                }
            };

            editText.addTextChangedListener(textWatcher);
        }catch (Exception e)
        {
            e.printStackTrace();
        }
    }

    public boolean validate(final String password){

        matcher = pattern.matcher(password);
        return matcher.matches();

    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        if(requestCode==124&&resultCode==RESULT_OK)
        {
            finish();
        }
    }
}
