package com.avoconnect.activity;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.avoconnect.Constants.Constant;
import com.avoconnect.Prefrence.UserPrefrence;
import com.avoconnect.R;
import com.avoconnect.Util.ProgressBarCustom;
import com.avoconnect.Util.Utils;
import com.avoconnect.api.RestClient;
import com.avoconnect.responsemodel.LoginUser_Model;
import com.avoconnect.responsemodel.RegistrationOtp_Model;

import org.json.JSONObject;

import java.util.LinkedHashMap;

import okhttp3.RequestBody;
import retrofit.RetrofitError;
import retrofit.client.Response;
import retrofit.mime.TypedByteArray;
import retrofit.mime.TypedInput;

public class Otpverify_Actitivity extends AppCompatActivity implements TextWatcher,View.OnClickListener{

    TextView txt_copyrite,txt_login,resend_otp;
    EditText etOTP1,etOTP2,etOTP3,etOTP4;

    Utils utils;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_otpverify__actitivity);
         utils=new Utils(Otpverify_Actitivity.this);

        txt_copyrite=(TextView)findViewById(R.id.txt_copyrite);
        txt_login=(TextView)findViewById(R.id.txt_login);
        resend_otp=(TextView)findViewById(R.id.resend_otp);
        etOTP1=(EditText)findViewById(R.id.edit_field1);
        etOTP2=(EditText)findViewById(R.id.edit_field2);
        etOTP3=(EditText)findViewById(R.id.edit_field3);
        etOTP4=(EditText)findViewById(R.id.edit_field4);


        etOTP1.addTextChangedListener(this);
        etOTP2.addTextChangedListener(this);
        etOTP3.addTextChangedListener(this);
        etOTP4.addTextChangedListener(this);

        resend_otp.setOnClickListener(this);
        txt_login.setOnClickListener(this);
        txt_copyrite.setText(getResources().getString(R.string.copyrite));
    }


    @Override
    public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

    }

    @Override
    public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

    }

    @Override
    public void afterTextChanged(Editable editable) {
        if (editable.length() == 1) {
            if (etOTP1.length() == 1) {
                etOTP2.requestFocus();
            }

            if (etOTP2.length() == 1) {
                etOTP3.requestFocus();
            }
            if (etOTP3.length() == 1) {
                etOTP4.requestFocus();
            }
        } else if (editable.length() == 0) {
            if (etOTP4.length() == 0) {
                etOTP3.requestFocus();
            }
            if (etOTP3.length() == 0) {
                etOTP2.requestFocus();
            }
            if (etOTP2.length() == 0) {
                etOTP1.requestFocus();
            }
        }
    }


    @Override
    public void onClick(View v) {
        switch (v.getId())
        {
            case R.id.txt_login:
                validation();
                break;
            case R.id.resend_otp:
                etOTP1.setText("");
                etOTP2.setText("");
                etOTP3.setText("");
                etOTP4.setText("");
                callresndapi();
                break;
        }
    }

    private void validation() {
        if(etOTP1.getText().length()==0||etOTP2.getText().length()==0||etOTP3.getText().length()==0||etOTP4.getText().length()==0)
        {
            ProgressBarCustom.customDialog(Otpverify_Actitivity.this,getResources().getString(R.string.enter_valid_otp),false);
        }else{
//            Intent intent=new Intent(Otpverify_Actitivity.this,GeneratePopAskEmail.class);
//            startActivity(intent);
            callApi();
        }

    }
    private void callresndapi() {


        final ProgressBarCustom progressBarCustom=new ProgressBarCustom(Otpverify_Actitivity.this);
        progressBarCustom.showProgress();
        try {

            RestClient.getInstance(this).get().registration_otp(UserPrefrence.getInstance(this).getMobile(),new retrofit.Callback<RegistrationOtp_Model>() {

                @Override
                public void success(final RegistrationOtp_Model res, Response response) {
                    progressBarCustom.cancelProgress();



                    if(res.getResponseMessage().equalsIgnoreCase(Constant.SUCCESS))
                    {

                        UserPrefrence.getInstance(Otpverify_Actitivity.this).setMobile(res.getMobile().toString());
                        Toast.makeText(Otpverify_Actitivity.this, res.getMessage(), Toast.LENGTH_SHORT).show();

//                        if(res.getNewUser()==true)
//                        {
//                            Intent intent=new Intent(Otpverify_Actitivity.this,TermsCondition_Actitivity.class);
//                            startActivity(intent);
//                        }else{
//                            Toast.makeText(Otpverify_Actitivity.this, res.getMessage(), Toast.LENGTH_SHORT).show();
//                            Intent intent=new Intent(Otpverify_Actitivity.this,Otpverify_Actitivity.class);
//                            startActivity(intent);
//                        }




//                        UserPrefrence.getInstance(Login_Activity.this)
//                                .setUserid(res.getUserid().toString());
////
//                        UserPrefrence.getInstance(Login_Activity.this)
//                                .setIsLogin(res.getMessage());
//
//                        UserPrefrence.getInstance(Login_Activity.this)
//                                .setUsername(res.getFirstName().toString());
//
//                        UserPrefrence.getInstance(Login_Activity.this)
//                                .setEmail(res.getEmail().toString());
//
//                        UserPrefrence.getInstance(Login_Activity.this)
//                                .setAuthtoken(res.getAccessToken().toString());
//
//
//                        if(res.getUserType()!=null)
//                        {
//                            if(res.getUserType().length()==0)
//                            {
//                                UserPrefrence.getInstance(Login_Activity.this).setUsertype(Constant.USERTYPE_FREE);
//                            }else{
//                                UserPrefrence.getInstance(Login_Activity.this).setUsertype(res.getUserType().toString());
//                            }
//                        }
//
//                       //UserPrefrence.getInstance(Login_Activity.this).setUsertype(Constant.USERTYPE_FREE);
//
//                        if(res.getMobile()!=null)
//                        {
//                            UserPrefrence.getInstance(Login_Activity.this).setMobile(res.getMobile().toString());
//                        }
//                        try {
//                            UserPrefrence.getInstance(Login_Activity.this).setIsAskemail(res.getAskEmailOnViewCard() + "");
//                            UserPrefrence.getInstance(Login_Activity.this).setIsNotificationOn(res.getIsNotificationActive() + "");
//                            UserPrefrence.getInstance(Login_Activity.this).setIsAuthenticaltion(res.getIsTwoWayAuthEnabled() + "");
//                        }catch (Exception e)
//                        {
//                            e.printStackTrace();
//                        }
//
//                        Toast.makeText(Login_Activity.this, res.getMessage(), Toast.LENGTH_SHORT).show();
//                        Intent intent=new Intent(Login_Activity.this,HomeActivity.class);
//                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
//                        startActivity(intent);
//                        finish();

                    }else{
                        Toast.makeText(Otpverify_Actitivity.this, res.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                }

                @Override
                public void failure(final RetrofitError error) {
                    progressBarCustom.cancelProgress();
                    if (error != null) {
                        utils.showError(error);
                    }
                }
            });

        }catch (Exception e)
        {
            progressBarCustom.cancelProgress();
            //progress.cancelProgress();
            e.printStackTrace();
        }
    }

    private void callApi() {

        JSONObject jsonObject = new JSONObject();
        RequestBody body = null;
        TypedInput in=null;
        final ProgressBarCustom progressBarCustom=new ProgressBarCustom(Otpverify_Actitivity.this);
        progressBarCustom.showProgress();

        try {
            final String getpassword=etOTP1.getText().toString()+""+etOTP2.getText().toString()
                    +etOTP3.getText().toString()+etOTP4.getText().toString();
            LinkedHashMap<String, String> queryparms = new LinkedHashMap<>();
            queryparms.put("grant_type", "password");
            queryparms.put("roleId", 2+"");
            queryparms.put("username",UserPrefrence.getInstance(this).getMobile());
            queryparms.put("password", utils.convertpasswordtobase64(getpassword));
            queryparms.put("deviceAgent","ANDROID");
            queryparms.put("deviceId", UserPrefrence.getInstance(Otpverify_Actitivity.this).getFcmToken());
            in = new TypedByteArray("application/json", jsonObject.toString().getBytes("UTF-8"));
            RestClient.getInstance(this).get().loginuser(in,queryparms,new retrofit.Callback<LoginUser_Model>() {

                @Override
                public void success(final LoginUser_Model res, Response response) {
                    progressBarCustom.cancelProgress();
                    if(res.getResponseMessage().equalsIgnoreCase(Constant.SUCCESS))
                    {
                        UserPrefrence.getInstance(Otpverify_Actitivity.this).setOtpnumnber(getpassword.trim());
                        UserPrefrence.getInstance(Otpverify_Actitivity.this).setExpiresin(res.getExpiresIn()+"");
                        UserPrefrence.getInstance(Otpverify_Actitivity.this).setJti(res.getJti()+"");
                        UserPrefrence.getInstance(Otpverify_Actitivity.this).setRefreshtoken(res.getRefreshToken()+"");
                        UserPrefrence.getInstance(Otpverify_Actitivity.this).setPermission(res.getPermission()+"");

                        if(res.getIsEmailExists()==true) {
                            UserPrefrence.getInstance(Otpverify_Actitivity.this)
                                    .setUserid(res.getUserid().toString());
//
                            UserPrefrence.getInstance(Otpverify_Actitivity.this)
                                    .setIsLogin(res.getMessage());

                            UserPrefrence.getInstance(Otpverify_Actitivity.this)
                                    .setUsername(res.getFirstName().toString());

                            UserPrefrence.getInstance(Otpverify_Actitivity.this)
                                    .setEmail(res.getEmail().toString());

                            UserPrefrence.getInstance(Otpverify_Actitivity.this)
                                    .setAuthtoken(res.getAccessToken().toString());

                            if(res.getAvoId()!=null) {
                                UserPrefrence.getInstance(Otpverify_Actitivity.this)
                                        .setAvoid(res.getAvoId().toString());
                            }


                            if (res.getUserType() != null) {
                                if (res.getUserType().length() == 0) {
                                    UserPrefrence.getInstance(Otpverify_Actitivity.this).setUsertype(Constant.USERTYPE_FREE);
                                } else {
                                    UserPrefrence.getInstance(Otpverify_Actitivity.this).setUsertype(res.getUserType().toString());
                                }
                            }

                            //UserPrefrence.getInstance(Otpverify_Actitivity.this).setUsertype(Constant.USERTYPE_FREE);

                            if (res.getMobile() != null) {
                                UserPrefrence.getInstance(Otpverify_Actitivity.this).setMobile(res.getMobile().toString());
                            }
                            try {
                                UserPrefrence.getInstance(Otpverify_Actitivity.this).setIsAskemail(res.getAskEmailOnViewCard() + "");
                                UserPrefrence.getInstance(Otpverify_Actitivity.this).setIsNotificationOn(res.getIsNotificationActive() + "");
                                UserPrefrence.getInstance(Otpverify_Actitivity.this).setIsAuthenticaltion(res.getIsTwoWayAuthEnabled() + "");
                            } catch (Exception e) {
                                e.printStackTrace();
                            }

                            Toast.makeText(Otpverify_Actitivity.this, res.getMessage(), Toast.LENGTH_SHORT).show();
                            Intent intent = new Intent(Otpverify_Actitivity.this, HomeActivity.class);
                            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                            startActivity(intent);
                           // finish();
                        }else{
                            UserPrefrence.getInstance(Otpverify_Actitivity.this)
                                    .setUserid(res.getUserid().toString());


                            UserPrefrence.getInstance(Otpverify_Actitivity.this)
                                    .setUsername(res.getFirstName().toString());

                            UserPrefrence.getInstance(Otpverify_Actitivity.this)
                                    .setEmail(res.getEmail().toString());

                            UserPrefrence.getInstance(Otpverify_Actitivity.this)
                                    .setAuthtoken(res.getAccessToken().toString());

                            UserPrefrence.getInstance(Otpverify_Actitivity.this)
                                    .setAvoid(res.getAvoId().toString());

                            if (res.getUserType() != null) {
                                if (res.getUserType().length() == 0) {
                                    UserPrefrence.getInstance(Otpverify_Actitivity.this).setUsertype(Constant.USERTYPE_FREE);
                                } else {
                                    UserPrefrence.getInstance(Otpverify_Actitivity.this).setUsertype(res.getUserType().toString());
                                }
                            }

                            //UserPrefrence.getInstance(Otpverify_Actitivity.this).setUsertype(Constant.USERTYPE_FREE);

                            if (res.getMobile() != null) {
                                UserPrefrence.getInstance(Otpverify_Actitivity.this).setMobile(res.getMobile().toString());
                            }
                            try {
                                UserPrefrence.getInstance(Otpverify_Actitivity.this).setIsAskemail(res.getAskEmailOnViewCard() + "");
                                UserPrefrence.getInstance(Otpverify_Actitivity.this).setIsNotificationOn(res.getIsNotificationActive() + "");
                                UserPrefrence.getInstance(Otpverify_Actitivity.this).setIsAuthenticaltion(res.getIsTwoWayAuthEnabled() + "");
                            } catch (Exception e) {
                                e.printStackTrace();
                            }


                            Intent intent = new Intent(Otpverify_Actitivity.this, EditProfileAcitivity.class);
                            intent.putExtra("from_first_login","yes");
                            startActivity(intent);
                         //   finish();
                        }

                    }else{

                        Toast.makeText(Otpverify_Actitivity.this, res.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                }

                @Override
                public void failure(final RetrofitError error) {
                    progressBarCustom.cancelProgress();
                    if (error != null) {

                        utils.showError(error);
                    }
                }
            });

        }catch (Exception e)
        {
            progressBarCustom.cancelProgress();
            //progress.cancelProgress();
            e.printStackTrace();
        }
    }
}
