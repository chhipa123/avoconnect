package com.avoconnect.activity;

import android.app.Activity;
import android.app.TimePickerDialog;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Handler;

import com.avoconnect.Util.CustomTypefaceSpan;
import com.google.android.material.textfield.TextInputLayout;

import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import androidx.cardview.widget.CardView;
import android.text.SpannableStringBuilder;
import android.text.style.StyleSpan;
import android.util.Log;
import android.view.View;
import android.webkit.URLUtil;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.avoconnect.Constants.Constant;
import com.avoconnect.Prefrence.UserPrefrence;
import com.avoconnect.R;
import com.avoconnect.Util.ProgressBarCustom;
import com.avoconnect.Util.Utils;
import com.avoconnect.api.RestClient;
import com.avoconnect.responsemodel.AddLable_Modele;
import com.avoconnect.responsemodel.AddLocationTime_Modele;
import com.avoconnect.responsemodel.AddNote_Modele;
import com.avoconnect.responsemodel.AddTag_Modele;
import com.avoconnect.responsemodel.AdditionalUpdate_Model;

import org.json.JSONObject;

import java.util.Calendar;
import java.util.Locale;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;
import retrofit.mime.TypedByteArray;
import retrofit.mime.TypedInput;

public class Otheruserinfoadd_Activity extends AppCompatActivity implements View.OnClickListener{

    TextView heading,subheading,txt_save,txt_time;
    String headingtype="",card_id="",card_username="";
    View view_line;
    EditText et_label,et_note,et_location,et_linktitile,et_link;
    TextInputLayout fl_et_label,fl_et_note,fl_et_location,fl_et_linktitile,fl_et_link;
    LinearLayout add_tag_lay;
    CardView cardview_warm,cardview_hot,cardview_connected;
    ImageView backimage;
    String servertime="";
    int select=0;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_otheruserinfoadd_);
        heading=(TextView)findViewById(R.id.heading);
        subheading=(TextView)findViewById(R.id.subheading);
        txt_save=(TextView)findViewById(R.id.txt_save);
        txt_time=(TextView)findViewById(R.id.txt_time);
        view_line=(View)findViewById(R.id.view_line);
        et_label=(EditText)findViewById(R.id.et_label);
        et_note=(EditText)findViewById(R.id.et_note);
        et_location=(EditText)findViewById(R.id.et_location);

        et_linktitile=(EditText)findViewById(R.id.et_linktitile);
        et_link=(EditText)findViewById(R.id.et_link);
        add_tag_lay=(LinearLayout)findViewById(R.id.add_tag_lay);

        fl_et_label=(TextInputLayout)findViewById(R.id.fl_et_label);
        fl_et_note=(TextInputLayout)findViewById(R.id.fl_et_note);
        fl_et_location=(TextInputLayout)findViewById(R.id.fl_et_location);

        cardview_warm=(CardView) findViewById(R.id.cardview_warm);
        cardview_hot=(CardView)findViewById(R.id.cardview_hot);
        cardview_connected=(CardView)findViewById(R.id.cardview_connected);
        backimage=(ImageView)findViewById(R.id.backimage);

        backimage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
        cardview_warm.setOnClickListener(this);
        cardview_hot.setOnClickListener(this);
        cardview_connected.setOnClickListener(this);
        txt_save.setOnClickListener(this);
        txt_time.setOnClickListener(this);

        fl_et_linktitile=(TextInputLayout)findViewById(R.id.fl_et_linktitile);
        fl_et_link=(TextInputLayout)findViewById(R.id.fl_et_link);

        Bundle bundle=getIntent().getExtras();
        if(bundle!=null)
        {
            heading.setText(bundle.getString("add_type"));
            headingtype=bundle.getString("add_type");
            card_id=bundle.getString("card_id");
            card_username=bundle.getString("card_username");

            if(bundle.getString("lead_type")!=null)
            {
               String lead_type=bundle.getString("lead_type");
               Log.e("======",lead_type+" ");
                if(lead_type.equalsIgnoreCase("3"))
                {

                    cardview_connected.setCardBackgroundColor(getResources().getColor(R.color.background_color));
                }
                else if(lead_type.equalsIgnoreCase("2"))
                {

                    cardview_hot.setCardBackgroundColor(getResources().getColor(R.color.background_color));
                }
                else if(lead_type.equalsIgnoreCase("1"))
                {
                    cardview_warm.setCardBackgroundColor(getResources().getColor(R.color.background_color));
                }
               
            }

        }

        if(headingtype.equalsIgnoreCase(getResources().getString(R.string.add_tag)))
        {

            String boldsomepart=getResources().getString(R.string.addtag_subheading);
//            subheading.setText(makeSectionOfTextBold(boldsomepart,"Teresa’s"));
            String s2=boldsomepart.replace("Teresa’s",card_username);
            subheading.setText(makeSectionOfTextBold(s2,card_username));

           // subheading.setText(getResources().getString(R.string.addtag_subheading));
            add_tag_lay.setVisibility(View.VISIBLE);
        }else if(headingtype.equalsIgnoreCase(getResources().getString(R.string.add_label)))
        {
            String boldsomepart=getResources().getString(R.string.addlabel_subheading);
           // subheading.setText(makeSectionOfTextBold(boldsomepart,"Teresa’s"));

            String s2=boldsomepart.replace("Teresa’s",card_username);
            subheading.setText(makeSectionOfTextBold(s2,card_username));

           // subheading.setText(getResources().getString(R.string.addlabel_subheading));
            fl_et_label.setVisibility(View.VISIBLE);
        }
        else if(headingtype.equalsIgnoreCase(getResources().getString(R.string.add_note)))
        {
            String boldsomepart=getResources().getString(R.string.addnotes_subheading);
          //  subheading.setText(makeSectionOfTextBold(boldsomepart,"Teresa’s"));
            String s2=boldsomepart.replace("Teresa’s",card_username);
            subheading.setText(makeSectionOfTextBold(s2,card_username));

         //   subheading.setText(getResources().getString(R.string.addnotes_subheading));
            fl_et_note.setVisibility(View.VISIBLE);
        }
        else if(headingtype.equalsIgnoreCase(getResources().getString(R.string.add_locatiotime)))
        {
            String boldsomepart=getResources().getString(R.string.addlocation_subheading);
           // subheading.setText(makeSectionOfTextBold(boldsomepart,"Teresa’s"));

            String s2=boldsomepart.replace("Teresa’s",card_username);
            subheading.setText(makeSectionOfTextBold(s2,card_username));

           // subheading.setText(getResources().getString(R.string.addlocation_subheading));
            fl_et_location.setVisibility(View.VISIBLE);
            txt_time.setVisibility(View.VISIBLE);
            view_line.setVisibility(View.VISIBLE);
        }
        else if(headingtype.equalsIgnoreCase(getResources().getString(R.string.add_links)))
        {
            String boldsomepart=getResources().getString(R.string.addlinks_subheading);
            String s2=boldsomepart.replace("Teresa’s",card_username);
            subheading.setText(makeSectionOfTextBold(s2,card_username));
          //  subheading.setText(makeSectionOfTextBold(boldsomepart,"Teresa’s"));
           // subheading.setText(getResources().getString(R.string.addlinks_subheading));
            fl_et_linktitile.setVisibility(View.VISIBLE);
            fl_et_link.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId())
        {
            case R.id.cardview_warm:
                select=1;
                changecolor(cardview_warm);
                break;
            case R.id.cardview_hot:
                select=2;
                changecolor(cardview_hot);
                break;
            case R.id.cardview_connected:
                select=3;
                changecolor(cardview_connected);
                break;
            case R.id.txt_save:
                validation();
                break;
            case R.id.txt_time:
                Calendar mcurrentTime = Calendar.getInstance();
                int hour = mcurrentTime.get(Calendar.HOUR_OF_DAY);
                int minute = mcurrentTime.get(Calendar.MINUTE);
                TimePickerDialog mTimePicker;
                mTimePicker = new TimePickerDialog(Otheruserinfoadd_Activity.this, new TimePickerDialog.OnTimeSetListener() {
                    @Override
                    public void onTimeSet(TimePicker timePicker, int selectedHour, int selectedMinute) {
                        servertime=selectedHour + ":" + selectedMinute;
                        Utils utils=new Utils(Otheruserinfoadd_Activity.this);
                       String time= utils.convertTime(selectedHour + ":" + selectedMinute);
                        txt_time.setText( time);
                    }
                }, hour, minute,false);
                mTimePicker.setTitle("Select Time");
                mTimePicker.show();
                break;

        }
    }

    private void changecolor(CardView color) {

        cardview_warm.setCardBackgroundColor(getResources().getColor(R.color.light_grey));
        cardview_hot.setCardBackgroundColor(getResources().getColor(R.color.light_grey));
        cardview_connected.setCardBackgroundColor(getResources().getColor(R.color.light_grey));

        color.setCardBackgroundColor(getResources().getColor(R.color.background_color));
    }

    private void validation() {

        if(headingtype.equalsIgnoreCase(getResources().getString(R.string.add_tag)))
        {
            if(select==0)
            {
                ProgressBarCustom.customDialog(this,getResources().getString(R.string.select_tag),false);
            }else{
                callApiaddtag();
            }
//           selected cardview colors
        }else if(headingtype.equalsIgnoreCase(getResources().getString(R.string.add_label)))
        {

            if(et_label.getText().toString().length()==0)
            {
                ProgressBarCustom.customDialog(this,getResources().getString(R.string.enter_lable),false);
            }else{
                callApiaddlabel();
            }
        }
        else if(headingtype.equalsIgnoreCase(getResources().getString(R.string.add_note)))
        {
            if(et_note.getText().toString().length()==0)
            {
                ProgressBarCustom.customDialog(this,getResources().getString(R.string.enter_note),false);
            }else{
                callApiaddnote();
            }
        }
        else if(headingtype.equalsIgnoreCase(getResources().getString(R.string.add_locatiotime)))
        {
            if(et_location.getText().toString().length()==0)
            {
                ProgressBarCustom.customDialog(this,getResources().getString(R.string.enter_location),false);
            }
            else if(txt_time.getText().toString().length()==0)
            {
                ProgressBarCustom.customDialog(this,getResources().getString(R.string.select_time),false);
            }
            else{
                callApiaddlocationtime();
            }
        }
        else if(headingtype.equalsIgnoreCase(getResources().getString(R.string.add_links)))
        {
            Utils utils=new Utils(this);
            if(et_linktitile.getText().toString().length()==0)
            {
                ProgressBarCustom.customDialog(this,getResources().getString(R.string.enter_link_title),false);
            }
            else if (et_link.getText().length() != 0&&utils.validwebsite(et_link.getText().toString().trim()) == false) {
                ProgressBarCustom.customDialog(this, getResources().getString(R.string.enter_valid_url), false);

            }

            else{
                callApiaddlinks();
            }
        }

    }

    private void callApiaddtag() {
        final ProgressBarCustom progressBarCustom=new ProgressBarCustom(this);
        progressBarCustom.showProgress();
        try {
            String token = UserPrefrence.getInstance(this).getAuthtoken();
            token="bearer " + token;

            RestClient.getInstance(this).get().addtags(token,UserPrefrence.getInstance(this).getUserid(),card_id, select+"",new Callback<AddTag_Modele>() {

                @Override
                public void success(final AddTag_Modele res, Response response) {
                    progressBarCustom.cancelProgress();

                    if(res.getResponseMessage().equalsIgnoreCase(Constant.SUCCESS))
                    {
                        Toast.makeText(Otheruserinfoadd_Activity.this, res.getMessage(), Toast.LENGTH_SHORT).show();
                        Intent intent11=new Intent();
                        setResult(Activity.RESULT_OK,intent11);
                        finish();
                    }else{
                        ProgressBarCustom.customDialog(Otheruserinfoadd_Activity.this,res.getMessage(),false);
                    }

                }

                @Override
                public void failure(final RetrofitError error) {
                    progressBarCustom.cancelProgress();
                    error.printStackTrace();
                    if (error != null) {
                        Utils utils=new Utils(Otheruserinfoadd_Activity.this);
                        utils.showError(error);
                    }
                }
            });

        }catch (Exception e)
        {
            progressBarCustom.cancelProgress();
            e.printStackTrace();
        }
    }


    private void callApiaddlabel() {
        final ProgressBarCustom progressBarCustom=new ProgressBarCustom(this);
        progressBarCustom.showProgress();
        try {
            String token = UserPrefrence.getInstance(this).getAuthtoken();
            token="bearer " + token;
            RestClient.getInstance(this).get().addlable(token,UserPrefrence.getInstance(this).getUserid(),card_id, et_label.getText().toString(),new Callback<AddLable_Modele>() {

                @Override
                public void success(final AddLable_Modele res, Response response) {
                    progressBarCustom.cancelProgress();

                    if(res.getResponseMessage().equalsIgnoreCase(Constant.SUCCESS))
                    {

                        Toast.makeText(Otheruserinfoadd_Activity.this, res.getMessage(), Toast.LENGTH_SHORT).show();
                        Intent intent11=new Intent();
                        setResult(Activity.RESULT_OK,intent11);
                        finish();
                    }else{
                        ProgressBarCustom.customDialog(Otheruserinfoadd_Activity.this,res.getMessage(),false);
                    }

                }

                @Override
                public void failure(final RetrofitError error) {
                    progressBarCustom.cancelProgress();
                    error.printStackTrace();
                    if (error != null) {
                        Utils utils=new Utils(Otheruserinfoadd_Activity.this);
                        utils.showError(error);
                    }
                }
            });

        }catch (Exception e)
        {
            progressBarCustom.cancelProgress();
            e.printStackTrace();
        }
    }

    private void callApiaddnote() {
        final ProgressBarCustom progressBarCustom=new ProgressBarCustom(this);
        progressBarCustom.showProgress();
        try {
            String token = UserPrefrence.getInstance(this).getAuthtoken();
            token="bearer " + token;

            JSONObject jsonObject3 = new JSONObject();
            jsonObject3.put("notes", et_note.getText().toString());
            TypedInput in3 = new TypedByteArray("application/json", jsonObject3.toString().getBytes("UTF-8"));

            RestClient.getInstance(this).get().addnote(token,UserPrefrence.getInstance(this).getUserid(),card_id,in3,new Callback<AddNote_Modele>() {

                @Override
                public void success(final AddNote_Modele res, Response response) {
                    progressBarCustom.cancelProgress();

                    if(res.getResponseMessage().equalsIgnoreCase(Constant.SUCCESS))
                    {
                        Toast.makeText(Otheruserinfoadd_Activity.this, res.getMessage(), Toast.LENGTH_SHORT).show();
                        Intent intent11=new Intent();
                        setResult(Activity.RESULT_OK,intent11);
                        finish();
                    }else{
                        ProgressBarCustom.customDialog(Otheruserinfoadd_Activity.this,res.getMessage(),false);
                    }

                }

                @Override
                public void failure(final RetrofitError error) {
                    progressBarCustom.cancelProgress();
                    error.printStackTrace();
                    if (error != null) {
                        Utils utils=new Utils(Otheruserinfoadd_Activity.this);
                        utils.showError(error);
                    }
                }
            });

        }catch (Exception e)
        {
            progressBarCustom.cancelProgress();
            e.printStackTrace();
        }
    }
    private void callApiaddlocationtime() {
        final ProgressBarCustom progressBarCustom=new ProgressBarCustom(this);
        progressBarCustom.showProgress();
        try {
            String token = UserPrefrence.getInstance(this).getAuthtoken();
            token="bearer " + token;
            String location=et_location.getText().toString();
            String time=servertime;
            RestClient.getInstance(this).get().addlocationtime(token,UserPrefrence.getInstance(this).getUserid(),
                    card_id, location,time,new Callback<AddLocationTime_Modele>() {

                @Override
                public void success(final AddLocationTime_Modele res, Response response) {
                    progressBarCustom.cancelProgress();

                    if(res.getResponseMessage().equalsIgnoreCase(Constant.SUCCESS))
                    {

                        Handler handler = new Handler();
                        handler.post(new Runnable() {
                            @Override
                            public void run() {
                                Toast.makeText(Otheruserinfoadd_Activity.this, res.getMessage(), Toast.LENGTH_SHORT).show();
                                Intent intent11=new Intent();
                                setResult(Activity.RESULT_OK,intent11);
                                finish();
                            }
                        });

                    }else{
                        ProgressBarCustom.customDialog(Otheruserinfoadd_Activity.this,res.getMessage(),false);
                    }

                }

                @Override
                public void failure(final RetrofitError error) {
                    progressBarCustom.cancelProgress();
                    error.printStackTrace();
                    if (error != null) {
                        Utils utils=new Utils(Otheruserinfoadd_Activity.this);
                        utils.showError(error);
                    }
                }
            });

        }catch (Exception e)
        {
            progressBarCustom.cancelProgress();
            e.printStackTrace();
        }
    }
    private void callApiaddlinks() {

        final ProgressBarCustom progressBarCustom=new ProgressBarCustom(this);
        progressBarCustom.showProgress();
        try {
            String token = UserPrefrence.getInstance(this).getAuthtoken();
            token="bearer " + token;

            JSONObject jsonObject3 = new JSONObject();
            jsonObject3.put("title", et_linktitile.getText().toString());
            jsonObject3.put("url", et_link.getText().toString());
            TypedInput in3 = new TypedByteArray("application/json", jsonObject3.toString().getBytes("UTF-8"));

            RestClient.getInstance(this).get().updateAdditional_link(token,UserPrefrence.getInstance(this).getUserid(),card_id,in3,new Callback<AdditionalUpdate_Model>() {

                @Override
                public void success(final AdditionalUpdate_Model res, Response response) {
                    progressBarCustom.cancelProgress();

                    if(res.getResponseMessage().equalsIgnoreCase(Constant.SUCCESS))
                    {
                        Toast.makeText(Otheruserinfoadd_Activity.this, res.getMessage(), Toast.LENGTH_SHORT).show();
                        Intent intent11=new Intent();
                        setResult(Activity.RESULT_OK,intent11);
                        finish();
                    }else{
                        ProgressBarCustom.customDialog(Otheruserinfoadd_Activity.this,res.getMessage(),false);
                    }

                }

                @Override
                public void failure(final RetrofitError error) {
                    progressBarCustom.cancelProgress();
                    error.printStackTrace();
                    if (error != null) {
                        Utils utils=new Utils(Otheruserinfoadd_Activity.this);
                        utils.showError(error);
                    }
                }
            });

        }catch (Exception e)
        {
            progressBarCustom.cancelProgress();
            e.printStackTrace();
        }
    }

    public SpannableStringBuilder makeSectionOfTextBold(String text, String textToBold){

        SpannableStringBuilder builder=new SpannableStringBuilder();

        if(textToBold.length() > 0 && !textToBold.trim().equals("")){

            //for counting start/end indexes
            String testText = text.toLowerCase(Locale.US);
            String testTextToBold = textToBold.toLowerCase(Locale.US);
            int startingIndex = testText.indexOf(testTextToBold);
            int endingIndex = startingIndex + testTextToBold.length();
            //for counting start/end indexes

            if(startingIndex < 0 || endingIndex <0){
                return builder.append(text);
            }
            else if(startingIndex >= 0 && endingIndex >=0){

                builder.append(text);
           //    Typeface type_bold = Typeface.createFromAsset(getAssets(), "font/Montserrat_SemiBold.ttf");
                Typeface type_bold = Typeface.createFromAsset(getAssets(), "font/Montserrat_SemiBold.ttf");
                builder.setSpan (new CustomTypefaceSpan(type_bold), startingIndex, endingIndex, 0);
               // builder.setSpan(new StyleSpan(Typeface.BOLD), startingIndex, endingIndex, 0);
            }
        }else{
            return builder.append(text);
        }

        return builder;
    }
}
