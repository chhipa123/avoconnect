package com.avoconnect.activity;

import android.Manifest;
import android.app.Dialog;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ShortcutInfo;
import android.content.pm.ShortcutManager;
import android.graphics.drawable.Icon;
import android.media.Image;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ToggleButton;

import com.avoconnect.Constants.Constant;
import com.avoconnect.Prefrence.UserPrefrence;
import com.avoconnect.R;
import com.avoconnect.Util.CameraUtils;
import com.avoconnect.Util.GPS_TRACKER;
import com.avoconnect.Util.ProgressBarCustom;
import com.avoconnect.Util.Utils;
import com.avoconnect.api.RestClient;
import com.avoconnect.responsemodel.AskEmail_Model;
import com.avoconnect.responsemodel.AskNotification_Model;
import com.avoconnect.responsemodel.BlockSearchModal;
import com.avoconnect.responsemodel.CheckRequested_Model;
import com.avoconnect.responsemodel.Deleteaccount_Model;
import com.avoconnect.responsemodel.NearByUser_Model;
import com.avoconnect.responsemodel.SendNotificationRequest_Model;
import com.avoconnect.responsemodel.TwoWayAuth_Model;
import com.avoconnect.responsemodel.UpdateMobile_Modele;
import com.google.android.gms.vision.text.Line;
import com.google.api.services.calendar.model.Setting;
import com.karumi.dexter.Dexter;
import com.karumi.dexter.MultiplePermissionsReport;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.DexterError;
import com.karumi.dexter.listener.PermissionDeniedResponse;
import com.karumi.dexter.listener.PermissionGrantedResponse;
import com.karumi.dexter.listener.PermissionRequest;
import com.karumi.dexter.listener.PermissionRequestErrorListener;
import com.karumi.dexter.listener.multi.MultiplePermissionsListener;
import com.karumi.dexter.listener.single.PermissionListener;
import com.twitter.sdk.android.core.models.User;

import org.json.JSONObject;

import java.util.List;

import retrofit.RetrofitError;
import retrofit.client.Response;
import retrofit.mime.TypedByteArray;
import retrofit.mime.TypedInput;

import static java.security.AccessController.getContext;

public class SettingActivity extends AppCompatActivity implements View.OnClickListener {

    LinearLayout changepass_lay,delete_lay,createshortcut_lay;
    ImageView backimage;
    TextView upgrade_txt;
    ToggleButton toggleButton_email,toggleButton_notification,toggleButton_block,toggleButton_auth;

    LinearLayout notification_setting,ask_email,block_lay,auth_lay;

    TextView txt_block,txt_auth,txt_purachase;
    View viewline_delete,viewline_auth,view_line_puchase;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_setting);
        changepass_lay=(LinearLayout)findViewById(R.id.changepass_lay);
        delete_lay=(LinearLayout)findViewById(R.id.delete_lay);
        createshortcut_lay=(LinearLayout)findViewById(R.id.createshortcut_lay);

        block_lay=(LinearLayout)findViewById(R.id.block_lay);
        auth_lay=(LinearLayout)findViewById(R.id.auth_lay);


        txt_block=(TextView) findViewById(R.id.txt_block);
        txt_auth=(TextView) findViewById(R.id.txt_auth);
        txt_purachase=(TextView) findViewById(R.id.txt_purachase);
        viewline_delete=(View)findViewById(R.id.viewline_delete);
        viewline_auth=(View)findViewById(R.id.viewline_auth);
        view_line_puchase=(View)findViewById(R.id.view_line_puchase);

        backimage=(ImageView) findViewById(R.id.backimage);
        upgrade_txt=(TextView) findViewById(R.id.upgrade_txt);

        toggleButton_email=(ToggleButton)findViewById(R.id.toggleButton_email);
        toggleButton_notification=(ToggleButton)findViewById(R.id.toggleButton_notification);
        toggleButton_block=(ToggleButton)findViewById(R.id.toggleButton_block);
        toggleButton_auth=(ToggleButton)findViewById(R.id.toggleButton_auth);

        notification_setting=(LinearLayout)findViewById(R.id.notification_setting);
        ask_email=(LinearLayout)findViewById(R.id.ask_email);


        if(!UserPrefrence.getInstance(this).getUsertype().equalsIgnoreCase(Constant.USERTYPE_FREE))
        {
            upgrade_txt.setVisibility(View.GONE);
            txt_purachase.setVisibility(View.GONE);
            view_line_puchase.setVisibility(View.GONE);
        }


        upgrade_txt.setOnClickListener(this);
        delete_lay.setOnClickListener(this);
        changepass_lay.setOnClickListener(this);
        createshortcut_lay.setOnClickListener(this);
        notification_setting.setOnClickListener(this);
        ask_email.setOnClickListener(this);
        backimage.setOnClickListener(this);


        if(UserPrefrence.getInstance(this).getIsAskemail().equalsIgnoreCase("true"))
        {
            toggleButton_email.setChecked(true);
        }
        else if(UserPrefrence.getInstance(this).getIsAskemail().equalsIgnoreCase("false"))
        {
            toggleButton_email.setChecked(false);
        }

        if(UserPrefrence.getInstance(this).getIsNotificationOn().equalsIgnoreCase("true"))
        {
            toggleButton_notification.setChecked(true);
        }
        else if(UserPrefrence.getInstance(this).getIsNotificationOn().equalsIgnoreCase("false"))
        {
            toggleButton_notification.setChecked(false);
        }

        if(UserPrefrence.getInstance(this).getIsBlock().equalsIgnoreCase("true"))
        {
            toggleButton_block.setChecked(true);
        }
        else if(UserPrefrence.getInstance(this).getIsBlock().equalsIgnoreCase("false"))
        {
            toggleButton_block.setChecked(false);
        }

        if(UserPrefrence.getInstance(this).getIsAuthenticaltion().equalsIgnoreCase("true"))
        {
            toggleButton_auth.setChecked(true);
        }
        else if(UserPrefrence.getInstance(this).getIsAuthenticaltion().equalsIgnoreCase("false"))
        {
            toggleButton_auth.setChecked(false);
        }



        toggleButton_email.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if(b==true)
                {
                    callAskEmail(1,b);
                }else{
                    callAskEmail(0,b);
                }
            }
        });

        toggleButton_notification.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if(b==true)
                {
                    callApiAskNotification(1,b);
                }else{
                    callApiAskNotification(0,b);
                }
            }
        });

        toggleButton_block.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {

                if(UserPrefrence.getInstance(SettingActivity.this).getUsertype().equalsIgnoreCase(Constant.USERTYPE_PREMIUM)
                        ||UserPrefrence.getInstance(SettingActivity.this).getUsertype().equalsIgnoreCase(Constant.USERTYPE_CORPORATE)
                ){

                   callApiBloackUser(b);
                    //   openfilter(view);
                }else{
                    toggleButton_block.setChecked(false);
                    Utils.customYesDialog(SettingActivity.this,SettingActivity.this);
                }
            }
        });

        toggleButton_auth.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if(UserPrefrence.getInstance(SettingActivity.this).getUsertype().equalsIgnoreCase(Constant.USERTYPE_PREMIUM)
                        ||UserPrefrence.getInstance(SettingActivity.this).getUsertype().equalsIgnoreCase(Constant.USERTYPE_CORPORATE)
                ){

                    if(UserPrefrence.getInstance(SettingActivity.this).getMobile().length()==0)
                    {
                        toggleButton_auth.setChecked(false);
                        updateMobileNumber(b);
                    }else{
                        callApiAuth(b);
                    }
                    //   openfilter(view);
                }else{
                    toggleButton_auth.setChecked(false);
                    Utils.customYesDialog(SettingActivity.this,SettingActivity.this);
                }
            }
        });
    }

    private void updateMobileNumber(final boolean b) {

            AlertDialog.Builder builder=new AlertDialog.Builder(SettingActivity.this);
            View view= LayoutInflater.from(this).inflate(R.layout.asknumber,null);
           TextView txt_save=(TextView)view.findViewById(R.id.txt_save);
            final EditText et_mobile=(EditText)view.findViewById(R.id.et_mobile);
            ImageView cut_img=(ImageView)view.findViewById(R.id.cut_img);
            builder.setView(view);
            final Dialog dialog=builder.create();
            dialog.setCancelable(false);
            dialog.setCanceledOnTouchOutside(false);

             txt_save.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if(et_mobile.getText().toString().length()==0)
                    {
                        ProgressBarCustom.customDialog(SettingActivity.this,getResources().getString(R.string.enter_mobile_number),false);
                    }
                    else if(et_mobile.getText().toString().length()<10)
                    {
                        ProgressBarCustom.customDialog(SettingActivity.this,getResources().getString(R.string.invalid_mobile),false);
                    }
                    else{
                        callApiUpdatemobileNumber(et_mobile.getText().toString(),b);
                        dialog.dismiss();
                    }


                   // callApiLgout();

                }
            });
        cut_img.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });
            dialog.show();

    }

    private void callApiUpdatemobileNumber(final String mobile, final boolean b) {
        final ProgressBarCustom progressBarCustom=new ProgressBarCustom(SettingActivity.this);
        progressBarCustom.showProgress();
        try {
            String token = UserPrefrence.getInstance(this).getAuthtoken();
            token="bearer " + token;
            JSONObject jsonObject3 = new JSONObject();

            TypedInput in3 = new TypedByteArray("application/json", jsonObject3.toString().getBytes("UTF-8"));

            RestClient.getInstance(this).get().updatemobilenumber( token,UserPrefrence.getInstance(this).getUserid(),
                    mobile+"",in3,
                    new retrofit.Callback<UpdateMobile_Modele>() {

                        @Override
                        public void success(final UpdateMobile_Modele res, Response response) {
                            progressBarCustom.cancelProgress();

                            if(res.getResponseMessage().equalsIgnoreCase(Constant.SUCCESS))
                            {
                                UserPrefrence.getInstance(SettingActivity.this).setMobile(mobile);
                                toggleButton_auth.setChecked(b);
                                callApiBloackUser(b);
                            }else{
                                Toast.makeText(SettingActivity.this, res.getMessage(), Toast.LENGTH_SHORT).show();
                            }

                        }
                        @Override
                        public void failure(final RetrofitError error) {
                            progressBarCustom.cancelProgress();
                            error.printStackTrace();
                            if (error != null) {
                                Utils utils=new Utils(SettingActivity.this);
                                utils.showError(error);
                            }

                        }
                    });
        }catch (Exception e)
        {
            progressBarCustom.cancelProgress();
            //progress.cancelProgress();
            e.printStackTrace();
        }
    }

    private void callApiAuth(final boolean b) {

        final ProgressBarCustom progressBarCustom=new ProgressBarCustom(SettingActivity.this);
        progressBarCustom.showProgress();
        try {
            String status="";
            if(b==true)
            {
                status="1";
            }else{
                status="0";
            }
            String token = UserPrefrence.getInstance(SettingActivity.this).getAuthtoken();
            token="bearer " + token;
            JSONObject jsonObject3 = new JSONObject();

            TypedInput in3 = new TypedByteArray("application/json", jsonObject3.toString().getBytes("UTF-8"));
            RestClient.getInstance(this).get().twowayauth( token,UserPrefrence.getInstance(this).getUserid(),status,in3,new retrofit.Callback<TwoWayAuth_Model>() {

                @Override
                public void success(final TwoWayAuth_Model res, Response response) {
                    progressBarCustom.cancelProgress();

                    if(res.getResponseMessage().equalsIgnoreCase(Constant.SUCCESS))
                    {

                        Handler handler = new Handler();
                        handler.post(new Runnable() {
                            @Override
                            public void run() {
                                UserPrefrence.getInstance(SettingActivity.this).setIsAuthenticaltion(b+"");
                                toggleButton_auth.setChecked(b);
                            }
                        });
                    }else{
                        toggleButton_auth.setChecked(false);
                        //ProgressBarCustom.customDialog(SettingActivity.this,res.getMessage(),false);
                    }

                }
                @Override
                public void failure(final RetrofitError error) {
                    progressBarCustom.cancelProgress();
                    error.printStackTrace();
                    if (error != null) {
                        Utils utils=new Utils(SettingActivity.this);
                        utils.showError(error);
                    }

                }
            });


        }catch (Exception e)
        {
            progressBarCustom.cancelProgress();
            //progress.cancelProgress();
            e.printStackTrace();
        }

    }

    private void callApiBloackUser(final boolean b) {
        final ProgressBarCustom progressBarCustom=new ProgressBarCustom(SettingActivity.this);
        progressBarCustom.showProgress();
        try {
            String token = UserPrefrence.getInstance(SettingActivity.this).getAuthtoken();
            token="bearer " + token;
            RestClient.getInstance(SettingActivity.this).get().blocksearchApo( token,UserPrefrence.getInstance(SettingActivity.this).getUserid()+"",
                    b,
                    new retrofit.Callback<BlockSearchModal>() {

                        @Override
                        public void success(final BlockSearchModal res, Response response) {
                            progressBarCustom.cancelProgress();

                            if(res.getResponseMessage().equalsIgnoreCase(Constant.SUCCESS))
                            {
                                String types=b+"";
                                UserPrefrence.getInstance(SettingActivity.this).setIsBlock(types);
                                Toast.makeText(SettingActivity.this, res.getMessage(), Toast.LENGTH_SHORT).show();
                            }else{
                                Toast.makeText(SettingActivity.this, res.getMessage(), Toast.LENGTH_SHORT).show();
                            }

                        }
                        @Override
                        public void failure(final RetrofitError error) {
                            progressBarCustom.cancelProgress();
                            error.printStackTrace();
                            if (error != null) {
                                Utils utils=new Utils(SettingActivity.this);
                                utils.showError(error);
                            }

                        }
                    });
        }catch (Exception e)
        {
            progressBarCustom.cancelProgress();
            //progress.cancelProgress();
            e.printStackTrace();
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId())
        {
            case R.id.ask_email:
//                if(toggleButton_email.isChecked()==true)
//                {
//                    callAskEmail(1);
//                }else{
//                    callAskEmail(0);
//                }


                break;

            case R.id.notification_setting:

//                if(toggleButton_email.isChecked()==true)
//                {
//                    callApiAskNotification(1);
//                }else{
//                    callApiAskNotification(0);
//                }


                break;
            case R.id.delete_lay:

                generatepop();

                break;

            case R.id.upgrade_txt:
                Intent intent11=new Intent(SettingActivity.this,PlanUserSubcription_Act.class);
                startActivityForResult(intent11,155);
                break;
            case R.id.changepass_lay:
                Intent intent=new Intent(SettingActivity.this,ChangePassword_Actitivty.class);
                startActivity(intent);
                break;

            case R.id.createshortcut_lay:
                addShortcut(SettingActivity.this);
                break;
            case R.id.backimage:
                finish();
                break;
            case R.id.txt_upgrade:
                if(Utils.yesNoDialog!=null&&Utils.yesNoDialog.isShowing()==true)
                {
                    Intent intent10=new Intent(SettingActivity.this, PlanUserSubcription_Act.class);
                    startActivityForResult(intent10,155);
                    Utils.yesNoDialog.dismiss();
                }
                break;
        }

    }

    private void callApiAskNotification(int status,final boolean type) {
        final ProgressBarCustom progressBarCustom=new ProgressBarCustom(SettingActivity.this);
        progressBarCustom.showProgress();
        try {
            String token = UserPrefrence.getInstance(SettingActivity.this).getAuthtoken();
            token="bearer " + token;
            RestClient.getInstance(SettingActivity.this).get().asknotification( token,UserPrefrence.getInstance(SettingActivity.this).getUserid()+"",
                    status,
                    new retrofit.Callback<AskNotification_Model>() {

                        @Override
                        public void success(final AskNotification_Model res, Response response) {
                            progressBarCustom.cancelProgress();

                            if(res.getResponseMessage().equalsIgnoreCase(Constant.SUCCESS))
                            {
                                String types=type+"";
                                UserPrefrence.getInstance(SettingActivity.this).setIsNotificationOn(types);
                                Toast.makeText(SettingActivity.this, res.getMessage(), Toast.LENGTH_SHORT).show();
                            }else{
                                Toast.makeText(SettingActivity.this, res.getMessage(), Toast.LENGTH_SHORT).show();
                            }

                        }
                        @Override
                        public void failure(final RetrofitError error) {
                            progressBarCustom.cancelProgress();
                            error.printStackTrace();
                            if (error != null) {
                                Utils utils=new Utils(SettingActivity.this);
                                utils.showError(error);
                            }

                        }
                    });
        }catch (Exception e)
        {
            progressBarCustom.cancelProgress();
            //progress.cancelProgress();
            e.printStackTrace();
        }
    }

    private void callAskEmail(int status, final boolean type) {
        final ProgressBarCustom progressBarCustom=new ProgressBarCustom(SettingActivity.this);
        progressBarCustom.showProgress();
        try {
            String token = UserPrefrence.getInstance(SettingActivity.this).getAuthtoken();
            token="bearer " + token;
            RestClient.getInstance(SettingActivity.this).get().askEmailToViewCardOnBrowser( token,UserPrefrence.getInstance(SettingActivity.this).getUserid()+"",
                    status,
                    new retrofit.Callback<AskEmail_Model>() {

                        @Override
                        public void success(final AskEmail_Model res, Response response) {
                            progressBarCustom.cancelProgress();

                            if(res.getResponseMessage().equalsIgnoreCase(Constant.SUCCESS))
                            {
                                String types=type+"";
                                UserPrefrence.getInstance(SettingActivity.this).setIsAskemail(types);
                                Toast.makeText(SettingActivity.this, res.getMessage(), Toast.LENGTH_SHORT).show();
                            }else{
                                Toast.makeText(SettingActivity.this, res.getMessage(), Toast.LENGTH_SHORT).show();
                            }

                        }
                        @Override
                        public void failure(final RetrofitError error) {
                            progressBarCustom.cancelProgress();
                            error.printStackTrace();
                            if (error != null) {
                                Utils utils=new Utils(SettingActivity.this);
                                utils.showError(error);
                            }

                        }
                    });
        }catch (Exception e)
        {
            progressBarCustom.cancelProgress();
            //progress.cancelProgress();
            e.printStackTrace();
        }
    }

    private void generatepop() {
        AlertDialog.Builder builder=new AlertDialog.Builder(SettingActivity.this);
        View view= LayoutInflater.from(this).inflate(R.layout.logout_lay,null);
        TextView txtDialogMessage=(TextView)view.findViewById(R.id.txtDialogMessage);
        txtDialogMessage.setText(getResources().getString(R.string.delete_account));
        Button btnDialogYes=(Button)view.findViewById(R.id.btnDialogYes);
        Button btnDialogNo=(Button)view.findViewById(R.id.btnDialogNo);
        builder.setView(view);
        final Dialog dialog=builder.create();
        dialog.setCancelable(false);
        dialog.setCanceledOnTouchOutside(false);

        btnDialogYes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
                callApiAccountDelete();

            }
        });
        btnDialogNo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });
        dialog.show();

    }

    private void addShortcut(final Context context) {


        Dexter.withActivity(this)
                .withPermission(Manifest.permission.INSTALL_SHORTCUT)
                .withListener(new PermissionListener() {
                    @Override public void onPermissionGranted(PermissionGrantedResponse response) {
                        ShortcutManager shortcutManager = null;
                        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N_MR1) {
                            shortcutManager = context.getSystemService(ShortcutManager.class);
                        }
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                            if (shortcutManager != null) {
                                if (shortcutManager.isRequestPinShortcutSupported()) {
                                    ShortcutInfo shortcut = new ShortcutInfo.Builder(context, "112")
                                            .setShortLabel("Avoconnect")
                                            .setLongLabel("Open the Android Document")
                                            .setIcon(Icon.createWithResource(context, R.drawable.nav_appinfo))
                                            .setIntent(new Intent(Intent.ACTION_VIEW).setClass(context, ShortcutActivityHanlder.class))
                                            .build();
                                    shortcutManager.requestPinShortcut(shortcut, null);


                                } else
                                    Toast.makeText(context, "Pinned shortcuts are not supported!", Toast.LENGTH_SHORT).show();
                            }
                        }

                    }
                    @Override public void onPermissionDenied(PermissionDeniedResponse response) {

                    }
                    @Override public void onPermissionRationaleShouldBeShown(PermissionRequest permission, PermissionToken token) {

                    }
                }).check();



    }

    private void callApiAccountDelete() {
        final ProgressBarCustom progressBarCustom = new ProgressBarCustom(SettingActivity.this);
        progressBarCustom.showProgress();
        try {
            String token = UserPrefrence.getInstance(SettingActivity.this).getAuthtoken();
            token = "bearer " + token;
            RestClient.getInstance(SettingActivity.this).get().deleteaccount(token, UserPrefrence.getInstance(SettingActivity.this).getUserid(), new retrofit.Callback<Deleteaccount_Model>() {

                @Override
                public void success(final Deleteaccount_Model res, Response response) {
                    progressBarCustom.cancelProgress();

                        if (res.getResponseMessage().equalsIgnoreCase(Constant.SUCCESS)) {
                            UserPrefrence.getInstance(SettingActivity.this).logoutDone();
                            Intent intent=new Intent(SettingActivity.this,Login_Activity.class);
                            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                            startActivity(intent);
                            finish();
                        } else {
                            Toast.makeText(SettingActivity.this, res.getMessage(), Toast.LENGTH_SHORT).show();
                        }

                }

                @Override
                public void failure(final RetrofitError error) {
                    progressBarCustom.cancelProgress();
                    error.printStackTrace();
                    if (error != null) {
                        Utils utils = new Utils(SettingActivity.this);
                        utils.showError(error);
                    }
                }
            });

        } catch (Exception e) {
            progressBarCustom.cancelProgress();
            e.printStackTrace();
        }

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        if(requestCode==155&&resultCode==RESULT_OK)
        {
            upgrade_txt.setVisibility(View.GONE);
            txt_purachase.setVisibility(View.GONE);
            view_line_puchase.setVisibility(View.GONE);
        }
    }
}
