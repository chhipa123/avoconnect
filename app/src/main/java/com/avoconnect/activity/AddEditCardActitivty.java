package com.avoconnect.activity;

import android.Manifest;
import android.app.Dialog;
import android.app.TimePickerDialog;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Point;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Environment;
import android.os.Handler;
import android.os.Looper;
import android.os.StrictMode;
import android.provider.MediaStore;
import androidx.annotation.Nullable;

import com.amazonaws.ClientConfiguration;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferListener;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferObserver;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferState;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferUtility;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.s3.AmazonS3Client;
import com.amazonaws.services.s3.model.CannedAccessControlList;
import com.avoconnect.BuildConfig;
import com.avoconnect.Util.CustomImageVIew;
import com.avoconnect.Util.DialogService;
import com.avoconnect.Util.GPS_TRACKER;
import com.avoconnect.testingcard.TextActivity;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.github.chrisbanes.photoview.PhotoView;
import com.google.android.material.bottomsheet.BottomSheetBehavior;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import androidx.cardview.widget.CardView;
import androidx.core.content.FileProvider;

import android.text.Editable;
import android.text.InputFilter;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.avoconnect.Constants.Constant;
import com.avoconnect.Fragments.BottomSheetFragment;
import com.avoconnect.Prefrence.UserPrefrence;
import com.avoconnect.R;
import com.avoconnect.Util.CameraUtils;
import com.avoconnect.Util.FileUtilAbove19;
import com.avoconnect.Util.NonScrollListView;
import com.avoconnect.Util.ProgressBarCustom;
import com.avoconnect.Util.RealPathUtil;
import com.avoconnect.Util.Utils;
import com.avoconnect.adapter.AdditionalLink_Adapter;
import com.avoconnect.adapter.ImportantLink_Adpater;
import com.avoconnect.adapter.MediaListAdapter;
import com.avoconnect.adapter.SocialLinkAdapter;
import com.avoconnect.api.RestClient;
import com.avoconnect.beans.AdditionalLinkModel;
import com.avoconnect.beans.Important_Model;
import com.avoconnect.beans.Media_Model;
import com.avoconnect.beans.Social_Model;
import com.avoconnect.responsemodel.AddManualCard_model;
import com.avoconnect.responsemodel.CardDetails_Model;
import com.avoconnect.responsemodel.Createcard_Model;
import com.avoconnect.responsemodel.RecivedCard_model;
import com.avoconnect.responsemodel.UpdateAddtionalInfo_Model;
import com.avoconnect.responsemodel.UpdateCard_Model;
import com.karumi.dexter.Dexter;
import com.karumi.dexter.MultiplePermissionsReport;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.DexterError;
import com.karumi.dexter.listener.PermissionRequest;
import com.karumi.dexter.listener.PermissionRequestErrorListener;
import com.karumi.dexter.listener.multi.MultiplePermissionsListener;
import com.squareup.picasso.Picasso;
import com.twitter.sdk.android.core.models.Image;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URL;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.concurrent.TimeUnit;

import de.hdodenhof.circleimageview.CircleImageView;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;
import retrofit.mime.TypedByteArray;
import retrofit.mime.TypedInput;

public class AddEditCardActitivty extends AppCompatActivity implements View.OnClickListener{


    EditText et_fullname,et_desingnation,et_mobile,et_email,et_company,et_website,et_summmary,et_address;
    Utils utils;
    NonScrollListView lsitview_media,listview_addtional;

    private BottomSheetBehavior mBottomSheetBehavior;
    RelativeLayout theme_selection;
    NonScrollListView listview,listview_social;
    LinearLayout tab_media,tab_socialmedia,tab_implink,tab_additionallink;
    ImageView background_image;
    CircleImageView image;
    List<Media_Model>media_list=new ArrayList<>();
    List<Social_Model>social_list=new ArrayList<>();
    List<Important_Model>important_list=new ArrayList<>();
    List<AdditionalLinkModel>additional_list=new ArrayList<>();
    SocialLinkAdapter socialLinkAdapter;
    ImportantLink_Adpater importantLink_adpater;
    MediaListAdapter mediaAdpater;
    AdditionalLink_Adapter additionalLink_adapter;
    TextView txt_save;
    String backgroundcolor="#ffffff";
    ImageView backimage;
    String card_id="";
    boolean isPrimary=false;
    CheckBox primary_checkbox;

    private static final int CAMERA_REQUEST = 0;
    private static final int GALLERY_PICTURE = 1;
    public static final int MEDIA_TYPE_IMAGE = 1;
    private static final int BITMAP_SAMPLE_SIZE = 8;
    Context mcontext;
    String profilePath="";
    Bitmap profileBitmap=null;
    ImageView card_view_image;
    String clickfrom;
    public static final String GALLERY_DIRECTORY_NAME = "AvoCard";
    public static final String IMAGE_EXTENSION = "jpg";
    private final int RESULT_CROP = 400;
    LinearLayout addtionaladd_lay;
    boolean isaddiotnalinfo=false;
    CardView cardview_warm,cardview_hot,cardview_connected;
    EditText et_label,et_note,et_location;
    TextView txt_time;
    int select=0;
    String servertime="";
    String manual="";
    boolean isafter_api_call=false;

    AmazonS3Client s3;
    String cropfilepath="";
    BasicAWSCredentials credentials;
    TransferUtility transferUtility;
    TransferObserver observer;

    String upload_user_card="",upload_banner_card="";
    TextView txt_user_placeholder;
    RelativeLayout profile_Lay;
    ImageView view_img;
    String ocr_image="";
    boolean isfrom_ocr=false;
    Bitmap myBitmap;
    String logo_delete="",banner_delete="";
     CharSequence[] options;
     String mycard="";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        StrictMode.VmPolicy.Builder builder = new StrictMode.VmPolicy.Builder();
        StrictMode.setVmPolicy(builder.build());
        setContentView(R.layout.activity_add_edit_card_actitivty);
        mcontext=AddEditCardActitivty.this;


        credentials = new BasicAWSCredentials(Constant.accessKey,Constant.secretKey);
        ClientConfiguration cc = new ClientConfiguration();
        cc.setSocketTimeout(120000);
        s3 = new AmazonS3Client(credentials,cc);
       // s3.setRegion(com.amazonaws.regions.Region.getRegion(Regions.AP_SOUTH_1));
        transferUtility = new TransferUtility(s3, this);
        inits();
//  ocrImageOfManualCard
    }

    private void inits() {
        utils = new Utils(this);
        txt_save = (TextView) findViewById(R.id.txt_save);

        cardview_warm = (CardView) findViewById(R.id.cardview_warm);
        cardview_hot = (CardView) findViewById(R.id.cardview_hot);
        cardview_connected = (CardView) findViewById(R.id.cardview_connected);
        profile_Lay = (RelativeLayout) findViewById(R.id.profile_Lay);
        profile_Lay.setOnClickListener(this);
        cardview_warm.setOnClickListener(this);
        cardview_hot.setOnClickListener(this);
        cardview_connected.setOnClickListener(this);
        txt_user_placeholder = (TextView) findViewById(R.id.txt_user_placeholder);

        et_fullname = (EditText) findViewById(R.id.et_fullname);
        et_address = (EditText) findViewById(R.id.et_address);
        et_desingnation = (EditText) findViewById(R.id.et_desingnation);
        et_mobile = (EditText) findViewById(R.id.et_mobile);
        et_email = (EditText) findViewById(R.id.et_email);
        et_company = (EditText) findViewById(R.id.et_company);
        et_website = (EditText) findViewById(R.id.et_website);
        et_summmary = (EditText) findViewById(R.id.et_summmary);
        theme_selection = (RelativeLayout) findViewById(R.id.theme_selection);
        listview = (NonScrollListView) findViewById(R.id.listview);
        listview_social = (NonScrollListView) findViewById(R.id.listview_social);
        background_image = (ImageView) findViewById(R.id.background_image);
        primary_checkbox = (CheckBox) findViewById(R.id.primary_checkbox);
        addtionaladd_lay = (LinearLayout) findViewById(R.id.addtionaladd_lay);
        tab_additionallink = (LinearLayout) findViewById(R.id.tab_additionallink);
        backimage = (ImageView) findViewById(R.id.backimage);
        listview_addtional = (NonScrollListView) findViewById(R.id.listview_addtional);
        tab_media = (LinearLayout) findViewById(R.id.tab_media);
        tab_socialmedia = (LinearLayout) findViewById(R.id.tab_socialmedia);
        tab_implink = (LinearLayout) findViewById(R.id.tab_implink);
        backimage.setOnClickListener(this);
        card_view_image = (ImageView) findViewById(R.id.card_view_image);
        card_view_image.setTag("remove");
        view_img = (ImageView) findViewById(R.id.view_img);
        view_img.setVisibility(View.GONE);
        image = (CircleImageView) findViewById(R.id.image_profile);
        image.setTag("logo");
        et_label = (EditText) findViewById(R.id.et_label);
        et_note = (EditText) findViewById(R.id.et_note);
        et_location = (EditText) findViewById(R.id.et_location);
        txt_time = (TextView) findViewById(R.id.txt_time);

        txt_time.setOnClickListener(this);
        tab_additionallink.setOnClickListener(this);
        setCapitalizeTextWatcher(et_fullname);
        setCapitalizeTextWatcher(et_company);
        setCapitalizeTextWatcher(et_desingnation);
        setCapitalizeTextWatcher(et_summmary);
        LinearLayout bottomSheet = (LinearLayout) findViewById(R.id.bottom_sheet);
        mBottomSheetBehavior = BottomSheetBehavior.from(bottomSheet);
        mBottomSheetBehavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
        mBottomSheetBehavior.setPeekHeight(0);
        theme_selection.setOnClickListener(this);
//        image_background.setOnClickListener(this);
//        image_logo.setOnClickListener(this);

        tab_media.setOnClickListener(this);
        tab_socialmedia.setOnClickListener(this);
        tab_implink.setOnClickListener(this);
        txt_save.setOnClickListener(this);

        background_image.setOnClickListener(this);
        et_label.setOnClickListener(this);
        // image.setOnClickListener(this);

        lsitview_media = (NonScrollListView) findViewById(R.id.recylerview);

        try {
            Bundle bundle = getIntent().getExtras();
            if (bundle != null) {
                if (bundle.getString("type") != null) {
                    txt_save.setText("UPDATE");
                    card_id = bundle.getString("card_id");

                    if (bundle.getString("additional_info") != null) {
                        isafter_api_call = true;
                        isaddiotnalinfo = true;
                        addtionaladd_lay.setVisibility(View.VISIBLE);
                        theme_selection.setVisibility(View.GONE);
                        disbale_update();
                    }


                    if (isaddiotnalinfo == true) {
                        primary_checkbox.setVisibility(View.GONE);
                        callApiGetDetailsAdditional();
                    } else {

                        callApigetDetails();
                    }

                }



                if (bundle.getString("manual") != null) {
                    isafter_api_call = true;
                    manual = bundle.getString("manual");
                    primary_checkbox.setVisibility(View.GONE);
                }



                // condition when open from ocr code
                if (bundle.getString("ocrResult") != null) {
                    isfrom_ocr=true;

                    manual = "yes";
                    primary_checkbox.setVisibility(View.GONE);
                    if (bundle.getString("fullName") != null) {
                        et_fullname.setFilters(new InputFilter[] { new InputFilter.LengthFilter(500) });
                        et_fullname.setText(bundle.getString("fullName"));

                    }
                    if (bundle.getString("position") != null) {
                        et_desingnation.setFilters(new InputFilter[] { new InputFilter.LengthFilter(500) });
                        et_desingnation.setText(bundle.getString("position"));

                    }
                    if (bundle.getString("mobileNumber") != null) {
                        et_mobile.setFilters(new InputFilter[] { new InputFilter.LengthFilter(500) });
                        et_mobile.setText(bundle.getString("mobileNumber"));

                    }
                    if (bundle.getString("emailAddress") != null) {
                        et_email.setFilters(new InputFilter[] { new InputFilter.LengthFilter(500) });
                        et_email.setText(bundle.getString("emailAddress"));

                    }
                    if (bundle.getString("companyName") != null) {
                        et_company.setFilters(new InputFilter[] { new InputFilter.LengthFilter(500) });
                        et_company.setText(bundle.getString("companyName"));

                    }
                    if (bundle.getString("website") != null) {
                        et_website.setFilters(new InputFilter[] { new InputFilter.LengthFilter(500) });
                        et_website.setText(bundle.getString("website"));

                    }
                    if (bundle.getString("address") != null) {
                        et_address.setFilters(new InputFilter[] { new InputFilter.LengthFilter(1000) });
                        et_address.setText(bundle.getString("address"));

                    }


                    if (TextActivity.profileBitmap != null) {
                        view_img.setVisibility(View.VISIBLE);
                        //  card_view_image.setImageBitmap(ScanBusinessCardActivity.profileBitmap);


                        view_img.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {

                                Intent intent=new Intent(AddEditCardActitivty.this,ImagePopActivity.class);
                                intent.putExtra("local_url",TextActivity.cropfilepath);
                                startActivity(intent);

                               // imagepop(TextActivity.profileBitmap);

                            }
                        });
                    }

                    if (TextActivity.profileBitmap != null) {
                        callS3Bucket_Upload(TextActivity.cropfilepath, "ocr_image");
                    }

//                if(getIntent().getByteArrayExtra("profileBitmap")!=null)
//                {
//                    background_image=null;
//                    background_image.setImageDrawable(null);
//                    byte[] byteArray = getIntent().getByteArrayExtra("profileBitmap");
//                   Bitmap bmp = BitmapFactory.decodeByteArray(byteArray, 0, byteArray.length);
//                  background_image.setImageBitmap(bmp);
//                }
                }

                if(bundle.getString("mycard")!=null)
                {
                    mycard=bundle.getString("mycard");
                    primary_checkbox.setVisibility(View.VISIBLE);
                }

                if (bundle.getString("myfirstcard") != null) {

                    primary_checkbox.setChecked(true);
                    primary_checkbox.setClickable(false);
                    primary_checkbox.setVisibility(View.VISIBLE);

                }

            }

        }catch (Exception e)
        {
            e.printStackTrace();

    }

        setAdaterLinks();

        primary_checkbox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if(b==true) {
                    if(isafter_api_call==false) {
                        opendialog();
                    }
                }
            }
        });

    }

    private void imagepop(Bitmap profileBitmap) {
        android.app.AlertDialog.Builder dialog = new android.app.AlertDialog.Builder(this);
        Dialog settingsDialog=dialog.create();
        settingsDialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        int width = (int)(getResources().getDisplayMetrics().widthPixels*0.90);
        int height = (int)(getResources().getDisplayMetrics().heightPixels*0.90);
        settingsDialog.getWindow().setLayout(width, height);

        View view= LayoutInflater.from(this).inflate(R.layout.image_layout,null);
        PhotoView image_view=(PhotoView)view.findViewById(R.id.image_view);

        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        profileBitmap.compress(Bitmap.CompressFormat.PNG, 100, stream);

//        DisplayMetrics metrics = new DisplayMetrics();
//        getWindowManager().getDefaultDisplay().getMetrics(metrics);
//        int usableHeight = metrics.widthPixels;
//        getWindowManager().getDefaultDisplay().getRealMetrics(metrics);
//        int realHeight = metrics.heightPixels;

        int Measuredwidth = 0;
        int Measuredheight = 0;
        Point size = new Point();
        WindowManager w = getWindowManager();

        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB)    {
            w.getDefaultDisplay().getSize(size);
            Measuredwidth = size.x;
            Measuredheight = size.y;
        }else{
            Display d = w.getDefaultDisplay();
            Measuredwidth = d.getWidth();
            Measuredheight = d.getHeight();
        }


        Glide.with(this)
                .asBitmap()
                .load(stream.toByteArray())
                .apply(new RequestOptions().override(Measuredwidth, Measuredheight))
                .into(image_view);


        //     image_view.setImageBitmap(myBitmap);
        dialog.setView(view);
        dialog.show();
    }



    private void opendialog() {
            AlertDialog.Builder builder=new AlertDialog.Builder(AddEditCardActitivty.this);
            View view= LayoutInflater.from(this).inflate(R.layout.logout_lay,null);
            TextView txtDialogMessage=(TextView)view.findViewById(R.id.txtDialogMessage);
             txtDialogMessage.setText(getResources().getString(R.string.primary_premission));
            Button btnDialogYes=(Button)view.findViewById(R.id.btnDialogYes);
            Button btnDialogNo=(Button)view.findViewById(R.id.btnDialogNo);
            builder.setView(view);
            final Dialog dialog=builder.create();
            dialog.setCancelable(false);
            dialog.setCanceledOnTouchOutside(false);

            btnDialogYes.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    dialog.dismiss();
                    primary_checkbox.setChecked(true);
                }
            });
            btnDialogNo.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    dialog.dismiss();
                    primary_checkbox.setChecked(false);
                }
            });
            dialog.show();


    }

    private void disbale_update() {
        et_fullname.setEnabled(false);
        et_desingnation.setEnabled(false);
        et_email.setEnabled(false);
        et_address.setEnabled(false);
        et_company.setEnabled(false);
        et_website.setEnabled(false);
        et_summmary.setEnabled(false);
        et_mobile.setEnabled(false);
        primary_checkbox.setClickable(false);
        primary_checkbox.setEnabled(false);
        background_image.setClickable(false);
        background_image.setVisibility(View.GONE);
        image.setClickable(false);
        theme_selection.setClickable(false);
        tab_implink.setClickable(false);
        tab_media.setClickable(false);
        tab_socialmedia.setClickable(false);
        profile_Lay.setClickable(false);

    }

    /*When user MY CARD DEATILS*/
    private void callApigetDetails() {
        final ProgressBarCustom progressBarCustom=new ProgressBarCustom(AddEditCardActitivty.this);
        progressBarCustom.showProgress();
        try {
            String token =UserPrefrence.getInstance(this).getAuthtoken();
            token="bearer " + token;
            RestClient.getInstance(this).get().getcarddetails( token,card_id, new retrofit.Callback<CardDetails_Model>() {

                @Override
                public void success(final CardDetails_Model res, Response response) {
                    progressBarCustom.cancelProgress();

                    if(res.getResponseMessage().equalsIgnoreCase(Constant.SUCCESS))
                    {
                        setData(res);
                        if(isaddiotnalinfo==true)
                        {
                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                 //   callApiGetAddtionalInfo();
                                }
                            });

                        }
                    }else{
                        Toast.makeText(AddEditCardActitivty.this, res.getMessage(), Toast.LENGTH_SHORT).show();
                    }

                }

                @Override
                public void failure(final RetrofitError error) {
                    progressBarCustom.cancelProgress();
                    error.printStackTrace();

                }
            });

        }catch (Exception e)
        {
            progressBarCustom.cancelProgress();
            //progress.cancelProgress();
            e.printStackTrace();
        }


    }
    /*When user RECIVED CARD DEATILS*/
    private void callApiGetDetailsAdditional() {
        final ProgressBarCustom progressBarCustom=new ProgressBarCustom(AddEditCardActitivty.this);
        progressBarCustom.showProgress();
        try {
            String token =UserPrefrence.getInstance(this).getAuthtoken();
            token="bearer " + token;
            RestClient.getInstance(this).get().getReceivedCardDetails( token,UserPrefrence.getInstance(this).getUserid(),card_id, new retrofit.Callback<RecivedCard_model>() {

                @Override
                public void success(final RecivedCard_model res, Response response) {
                    progressBarCustom.cancelProgress();

                    if(res.getResponseMessage().equalsIgnoreCase(Constant.SUCCESS))
                    {
                        try {
                            setData_Additonal(res);
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }else{
                        Toast.makeText(AddEditCardActitivty.this, res.getMessage(), Toast.LENGTH_SHORT).show();
                    }

                }
                @Override
                public void failure(final RetrofitError error) {
                    progressBarCustom.cancelProgress();
                    error.printStackTrace();
                    if (error != null) {
                        utils.showError(error);
                    }

                }
            });
        }catch (Exception e)
        {
            progressBarCustom.cancelProgress();
            //progress.cancelProgress();
            e.printStackTrace();
        }
    }

    private void setData_Additonal(final RecivedCard_model res) throws IOException {
        if(res.getResourceData()!=null)
        {


            if(res.getResourceData().getOcrImageOfManualCard()!=null&&res.getResourceData().getOcrImageOfManualCard().length()>0)
            {

                et_fullname.setFilters(new InputFilter[] { new InputFilter.LengthFilter(500) });
                et_desingnation.setFilters(new InputFilter[] { new InputFilter.LengthFilter(500) });
                et_mobile.setFilters(new InputFilter[] { new InputFilter.LengthFilter(500) });
                et_email.setFilters(new InputFilter[] { new InputFilter.LengthFilter(500) });
                et_company.setFilters(new InputFilter[] { new InputFilter.LengthFilter(500) });
                et_website.setFilters(new InputFilter[] { new InputFilter.LengthFilter(500) });
                et_address.setFilters(new InputFilter[] { new InputFilter.LengthFilter(1000) });
            }


            et_fullname.setText(res.getResourceData().getName());
            et_desingnation.setText(res.getResourceData().getDesignation());
            et_mobile.setText(res.getResourceData().getMobile());
            et_email.setText(res.getResourceData().getEmail());
            et_address.setText(res.getResourceData().getAddress());
            et_company.setText(res.getResourceData().getCompanyName());
            et_website.setText(res.getResourceData().getWebsite());
            et_summmary.setText(res.getResourceData().getSummary());
            backgroundcolor=res.getResourceData().getCardBackGroundColor();

//            if(res.getResourceData().getOcrImageOfManualCard()!=null)
//            {
//                if(res.getResourceData().getOcrImageOfManualCard().length()>0)
//                {
//                    view_img.setVisibility(View.VISIBLE);
//
//                    new Handler(Looper.getMainLooper()).post(new Runnable() {
//                        @Override
//                        public void run() {
//                            new GetImage(res.getResourceData().getOcrImageOfManualCard()).execute();
//                        }
//                    });
//                    view_img.setOnClickListener(new View.OnClickListener() {
//                        @Override
//                        public void onClick(View view) {
//                            imagepop(myBitmap);
//
//                        }
//                    });
//                }
//            }

            if(UserPrefrence.getInstance(AddEditCardActitivty.this).getUsertype().equalsIgnoreCase(Constant.USERTYPE_PREMIUM)
                    ||UserPrefrence.getInstance(AddEditCardActitivty.this).getUsertype().equalsIgnoreCase(Constant.USERTYPE_CORPORATE)
            ){
                et_label.setEnabled(true);
                et_label.setClickable(true);
            }else{
                et_label.setFocusable(false);
                et_label.setClickable(true);
            }

            if(res.getResourceData().getIsPrimary()==1)
            {
                primary_checkbox.setChecked(true);
                primary_checkbox.setClickable(false);
                primary_checkbox.setEnabled(false);
            }else{
                primary_checkbox.setChecked(false);
            }



            try {
                if (res.getResourceData().getLogo()!= null && res.getResourceData().getLogo().length() > 0) {
                    image.setVisibility(View.VISIBLE);
                   txt_user_placeholder.setVisibility(View.GONE);

                    Picasso.with(AddEditCardActitivty.this).load(res.getResourceData().getLogo()).placeholder(R.drawable.placeholder_camera).into(image);

                   image.setTag("image");
                } else if (Utils.checkNull(res.getResourceData().getName().trim())) {
                    image.setVisibility(View.GONE);
                   txt_user_placeholder.setVisibility(View.VISIBLE);
                    txt_user_placeholder.setText(Utils.nameInital(res.getResourceData().getName().trim()) + "");
                    image.setTag("logo");
                    //  holder. txt_user_placeholder.setText(new Utils(mContext).getFirstLastCharacter(resourceData.get(position).getFullName()));
                }
            }catch (Exception e)
            {
                e.printStackTrace();
            }



            if(res.getResourceData().getBackgroundImage()!=null
                    &&res.getResourceData().getBackgroundImage().length()>0)
            {

                Picasso.with(AddEditCardActitivty.this).load(res.getResourceData().getBackgroundImage()).placeholder(R.drawable.bridege).into(card_view_image);

                card_view_image.setTag("card_view_image");
            }else{
                card_view_image.setTag("remove");
                //  banner_image.setImageResource(R.drawable.placeholder);
            }

//        Media Image
            if(res.getResourceData().getImagesLinks().size()>0)
            {
                for(int j=0;j<res.getResourceData().getImagesLinks().size();j++)
                {
                    Media_Model media_model=new Media_Model();
                    media_model.setUrl_link(res.getResourceData().getImagesLinks().get(j).getUrl());
                    media_list.add(media_model);
                    new Handler(Looper.getMainLooper()).post(new Runnable() {
                        @Override
                        public void run() {
                            mediaAdpater.notifyDataSetChanged();
                        }
                    });
                }
            }

//        Important LInks
            if(res.getResourceData().getImpLinks().size()>0)
            {
                for(int j=0;j<res.getResourceData().getImpLinks().size();j++)
                {
                    Important_Model media_model=new Important_Model();
                    media_model.setTitle(res.getResourceData().getImpLinks().get(j).getTitle());
                    media_model.setLink(res.getResourceData().getImpLinks().get(j).getUrl());
                    important_list.add(media_model);
                    new Handler(Looper.getMainLooper()).post(new Runnable() {
                        @Override
                        public void run() {
                            importantLink_adpater.notifyDataSetChanged();
                        }
                    });
                }
            }

            if(res.getResourceData().getSocialLinks().size()>0)
            {
                for(int j=0;j<res.getResourceData().getSocialLinks().size();j++)
                {
                    Social_Model media_model=new Social_Model();
                    media_model.setTitle(res.getResourceData().getSocialLinks().get(j).getTitle());
                    media_model.setLink(res.getResourceData().getSocialLinks().get(j).getUrl());
                    social_list.add(media_model);
                    new Handler(Looper.getMainLooper()).post(new Runnable() {
                        @Override
                        public void run() {
                            socialLinkAdapter.notifyDataSetChanged();
                        }
                    });
                }
            }

            if (res.getResourceData().getCardAdditionalInfoResp() != null) {
                if (res.getResourceData().getAdditionalInfoAvailable() == true) {

                    et_label.setText(res.getResourceData().getCardAdditionalInfoResp().getLabel());
                    et_note.setText(res.getResourceData().getCardAdditionalInfoResp().getNotes());
                    et_location.setText(res.getResourceData().getCardAdditionalInfoResp().getLocation());
                    servertime=res.getResourceData().getCardAdditionalInfoResp().getTime();
                    txt_time.setText(utils.convertTime(res.getResourceData().getCardAdditionalInfoResp().getTime()));
                    select=res.getResourceData().getCardAdditionalInfoResp().getLead();

                    if(UserPrefrence.getInstance(this).getUsertype().equalsIgnoreCase(Constant.USERTYPE_PREMIUM)
                        ||UserPrefrence.getInstance(this).getUsertype().equalsIgnoreCase(Constant.USERTYPE_CORPORATE)
                    )
                    {
                        if(select==1)
                        {
                            changecolor(cardview_warm);
                        }else if(select==2)
                        {
                            changecolor(cardview_hot);
                        }else if(select==3)
                        {
                            changecolor(cardview_connected);
                        }
                    }

                    additional_list.clear();
                    if(res.getResourceData().getLinkAvailable()==true) {
                        for (int a = 0; a < res.getResourceData().getCardLinkResponse().size(); a++) {
                            AdditionalLinkModel additionalLinkModel = new AdditionalLinkModel();
                            additionalLinkModel.setLink(res.getResourceData().getCardLinkResponse().get(a).getUrl());
                            additionalLinkModel.setTitle(res.getResourceData().getCardLinkResponse().get(a).getTitle());
                            additional_list.add(additionalLinkModel);
                        }

                        new Handler(Looper.getMainLooper()).post(new Runnable() {
                            @Override
                            public void run() {
                                additionalLink_adapter.notifyDataSetChanged();
                            }
                        });
                    }
                } else {
                    addtionaladd_lay.setVisibility(View.VISIBLE);
                }
            }
            else {
                addtionaladd_lay.setVisibility(View.VISIBLE);
            }
        }else{
            addtionaladd_lay.setVisibility(View.VISIBLE);
        }
    }

    private Bitmap convertImagetobitmap(String imagepath) throws IOException {

        if (android.os.Build.VERSION.SDK_INT > 9) {
            StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
            StrictMode.setThreadPolicy(policy);
        }

        URL url = new URL(imagepath);
        HttpURLConnection connection = (HttpURLConnection) url.openConnection();
        connection.setDoInput(true);
        connection.connect();
        InputStream input = connection.getInputStream();
         myBitmap = BitmapFactory.decodeStream(input);
        return myBitmap;
    }


    private void setData(CardDetails_Model res) {


        backgroundcolor=res.getResourceData().getCardBackGroundColor();
        if(res.getResourceData().getIsPrimary()==1)
        {
            isafter_api_call=true;
            primary_checkbox.setChecked(true);
            primary_checkbox.setClickable(false);
            primary_checkbox.setEnabled(false);

        }else{
            primary_checkbox.setChecked(false);
        }

        if(res.getResourceData().getOcrImageOfManualCard()!=null&&res.getResourceData().getOcrImageOfManualCard().length()>0)
        {

                et_fullname.setFilters(new InputFilter[] { new InputFilter.LengthFilter(500) });
                et_desingnation.setFilters(new InputFilter[] { new InputFilter.LengthFilter(500) });
                et_mobile.setFilters(new InputFilter[] { new InputFilter.LengthFilter(500) });
                et_email.setFilters(new InputFilter[] { new InputFilter.LengthFilter(500) });
                et_company.setFilters(new InputFilter[] { new InputFilter.LengthFilter(500) });
                et_website.setFilters(new InputFilter[] { new InputFilter.LengthFilter(500) });
                et_address.setFilters(new InputFilter[] { new InputFilter.LengthFilter(1000) });
        }

        et_fullname.setText(res.getResourceData().getName());
        et_desingnation.setText(res.getResourceData().getDesignation());
        et_mobile.setText(res.getResourceData().getMobile());
        et_address.setText(res.getResourceData().getAddress());
        et_email.setText(res.getResourceData().getEmail());
        et_company.setText(res.getResourceData().getCompanyName());
        et_website.setText(res.getResourceData().getWebsite());
        et_summmary.setText(res.getResourceData().getSummary());

        try {
            if (res.getResourceData().getLogo()!= null && res.getResourceData().getLogo().length() > 0) {
                image.setVisibility(View.VISIBLE);
                txt_user_placeholder.setVisibility(View.GONE);
                image.setTag("image");

                Picasso.with(this).load(res.getResourceData().getLogo()).placeholder(R.drawable.placeholder_camera).into(image);
//                Glide.with(this)
//                        .load(res.getResourceData().getLogo())
//                        .apply(RequestOptions.circleCropTransform())
//                        .into(image);

            } else if (Utils.checkNull(res.getResourceData().getName().trim())) {
                image.setVisibility(View.GONE);
                txt_user_placeholder.setVisibility(View.VISIBLE);
                txt_user_placeholder.setText(Utils.nameInital(res.getResourceData().getName().trim()) + "");
                image.setTag("logo");
                //  holder. txt_user_placeholder.setText(new Utils(mContext).getFirstLastCharacter(resourceData.get(position).getFullName()));
            }
        }catch (Exception e)
        {
            e.printStackTrace();
        }


        if(res.getResourceData().getBackgroundImage()!=null
                &&res.getResourceData().getBackgroundImage().length()>0)
        {

            Picasso.with(this).load(res.getResourceData().getBackgroundImage()).placeholder(R.drawable.bridege).into(card_view_image);

            card_view_image.setTag("card_view_image");
        }else{
            card_view_image.setTag("remove");
            //  banner_image.setImageResource(R.drawable.placeholder);
        }

//        Media Image
        if(res.getResourceData().getImagesLinks().size()>0)
        {
            for(int j=0;j<res.getResourceData().getImagesLinks().size();j++)
            {
                Media_Model media_model=new Media_Model();
                media_model.setUrl_link(res.getResourceData().getImagesLinks().get(j).getUrl());
                media_list.add(media_model);
                new Handler(Looper.getMainLooper()).post(new Runnable() {
                    @Override
                    public void run() {
                        mediaAdpater.notifyDataSetChanged();
                    }
                });
            }
        }

//        Important LInks
        if(res.getResourceData().getImpLinks().size()>0)
        {
            for(int j=0;j<res.getResourceData().getImpLinks().size();j++)
            {
                Important_Model media_model=new Important_Model();
                media_model.setTitle(res.getResourceData().getImpLinks().get(j).getTitle());
                media_model.setLink(res.getResourceData().getImpLinks().get(j).getUrl());
                important_list.add(media_model);
                new Handler(Looper.getMainLooper()).post(new Runnable() {
                    @Override
                    public void run() {
                        importantLink_adpater.notifyDataSetChanged();
                    }
                });
            }
        }

        if(res.getResourceData().getSocialLinks().size()>0)
        {
            for(int j=0;j<res.getResourceData().getSocialLinks().size();j++)
            {
                Social_Model media_model=new Social_Model();
                media_model.setTitle(res.getResourceData().getSocialLinks().get(j).getTitle());
                media_model.setLink(res.getResourceData().getSocialLinks().get(j).getUrl());
                social_list.add(media_model);
                new Handler(Looper.getMainLooper()).post(new Runnable() {
                    @Override
                    public void run() {
                        socialLinkAdapter.notifyDataSetChanged();
                    }
                });
            }
        }

    }

    private void setAdaterLinks() {
//        Implistadpater
        if(isaddiotnalinfo==true){
            importantLink_adpater=new ImportantLink_Adpater(AddEditCardActitivty.this,important_list,"yes");
        }else{
            importantLink_adpater=new ImportantLink_Adpater(AddEditCardActitivty.this,important_list,"");
        }

         listview.setAdapter(importantLink_adpater);

        importantLink_adpater.onCallBackReturn(new ImportantLink_Adpater.Callback() {
            @Override
            public void clickaction(final int position) {


                new Handler(Looper.getMainLooper()).post(new Runnable() {
                    @Override
                    public void run() {
                        important_list.remove(position);
                        importantLink_adpater.notifyDataSetChanged();
                    }
                });

            }
        });

//Social link adpter
        if(isaddiotnalinfo==true){
            socialLinkAdapter=new SocialLinkAdapter(AddEditCardActitivty.this,social_list,"yes");
        }else{
            socialLinkAdapter=new SocialLinkAdapter(AddEditCardActitivty.this,social_list,"");
        }

        listview_social.setAdapter(socialLinkAdapter);

        socialLinkAdapter.onCallBackReturn(new SocialLinkAdapter.Callback() {
            @Override
            public void clickaction(final int position) {


                new Handler(Looper.getMainLooper()).post(new Runnable() {
                    @Override
                    public void run() {
                        social_list.remove(position);
                        socialLinkAdapter.notifyDataSetChanged();
                    }
                });

            }
        });

// MEdia Link Adapter
        if(isaddiotnalinfo==true) {
            mediaAdpater=new MediaListAdapter(this,media_list,"");
        }else{
            mediaAdpater=new MediaListAdapter(this,media_list,"disable");
        }
        lsitview_media.setAdapter(mediaAdpater);

        mediaAdpater.onCallBackReturn(new MediaListAdapter.Callback() {
            @Override
            public void clickaction(final int position) {


                new Handler(Looper.getMainLooper()).post(new Runnable() {
                    @Override
                    public void run() {
                        media_list.remove(position);
                        mediaAdpater.notifyDataSetChanged();
                    }
                });

            }
        });

    /*Addtional Link Adapter*/
        additionalLink_adapter=new AdditionalLink_Adapter(this,additional_list,"");
        listview_addtional.setAdapter(additionalLink_adapter);
        additionalLink_adapter.onCallBackReturn(new AdditionalLink_Adapter.Callback() {
            @Override
            public void clickaction(final int position) {


                new Handler(Looper.getMainLooper()).post(new Runnable() {
                    @Override
                    public void run() {
                        additional_list.remove(position);
                        additionalLink_adapter.notifyDataSetChanged();
                    }
                });

            }
        });

    }


    public  void setCapitalizeTextWatcher(final EditText editText) {
        try {
            final TextWatcher textWatcher = new TextWatcher() {

                int mStart = 0;

                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                }

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {
                    mStart = start + count;
                }

                @Override
                public void afterTextChanged(Editable s) {
                    String input = s.toString();
                    String capitalizedText;
                    if (input.length() < 1)
                        capitalizedText = input;
                    else
                        capitalizedText = input.substring(0, 1).toUpperCase() + input.substring(1);
                    if (!capitalizedText.equals(editText.getText().toString())) {
                        editText.addTextChangedListener(new TextWatcher() {
                            @Override
                            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                            }

                            @Override
                            public void onTextChanged(CharSequence s, int start, int before, int count) {

                            }

                            @Override
                            public void afterTextChanged(Editable s) {
                                editText.setSelection(mStart);
                                editText.removeTextChangedListener(this);
                            }
                        });
                        editText.setText(capitalizedText);
                    }
                }
            };

            editText.addTextChangedListener(textWatcher);
        }catch (Exception e)
        {
            e.printStackTrace();
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId())
        {

            case R.id.theme_selection:
                openbottomDialog();
                break;
            case R.id.tab_media:
                Intent intent=new Intent(AddEditCardActitivty.this,SocialLinkAddActtivity.class);
                intent.putExtra("type","media");
                startActivityForResult(intent,141);
                break;
            case R.id.tab_socialmedia:
                Intent intent01=new Intent(AddEditCardActitivty.this,SocialLinkAddActtivity.class);
                startActivityForResult(intent01,121);
                break;
            case R.id.tab_implink:
                Intent intent02=new Intent(AddEditCardActitivty.this,SocialLinkAddActtivity.class);
                intent02.putExtra("type","important_link");
                startActivityForResult(intent02,121);
                break;
            case R.id.tab_additionallink:
                Intent intent03=new Intent(AddEditCardActitivty.this,SocialLinkAddActtivity.class);
                intent03.putExtra("type","additional_link");
                startActivityForResult(intent03,121);
                break;

            case R.id.background_image:
                clickfrom="cameraClick";
                requestStoragePermission(clickfrom);
                break;

            case R.id.profile_Lay:
                clickfrom="logoClick";
                requestStoragePermission(clickfrom);
                break;
            case R.id.txt_save:
                if(isaddiotnalinfo==false)
                {
                    validation();
                }else{
                    callUpdateAddtionalInfo();
                }

                break;
            case R.id.backimage:
                generatepopoutside();
             //   finish();
                break;
            case R.id.txt_upgrade:
                if(Utils.yesNoDialog!=null&&Utils.yesNoDialog.isShowing()==true)
                {
                    Intent intent11=new Intent(AddEditCardActitivty.this, PlanUserSubcription_Act.class);
                    startActivityForResult(intent11,155);
                    Utils.yesNoDialog.dismiss();
                }
                break;

            case R.id.et_label:
                if(UserPrefrence.getInstance(this).getUsertype().equalsIgnoreCase(Constant.USERTYPE_PREMIUM)
                        ||UserPrefrence.getInstance(this).getUsertype().equalsIgnoreCase(Constant.USERTYPE_CORPORATE)
                ){

                }else{
                    Utils.customYesDialog(this,this);
                }
                break;
            case R.id.cardview_warm:

                if(UserPrefrence.getInstance(this).getUsertype().equalsIgnoreCase(Constant.USERTYPE_PREMIUM)
                        ||UserPrefrence.getInstance(this).getUsertype().equalsIgnoreCase(Constant.USERTYPE_CORPORATE)
                ){
                    select=1;
                    changecolor(cardview_warm);
                }else{
                    Utils.customYesDialog(this,this);
                }


                break;
            case R.id.cardview_hot:

                if(UserPrefrence.getInstance(this).getUsertype().equalsIgnoreCase(Constant.USERTYPE_PREMIUM)
                        ||UserPrefrence.getInstance(this).getUsertype().equalsIgnoreCase(Constant.USERTYPE_CORPORATE)
                ){

                    select=2;
                    changecolor(cardview_hot);
                }else{
                    Utils.customYesDialog(this,this);
                }

                break;
            case R.id.cardview_connected:

                if(UserPrefrence.getInstance(this).getUsertype().equalsIgnoreCase(Constant.USERTYPE_PREMIUM)
                        ||UserPrefrence.getInstance(this).getUsertype().equalsIgnoreCase(Constant.USERTYPE_CORPORATE)
                ){
                    select=3;
                    changecolor(cardview_connected);
                }else{
                    Utils.customYesDialog(this,this);
                }


                break;
            case R.id.txt_time:
                Calendar mcurrentTime = Calendar.getInstance();
                int hour = mcurrentTime.get(Calendar.HOUR_OF_DAY);
                int minute = mcurrentTime.get(Calendar.MINUTE);
                TimePickerDialog mTimePicker;
                mTimePicker = new TimePickerDialog(AddEditCardActitivty.this, new TimePickerDialog.OnTimeSetListener() {
                    @Override
                    public void onTimeSet(TimePicker timePicker, int selectedHour, int selectedMinute) {
                        servertime=selectedHour + ":" + selectedMinute;
                        Utils utils=new Utils(AddEditCardActitivty.this);
                        String time=utils.convertTime(selectedHour + ":" + selectedMinute);
                        txt_time.setText(time);
                    }
                }, hour, minute,false);
                mTimePicker.setTitle("Select Time");
                mTimePicker.show();
                break;


        }
    }

    /*When user UPDATE ADDTIONAL CARD INFO*/

    private void callUpdateAddtionalInfo() {
        String token =UserPrefrence.getInstance(this).getAuthtoken();
        token="bearer " + token;
        final ProgressBarCustom progressBarCustom=new ProgressBarCustom(AddEditCardActitivty.this);
        progressBarCustom.showProgress();
        try {

            JSONObject jsonObject3 = new JSONObject();

            jsonObject3.put("label",et_label.getText().toString());
            jsonObject3.put("lead",select+"");
            jsonObject3.put("notes",et_note.getText().toString());
            jsonObject3.put("location",et_location.getText().toString());
            jsonObject3.put("time",servertime);

            JSONArray jsonArray_social=new JSONArray();
            for(int i=0;i<additional_list.size();i++)
            {
                JSONObject jsonObject=new JSONObject();
                jsonObject.put("title",additional_list.get(i).getTitle());
                jsonObject.put("url",additional_list.get(i).getLink());
                jsonArray_social.put(jsonObject);
            }
            jsonObject3.put("receivedCardLinks",jsonArray_social);

            TypedInput in3 = new TypedByteArray("application/json", jsonObject3.toString().getBytes("UTF-8"));

            RestClient.getInstance(this).get().update_addtionalinfo(token,UserPrefrence.getInstance(this).getUserid(),card_id, in3, new Callback<UpdateAddtionalInfo_Model>() {

                @Override
                public void success(final UpdateAddtionalInfo_Model res, Response response) {
                    progressBarCustom.cancelProgress();

                    if(res.getResponseMessage().equalsIgnoreCase(Constant.SUCCESS))
                    {

                        Toast.makeText(AddEditCardActitivty.this, res.getMessage(), Toast.LENGTH_SHORT).show();
                        Intent intent=new Intent();
                        setResult(RESULT_OK,intent);
                        finish();
                    }else{
                        ProgressBarCustom.customDialog(AddEditCardActitivty.this,res.getMessage(),false);
                    }
                }

                @Override
                public void failure(final RetrofitError error) {
                    progressBarCustom.cancelProgress();
                    error.printStackTrace();
                    if (error != null) {
                        utils.showError(error);
                    }

                }
            });

        }catch (Exception e)
        {
            progressBarCustom.cancelProgress();
            //progress.cancelProgress();
            e.printStackTrace();
        }
    }

    private void changecolor(CardView color) {

        cardview_warm.setCardBackgroundColor(getResources().getColor(R.color.light_grey));
        cardview_hot.setCardBackgroundColor(getResources().getColor(R.color.light_grey));
        cardview_connected.setCardBackgroundColor(getResources().getColor(R.color.light_grey));

        color.setCardBackgroundColor(getResources().getColor(R.color.background_color));
    }

    private void validation() {
        String getfullname=et_fullname.getText().toString();
        String getemail=et_email.getText().toString();

        if(isfrom_ocr==true){

            if (et_fullname.getText().length() == 0) {
                ProgressBarCustom.customDialog(this, getResources().getString(R.string.enter_fulllname), false);
            }
            else if (et_desingnation.getText().length() == 0) {
                ProgressBarCustom.customDialog(this, getResources().getString(R.string.enter_desgination), false);
            }
            else if (et_mobile.getText().length() < 10) {
                ProgressBarCustom.customDialog(this, getResources().getString(R.string.enter_mobile), false);
            }
            else if (et_email.getText().length() == 0) {
                ProgressBarCustom.customDialog(this, getResources().getString(R.string.enter_email), false);
            }
//            else if (et_company.getText().length() == 0) {
//                ProgressBarCustom.customDialog(this, getResources().getString(R.string.enter_companyname), false);
//            }
//            else if (et_website.getText().length() == 0) {
//                ProgressBarCustom.customDialog(this, getResources().getString(R.string.enter_website), false);
//            }

            else{
                if(mycard.length()>0)
                {
                    crearecardapi();
                }else{
                    CreateManualCard();
                }
              //  CreateManualCard();
            }
        }else {
            if (et_fullname.getText().length() == 0) {
                ProgressBarCustom.customDialog(this, getResources().getString(R.string.enter_fulllname), false);
            } else if (utils.validateFirstName(getfullname) == false) {
                ProgressBarCustom.customDialog(this, getResources().getString(R.string.valid_firstname), false);
            } else if (et_desingnation.getText().length() == 0) {
                ProgressBarCustom.customDialog(this, getResources().getString(R.string.enter_desgination), false);
            } else if (et_mobile.getText().length() < 10) {
                ProgressBarCustom.customDialog(this, getResources().getString(R.string.enter_mobile), false);
            } else if (et_email.getText().length() == 0) {
                ProgressBarCustom.customDialog(this, getResources().getString(R.string.enter_email), false);
            } else if (utils.isvalidaEmail(getemail) == false) {
                ProgressBarCustom.customDialog(this, getResources().getString(R.string.enter_valid_email), false);
            }
//        else if(et_address.getText().toString().length()==0)
//        {
//            ProgressBarCustom.customDialog(this,getResources().getString(R.string.enter_address),false);
//        }
//            else if (et_company.getText().length() == 0) {
//                ProgressBarCustom.customDialog(this, getResources().getString(R.string.enter_companyname), false);
//            }
//            else if (et_website.getText().length() == 0) {
//                ProgressBarCustom.customDialog(this, getResources().getString(R.string.enter_website), false);
//            }
            else if (et_website.getText().length() != 0&&utils.validwebsite(et_website.getText().toString().trim()) == false) {
                ProgressBarCustom.customDialog(this, getResources().getString(R.string.enter_valid_url), false);

            } else {

                if (utils.isNetworkAvailable(AddEditCardActitivty.this) == true) {
                    if (txt_save.getText().toString().equalsIgnoreCase("UPDATE")) {
                        cardUpdateApi();
                    } else {
                        if (manual.length() > 0) {
                            CreateManualCard();
                        } else {
                            crearecardapi();
                        }

                    }

                } else {
                    ProgressBarCustom.customDialog(this, getResources().getString(R.string.internet_connection), false);
                }

            }
        }

    }
    /*When user ADD My Cards*/
    private void crearecardapi() {
        int ischeck=0;
        if(primary_checkbox.isChecked()==true)
        {
            ischeck=1;
        }else{
            ischeck=0;
        }

        String token =UserPrefrence.getInstance(this).getAuthtoken();
        token="bearer " + token;
        final ProgressBarCustom progressBarCustom=new ProgressBarCustom(AddEditCardActitivty.this);
        progressBarCustom.showProgress();
        try {
            JSONObject jsonObject3 = new JSONObject();
            jsonObject3.put("userId",UserPrefrence.getInstance(this).getUserid());
            jsonObject3.put("name",et_fullname.getText().toString().trim());
            jsonObject3.put("designation",et_desingnation.getText().toString());
            jsonObject3.put("mobile",et_mobile.getText().toString());
            jsonObject3.put("address",et_address.getText().toString());
            jsonObject3.put("email",et_email.getText().toString());
            jsonObject3.put("companyName",et_company.getText().toString());
            jsonObject3.put("website",et_website.getText().toString().trim());
            jsonObject3.put("summary",et_summmary.getText().toString());
            jsonObject3.put("isPrimary",ischeck);
            jsonObject3.put("cardBackGroundColor",backgroundcolor.toString());

            if(ocr_image.length()>0)
            {

                jsonObject3.put("ocrImageOfManualCard",Constant.AWS_URL+ocr_image);
            }

            if(upload_banner_card.length()>0)
            {
                jsonObject3.put("backGroundImage",Constant.AWS_URL+upload_banner_card);
            }else if(banner_delete.equalsIgnoreCase("DELETE")){
                jsonObject3.put("backGroundImage","");
            }
            if(upload_user_card.length()>0)
            {
                jsonObject3.put("logo",Constant.AWS_URL+upload_user_card);
            }else if(logo_delete.equalsIgnoreCase("DELETE")){
                jsonObject3.put("logo","");
            }
            JSONArray jsonArray_social=new JSONArray();
            for(int i=0;i<social_list.size();i++)
            {
                JSONObject jsonObject=new JSONObject();
                jsonObject.put("title",social_list.get(i).getTitle());
                jsonObject.put("url",social_list.get(i).getLink());
                jsonArray_social.put(jsonObject);
            }
            jsonObject3.put("socialLinks",jsonArray_social);

            JSONArray jsonArray_medial=new JSONArray();
            for(int i=0;i<media_list.size();i++)
            {
                JSONObject jsonObject=new JSONObject();
                jsonObject.put("title","");
                jsonObject.put("url",media_list.get(i).getUrl_link());
                jsonArray_medial.put(jsonObject);
            }
            jsonObject3.put("imagesLinks",jsonArray_medial);

            JSONArray jsonArray_imortant=new JSONArray();
            for(int i=0;i<important_list.size();i++)
            {
                JSONObject jsonObject=new JSONObject();
                jsonObject.put("title",important_list.get(i).getTitle());
                jsonObject.put("url",important_list.get(i).getLink());
                jsonArray_imortant.put(jsonObject);
            }
            jsonObject3.put("impLinks",jsonArray_imortant);

            TypedInput in3 = new TypedByteArray("application/json", jsonObject3.toString().getBytes("UTF-8"));

            RestClient.getInstance(this).get().createcard( in3, new Callback<Createcard_Model>() {

                @Override
                public void success(final Createcard_Model res, Response response) {
                    progressBarCustom.cancelProgress();

                    if(res.getResponseMessage().equalsIgnoreCase(Constant.SUCCESS))
                    {
                        Toast.makeText(AddEditCardActitivty.this, res.getMessage(), Toast.LENGTH_SHORT).show();
                        Intent intent=new Intent();
                        setResult(RESULT_OK,intent);
                        finish();
                    }else{
                       ProgressBarCustom.customDialog(AddEditCardActitivty.this,res.getMessage(),false);
                    }
                }

                @Override
                public void failure(final RetrofitError error) {
                    progressBarCustom.cancelProgress();
                    error.printStackTrace();
                    if (error != null) {
                        utils.showError(error);
                    }

                }
            });

        }catch (Exception e)
        {
            progressBarCustom.cancelProgress();
            //progress.cancelProgress();
            e.printStackTrace();
        }
    }

    private void openbottomDialog() {
        BottomSheetFragment bottomSheetFragment = new BottomSheetFragment();
        //bottomSheetFragment.setCancelable(false);
        bottomSheetFragment.show(getSupportFragmentManager(), bottomSheetFragment.getTag());

        bottomSheetFragment.onCallBackReturn(new BottomSheetFragment.Callback() {
            @Override
            public void clickaction(String backgroundcolor_back) {
                backgroundcolor=backgroundcolor_back;
                Log.e("=====background",backgroundcolor);
            }
        });
        //    mBottomSheetBehavior.setState(BottomSheetBehavior.STATE_EXPANDED);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        Bitmap bitmap=null;
        File imageFile=null;

        if(requestCode==121&&resultCode==RESULT_OK) {
            String values="",link="";
            if (data.getStringExtra("value") != null) {
                values=data.getStringExtra("value");
            }
            if(data.getStringExtra("link")!=null)
            {
                link=data.getStringExtra("link");
            }
            if(data.getStringExtra("type")!=null)
            {
               if(data.getStringExtra("type").equalsIgnoreCase("important"))
               {
                   Important_Model social_model=new Important_Model();
                   social_model.setLink(link);
                   social_model.setTitle(values);
                   important_list.add(social_model);
                   new Handler(Looper.getMainLooper()).post(new Runnable() {
                       @Override
                       public void run() {
                           importantLink_adpater.notifyDataSetChanged();
                       }
                   });
               }
               else if(data.getStringExtra("type").equalsIgnoreCase("additional"))
               {
                   AdditionalLinkModel additionalLinkModel=new AdditionalLinkModel();
                   additionalLinkModel.setLink(link);
                   additionalLinkModel.setTitle(values);
                   additional_list.add(additionalLinkModel);
                   new Handler(Looper.getMainLooper()).post(new Runnable() {
                       @Override
                       public void run() {
                          additionalLink_adapter.notifyDataSetChanged();
                       }
                   });

               }
               else if(data.getStringExtra("type").equalsIgnoreCase("social")){
                   Social_Model social_model=new Social_Model();
                   social_model.setLink(link);
                   social_model.setTitle(values);

                   if(data.getStringExtra("image_type")!=null)
                   {
                       social_model.setImage_type(data.getStringExtra("image_type"));
                   }
                   social_list.add(social_model);
                   new Handler(Looper.getMainLooper()).post(new Runnable() {
                       @Override
                       public void run() {
                           socialLinkAdapter.notifyDataSetChanged();
                       }
                   });
               }
            }
        }else if(requestCode==141&&resultCode==RESULT_OK)
        {

            if(data.getStringExtra("link")!=null)
            {
                Media_Model media_model=new Media_Model();
                media_model.setUrl_link(data.getStringExtra("link"));
                media_list.add(media_model);
                new Handler(Looper.getMainLooper()).post(new Runnable() {
                    @Override
                    public void run() {
                        mediaAdpater.notifyDataSetChanged();
                    }
                });

            }
        }
        else if(requestCode==155&&resultCode==RESULT_OK)
        {
            et_label.setClickable(true);
            et_label.setFocusable(true);
        }
        else  if (resultCode == RESULT_OK && requestCode == CAMERA_REQUEST) {

                if (resultCode == RESULT_OK) {

                    CameraUtils.refreshGallery(getApplicationContext(), profilePath);
                    profileBitmap = CameraUtils.optimizeBitmap(BITMAP_SAMPLE_SIZE, profilePath);
                    if(profilePath.length()==0&&profilePath==null){
                        Toast.makeText(mcontext, "Something went wrong", Toast.LENGTH_SHORT).show();
                    }else {
                        performCrop(profilePath);
                    }

                }


            } else if (resultCode == RESULT_OK && requestCode == GALLERY_PICTURE) {
                if (data != null&& data.getData() != null) {
                    try {
                        Uri uri= data.getData();
                        String filepath;
                        if (Build.VERSION.SDK_INT < 11) {
                            filepath = RealPathUtil.getRealPathFromURI_BelowAPI11(this, data.getData());
                            // SDK >= 11 && SDK < 19
                        }else if (Build.VERSION.SDK_INT < 19)
                            filepath = RealPathUtil.getRealPathFromURI_API11to18(this, data.getData());
                            // Lolipop
                        else if(Build.VERSION.SDK_INT<=22)
                            filepath = RealPathUtil.getRealPathFromURI_API19(this, data.getData());
                            /*Kitkat to Pie*/
                        else
                            filepath = FileUtilAbove19.getPath(this, data.getData());

                        Log.e("=====filepath======",filepath.toString());
                        if(filepath.length()==0&&filepath==null)
                        {
                            Toast.makeText(mcontext, "Something went wrong", Toast.LENGTH_SHORT).show();
                        }else {

                            profilePath=filepath;
                            performCrop(filepath);
                        }


                    }catch (Exception e)
                    {
                        e.printStackTrace();
                        Toast.makeText(mcontext, "Something went wrong", Toast.LENGTH_SHORT).show();
                    }
                } else {
                    Toast.makeText(getApplicationContext(), "Cancelled",
                            Toast.LENGTH_SHORT).show();
                }

            }else if (requestCode == RESULT_CROP) {
                if (resultCode == RESULT_OK) {
                    Bundle extras = data.getExtras();
                    try {
                        if (extras != null) {
                            if (data.getData() != null) {

                                Uri selectedImageUri = data.getData();
                                Log.e("selectedImageUri", selectedImageUri.getPath());
                                //  File file=new File(selectedImageUri);
                                if (selectedImageUri != null) {

                                    profilePath = selectedImageUri.getPath();
                                    profileBitmap = BitmapFactory.decodeFile(cropfilepath);
                                    imageFile = new File(profilePath);
                                    if (clickfrom.equals("logoClick")) {
                                        Log.e("=====imageFile======",imageFile.toString());
                                        image.setVisibility(View.VISIBLE);
                                        txt_user_placeholder.setVisibility(View.GONE);
                                        image.setBackground(null);
                                        image.setImageDrawable(null);
                                        image.setImageBitmap(profileBitmap);
                                        image.setTag("image");
                                        callS3Bucket_Upload(cropfilepath,"logoClick");
                                    } else {
                                        Log.e("=====imageFile======",imageFile.toString());
                                        card_view_image.setImageBitmap(profileBitmap);
                                        card_view_image.setTag("card_view_image");
                                        callS3Bucket_Upload(cropfilepath,"");
                                    }
                                }else{
                                    profilePath = cropfilepath;
                                    profileBitmap = BitmapFactory.decodeFile(cropfilepath);
                                    imageFile = new File(profilePath);
                                    if (clickfrom.equals("logoClick")) {
                                        Log.e("=====imageFile======",imageFile.toString());
                                        image.setVisibility(View.VISIBLE);
                                        txt_user_placeholder.setVisibility(View.GONE);
                                        image.setBackground(null);
                                        image.setImageDrawable(null);
                                        image.setImageBitmap(profileBitmap);
                                        image.setTag("image");
                                        callS3Bucket_Upload(cropfilepath,"logoClick");
                                    } else {
                                        Log.e("=====imageFile======",imageFile.toString());
                                        card_view_image.setImageBitmap(profileBitmap);
                                        card_view_image.setTag("card_view_image");
                                        callS3Bucket_Upload(cropfilepath,"");
                                    }
                                }
                            }
                            else {
                                profileBitmap = (Bitmap) data.getExtras().get("data");
                                if(clickfrom.equals("logoClick")){
                                    Log.e("=====imageFile======",imageFile.toString());
                                    image.setVisibility(View.VISIBLE);
                                    txt_user_placeholder.setVisibility(View.GONE);
                                    image.setBackground(null);
                                    image.setImageDrawable(null);
                                    image.setImageBitmap(profileBitmap);
                                    image.setTag("image");
                                    callS3Bucket_Upload(cropfilepath,"logoClick");
                                }else {
                                    Log.e("=====imageFile======",imageFile.toString());
                                    card_view_image.setImageBitmap(profileBitmap);
                                    card_view_image.setTag("card_view_image");
                                    callS3Bucket_Upload(cropfilepath,"");
                                }


                            }
                        } else {
                            Uri selectedImageUri = data.getData();
                            if (selectedImageUri != null) {
                                profilePath = selectedImageUri.getPath();
                                profileBitmap = BitmapFactory.decodeFile(cropfilepath);
                                imageFile = new File(profilePath);
                                if (clickfrom.equals("logoClick")) {
                                    image.setVisibility(View.VISIBLE);
                                    txt_user_placeholder.setVisibility(View.GONE);
                                    image.setBackground(null);
                                    image.setImageDrawable(null);
                                    Log.e("=====imageFile======",imageFile.toString());
                                    image.setImageBitmap(profileBitmap);
                                    image.setTag("image");
                                    callS3Bucket_Upload(cropfilepath,"logoClick");
                                } else {
                                    Log.e("=====imageFile======",imageFile.toString());
                                    card_view_image.setImageBitmap(profileBitmap);
                                    card_view_image.setTag("card_view_image");
                                    callS3Bucket_Upload(cropfilepath,"");
                                }

                                // personProfile.setImageBitmap(profileBitmap);
                            }else{
                                profilePath = cropfilepath;
                                profileBitmap = BitmapFactory.decodeFile(cropfilepath);
                                imageFile = new File(profilePath);
                                if (clickfrom.equals("logoClick")) {
                                    Log.e("=====imageFile======",imageFile.toString());
                                    image.setVisibility(View.VISIBLE);
                                    txt_user_placeholder.setVisibility(View.GONE);
                                    image.setBackground(null);
                                    image.setImageDrawable(null);
                                    image.setImageBitmap(profileBitmap);
                                    image.setTag("image");
                                    callS3Bucket_Upload(cropfilepath,"logoClick");
                                } else {
                                    Log.e("=====imageFile======",imageFile.toString());
                                    card_view_image.setImageBitmap(profileBitmap);
                                    card_view_image.setTag("card_view_image");
                                    callS3Bucket_Upload(cropfilepath,"");
                                }
                            }
                        }
                    }catch (Exception e)
                    {
                        e.printStackTrace();
                    }
                }
            }
    }
    /*When user Update My Cards*/
    public void cardUpdateApi(){

        int ischeck=0;
        if(primary_checkbox.isChecked()==true)
        {
            ischeck=1;
        }else{
            ischeck=0;
        }
        String token =UserPrefrence.getInstance(this).getAuthtoken();
        token="bearer " + token;
        final ProgressBarCustom progressBarCustom=new ProgressBarCustom(AddEditCardActitivty.this);
        progressBarCustom.showProgress();
        try {

            JSONObject jsonObject3 = new JSONObject();
            jsonObject3.put("userId",UserPrefrence.getInstance(this).getUserid());
            jsonObject3.put("cardId",card_id);
            jsonObject3.put("name",et_fullname.getText().toString().trim());
            jsonObject3.put("designation",et_desingnation.getText().toString());
            jsonObject3.put("address",et_address.getText().toString());
            jsonObject3.put("mobile",et_mobile.getText().toString());
            jsonObject3.put("email",et_email.getText().toString());
            jsonObject3.put("companyName",et_company.getText().toString());
            jsonObject3.put("website",et_website.getText().toString().trim());
            jsonObject3.put("summary",et_summmary.getText().toString());
            jsonObject3.put("isPrimary",ischeck);
            jsonObject3.put("cardBackGroundColor",backgroundcolor.toString());

            if(upload_banner_card.length()>0)
            {
                jsonObject3.put("backGroundImage",Constant.AWS_URL+upload_banner_card);
            }else if(banner_delete.equalsIgnoreCase("DELETE")){
                jsonObject3.put("backGroundImage","");
            }
            if(upload_user_card.length()>0)
            {
                jsonObject3.put("logo",Constant.AWS_URL+upload_user_card);
            }else if(logo_delete.equalsIgnoreCase("DELETE")){
                jsonObject3.put("logo","");
            }


            JSONArray jsonArray_social=new JSONArray();
            for(int i=0;i<social_list.size();i++)
            {
                JSONObject jsonObject=new JSONObject();
                jsonObject.put("title",social_list.get(i).getTitle());
                jsonObject.put("url",social_list.get(i).getLink());
                jsonArray_social.put(jsonObject);
            }
            jsonObject3.put("socialLinks",jsonArray_social);
            JSONArray jsonArray_medial=new JSONArray();
            for(int i=0;i<media_list.size();i++)
            {
                JSONObject jsonObject=new JSONObject();
                jsonObject.put("title","");
                jsonObject.put("url",media_list.get(i).getUrl_link());
                jsonArray_medial.put(jsonObject);
            }
            jsonObject3.put("imagesLinks",jsonArray_medial);

            JSONArray jsonArray_imortant=new JSONArray();
            for(int i=0;i<important_list.size();i++)
            {
                JSONObject jsonObject=new JSONObject();
                jsonObject.put("title",important_list.get(i).getTitle());
                jsonObject.put("url",important_list.get(i).getLink());
                jsonArray_imortant.put(jsonObject);
            }
            jsonObject3.put("impLinks",jsonArray_imortant);

            TypedInput in3 = new TypedByteArray("application/json", jsonObject3.toString().getBytes("UTF-8"));

            RestClient.getInstance(this).get().updatecard(token,card_id, in3, new Callback<UpdateCard_Model>() {

                @Override
                public void success(final UpdateCard_Model res, Response response) {
                    progressBarCustom.cancelProgress();

                    if(res.getResponseMessage().equalsIgnoreCase(Constant.SUCCESS))
                    {

                        Toast.makeText(AddEditCardActitivty.this, res.getMessage(), Toast.LENGTH_SHORT).show();
                        Intent intent=new Intent();
                        setResult(RESULT_OK,intent);
                        finish();
                    }else{
                        ProgressBarCustom.customDialog(AddEditCardActitivty.this,res.getMessage(),false);
                    }
                }

                @Override
                public void failure(final RetrofitError error) {
                    progressBarCustom.cancelProgress();
                    error.printStackTrace();
                    if (error != null) {
                        utils.showError(error);
                    }

                }
            });

        }catch (Exception e)
        {
            progressBarCustom.cancelProgress();
            //progress.cancelProgress();
            e.printStackTrace();
        }
    }


    /*Image code starting


    from dont touch if works
    * */
    private void requestStoragePermission(final String clickfrom) {

        Dexter.withActivity(this)
                .withPermissions(
                        Manifest.permission.CAMERA,
                        Manifest.permission.READ_EXTERNAL_STORAGE,
                        Manifest.permission.WRITE_EXTERNAL_STORAGE)

                .withListener(new MultiplePermissionsListener() {
                    @Override
                    public void onPermissionsChecked(MultiplePermissionsReport report) {

                        if (report.areAllPermissionsGranted()) {
//                            //  openGallery();
                            selectImage(clickfrom);

                        }
                        if (report.isAnyPermissionPermanentlyDenied()) {

                            CameraUtils.showSettingsDialog(mcontext);
                        }
                    }

                    @Override
                    public void onPermissionRationaleShouldBeShown(List<PermissionRequest> permissions, PermissionToken token) {
                        token.continuePermissionRequest();
                    }
                }).
                withErrorListener(new PermissionRequestErrorListener() {
                    @Override
                    public void onError(DexterError error) {
                        Toast.makeText(getApplicationContext(), "Error occurred! ", Toast.LENGTH_SHORT).show();
                    }
                })
                .onSameThread()
                .check();
    }
    private void selectImage(final String clickfrom) {
        try {

            if(clickfrom.equalsIgnoreCase("logoClick"))
            {
                if (image.getTag().equals("logo")){
                    //Image doesn´t exist.
                    options = new CharSequence[]{"Take Photo", "Choose From Gallery", "Cancel"};
                }else{
                    options = new CharSequence[]{"Take Photo", "Choose From Gallery","Cancel","Remove"};
                    //Image Exists!.
                }
            }else if(clickfrom.equalsIgnoreCase("cameraClick")){
                if (card_view_image.getTag().equals("remove")){
                    //Image doesn´t exist.
                    options = new CharSequence[]{"Take Photo", "Choose From Gallery", "Cancel"};
                }else{
                    //Image Exists!.
                    options = new CharSequence[]{"Take Photo", "Choose From Gallery","Cancel","Remove"};
                }
            }
            androidx.appcompat.app.AlertDialog.Builder builder = new androidx.appcompat.app.AlertDialog.Builder(this);
            builder.setTitle("Select Option");
            builder.setItems(options, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int item) {
                    if (options[item].equals("Take Photo")) {
                        dialog.dismiss();

                        captureImage();
                    } else if (options[item].equals("Choose From Gallery")) {
                        dialog.dismiss();

                        galleryIntent();
                    } else if (options[item].equals("Cancel")) {

                        dialog.dismiss();
                    }
                    else if (options[item].equals("Remove")) {

                        if(clickfrom.equalsIgnoreCase("logoClick"))
                        {
                            logo_delete = "DELETE";
                            if (Utils.checkNull(et_fullname.getText().toString().trim())) {
                                image.setVisibility(View.GONE);
                                txt_user_placeholder.setVisibility(View.VISIBLE);
                                txt_user_placeholder.setText(Utils.nameInital(et_fullname.getText().toString().trim()) + "");
                                image.setTag("logo");
                                //  holder. txt_user_placeholder.setText(new Utils(mContext).getFirstLastCharacter(resourceData.get(position).getFullName()));
                            } else {
                                image.setVisibility(View.VISIBLE);
                                txt_user_placeholder.setVisibility(View.GONE);


                                Picasso.with(AddEditCardActitivty.this).load(R.drawable.placeholder_camera).placeholder(R.drawable.placeholder_camera).into(image);

                                image.setTag("logo");

                            }
                        }else if(clickfrom.equalsIgnoreCase("cameraClick")){
                            card_view_image.setTag("remove");
                            banner_delete="DELETE";

                            Picasso.with(AddEditCardActitivty.this).load(R.drawable.bridege).placeholder(R.drawable.bridege).into(card_view_image);

                        }
                        dialog.dismiss();
                    }
                }
            });
            builder.show();
        } catch (Exception e) {
            Toast.makeText(this, "Camera Permission error", Toast.LENGTH_SHORT).show();
            e.printStackTrace();
        }
    }
    private void captureImage() {
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);


        File file = CameraUtils.getOutputMediaFile(MEDIA_TYPE_IMAGE);
        Log.i("==clickfrom",clickfrom);
        if (file != null) {
            profilePath = file.getAbsolutePath();
            ///  uploadPathMap.put("profile",file.getAbsolutePath());
        }


        Uri fileUri = CameraUtils.getOutputMediaFileUri(getApplicationContext(), file);
        intent.putExtra(MediaStore.EXTRA_OUTPUT, fileUri);
        // start the image capture Intent
        startActivityForResult(intent, CAMERA_REQUEST);
    }
    public void galleryIntent(){
        Intent pictureActionIntent = null;

        pictureActionIntent = new Intent(
                Intent.ACTION_PICK,
                MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        startActivityForResult(
                pictureActionIntent,
                GALLERY_PICTURE);
    }


    private void performCrop(String picUri) {
        Uri mCropImagedUri;
        try {

            //imageStoragePath = picUri;
            Intent cropIntent = new Intent("com.android.camera.action.CROP");

           File f = new File(picUri);
            Uri contentUri = FileProvider.getUriForFile(AddEditCardActitivty.this, BuildConfig.APPLICATION_ID + ".provider",f);
          //  Uri contentUri = Uri.fromFile(f);
            cropIntent.setDataAndType(contentUri, "image/*");
            cropIntent.putExtra("crop", "true");
            cropIntent.putExtra("aspectX", 1);
            cropIntent.putExtra("aspectY", 1);
            cropIntent.putExtra("outputX", 500);
            cropIntent.putExtra("outputY", 500);
            cropIntent.putExtra("return-data", true);
            cropIntent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
            File fileN = createNewFile("CROP_");
            try {
                fileN.createNewFile();
            } catch (IOException ex) {
                Log.e("io", ex.getMessage());
            }
            mCropImagedUri = Uri.fromFile(fileN);
            cropfilepath=mCropImagedUri.getPath();
            cropIntent.putExtra(MediaStore.EXTRA_OUTPUT, mCropImagedUri);
            startActivityForResult(cropIntent, RESULT_CROP);
        }

        catch (ActivityNotFoundException anfe) {
            String errorMessage = "your device doesn't support the crop action!";
            Toast toast = Toast.makeText(this, errorMessage, Toast.LENGTH_SHORT);
            toast.show();
        }
    }
    private File createNewFile(String prefix){



        if(prefix==null || "".equalsIgnoreCase(prefix)){
            prefix="IMG_";
        }
        File newDirectory = new File(Environment.getExternalStorageDirectory()+"/Avocard/");
        if(!newDirectory.exists()){
            if(newDirectory.mkdir()){
                Log.d(mcontext.getClass().getName(), newDirectory.getAbsolutePath()+" directory created");
            }
        }
        File file = new File(newDirectory,(prefix+System.currentTimeMillis()+".jpg"));
        if(file.exists()){
            //this wont be executed
            file.delete();
            try {
                file.createNewFile();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        return file;
    }


    /*When user Add Manual

    Card From Recieve Cards*/
    private void CreateManualCard() {
        int ischeck=0;
        if(primary_checkbox.isChecked()==true)
        {
            ischeck=1;
        }else{
            ischeck=0;
        }

        String token =UserPrefrence.getInstance(this).getAuthtoken();
        token="bearer " + token;
        final ProgressBarCustom progressBarCustom=new ProgressBarCustom(AddEditCardActitivty.this);
        progressBarCustom.showProgress();
        try {
            JSONObject jsonObject3 = new JSONObject();
            jsonObject3.put("cardType","MANUAL");
            jsonObject3.put("manualCardCreatorUserId",UserPrefrence.getInstance(this).getUserid());
            jsonObject3.put("name",et_fullname.getText().toString().trim());
            jsonObject3.put("designation",et_desingnation.getText().toString());
            jsonObject3.put("mobile",et_mobile.getText().toString());
            jsonObject3.put("address",et_address.getText().toString());
            jsonObject3.put("email",et_email.getText().toString());
            jsonObject3.put("companyName",et_company.getText().toString());
            jsonObject3.put("website",et_website.getText().toString().trim());
            jsonObject3.put("summary",et_summmary.getText().toString());
            jsonObject3.put("isPrimary",ischeck);
            jsonObject3.put("cardBackGroundColor",backgroundcolor.toString());
            if(upload_banner_card.length()>0)
            {
                jsonObject3.put("backGroundImage",Constant.AWS_URL+upload_banner_card);
            }else if(banner_delete.equalsIgnoreCase("DELETE")){
                jsonObject3.put("backGroundImage","");
            }


            if(upload_user_card.length()>0)
            {
                jsonObject3.put("logo",Constant.AWS_URL+upload_user_card);
               // jsonObject3.put("ocrImageOfManualCard",Constant.AWS_URL+upload_user_card);
            }else if(logo_delete.equalsIgnoreCase("DELETE")){
                jsonObject3.put("logo","");
            }

            if(ocr_image.length()>0)
            {

                jsonObject3.put("ocrImageOfManualCard",Constant.AWS_URL+ocr_image);
            }

            JSONArray jsonArray_social=new JSONArray();
            for(int i=0;i<social_list.size();i++)
            {
                JSONObject jsonObject=new JSONObject();
                jsonObject.put("title",social_list.get(i).getTitle());
                jsonObject.put("url",social_list.get(i).getLink());
                jsonArray_social.put(jsonObject);
            }
            jsonObject3.put("socialLinks",jsonArray_social);

            JSONArray jsonArray_medial=new JSONArray();
            for(int i=0;i<media_list.size();i++)
            {
                JSONObject jsonObject=new JSONObject();
                jsonObject.put("title","");
                jsonObject.put("url",media_list.get(i).getUrl_link());
                jsonArray_medial.put(jsonObject);
            }
            jsonObject3.put("imagesLinks",jsonArray_medial);

            JSONArray jsonArray_imortant=new JSONArray();
            for(int i=0;i<important_list.size();i++)
            {
                JSONObject jsonObject=new JSONObject();
                jsonObject.put("title",important_list.get(i).getTitle());
                jsonObject.put("url",important_list.get(i).getLink());
                jsonArray_imortant.put(jsonObject);
            }
            jsonObject3.put("impLinks",jsonArray_imortant);

            TypedInput in3 = new TypedByteArray("application/json", jsonObject3.toString().getBytes("UTF-8"));

            RestClient.getInstance(this).get().addmanualcard( token,in3, new Callback<AddManualCard_model>() {

                @Override
                public void success(final AddManualCard_model res, Response response) {
                    progressBarCustom.cancelProgress();

                    if(res.getResponseMessage().equalsIgnoreCase(Constant.SUCCESS))
                    {
                        Toast.makeText(AddEditCardActitivty.this, res.getMessage(), Toast.LENGTH_SHORT).show();
                        Intent intent=new Intent();
                        setResult(RESULT_OK,intent);
                        finish();
                    }else{
                        ProgressBarCustom.customDialog(AddEditCardActitivty.this,res.getMessage(),false);
                    }
                }

                @Override
                public void failure(final RetrofitError error) {
                    progressBarCustom.cancelProgress();
                    error.printStackTrace();
                    if (error != null) {
                        utils.showError(error);
                    }

                }
            });

        }catch (Exception e)
        {
            progressBarCustom.cancelProgress();
            //progress.cancelProgress();
            e.printStackTrace();
        }
    }



    private void callS3Bucket_Upload(final String filepath,String image_path) {
        //    final CustomProgress customProgress = CustomProgress.show(mcontext, "", false, false, null);
//
       final ProgressBarCustom progressBarCustom=new ProgressBarCustom(this);
        progressBarCustom.showProgress();

        String timeStamp = TimeUnit.MILLISECONDS.toSeconds(System.currentTimeMillis()) + "";
        System.out.println("timeStamp"+" "+timeStamp);

        String oririnalFile="Avoconnect_"+timeStamp+".jpg";
        String bucketPath="";
        if(image_path.equalsIgnoreCase("logoClick"))
        {
             bucketPath=Constant.CARDIMAGE+"/"+UserPrefrence.getInstance(this).getUserid()+"/"+oririnalFile;
            upload_user_card=bucketPath;
        }
        else if(image_path.equalsIgnoreCase("ocr_image"))
        {
            bucketPath=Constant.CARDIMAGE+"/"+UserPrefrence.getInstance(this).getUserid()+"/"+oririnalFile;
            ocr_image=bucketPath;
        }

        else{
             bucketPath=Constant.CARDIMAGE+"/"+UserPrefrence.getInstance(this).getUserid()+"/"+oririnalFile;
             upload_banner_card=bucketPath;
        }

        System.out.println("ORiginalpath"+" "+oririnalFile);

        File file=new File(filepath);
        observer = transferUtility.upload(Constant.BUCKETNAME, bucketPath, file,CannedAccessControlList.PublicRead);
        observer.setTransferListener(new TransferListener() {
            @Override
            public void onStateChanged(int id, TransferState state) {

                Log.e("status",state+" "+id);
                if (state.COMPLETED.equals(observer.getState())) {
                    progressBarCustom.cancelProgress();
                    s3.getUrl(Constant.BUCKETNAME,observer.getKey());
                    Log.e("State"," "+state+" "+id);
                    Log.e("State"," "+state+" "+id);
                    Log.e("checking",""+observer.getAbsoluteFilePath()+"\n "+observer.getKey()+"\n "+observer.getId()+"\n "+observer.getState()+"\n ");
                    Log.e("responce_id"," "+observer.getId()+" "+observer.getId());
                    Log.e("responce_id"," "+ s3.getUrl(Constant.BUCKETNAME,observer.getKey()));
                   String userImage=String.valueOf(s3.getUrl(Constant.BUCKETNAME,observer.getKey()));


//                    textViewProfile.setVisibility(View.GONE);
//
//                    if(filepath.exists())
//                    {
//                        personProfile.setImageURI(Uri.fromFile(filepath));
//                    }else
//                        personProfile.setImageBitmap(profileBitmap);
                }

            }

            @Override
            public void onProgressChanged(int id, long bytesCurrent, long bytesTotal) {

                long _bytesCurrent = bytesCurrent;
                long _bytesTotal = bytesTotal;
                float percentage =  ((float)_bytesCurrent /(float)_bytesTotal * 100);

                Log.e("percentage","" +percentage);

            }

            @Override
            public void onError(int id, Exception ex) {
               /* if (customProgress != null && customProgress.isShowing()) {
                    customProgress.dismiss();

                }*/
                progressBarCustom.cancelProgress();
                Toast.makeText(getApplicationContext(),R.string.error_message, Toast.LENGTH_SHORT).show();
                Log.e("error", ex.getMessage());
                final int sdk = Build.VERSION.SDK_INT;
                if(sdk < Build.VERSION_CODES.JELLY_BEAN) {
                    //   speakerphoto.setBackgroundDrawable(ContextCompat.getDrawable(mcontext, R.drawable.profile_icon_1) );
                } else {
                    //   speakerphoto.setBackground(ContextCompat.getDrawable(mcontext, R.drawable.profile_icon_1));
                }
            }
        });


    }


   public class GetImage extends AsyncTask<Void, Void, Void> {

        String url_image="";
        ProgressBarCustom progressBarCustom;
       public GetImage( String ocrImageOfManualCard) {
           this.url_image=ocrImageOfManualCard;
       }
       @Override
       protected void onPreExecute() {
           super.onPreExecute();
           progressBarCustom=new ProgressBarCustom(AddEditCardActitivty.this);
           progressBarCustom.showProgress();;
       }


       @Override
       protected Void doInBackground(Void... voids) {
           try {

               if (android.os.Build.VERSION.SDK_INT > 9) {
                   StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
                   StrictMode.setThreadPolicy(policy);
               }

               URL url = new URL(url_image);
               HttpURLConnection connection =(HttpURLConnection)url.openConnection();
               connection.setDoInput(true);
               connection.connect();
               InputStream input = connection.getInputStream();
               myBitmap= BitmapFactory.decodeStream(input);


           } catch (Exception e) {

               e.printStackTrace();

           }

           return null;
       }

       @Override
       protected void onPostExecute(Void aVoid) {
           super.onPostExecute(aVoid);
           if(progressBarCustom!=null)
           {
               progressBarCustom.cancelProgress();
           }
       }
   }


    private void generatepopoutside() {

        AlertDialog.Builder builder=new AlertDialog.Builder(AddEditCardActitivty.this);
        View view= LayoutInflater.from(this).inflate(R.layout.logout_lay,null);
        TextView txtDialogMessage=(TextView)view.findViewById(R.id.txtDialogMessage);
        txtDialogMessage.setText(getResources().getString(R.string.leavepage));
        Button btnDialogYes=(Button)view.findViewById(R.id.btnDialogYes);
        Button btnDialogNo=(Button)view.findViewById(R.id.btnDialogNo);
        builder.setView(view);
        final Dialog dialog=builder.create();
        dialog.setCancelable(false);
        dialog.setCanceledOnTouchOutside(false);

        btnDialogYes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
                finish();
               // finishAffinity();

            }
        });
        btnDialogNo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();

            }
        });
        dialog.show();


    }

    @Override
    public void onBackPressed() {
        generatepopoutside();
    }
}
