package com.avoconnect.activity;

import android.accounts.AccountManager;
import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;

import com.avoconnect.Constants.Constant;
import com.avoconnect.Prefrence.UserPrefrence;
import com.avoconnect.Util.ProgressBarCustom;
import com.avoconnect.Util.Utils;
import com.avoconnect.api.RestClient;
import com.avoconnect.responsemodel.DeleteEvent_Model;
import com.avoconnect.responsemodel.EventSynup_Model;
import com.avoconnect.responsemodel.GetAllEvent_Model;
import com.avoconnect.responsemodel.RecivedCard_model;
import com.avoconnect.responsemodel.ResourceDatum;
import com.avoconnect.responsemodel.UpdateEmail_Model;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.ItemTouchHelper;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.provider.CalendarContract;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.PopupMenu;
import android.widget.TextView;
import android.widget.Toast;

import com.avoconnect.R;
import com.avoconnect.adapter.EventListAdapter;
import com.avoconnect.beans.CalenderEventsBeans;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.api.client.extensions.android.http.AndroidHttp;
import com.google.api.client.googleapis.extensions.android.gms.auth.GoogleAccountCredential;
import com.google.api.client.googleapis.extensions.android.gms.auth.GooglePlayServicesAvailabilityIOException;
import com.google.api.client.googleapis.extensions.android.gms.auth.UserRecoverableAuthIOException;
import com.google.api.client.http.HttpTransport;
import com.google.api.client.json.JsonFactory;
import com.google.api.client.json.jackson2.JacksonFactory;
import com.google.api.client.util.DateTime;
import com.google.api.client.util.ExponentialBackOff;
import com.google.api.services.calendar.CalendarScopes;
import com.google.api.services.calendar.model.Event;
import com.google.api.services.calendar.model.Events;

import java.io.IOException;
import java.text.DateFormat;
import java.text.DateFormatSymbols;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import retrofit.RetrofitError;
import retrofit.client.Response;

public class EventsListActitivty extends AppCompatActivity implements View.OnClickListener {


    RecyclerView listview;
    FloatingActionButton card_add;
    TextView selected_month,txt_head;
    LinearLayout selectmonth_lay;
    EventListAdapter eventListAdapter;
    GetAllEvent_Model res_global;
    ImageView backimage;
    int currentmonth=0;
    ImageView noevent_img;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_events_list_actitivty);
        listview = (RecyclerView) findViewById(R.id.listview);

        card_add = (FloatingActionButton) findViewById(R.id.card_add);
        selected_month = (TextView) findViewById(R.id.selected_month);
        txt_head= (TextView) findViewById(R.id.txt_head);
        selectmonth_lay = (LinearLayout) findViewById(R.id.selectmonth_lay);

        backimage=(ImageView)findViewById(R.id.backimage);
        noevent_img=(ImageView)findViewById(R.id.noevent_img);
        backimage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
        selectmonth_lay.setOnClickListener(this);

        card_add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent=new Intent(EventsListActitivty.this,EventaddupdateAcitivity.class);
                startActivityForResult(intent,145);
            }
        });

        Calendar calendar=Calendar.getInstance();
        SimpleDateFormat month_date = new SimpleDateFormat("MMM");
        String month_name = month_date.format(calendar.getTime());

        int year = calendar.get(Calendar.YEAR);

        selected_month.setText(month_name+","+year);


        DateFormat dateFormat = new SimpleDateFormat("MM");
        Date date = new Date();
        currentmonth= Integer.valueOf(dateFormat.format(date));

        getEventList();



    }

    private void getEventList() {

        final ProgressBarCustom progressBarCustom=new ProgressBarCustom(EventsListActitivty.this);
        progressBarCustom.showProgress();
        try {
            String token = UserPrefrence.getInstance(this).getAuthtoken();
            token="bearer " + token;
            RestClient.getInstance(this).get().getAllevents( token,UserPrefrence.getInstance(this).getUserid(), currentmonth,new retrofit.Callback<GetAllEvent_Model>() {

                @Override
                public void success(final GetAllEvent_Model res, Response response) {
                    progressBarCustom.cancelProgress();

                    if(res.getResponseMessage().equalsIgnoreCase(Constant.SUCCESS))
                    {
                        if(res.getResourceData().size()>0)
                        {
                            listview.setVisibility(View.VISIBLE);
                            noevent_img.setVisibility(View.GONE);
                            res_global=res;
                            setAdapter(res_global);
                        }else{
                            txt_head.setVisibility(View.GONE);
                            listview.setVisibility(View.GONE);
                            noevent_img.setVisibility(View.VISIBLE);
                        }

                    }else{
                        txt_head.setVisibility(View.GONE);
                        listview.setVisibility(View.GONE);
                        noevent_img.setVisibility(View.VISIBLE);
                        //Toast.makeText(EventsListActitivty.this, res.getMessage(), Toast.LENGTH_SHORT).show();
                    }

                }
                @Override
                public void failure(final RetrofitError error) {
                    progressBarCustom.cancelProgress();
                    error.printStackTrace();
                    if (error != null) {
                        Utils utils=new Utils(EventsListActitivty.this);
                        utils.showError(error);
                    }

                }
            });
        }catch (Exception e)
        {
            progressBarCustom.cancelProgress();
            //progress.cancelProgress();
            e.printStackTrace();
        }

    }

    private void setAdapter(GetAllEvent_Model res) {
         eventListAdapter = new EventListAdapter(EventsListActitivty.this,res);
        listview.setAdapter(eventListAdapter);
        listview.setLayoutManager(new LinearLayoutManager(EventsListActitivty.this));
        listview.setNestedScrollingEnabled(false);
        listview.setHasFixedSize(false);


        eventListAdapter.onCallBackReturn(new EventListAdapter.Callback() {
            @Override
            public void clickaction(final int position,int eventid) {

                callApiUpdateEventStatus(position,res_global.getResourceData().get(position).getTime(),eventid);
            }

            @Override
            public void deleteevent(int position, int eventid) {
                Log.e("========",position+" "+eventid);
                generatepop(position,eventid);
            }
        });

       enableSwipe();
    }


    private void generatepop(final int position, final int cardid) {
        AlertDialog.Builder builder=new AlertDialog.Builder(EventsListActitivty.this);
        View view= LayoutInflater.from(EventsListActitivty.this).inflate(R.layout.logout_lay,null);
        TextView txtDialogMessage=(TextView)view.findViewById(R.id.txtDialogMessage);
        txtDialogMessage.setText(getResources().getString(R.string.delete_Event));
        Button btnDialogYes=(Button)view.findViewById(R.id.btnDialogYes);
        Button btnDialogNo=(Button)view.findViewById(R.id.btnDialogNo);
        builder.setView(view);
        final Dialog dialog=builder.create();
        dialog.setCancelable(false);
        dialog.setCanceledOnTouchOutside(false);

        btnDialogYes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
               DeleteEventApi(cardid,position);

            }
        });
        btnDialogNo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
                res_global.getResourceData().get(position).setDeleteButtonShow(false);
                eventListAdapter.notifyDataSetChanged();
            }
        });
        dialog.show();

    }

    public void DeleteEventApi(int carid, final int position)
    {
        final ProgressBarCustom progressBarCustom=new ProgressBarCustom(EventsListActitivty.this);
        progressBarCustom.showProgress();

        try {
            String token = UserPrefrence.getInstance(EventsListActitivty.this).getAuthtoken();
            token="bearer " + token;

            RestClient.getInstance(this).get().delete_event(token,
                    UserPrefrence.getInstance(EventsListActitivty.this).getUserid(),
                    carid+"",new retrofit.Callback<DeleteEvent_Model>() {

                        @Override
                        public void success(final DeleteEvent_Model res, Response response) {
                            progressBarCustom.cancelProgress();
                            if(res.getResponseMessage().equalsIgnoreCase(Constant.SUCCESS))
                            {
                                Handler handler = new Handler();
                                handler.post(new Runnable() {
                                    @Override
                                    public void run() {
                                        res_global.getResourceData().remove(position);
                                        eventListAdapter.notifyDataSetChanged();

                                        if(res_global.getResourceData().size()==0)
                                        {
                                            txt_head.setVisibility(View.GONE);
                                            listview.setVisibility(View.GONE);
                                            noevent_img.setVisibility(View.VISIBLE);
                                        }
                                    }
                                });
                                Toast.makeText(EventsListActitivty.this, res.getMessage(), Toast.LENGTH_SHORT).show();
                            }else{
                                Toast.makeText(EventsListActitivty.this, res.getMessage(), Toast.LENGTH_SHORT).show();
                            }
                        }

                        @Override
                        public void failure(final RetrofitError error) {
                            progressBarCustom.cancelProgress();
                            if (error != null) {
                                Utils utils=new Utils(EventsListActitivty.this);
                                utils.showError(error);
                            }
                        }
                    });

        }catch (Exception e)
        {
            progressBarCustom.cancelProgress();
            //progress.cancelProgress();
            e.printStackTrace();
        }
    }

    private void callApiUpdateEventStatus(final int postion,final String time,final  int eventid) {


        final ProgressBarCustom progressBarCustom=new ProgressBarCustom(EventsListActitivty.this);
        progressBarCustom.showProgress();
        try {
            String token = UserPrefrence.getInstance(this).getAuthtoken();
            token="bearer " + token;
            RestClient.getInstance(this).get().syncevent( token,eventid+"", new retrofit.Callback<EventSynup_Model>() {

                @RequiresApi(api = Build.VERSION_CODES.O)
                @Override
                public void success(final EventSynup_Model res, Response response) {
                    progressBarCustom.cancelProgress();

                    if(res.getResponseMessage().equalsIgnoreCase(Constant.SUCCESS))
                    {
                        Handler handler = new Handler();
                        handler.post(new Runnable() {
                            @Override
                            public void run() {
                                res_global.getResourceData().get(postion).setPublish(true);

                                if(eventListAdapter!=null)
                                {
                                    eventListAdapter.notifyDataSetChanged();
                                }
                            }
                        });
                        addEvent(time,postion);
                    }else{
                        Toast.makeText(EventsListActitivty.this, res.getMessage(), Toast.LENGTH_SHORT).show();
                    }

                }
                @Override
                public void failure(final RetrofitError error) {
                    progressBarCustom.cancelProgress();
                    error.printStackTrace();
                    if (error != null) {
                        Utils utils=new Utils(EventsListActitivty.this);
                        utils.showError(error);
                    }

                }
            });
        }catch (Exception e)
        {
            progressBarCustom.cancelProgress();
            //progress.cancelProgress();
            e.printStackTrace();
        }




    }

    private void enableSwipe() {
        ItemTouchHelper.SimpleCallback simpleItemTouchCallback = new ItemTouchHelper.SimpleCallback(0, ItemTouchHelper.LEFT | ItemTouchHelper.RIGHT) {
            @Override
            public boolean onMove(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder, RecyclerView.ViewHolder target) {
                return false;
            }
            @Override
            public void onSwiped(RecyclerView.ViewHolder viewHolder, int direction) {
                int position = viewHolder.getAdapterPosition();

                    if (direction == ItemTouchHelper.LEFT) {
                        final int deletedPosition = position;
                        if (res_global.getResourceData().size() > 0) {
                            for (int i = 0; i < res_global.getResourceData().size(); i++) {

                                if (i == deletedPosition) {
                                    res_global.getResourceData().get(i).setDeleteButtonShow(true);
                                } else {
                                    res_global.getResourceData().get(i).setDeleteButtonShow(false);
                                }

                            }
                            if (eventListAdapter != null) {
                                eventListAdapter.notifyDataSetChanged();
                            }
                        }
                    } else {
                        if (res_global.getResourceData().size() > 0) {
                            for (int i = 0; i < res_global.getResourceData().size(); i++) {
                                res_global.getResourceData().get(i).setDeleteButtonShow(false);

                            }
                            if (eventListAdapter != null) {
                                eventListAdapter.notifyDataSetChanged();
                            }
                        }
                    }

            }
            @Override
            public int getMovementFlags(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder) {
                int position = viewHolder.getAdapterPosition();
                return makeMovementFlags(0, ItemTouchHelper.LEFT | ItemTouchHelper.RIGHT);

            }
        };
        ItemTouchHelper itemTouchHelper = new ItemTouchHelper(simpleItemTouchCallback);
        itemTouchHelper.attachToRecyclerView(listview);
    }


    @RequiresApi(api = Build.VERSION_CODES.O)
    private void addEvent(String time, int postion) {

        try {
            String idArr[] = time.split(":");
            Calendar beginTime = Calendar.getInstance();

            String tile = res_global.getResourceData().get(postion).getServerDate();
            tile = tile.substring(0, tile.indexOf("+"));
            DateTimeFormatter inputFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss.SSS", Locale.ENGLISH);
            DateTimeFormatter outputFormatter = DateTimeFormatter.ofPattern("dd-MM-yyy", Locale.ENGLISH);
            LocalDate date = LocalDate.parse(tile, inputFormatter);
            String formattedDate = outputFormatter.format(date);

            String idArr_date[] = formattedDate.split("-");


            beginTime.set(Integer.parseInt(idArr_date[2]), Integer.parseInt(idArr_date[1])-1, Integer.parseInt(idArr_date[0]), Integer.parseInt(idArr[0]), Integer.parseInt(idArr[1]));

            Calendar endTime = Calendar.getInstance();
            endTime.set(Integer.parseInt(idArr_date[2]), Integer.parseInt(idArr_date[1])-1, Integer.parseInt(idArr_date[0]), Integer.parseInt(idArr[0]), Integer.parseInt(idArr[1]));

            Intent intent = new Intent(Intent.ACTION_INSERT)
                    .setData(CalendarContract.Events.CONTENT_URI)
                    .putExtra(CalendarContract.EXTRA_EVENT_BEGIN_TIME,
                            beginTime.getTimeInMillis())
                    .putExtra(CalendarContract.EXTRA_EVENT_END_TIME,
                            endTime.getTimeInMillis())
                    .putExtra(CalendarContract.Events.TITLE, res_global.getResourceData().get(postion).getTitle())
                    .putExtra(CalendarContract.Events.DESCRIPTION, res_global.getResourceData().get(postion).getNote())
                    .putExtra(CalendarContract.Events.EVENT_LOCATION, "")
                    .putExtra(CalendarContract.Events.AVAILABILITY, CalendarContract.Events.AVAILABILITY_BUSY)
                    .putExtra(Intent.EXTRA_EMAIL, res_global.getResourceData().get(postion).getEmail());

            startActivity(intent);
        }catch (Exception e)
        {
            e.printStackTrace();
        }

    }

    public void showPopup(View v) {



        PopupMenu popup = new PopupMenu(this, v);
        Calendar cal = Calendar.getInstance();

        DateFormat dateFormat = new SimpleDateFormat("MM");
        Date date = new Date();
       int currentmonth_selected= Integer.valueOf(dateFormat.format(date));
        for(int k=currentmonth_selected;k<=12;k++)
        {
            String  monthString = new DateFormatSymbols().getMonths()[k-1];
            monthString = monthString.substring(0, 3);
            Log.e("Current Month in MMM" , monthString+" "+k);

            Calendar calendar = Calendar.getInstance();
            int year = calendar.get(Calendar.YEAR);
            popup.getMenu().add(Menu.NONE, 1, Menu.NONE, monthString+","+year);

        }


        popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
            public boolean onMenuItemClick(MenuItem item) {
                selected_month.setText(item.getTitle());
                Utils utils=new Utils(EventsListActitivty.this);

                Calendar calendar=Calendar.getInstance();
                int year = calendar.get(Calendar.YEAR);

                currentmonth=utils.getMonth(item.getTitle().toString(),year);
                Log.e("======curentmonth",currentmonth+" ");
                getEventList();
                return true;
            }
        });
        popup.show();
    }


    @Override
    public void onClick(View view) {
        switch (view.getId())
        {
            case R.id.selectmonth_lay:
                showPopup(view);
                break;
        }
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        if (requestCode == 145 && resultCode == RESULT_OK) {
            {
                getEventList();
            }
        }
    }
}
