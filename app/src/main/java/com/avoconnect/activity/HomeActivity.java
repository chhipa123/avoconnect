package com.avoconnect.activity;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.ActivityNotFoundException;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.graphics.Color;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;

import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.avoconnect.BottomSheetFragments.ShareCardFragment;
import com.avoconnect.Constants.Constant;
import com.avoconnect.Fragments.MyCard_Fragment;
import com.avoconnect.Fragments.Profile_Fragment;
import com.avoconnect.Fragments.ReceieveCard_Fragment;
import com.avoconnect.Fragments.Search_Fragment;
import com.avoconnect.Prefrence.UserPrefrence;
import com.avoconnect.R;
import com.avoconnect.Util.ProgressBarCustom;
import com.avoconnect.Util.Utils;
import com.avoconnect.api.RestClient;
import com.avoconnect.fcm.MyFirebaseMessagingService;
import com.avoconnect.responsemodel.Logout_Model;
import com.avoconnect.responsemodel.ShareCard_Model;
import com.avoconnect.responsemodel.ShareMyUserId_Model;
import com.avoconnect.responsemodel.ShareNotificationUserid_Model;
import com.avoconnect.responsemodel.UnreadNotficationCount_Model;
import com.google.android.gms.common.GoogleApiAvailability;
import com.google.android.gms.vision.text.Line;
import com.google.api.client.repackaged.org.apache.commons.codec.binary.Base64;
import com.twitter.sdk.android.core.models.User;

import org.jsoup.Jsoup;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

public class HomeActivity extends AppCompatActivity implements View.OnClickListener, Profile_Fragment.Profilecallback {

    LinearLayout img_mycard,img_receice_card,img_search,img_profile,nav_events,nav_temp;
    FrameLayout container;
    DrawerLayout mDrawerLayout;
    LinearLayout mainlayout;
    TextView txt_mycard,txt_recievecard,txt_search,txt_profile,txt_mycard_head;
    View view_line_tab1,view_line_tab2,view_line_tab3,view_line_tab4;
    Utils utils;
    ImageView navigation_icon,notification_icon;
    RelativeLayout notification_bar;
    String uri;
    TextView txt_badge;
    LinearLayout nav_userid,nav_bussiness,nav_setting,nav_support,nav_rateus,nav_terms,nav_aboutapp,nav_logout,nav_desktop;
    boolean iscurrentfragment_mycard=true;
    String currentVersion;
    boolean onbackfirstfragmentcheck=true;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        inits();


            HomeActivity.this.runOnUiThread(new Runnable() {
                public void run() {
                    try {
                        currentVersion = getPackageManager().getPackageInfo(getPackageName(), 0).versionName;
                    } catch (PackageManager.NameNotFoundException e) {
                        e.printStackTrace();
                    }
                    new GetVersionCode().execute();
                }
            });

        // UserPrefrence.getInstance(this).setUsertype(Constant.USERTYPE_PREMIUM);
        changeFragment();

       // Uri data = this.getIntent().getData();
        Intent intent=getIntent();
        if (intent != null ) {
            try {

                String card_id="";
                String userid="";


                if (intent.getStringExtra("cardId") != null) {
                    card_id=intent.getStringExtra("cardId");


                }
                if (intent.getStringExtra("senderId") != null) {
                    /*if card and sender this case belong to share card from emal othera*/
                    if(card_id!=null&&card_id.length()>0) {
                        userid = intent.getStringExtra("senderId");
                        callApiReceiveCard(card_id, userid);
                    }
                }

                /* Shaare My Userid*/
                if(intent.getStringExtra("userId") != null)
                {
                    userid = intent.getStringExtra("userId");
                    callApisharemyUserid(userid);
                }

                if(intent.getStringExtra("sender")!=null)
                {
                    if(intent.getStringExtra("clickAction")!=null)
                    {

                    }
                }


            }catch (Exception e)
            {
                e.printStackTrace();
            }
        }






    }

    private void callApisharemyUserid(String userid) {
        try {
            String token = UserPrefrence.getInstance(HomeActivity.this).getAuthtoken();
            token="bearer " + token;

            byte[] valueDecoded2 = Base64.decodeBase64(userid);
            String value2=new String(valueDecoded2);

            RestClient.getInstance(HomeActivity.this).get().sendNotificationToShareMyUserId( token,value2,
                    UserPrefrence.getInstance(HomeActivity.this).getUserid()+"",
                    new retrofit.Callback<ShareNotificationUserid_Model>() {

                        @Override
                        public void success(final ShareNotificationUserid_Model res, Response response) {

                            if(res.getResponseMessage().equalsIgnoreCase(Constant.SUCCESS))
                            {
                                Toast.makeText(HomeActivity.this, res.getMessage(), Toast.LENGTH_SHORT).show();
                            }else{
                                Toast.makeText(HomeActivity.this, res.getMessage(), Toast.LENGTH_SHORT).show();
                            }

                        }
                        @Override
                        public void failure(final RetrofitError error) {

                            error.printStackTrace();
                            if (error != null) {
                                Utils utils=new Utils(HomeActivity.this);
                                utils.showError(error);
                            }

                        }
                    });
        }catch (Exception e)
        {
            //progress.cancelProgress();
            e.printStackTrace();
        }

    }


    private void setFonts() {

        utils.setTextview(txt_mycard,1);
        utils.setTextview(txt_recievecard,1);
        utils.setTextview(txt_search,1);
        utils.setTextview(txt_profile,1);
       // utils.setTextview(txt_mycard_head,1);

    }

    private void changeFragment() {
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        Fragment fragment = new MyCard_Fragment();
        fragmentTransaction.add(R.id.container, fragment, "First");
        fragmentTransaction.commit();


        new BackgroundCallgetUnreadNotifcation().execute();
    }

    private void inits() {
        utils=new Utils(this);

        LocalBroadcastManager.getInstance(this).registerReceiver(notificationCounterBroadcast
                , new IntentFilter("notificattions"));

        txt_badge=(TextView)findViewById(R.id.txt_badge);
        img_mycard=(LinearLayout)findViewById(R.id.img_tab1);
        img_receice_card=(LinearLayout)findViewById(R.id.img_tab2);
        img_search=(LinearLayout)findViewById(R.id.img_tab3);
        img_profile=(LinearLayout)findViewById(R.id.img_tab4);
        container=(FrameLayout)findViewById(R.id.container);
        mainlayout=(LinearLayout) findViewById(R.id.mainlayout);
        nav_events=(LinearLayout) findViewById(R.id.nav_events);
        txt_mycard=(TextView)findViewById(R.id.txt_mycard);
        txt_profile=(TextView)findViewById(R.id.txt_profile);
        txt_recievecard=(TextView)findViewById(R.id.txt_recievecard);
        txt_search=(TextView)findViewById(R.id.txt_search);
        txt_mycard_head=(TextView)findViewById(R.id.txt_mycard_head);
        notification_bar=(RelativeLayout)findViewById(R.id.notification_bar);
        navigation_icon=(ImageView)findViewById(R.id.navigation_icon);
        notification_icon=(ImageView)findViewById(R.id.notification_icon);
        nav_desktop=(LinearLayout)findViewById(R.id.nav_desktop);

        view_line_tab1=(View)findViewById(R.id.view_line_tab1);
        view_line_tab2=(View)findViewById(R.id.view_line_tab2);
        view_line_tab3=(View)findViewById(R.id.view_line_tab3);
        view_line_tab4=(View)findViewById(R.id.view_line_tab4);

        nav_temp=(LinearLayout)findViewById(R.id.nav_temp);
        nav_userid=(LinearLayout)findViewById(R.id.nav_userid);
        nav_bussiness=(LinearLayout)findViewById(R.id.nav_bussiness);
        nav_setting=(LinearLayout)findViewById(R.id.nav_setting);
        nav_support=(LinearLayout)findViewById(R.id.nav_support);
        nav_rateus=(LinearLayout)findViewById(R.id.nav_rateus);
        nav_terms=(LinearLayout)findViewById(R.id.nav_terms);
        nav_aboutapp=(LinearLayout)findViewById(R.id.nav_aboutapp);
        nav_logout=(LinearLayout)findViewById(R.id.nav_logout);

        nav_userid.setOnClickListener(this);
        nav_bussiness.setOnClickListener(this);
        nav_setting.setOnClickListener(this);
        nav_support.setOnClickListener(this);
        nav_rateus.setOnClickListener(this);
        nav_terms.setOnClickListener(this);
        nav_aboutapp.setOnClickListener(this);
        nav_logout.setOnClickListener(this);
        nav_temp.setOnClickListener(this);
        nav_desktop.setOnClickListener(this);

        img_mycard.setOnClickListener(this);
        img_receice_card.setOnClickListener(this);
        img_search.setOnClickListener(this);
        img_profile.setOnClickListener(this);
        navigation_icon.setOnClickListener(this);
        notification_icon.setOnClickListener(this);
        nav_events.setOnClickListener(this);
        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        mDrawerLayout.setScrimColor(Color.TRANSPARENT);
        mDrawerLayout.setDrawerElevation(0f);
//        ActionBarDrawerToggle toggle=new ActionBarDrawerToggle(this,mDrawerLayout,R.string.navigation_drawer_open, R.string.navigation_drawer_close){
//            private float scaleFactor = 6f;
//            @Override
//            public void onDrawerSlide(View drawerView, float slideOffset) {
//                super.onDrawerSlide(drawerView, slideOffset);
//                float slideX = drawerView.getWidth()* slideOffset;
//                mainlayout.setTranslationX(slideX);
//
//            }
//        };

        ActionBarDrawerToggle actionBarDrawerToggle = new ActionBarDrawerToggle(this, mDrawerLayout, R.string.navigation_drawer_open, R.string.navigation_drawer_close) {
            private float scaleFactor = 3f;

            @Override
            public void onDrawerSlide(View drawerView, float slideOffset) {
                super.onDrawerSlide(drawerView, slideOffset);
                float slideX = drawerView.getWidth() * slideOffset;
                mainlayout.setTranslationX(slideX);
                mainlayout.setScaleX(1 - (slideOffset / scaleFactor));
                mainlayout.setScaleY(1 - (slideOffset / scaleFactor));
            }
        };
        mDrawerLayout.addDrawerListener(actionBarDrawerToggle);

        setFonts();


        if(UserPrefrence.getInstance(this).getUsertype().equalsIgnoreCase(Constant.USERTYPE_CORPORATE))
        {
            img_search.setVisibility(View.GONE);
        }

        setBadge();
    }

    private void setBadge() {
        if(UserPrefrence.getInstance(this).getNotificationUnread().length()>0)
        {
            txt_badge.setVisibility(View.VISIBLE);
        }else{
            txt_badge.setVisibility(View.GONE);
        }
    }

    @SuppressLint("WrongConstant")
    @Override
    public void onClick(View view) {
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        Fragment fragment;
        switch (view.getId())
        {

            case R.id.img_tab1:
                onbackfirstfragmentcheck=true;
                iscurrentfragment_mycard=true;
                notification_bar.setVisibility(View.VISIBLE);
                notification_bar.setBackgroundColor(getResources().getColor(R.color.background_color));
                txt_mycard_head.setText(getResources().getString(R.string.tab1_mycards));
                changeImagebackground(view_line_tab1);
                fragment = new MyCard_Fragment();
                fragmentTransaction.replace(R.id.container, fragment, "First");
                fragmentTransaction.commit();
                break;
            case R.id.img_tab2:
                onbackfirstfragmentcheck=false;
                iscurrentfragment_mycard=false;
                notification_bar.setVisibility(View.VISIBLE);
                notification_bar.setBackgroundColor(getResources().getColor(R.color.background_color));
                txt_mycard_head.setText(getResources().getString(R.string.tab2_reciverd));
                changeImagebackground(view_line_tab2);
                fragment = new ReceieveCard_Fragment();
                fragmentTransaction.replace(R.id.container, fragment, "First");
                fragmentTransaction.commit();

                break;
            case R.id.img_tab3:
                onbackfirstfragmentcheck=false;
                iscurrentfragment_mycard=false;
                notification_bar.setVisibility(View.VISIBLE);
                notification_bar.setBackgroundColor(getResources().getColor(R.color.background_color));
                txt_mycard_head.setText(getResources().getString(R.string.serachuser));
                changeImagebackground(view_line_tab3);
                fragment = new Search_Fragment();
                fragmentTransaction.replace(R.id.container, fragment, "First");
                fragmentTransaction.commit();
                break;
            case R.id.img_tab4:
                onbackfirstfragmentcheck=false;
                iscurrentfragment_mycard=false;
                notification_bar.setVisibility(View.GONE);
                notification_bar.setBackgroundColor(Color.parseColor("#00000000"));
                txt_mycard_head.setText(getResources().getString(R.string.tab4));
                changeImagebackground(view_line_tab4);
                fragment = new Profile_Fragment();
                fragmentTransaction.replace(R.id.container, fragment, "First");
                fragmentTransaction.commit();

                break;

            case R.id.navigation_icon:
                if(!mDrawerLayout.isDrawerOpen(Gravity.START)) mDrawerLayout.openDrawer(Gravity.START);
                else mDrawerLayout.closeDrawer(Gravity.END);
                break;
            case R.id.notification_icon:

                Intent intent=new Intent(HomeActivity.this,Notification_Activity.class);
                startActivity(intent);
                break;
            case R.id.nav_userid:
                Intent intent1=new Intent(HomeActivity.this,NavigationHandler_Actitivty.class);
                startActivity(intent1);
                break;
            case R.id.nav_bussiness:
                String url = Constant.BUSSINESSURL;
                Intent i = new Intent(Intent.ACTION_VIEW);
                i.setData(Uri.parse(url));
                startActivity(i);
                break;
            case R.id.nav_setting:
                Intent intent11=new Intent(HomeActivity.this,SettingActivity.class);
                startActivity(intent11);
                break;
            case R.id.nav_support:


                String recepientEmail = "avoconnect@gmail.com"; // either set to destination email or leave empty
                Intent share = new Intent(Intent.ACTION_SENDTO);
                share.setData(Uri.parse("mailto:" + recepientEmail));
                share.putExtra(android.content.Intent.EXTRA_SUBJECT, "Avoconnect Support");
                share.putExtra(android.content.Intent.EXTRA_TEXT, "Type some message here");
                try {
                    startActivity(share);
                } catch (android.content.ActivityNotFoundException ex) {
                    Toast.makeText(this, "Mail account not configured", Toast.LENGTH_LONG).show();
                }


                break;
            case R.id.nav_temp:
                Intent intent2=new Intent(HomeActivity.this,TemplateActivity.class);
                startActivity(intent2);

                break;
            case R.id.nav_rateus:

                Uri uri = Uri.parse("market://details?id=" + this.getPackageName());
                Intent goToMarket = new Intent(Intent.ACTION_VIEW, uri);
                // To count with Play market backstack, After pressing back button,
                // to taken back to our application, we need to add following flags to intent.
                goToMarket.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY |
                        Intent.FLAG_ACTIVITY_NEW_DOCUMENT |
                        Intent.FLAG_ACTIVITY_MULTIPLE_TASK);
                try {
                    startActivity(goToMarket);
                } catch (ActivityNotFoundException e) {
                    startActivity(new Intent(Intent.ACTION_VIEW,
                            Uri.parse("https://play.google.com/store/apps/details?id=" + "com.wandoos")));
                }

                break;

            case R.id.nav_terms:
                Intent intent4=new Intent(HomeActivity.this,WebView_Activity.class);
                intent4.putExtra("type",getResources().getString(R.string.nav_share_termcondition));
                intent4.putExtra("url",Constant.TERMSCONDITIOURL);
                startActivity(intent4);
                break;

            case R.id.nav_aboutapp:
                Intent intent5=new Intent(HomeActivity.this,WebView_Activity.class);
                intent5.putExtra("type",getResources().getString(R.string.nav_share_aboutapp));
                intent5.putExtra("url",Constant.ABOUTAPPURL);
                startActivity(intent5);

                break;
            case R.id.nav_logout:
                generatepop();
                break;
            case R.id.nav_events:
                Intent intent0=new Intent(HomeActivity.this,EventsListActitivty.class);
                startActivity(intent0);
                break;
            case R.id.nav_desktop:
                if(UserPrefrence.getInstance(HomeActivity.this).getUsertype().equalsIgnoreCase(Constant.USERTYPE_PREMIUM)
                        ||UserPrefrence.getInstance(HomeActivity.this).getUsertype().equalsIgnoreCase(Constant.USERTYPE_CORPORATE)
                ){
                   Intent intent3=new Intent(HomeActivity.this,LoginWithQrCode.class);
                   startActivity(intent3);
                }else{
                    Utils.customYesDialog(this,HomeActivity.this);
                }
                break;
            case R.id.txt_upgrade:
                if(Utils.yesNoDialog!=null&&Utils.yesNoDialog.isShowing()==true)
                {
                    Intent intent101=new Intent(HomeActivity.this, PlanUserSubcription_Act.class);
                    startActivityForResult(intent101,155);
                    Utils.yesNoDialog.dismiss();
                }
                break;

        }
    }

    private void generatepop() {
        AlertDialog.Builder builder=new AlertDialog.Builder(HomeActivity.this);
        View view= LayoutInflater.from(this).inflate(R.layout.logout_lay,null);
        TextView txtDialogMessage=(TextView)view.findViewById(R.id.txtDialogMessage);
        Button btnDialogYes=(Button)view.findViewById(R.id.btnDialogYes);
        Button btnDialogNo=(Button)view.findViewById(R.id.btnDialogNo);
        builder.setView(view);
        final Dialog dialog=builder.create();
        dialog.setCancelable(false);
        dialog.setCanceledOnTouchOutside(false);

        btnDialogYes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();

                callApiLgout();

            }
        });
        btnDialogNo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });
        dialog.show();

    }

    private void callApiLgout() {
        final ProgressBarCustom progressBarCustom=new ProgressBarCustom(HomeActivity.this);
        progressBarCustom.showProgress();
        try {
            String token = UserPrefrence.getInstance(HomeActivity.this).getAuthtoken();
            token="bearer " + token;
            RestClient.getInstance(HomeActivity.this).get().logoutapi(token
                    ,new Callback<Logout_Model>() {

                        @Override
                        public void success(final Logout_Model res, Response response) {
                            progressBarCustom.cancelProgress();

                            if(res.getResponseMessage().equalsIgnoreCase(Constant.SUCCESS))
                            {
                                  UserPrefrence.getInstance(HomeActivity.this).logoutDone();
                                 Intent intent=new Intent(HomeActivity.this,Login_Activity.class);
                                 intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                                 startActivity(intent);
                                 finish();

                            }else{
                                Toast.makeText(HomeActivity.this, res.getMessage(), Toast.LENGTH_SHORT).show();
                            }

                        }

                        @Override
                        public void failure(final RetrofitError error) {
                            progressBarCustom.cancelProgress();
                            error.printStackTrace();
                            if (error != null) {
                                Utils utils=new Utils(HomeActivity.this);
                                utils.showError(error);
                            }
                        }
                    });

        }catch (Exception e)
        {
            progressBarCustom.cancelProgress();
            e.printStackTrace();
        }
    }


    private void changeImagebackground(View view) {
        view_line_tab1.setVisibility(View.INVISIBLE);
        view_line_tab2.setVisibility(View.INVISIBLE);
        view_line_tab3.setVisibility(View.INVISIBLE);
        view_line_tab4.setVisibility(View.INVISIBLE);

        view.setVisibility(View.VISIBLE);

      //  txt_mycard_head.setText(getResources().getString(R.string.mycard_heading));
    }

    @SuppressLint("WrongConstant")
    @Override
    public void onBackPressed() {
        if(mDrawerLayout.isDrawerOpen(Gravity.START))
        {
            mDrawerLayout.closeDrawers();
        }else{
            if(onbackfirstfragmentcheck==false)
            {
                onbackfirstfragmentcheck=true;
                iscurrentfragment_mycard=true;
                notification_bar.setVisibility(View.VISIBLE);
                notification_bar.setBackgroundColor(getResources().getColor(R.color.background_color));
                txt_mycard_head.setText(getResources().getString(R.string.tab1_mycards));
                changeImagebackground(view_line_tab1);

                FragmentManager fragmentManager = getSupportFragmentManager();
                FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                Fragment fragment;

                fragment = new MyCard_Fragment();
                fragmentTransaction.replace(R.id.container, fragment, "First");
                fragmentTransaction.commit();
            }else{
               finishAffinity();
            }
          //  generatepopoutside();

        }
    }

    private void generatepopoutside() {

            AlertDialog.Builder builder=new AlertDialog.Builder(HomeActivity.this);
            View view= LayoutInflater.from(this).inflate(R.layout.logout_lay,null);
            TextView txtDialogMessage=(TextView)view.findViewById(R.id.txtDialogMessage);
            txtDialogMessage.setText(getResources().getString(R.string.ask_outsideapp));
            Button btnDialogYes=(Button)view.findViewById(R.id.btnDialogYes);
            Button btnDialogNo=(Button)view.findViewById(R.id.btnDialogNo);
            builder.setView(view);
            final Dialog dialog=builder.create();
            dialog.setCancelable(false);
            dialog.setCanceledOnTouchOutside(false);

            btnDialogYes.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    dialog.dismiss();
                    finishAffinity();

                }
            });
            btnDialogNo.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    dialog.dismiss();
                }
            });
            dialog.show();


    }

    private void callApiReceiveCard(String card_id,String sender_id) {
        final ProgressBarCustom progressBarCustom=new ProgressBarCustom(HomeActivity.this);
        progressBarCustom.showProgress();
        try {

            byte[] valueDecoded = Base64.decodeBase64(card_id);
            String value=new String(valueDecoded);

            byte[] valueDecoded2 = Base64.decodeBase64(sender_id);
            String value2=new String(valueDecoded2);

            String token = UserPrefrence.getInstance(HomeActivity.this).getAuthtoken();
            token="bearer " + token;
            RestClient.getInstance(HomeActivity.this).get().sharecard_id(token,value2,UserPrefrence.getInstance(HomeActivity.this).getAvoid()
                    ,value,new Callback<ShareCard_Model>() {

                        @Override
                        public void success(final ShareCard_Model res, Response response) {
                            progressBarCustom.cancelProgress();

                            if(res.getResponseMessage().equalsIgnoreCase(Constant.SUCCESS))
                            {
                                Toast.makeText(HomeActivity.this, res.getMessage(), Toast.LENGTH_SHORT).show();
                            }else{
                                Toast.makeText(HomeActivity.this, res.getMessage(), Toast.LENGTH_SHORT).show();
                            }

                        }

                        @Override
                        public void failure(final RetrofitError error) {
                            progressBarCustom.cancelProgress();
                            error.printStackTrace();
                            if (error != null) {
                                Utils utils=new Utils(HomeActivity.this);
                                utils.showError(error);
                            }
                        }
                    });
        }catch (Exception e)
        {
            progressBarCustom.cancelProgress();
            e.printStackTrace();
        }
    }


    @SuppressLint("WrongConstant")
    @Override
    public void clickaction(int position) {
        if(position==0)
        {
//            NAvigation

            if(!mDrawerLayout.isDrawerOpen(Gravity.START)) mDrawerLayout.openDrawer(Gravity.START);
            else mDrawerLayout.closeDrawer(Gravity.END);
        }else if(position==1){
//            Notification
            Intent intent=new Intent(HomeActivity.this,Notification_Activity.class);
            startActivity(intent);

        }
    }


    public BroadcastReceiver notificationCounterBroadcast = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent != null) {
                String click_action = intent.getStringExtra("click_action");
                Log.e("=======checlik",click_action+"");
                UserPrefrence.getInstance(HomeActivity.this).setNotificationUnread(click_action);
                txt_badge.setVisibility(View.VISIBLE);
            }
        }

    };


    @Override
    public void onDestroy() {
        LocalBroadcastManager.getInstance(this).unregisterReceiver(notificationCounterBroadcast);
        super.onDestroy();

    }

    @Override
    protected void onRestart() {
        super.onRestart();
        setBadge();

        if(TemplateActivity.ischange==true)
        {
            if(iscurrentfragment_mycard==true)
            {
                TemplateActivity.ischange=false;
                Intent intentBroadcast1 = new Intent("callapi");
                LocalBroadcastManager.getInstance(HomeActivity.this).sendBroadcast(intentBroadcast1);
            }
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        setBadge();
    }



    public class BackgroundCallgetUnreadNotifcation extends AsyncTask<Void,Void,Void>
    {

        @Override
        protected Void doInBackground(Void... voids) {

            runOnUiThread(new Runnable() {
                public void run() {
                    callApiGetUnreadNOtification();
                }
            });
            return null;
        }
    }

    private void callApiGetUnreadNOtification() {

        try {
            String token = UserPrefrence.getInstance(HomeActivity.this).getAuthtoken();
            token="bearer " + token;
            RestClient.getInstance(HomeActivity.this).get().getunreadcount( token,UserPrefrence.getInstance(HomeActivity.this).getUserid()+"",
                    new retrofit.Callback<UnreadNotficationCount_Model>() {

                        @Override
                        public void success(final UnreadNotficationCount_Model res, Response response) {

                            if(res.getResponseMessage().equalsIgnoreCase(Constant.SUCCESS))
                            {
                                if(res.getResourceData()!=null) {

                                   if(res.getResourceData()!=0)
                                   {
                                       UserPrefrence.getInstance(HomeActivity.this).setNotificationUnread(res.getResourceData().toString());
                                       setBadge();
                                   }
                                }
                            }else{
                                // Toast.makeText(NavigationHandler_Actitivty.this, res.getMessage(), Toast.LENGTH_SHORT).show();
                            }

                        }
                        @Override
                        public void failure(final RetrofitError error) {

                            error.printStackTrace();
                            if (error != null) {
                                Utils utils=new Utils(HomeActivity.this);
                                utils.showError(error);
                            }

                        }
                    });
        }catch (Exception e)
        {
            //progress.cancelProgress();
            e.printStackTrace();
        }

    }


    private class GetVersionCode extends AsyncTask<Void, String, String> {
        @Override
        protected String doInBackground(Void... voids) {

            String newVersion = null;
            try {
                newVersion = Jsoup.connect("https://play.google.com/store/apps/details?id=" + HomeActivity.this.getPackageName() + "&hl=en")
                        .timeout(30000)
                        .userAgent("Mozilla/5.0 (Windows; U; WindowsNT 5.1; en-US; rv1.8.1.6) Gecko/20070725 Firefox/2.0.0.6")
                        .referrer("http://www.google.com")
                        .get()
                        .select("div[itemprop=softwareVersion]")
                        .first()
                        .ownText();
                return newVersion;
            } catch (Exception e) {
                return newVersion;
            }
        }

        @Override
        protected void onPostExecute(String onlineVersion) {
            super.onPostExecute(onlineVersion);
            if (onlineVersion != null && !onlineVersion.isEmpty()) {
                if (Float.valueOf(currentVersion) < Float.valueOf(onlineVersion)) {
                    //show dialog
                    generatepop_update();
                }
            }
            Log.e("update", "Current version " + currentVersion + "playstore version " + onlineVersion);
        }
    }

    private void generatepop_update() {
        new AlertDialog.Builder(HomeActivity.this,R.style.AlertDialogTheme)
                .setIcon(R.drawable.applogo)
                .setTitle(getResources().getString(R.string.update_avialble))
                .setMessage(getResources().getString(R.string.update_msg_playstore))
                .setNegativeButton(getResources().getString(R.string.update_now), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                        /* User clicked OK so do some stuff */
                        dialog.dismiss();
                        final String appPackageName = getPackageName(); // getPackageName() from Context or Activity object
                        try {
                            startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + appPackageName)));
                        } catch (android.content.ActivityNotFoundException anfe) {
                            startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=" + appPackageName)));
                        }
                    }
                })
                .setPositiveButton(getResources().getString(R.string.update_later), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                        /* User clicked Cancel */
                       dialog.dismiss();
                    }
                })
                .show();
    }

}
