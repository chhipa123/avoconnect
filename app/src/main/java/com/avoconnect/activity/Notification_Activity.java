package com.avoconnect.activity;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

import com.avoconnect.Constants.Constant;
import com.avoconnect.Prefrence.UserPrefrence;
import com.avoconnect.R;
import com.avoconnect.Util.NonScrollListView;
import com.avoconnect.Util.ProgressBarCustom;
import com.avoconnect.Util.Utils;
import com.avoconnect.adapter.NotificationAdapter;
import com.avoconnect.api.RestClient;
import com.avoconnect.fcm.MyFirebaseMessagingService;
import com.avoconnect.responsemodel.CheckCardAvaiblability_Model;
import com.avoconnect.responsemodel.ListNotification_Model;
import com.avoconnect.responsemodel.ReadNotification_Model;

import retrofit.RetrofitError;
import retrofit.client.Response;

public class Notification_Activity extends AppCompatActivity {

    ImageView backimage;
    NonScrollListView listview_notification;
    NotificationAdapter notificationAdapter;
    ListNotification_Model res_global;
    ImageView noevent_img;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_notification_);

        backimage=(ImageView)findViewById(R.id.backimage);
        noevent_img=(ImageView)findViewById(R.id.noevent_img);
        listview_notification=(NonScrollListView)findViewById(R.id.listview_notification);
        backimage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });


        callApiNotificationList();
    }

    private void callApiNotificationList() {

        final ProgressBarCustom progressBarCustom=new ProgressBarCustom(Notification_Activity.this);
        progressBarCustom.showProgress();
        try {
            String token = UserPrefrence.getInstance(this).getAuthtoken();
            token="bearer " + token;
            RestClient.getInstance(this).get().getlistnotification( token,UserPrefrence.getInstance(this).getUserid(), new retrofit.Callback<ListNotification_Model>() {

                @Override
                public void success(final ListNotification_Model res, Response response) {
                    progressBarCustom.cancelProgress();

                    if(res.getResponseMessage().equalsIgnoreCase(Constant.SUCCESS))
                    {
                        UserPrefrence.getInstance(Notification_Activity.this).setNotificationUnread("");
                        Handler handler = new Handler();
                        handler.post(new Runnable() {
                            @Override
                            public void run() {
                                if(res.getResourceData()!=null&&res.getResourceData().size()>0)
                                {
                                    noevent_img.setVisibility(View.GONE);
                                    listview_notification.setVisibility(View.VISIBLE);
                                    res_global=res;
                                    setAdapter(res_global);
                                }else{
                                    noevent_img.setVisibility(View.VISIBLE);
                                    listview_notification.setVisibility(View.GONE);
                                }

                            }
                        });

                    }else{
                        UserPrefrence.getInstance(Notification_Activity.this).setNotificationUnread("");
                        noevent_img.setVisibility(View.VISIBLE);
                        listview_notification.setVisibility(View.GONE);
                      //  Toast.makeText(Notification_Activity.this, res.getMessage(), Toast.LENGTH_SHORT).show();
                    }

                }
                @Override
                public void failure(final RetrofitError error) {
                    progressBarCustom.cancelProgress();
                    error.printStackTrace();
                    if (error != null) {
                        Utils utils=new Utils(Notification_Activity.this);
                        utils.showError(error);
                    }

                }
            });
        }catch (Exception e)
        {
            progressBarCustom.cancelProgress();
            //progress.cancelProgress();
            e.printStackTrace();
        }
    }

    private void setAdapter(ListNotification_Model res) {
         notificationAdapter=new NotificationAdapter(Notification_Activity.this,res);
        listview_notification.setAdapter(notificationAdapter);

        notificationAdapter.onCallBackReturn(new NotificationAdapter.Callback() {
            @Override
            public void clickaction(int position,int notificationid) {
                callReadApi(position,notificationid);
            }

            @Override
            public void requestview(int position,String type) {

                if(type.equalsIgnoreCase("card_received"))
                {
                    checkcard_Availability(res_global.getResourceData().get(position).getSenderId()+"",UserPrefrence.getInstance(Notification_Activity.this).getUserid(),
                            res_global.getResourceData().get(position).getCardId()+"");

//                    Intent intent = new Intent(Notification_Activity.this, DetialsCardActivity.class);
//                    intent.putExtra("card_id", res_global.getResourceData().get(position).getCardId()+"");
//                    intent.putExtra("additional_info","res");
//                    startActivity(intent);

                }
                else if(type.equalsIgnoreCase("accept_request"))
                {
                    Intent intent = new Intent(Notification_Activity.this, ShowOnlyProfile_Actitivty.class);
                    intent.putExtra("sender_id", res_global.getResourceData().get(position).getSenderId() + "");
                    //   intent.putExtra("name",arrayList.get(position).getDisplayName());
                    startActivity(intent);
                }
                else {
                    if (res_global.getResourceData().get(position).getAccept() == true) {
                        Intent intent = new Intent(Notification_Activity.this, ShowOnlyProfile_Actitivty.class);
                        intent.putExtra("sender_id", res_global.getResourceData().get(position).getId() + "");
                        //   intent.putExtra("name",arrayList.get(position).getDisplayName());
                        startActivity(intent);
                    } else {
                        Intent intent = new Intent(Notification_Activity.this, RequestoView_Actitivty.class);
                        Log.e("====receiver_id======",res_global.getResourceData().get(position).getSenderId() + "");
                        intent.putExtra("receiver_id", res_global.getResourceData().get(position).getSenderId() + "");
                        intent.putExtra("notification_id", res_global.getResourceData().get(position).getId() + "");
                        intent.putExtra("name", res_global.getResourceData().get(position).getSenderName());
                        intent.putExtra("requestViewProfileId", res_global.getResourceData().get(position).getRequestViewProfileId() + "");
                        intent.putExtra("from_act", "act");
                        intent.putExtra("request", "res");
                        startActivityForResult(intent, 121);
                    }
                }
            }
        });
    }

    private void callReadApi(final int position,int notificationid) {
        final ProgressBarCustom progressBarCustom=new ProgressBarCustom(Notification_Activity.this);
        progressBarCustom.showProgress();
        try {
            String token = UserPrefrence.getInstance(this).getAuthtoken();
            token="bearer " + token;
            RestClient.getInstance(this).get().readnotification( token,notificationid+"", new retrofit.Callback<ReadNotification_Model>() {

                @Override
                public void success(final ReadNotification_Model res, final Response response) {
                    progressBarCustom.cancelProgress();

                    if(res.getResponseMessage().equalsIgnoreCase(Constant.SUCCESS))
                    {
                        Handler handler = new Handler();
                        handler.post(new Runnable() {
                            @Override
                            public void run() {
                                res_global.getResourceData().get(position).setIsRead(1);
                                if(notificationAdapter!=null)
                                {
                                    notificationAdapter.notifyDataSetChanged();
                                }

                                if(res_global.getResourceData().get(position).getAction().equalsIgnoreCase("card_received"))
                                {
                                    checkcard_Availability(res_global.getResourceData().get(position).getSenderId()+"",UserPrefrence.getInstance(Notification_Activity.this).getUserid(),
                                            res_global.getResourceData().get(position).getCardId()+"");

//                                    Intent intent = new Intent(Notification_Activity.this, DetialsCardActivity.class);
//                                    intent.putExtra("card_id", res_global.getResourceData().get(position).getCardId()+"");
//                                    intent.putExtra("additional_info","res");
//                                    startActivity(intent);
                                }else if(res_global.getResourceData().get(position).getAction().equalsIgnoreCase("accept_request"))
                                {
                                    Intent intent = new Intent(Notification_Activity.this, ShowOnlyProfile_Actitivty.class);
                                    intent.putExtra("sender_id", res_global.getResourceData().get(position).getSenderId()+"");
                                    intent.putExtra("request","res");
                                   // intent.putExtra("requestViewProfileId",requestViewProfileId);
                                    startActivity(intent);
                                }
                                else if(res_global.getResourceData().get(position).getAction().equalsIgnoreCase("event_invitation"))
                                {
                                    Intent intent = new Intent(Notification_Activity.this, EventsListActitivty.class);
                                    startActivity(intent);
                                }

                            }
                        });
                    }else{
                        Toast.makeText(Notification_Activity.this, res.getMessage(), Toast.LENGTH_SHORT).show();
                    }

                }
                @Override
                public void failure(final RetrofitError error) {
                    progressBarCustom.cancelProgress();
                    error.printStackTrace();
                    if (error != null) {
                        Utils utils=new Utils(Notification_Activity.this);
                        utils.showError(error);
                    }

                }
            });
        }catch (Exception e)
        {
            progressBarCustom.cancelProgress();
            //progress.cancelProgress();
            e.printStackTrace();
        }
    }

    @Override
    public void onBackPressed() {
        finish();
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        if(requestCode==121&&resultCode==RESULT_OK)
        {
            callApiNotificationList();
        }
    }


    public void checkcard_Availability(final String senderid,final String receiverid,final String cardid){
        final ProgressBarCustom progressBarCustom=new ProgressBarCustom(Notification_Activity.this);
        progressBarCustom.showProgress();
        try {
            String token = UserPrefrence.getInstance(this).getAuthtoken();
            token="bearer " + token;
            RestClient.getInstance(this).get().checknotificationreceivecard( token,senderid,receiverid,cardid,
                    new retrofit.Callback<CheckCardAvaiblability_Model>() {

                @Override
                public void success(final CheckCardAvaiblability_Model res, Response response) {
                    progressBarCustom.cancelProgress();

                    if(res.getResponseMessage().equalsIgnoreCase(Constant.SUCCESS))
                    {
                        Intent intent = new Intent(Notification_Activity.this, DetialsCardActivity.class);
                        intent.putExtra("card_id", cardid+"");
                        intent.putExtra("additional_info","res");
                        startActivity(intent);
                    }else{
                       ProgressBarCustom.customDialog(Notification_Activity.this,res.getMessage(),false);
                    }

                }
                @Override
                public void failure(final RetrofitError error) {
                    progressBarCustom.cancelProgress();
                    error.printStackTrace();
                    if (error != null) {
                        Utils utils=new Utils(Notification_Activity.this);
                        utils.showError(error);
                    }

                }
            });
        }catch (Exception e)
        {
            progressBarCustom.cancelProgress();
            //progress.cancelProgress();
            e.printStackTrace();
        }
    }



}
