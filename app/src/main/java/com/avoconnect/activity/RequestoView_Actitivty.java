package com.avoconnect.activity;

import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import com.avoconnect.Constants.Constant;
import com.avoconnect.Prefrence.UserPrefrence;
import com.avoconnect.R;
import com.avoconnect.Util.ProgressBarCustom;
import com.avoconnect.Util.Utils;
import com.avoconnect.api.RestClient;
import com.avoconnect.responsemodel.CheckRequested_Model;
import com.avoconnect.responsemodel.GetUserProfile_Model;
import com.avoconnect.responsemodel.ReadNotification_Model;
import com.avoconnect.responsemodel.SendNotificationRequest_Model;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.google.android.gms.vision.text.Line;

import org.json.JSONObject;

import de.hdodenhof.circleimageview.CircleImageView;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;
import retrofit.mime.TypedByteArray;
import retrofit.mime.TypedInput;

public class RequestoView_Actitivty extends AppCompatActivity implements View.OnClickListener{


    TextView txt_addcard,txt_username;
    String receiver_id="",requestViewProfileId="",sendID="";
    LinearLayout ll_blankCard,request_lay;
    TextView txt_accept,txt_decline;
    ImageView backimage;
    TextView txt_user_placeholder;
    CircleImageView image;
    String from_act="",notification_id="";
    ImageView card_view_image;
    String username="";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_requesto_view__actitivty);

        txt_addcard=(TextView)findViewById(R.id.txt_addcard);
        txt_username=(TextView)findViewById(R.id.txt_username);
        backimage=(ImageView)findViewById(R.id.backimage);
        card_view_image=(ImageView)findViewById(R.id.card_view_image);
        image=(CircleImageView) findViewById(R.id.image);
        txt_user_placeholder=(TextView)findViewById(R.id.txt_user_placeholder);

        backimage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
        txt_addcard.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                callApiRequesttoView(false);
            }
        });

        txt_accept=(TextView)findViewById(R.id.txt_accept);
        txt_decline=(TextView)findViewById(R.id.txt_decline);
        txt_accept.setOnClickListener(this);
        txt_decline.setOnClickListener(this);

        ll_blankCard=(LinearLayout)findViewById(R.id.ll_blankCard);
        request_lay=(LinearLayout)findViewById(R.id.request_lay);
        Bundle bundle=getIntent().getExtras();
        if(bundle!=null)
        {
            receiver_id=bundle.getString("receiver_id");
            if(bundle.getString("name")!=null)
            {
                txt_username.setText(bundle.getString("name"));
                ll_blankCard.setVisibility(View.VISIBLE);
                request_lay.setVisibility(View.GONE);

                if(bundle.getString("image_url")!=null&&bundle.getString("image_url").length()>0){
                    image.setVisibility(View.VISIBLE);
                    txt_user_placeholder.setVisibility(View.GONE);
                    Glide.with(RequestoView_Actitivty.this)
                            .load(bundle.getString("image_url"))
                            .apply(RequestOptions.circleCropTransform())
                            .into(image);
                }
                else if (Utils.checkNull(txt_username.getText().toString())) {
                    image.setVisibility(View.GONE);
                    txt_user_placeholder.setVisibility(View.VISIBLE);
                    txt_user_placeholder.setText(Utils.nameInital(txt_username.getText().toString().trim()) + "");
                }

                if(bundle.getString("banner_url")!=null&&bundle.getString("banner_url").length()>0){
                    Glide.with(RequestoView_Actitivty.this)
                            .load(bundle.getString("banner_url"))
                            .into(card_view_image);
                }
            }

            System.out.println("==========receiver"+" "+receiver_id+"");
            if(bundle.getString("from_act")!=null)
            {
                from_act=bundle.getString("from_act");
                Log.e("=========from_act",from_act.toString());
            }
            if(bundle.getString("notification_id")!=null)
            {
                notification_id=bundle.getString("notification_id");
            }
            if(bundle.getString("request")!=null)
            {
                if(bundle.getString("sender")!=null)
                {
                    receiver_id=bundle.getString("sender");
                    sendID=bundle.getString("sender");
                }

                if(bundle.getString("requestViewProfileId")!=null)
                {
                    requestViewProfileId=bundle.getString("requestViewProfileId");
                }

                request_lay.setVisibility(View.VISIBLE);
                ll_blankCard.setVisibility(View.GONE);
                if(sendID.length()>0)
                {
                    getDetailsUser(sendID);
                }else{
                    getDetailsUser(receiver_id);
                }

            }
        }
    }

    private void getDetailsUser(String sendID) {
        final ProgressBarCustom progressBarCustom=new ProgressBarCustom(RequestoView_Actitivty.this);
        progressBarCustom.showProgress();
        try {
            String token = UserPrefrence.getInstance(this).getAuthtoken();
            token="bearer " + token;
            RestClient.getInstance(this).get().getuserprofile(token,sendID, new Callback<GetUserProfile_Model>() {

                @Override
                public void success(final GetUserProfile_Model res, Response response) {
                    progressBarCustom.cancelProgress();

                    if(res.getResponseMessage().equalsIgnoreCase(Constant.SUCCESS))
                    {

                        if(res.getLastName()!=null)
                        {
                            txt_username.setText(res.getFirstName()+" "+res.getLastName());
                            username=res.getFirstName()+" "+res.getLastName();
                        }else{
                            txt_username.setText(res.getFirstName());
                            username=res.getFirstName();
                        }

                    }else{
                        ProgressBarCustom.customDialog(RequestoView_Actitivty.this,res.getMessage(),false);
                    }

                }

                @Override
                public void failure(final RetrofitError error) {
                    progressBarCustom.cancelProgress();
                    error.printStackTrace();
                    if (error != null) {
                        Utils utils=new Utils(RequestoView_Actitivty.this);
                        utils.showError(error);
                    }
                }
            });

        }catch (Exception e)
        {
            progressBarCustom.cancelProgress();
            e.printStackTrace();
        }
    }

    private void callApiRequesttoView(final boolean status) {
        final ProgressBarCustom progressBarCustom=new ProgressBarCustom(RequestoView_Actitivty.this);
        progressBarCustom.showProgress();
        try {
            String token = UserPrefrence.getInstance(this).getAuthtoken();
            token="bearer " + token;
            RestClient.getInstance(this).get().sendnotificationRequest( token,UserPrefrence.getInstance(this).getUserid()+"",
                    receiver_id+"",
                    new retrofit.Callback<SendNotificationRequest_Model>() {

                @Override
                public void success(final SendNotificationRequest_Model res, Response response) {
                    progressBarCustom.cancelProgress();

                    if(res.getResponseMessage().equalsIgnoreCase(Constant.SUCCESS))
                    {

                        if(status==true)
                        {
                            Intent intent = new Intent();
                            setResult(RESULT_OK, intent);
                            finish();
                        }else{
                            ProgressBarCustom.customDialog(RequestoView_Actitivty.this,res.getMessage(),true);
                        }
                    }else{
                        Toast.makeText(RequestoView_Actitivty.this, res.getMessage(), Toast.LENGTH_SHORT).show();
                    }

                }
                @Override
                public void failure(final RetrofitError error) {
                    progressBarCustom.cancelProgress();
                    error.printStackTrace();
                    if (error != null) {
                        Utils utils=new Utils(RequestoView_Actitivty.this);
                        utils.showError(error);
                    }

                }
            });
        }catch (Exception e)
        {
            progressBarCustom.cancelProgress();
            //progress.cancelProgress();
            e.printStackTrace();
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId())
        {
            case R.id.txt_decline:
                if(from_act.length()>0){
                    callApiReadNotification(0);
                }else{
                    if(notification_id.length()>0){
                        callApiReadNotification(0);
                    }else{
                        callApiRequestAcceptReject(0);
                    }

                }

                break;
            case R.id.txt_accept:
                if(from_act.length()>0){
                    callApiReadNotification(1);
                }else{
                    if(notification_id.length()>0){
                        callApiReadNotification(1);
                    }else{
                        callApiRequestAcceptReject(1);
                    }
                }

                break;
        }
    }

    private void callApiReadNotification(final int status) {

        final ProgressBarCustom progressBarCustom=new ProgressBarCustom(RequestoView_Actitivty.this);
        progressBarCustom.showProgress();
        try {
            String token = UserPrefrence.getInstance(this).getAuthtoken();
            token="bearer " + token;
            RestClient.getInstance(this).get().readnotification( token,notification_id+"", new retrofit.Callback<ReadNotification_Model>() {

                @Override
                public void success(final ReadNotification_Model res, final Response response) {
                    progressBarCustom.cancelProgress();

                    if(res.getResponseMessage().equalsIgnoreCase(Constant.SUCCESS))
                    {
                        callApiRequestAcceptReject(status);
                    }else{
                        Toast.makeText(RequestoView_Actitivty.this, res.getMessage(), Toast.LENGTH_SHORT).show();
                    }

                }
                @Override
                public void failure(final RetrofitError error) {
                    progressBarCustom.cancelProgress();
                    error.printStackTrace();
                    if (error != null) {
                        Utils utils=new Utils(RequestoView_Actitivty.this);
                        utils.showError(error);
                    }

                }
            });
        }catch (Exception e)
        {
            progressBarCustom.cancelProgress();
            //progress.cancelProgress();
            e.printStackTrace();
        }
    }

    private void callApiRequestAcceptReject(final int status) {
        final ProgressBarCustom progressBarCustom=new ProgressBarCustom(RequestoView_Actitivty.this);
        progressBarCustom.showProgress();
        try {
            String token = UserPrefrence.getInstance(this).getAuthtoken();
            token="bearer " + token;
            JSONObject jsonObject3 = new JSONObject();

            TypedInput in3 = new TypedByteArray("application/json", jsonObject3.toString().getBytes("UTF-8"));

            RestClient.getInstance(this).get().viewProfileRequestAcceptOrNotByReceivedUser( token,requestViewProfileId,
                    status+"",in3,
                    new retrofit.Callback<SendNotificationRequest_Model>() {

                        @Override
                        public void success(final SendNotificationRequest_Model res, Response response) {
                            progressBarCustom.cancelProgress();

                            if(res.getResponseMessage().equalsIgnoreCase(Constant.SUCCESS))
                            {

                                if(from_act.length()>0)
                                {
                                    Toast.makeText(RequestoView_Actitivty.this, res.getMessage(), Toast.LENGTH_SHORT).show();
//                                    Intent intent=new Intent();
//                                    setResult(RESULT_OK,intent);
//                                    finish();
                                    if(status==1)
                                    {
                                       // generatepopaskshareprofile();
                                        callApiCheckRequest();
                                    }

                                }else {

                                    if(status==1)
                                    {
                                      //  generatepopaskshareprofile();
                                        callApiCheckRequest();
                                    }
                                    Toast.makeText(RequestoView_Actitivty.this, res.getMessage(), Toast.LENGTH_SHORT).show();
                                   // ProgressBarCustom.customDialog(RequestoView_Actitivty.this, res.getMessage(), true);
                                }
                            }else{
                                Toast.makeText(RequestoView_Actitivty.this, res.getMessage(), Toast.LENGTH_SHORT).show();
                            }

                        }
                        @Override
                        public void failure(final RetrofitError error) {
                            progressBarCustom.cancelProgress();
                            error.printStackTrace();
                            if (error != null) {
                                Utils utils=new Utils(RequestoView_Actitivty.this);
                                utils.showError(error);
                            }

                        }
                    });
        }catch (Exception e)
        {
            progressBarCustom.cancelProgress();
            //progress.cancelProgress();
            e.printStackTrace();
        }
    }

    private void generatepopaskshareprofile() {
        AlertDialog.Builder builder=new AlertDialog.Builder(RequestoView_Actitivty.this);
        View view= LayoutInflater.from(this).inflate(R.layout.logout_lay,null);
        TextView txtDialogMessage=(TextView)view.findViewById(R.id.txtDialogMessage);
        txtDialogMessage.setText("Do you want to send request to view"+" "+username+" profile ?");
        Button btnDialogYes=(Button)view.findViewById(R.id.btnDialogYes);
        Button btnDialogNo=(Button)view.findViewById(R.id.btnDialogNo);
        builder.setView(view);
        final Dialog dialog=builder.create();
        dialog.setCancelable(false);
        dialog.setCanceledOnTouchOutside(false);

        btnDialogYes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
                callApiRequesttoView(true);
            }
        });
        btnDialogNo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });
        dialog.show();
    }


    private void callApiCheckRequest() {
        final ProgressBarCustom progressBarCustom=new ProgressBarCustom(RequestoView_Actitivty.this);
        progressBarCustom.showProgress();
        try {
            String token = UserPrefrence.getInstance(RequestoView_Actitivty.this).getAuthtoken();
            token="bearer " + token;
            RestClient.getInstance(RequestoView_Actitivty.this).get().checkAlreadyRequestedOrNotForSearchUserProfile( token,UserPrefrence.getInstance(RequestoView_Actitivty.this).getUserid()+"",
                    receiver_id+"",
                    new retrofit.Callback<CheckRequested_Model>() {

                        @Override
                        public void success(final CheckRequested_Model res, Response response) {
                            progressBarCustom.cancelProgress();

                            if(res.getResponseMessage().equalsIgnoreCase(Constant.SUCCESS))
                            {

                                if(res.getRequestSent()==true&&res.getAccept()==true)
                                {
                                   // callApiRequesttoView(true);
                                    Intent intent=new Intent();
                                    setResult(RESULT_OK,intent);
                                    finish();
                                }else{
                                    generatepopaskshareprofile();
                                }
//
                            }else{
                                if(res.getRequestSent()==true&&res.getAccept()==true)
                                {
                                  //  callApiRequesttoView(true);
                                    Intent intent=new Intent();
                                    setResult(RESULT_OK,intent);
                                    finish();
                                }else{
                                    generatepopaskshareprofile();
                                }

                            }

                        }
                        @Override
                        public void failure(final RetrofitError error) {
                            progressBarCustom.cancelProgress();
                            error.printStackTrace();
                            Intent intent=new Intent();
                            setResult(RESULT_OK,intent);
                            finish();
                            if (error != null) {
                                Utils utils=new Utils(RequestoView_Actitivty.this);
                                utils.showError(error);
                            }

                        }
                    });
        }catch (Exception e)
        {
            progressBarCustom.cancelProgress();
            //progress.cancelProgress();
            e.printStackTrace();
        }
    }


}
