package com.avoconnect.activity;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Point;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Handler;
import android.os.Looper;
import androidx.annotation.Nullable;

import com.avoconnect.BottomSheetFragments.CallMessageFragment;

import com.avoconnect.CrashLog.TopExceptionHandler;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.github.chrisbanes.photoview.PhotoView;
import com.google.android.material.bottomsheet.BottomSheetBehavior;

import android.os.Bundle;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.StrictMode;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.avoconnect.BottomSheetFragments.AddInfoOtherUserFragment;
import com.avoconnect.Constants.Constant;
import com.avoconnect.Fragments.BottomSheetFragment;
import com.avoconnect.Prefrence.UserPrefrence;
import com.avoconnect.R;
import com.avoconnect.Util.NonScrollListView;
import com.avoconnect.Util.ProgressBarCustom;
import com.avoconnect.Util.Utils;
import com.avoconnect.adapter.AdditionalLink_Adapter;
import com.avoconnect.adapter.ImportantLink_Adpater;
import com.avoconnect.adapter.MediaAdpater;
import com.avoconnect.api.RestClient;
import com.avoconnect.beans.AdditionalLinkModel;
import com.avoconnect.beans.Important_Model;
import com.avoconnect.beans.Media_Model;
import com.avoconnect.responsemodel.CardBackgroundColorChnage_Model;
import com.avoconnect.responsemodel.CardDetails_Model;
import com.avoconnect.responsemodel.RecivedCard_model;

import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import retrofit.RetrofitError;
import retrofit.client.Response;
import retrofit.mime.TypedByteArray;
import retrofit.mime.TypedInput;

public class DetialsCardActivity extends TopExceptionHandler implements View.OnClickListener {


    TextView txt_username,txt_email,txt_phone,txt_company,txt_link,
            txt_desciption,txt_media,txt_other_link,txt_location,txt_desgination;
   // ImageView imageview;
    Utils utils;
    RelativeLayout theme_selection;
    private BottomSheetBehavior mBottomSheetBehavior;
    RecyclerView recylerview;
    private ImageView edit_card;
    NonScrollListView listview,listview_social,listview_addtional;
    private ImageView backimage;
    String card_id="";
    List<Important_Model> important_list=new ArrayList<>();
    private ImageView  image_social1,image_social2,image_social3,image_social4,image_social5;
    List<Media_Model>media_list=new ArrayList<>();
    MediaAdpater mediaAdpater;
    boolean isotherusercard=false;
    LinearLayout additionalinfo_layout;
    boolean isaddiotnalapicall=false;
    List<AdditionalLinkModel>additional_list=new ArrayList<>();
    TextView txt_friends,txt_add_notes,txt_add_location,txt_add_time;
    TextView txt_leadtype;
    AdditionalLink_Adapter additionalLink_adapter;
    String themecolor_server="";
    LinearLayout social_linklay;
    TextView txt_important_link;
    LinearLayout linear_description,linear_location,linear_link;
    private ImageView image_logo,banner_image;
    TextView txt_user_placeholder,txt_addlink,txt_addtionalinfo;

    LinearLayout linearTime,linaerLocation,linearAddNotes,linearfriends;
    private ImageView view_img;
    View adddinto_line;
    Bitmap myBitmap;
    String ocr_img="";

    LinearLayout desingation_lay,address_lay;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detials_card);
        utils=new Utils(this);
        inits();

    }

    private void inits() {

        try {
       //     desingation_lay.setVisibility(View.GONE);
            desingation_lay = (LinearLayout) findViewById(R.id.desingation_lay);
            address_lay = (LinearLayout) findViewById(R.id.address_lay);
            image_logo = (ImageView) findViewById(R.id.image_logo);
            banner_image = (ImageView) findViewById(R.id.banner_image);
            view_img = (ImageView) findViewById(R.id.view_img);
            edit_card = (ImageView) findViewById(R.id.edit_card);
            additionalinfo_layout = (LinearLayout) findViewById(R.id.additionalinfo_layout);
            social_linklay = (LinearLayout) findViewById(R.id.social_linklay);
            theme_selection = (RelativeLayout) findViewById(R.id.theme_selection);
            adddinto_line = (View) findViewById(R.id.adddinto_line);
            linear_description = (LinearLayout) findViewById(R.id.linear_description);
            linear_location = (LinearLayout) findViewById(R.id.linear_location);
            linear_link = (LinearLayout) findViewById(R.id.linear_link);

            txt_user_placeholder = (TextView) findViewById(R.id.txt_user_placeholder);
            txt_addtionalinfo = (TextView) findViewById(R.id.txt_addtionalinfo);
            txt_addlink = (TextView) findViewById(R.id.txt_addlink);
            Bundle bundle = getIntent().getExtras();
            if (bundle != null) {
                if (bundle.getString("card_id") != null) {
                    card_id = bundle.getString("card_id");
                }
                if (bundle.getString("additional_info") != null) {
                    additionalinfo_layout.setVisibility(View.VISIBLE);
                    theme_selection.setVisibility(View.GONE);
                    isaddiotnalapicall = true;
                }
            }
            LinearLayout bottomSheet = (LinearLayout) findViewById(R.id.bottom_sheet);
            mBottomSheetBehavior = BottomSheetBehavior.from(bottomSheet);
            mBottomSheetBehavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
            mBottomSheetBehavior.setPeekHeight(0);


            linearTime = (LinearLayout) findViewById(R.id.linearTime);
            linaerLocation = (LinearLayout) findViewById(R.id.linaerLocation);
            linearAddNotes = (LinearLayout) findViewById(R.id.linearAddNotes);
            linearfriends = (LinearLayout) findViewById(R.id.linearfriends);


            image_social1 = (ImageView) findViewById(R.id.image_social1);
            image_social2 = (ImageView) findViewById(R.id.image_social2);
            image_social3 = (ImageView) findViewById(R.id.image_social3);
            image_social4 = (ImageView) findViewById(R.id.image_social4);
            image_social5 = (ImageView) findViewById(R.id.image_social5);

            txt_friends = (TextView) findViewById(R.id.txt_friends);
            txt_add_notes = (TextView) findViewById(R.id.txt_add_notes);
            txt_add_location = (TextView) findViewById(R.id.txt_add_location);
            txt_add_time = (TextView) findViewById(R.id.txt_add_time);
            txt_leadtype = (TextView) findViewById(R.id.txt_leadtype);
            txt_username = (TextView) findViewById(R.id.txt_username);
            txt_email = (TextView) findViewById(R.id.txt_email);
            txt_phone = (TextView) findViewById(R.id.txt_phone);
            txt_company = (TextView) findViewById(R.id.txt_company);
            txt_link = (TextView) findViewById(R.id.txt_link);
            txt_desciption = (TextView) findViewById(R.id.txt_desciption);
            txt_media = (TextView) findViewById(R.id.txt_media);
            txt_location = (TextView) findViewById(R.id.txt_location);
            txt_other_link = (TextView) findViewById(R.id.txt_other_link);
            txt_desgination = (TextView) findViewById(R.id.txt_desgination);
            txt_important_link = (TextView) findViewById(R.id.txt_important_link);
            listview = (NonScrollListView) findViewById(R.id.listview);
            listview_addtional = (NonScrollListView) findViewById(R.id.listview_addtional);
            backimage = (ImageView) findViewById(R.id.backimages);

            txt_email.setOnClickListener(this);
            txt_phone.setOnClickListener(this);
            txt_link.setOnClickListener(this);
            theme_selection.setOnClickListener(this);


          //  backimage.setOnClickListener(this);
            image_social1.setOnClickListener(this);
            image_social2.setOnClickListener(this);
            image_social3.setOnClickListener(this);
            image_social4.setOnClickListener(this);
            image_social5.setOnClickListener(this);
            view_img.setOnClickListener(this);
            edit_card.setOnClickListener(this);


            //imageview=(ImageView)findViewById(R.id.imageview);
            // imageview.setImageDrawable(getResources().getDrawable(R.drawable.test));
            setFonts();

            recylerview = (RecyclerView) findViewById(R.id.recylerview);

            LinearLayoutManager horizontalLayoutManagaer
                    = new LinearLayoutManager(DetialsCardActivity.this, LinearLayoutManager.VERTICAL, false);
            recylerview.setLayoutManager(horizontalLayoutManagaer);


            if (UserPrefrence.getInstance(this).getUsertype().equalsIgnoreCase(Constant.USERTYPE_PREMIUM)
                    || UserPrefrence.getInstance(this).getUsertype().equalsIgnoreCase(Constant.USERTYPE_CORPORATE)
            ) {
                txt_leadtype.setVisibility(View.VISIBLE);
            } else {
                txt_leadtype.setVisibility(View.GONE);
            }

            if (isaddiotnalapicall == true) {
                callApiGetDetailsAdditional();
            } else {
                callApi();
            }

            backimage.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent11=new Intent();
                    setResult(RESULT_OK,intent11);
                    finish();
                }
            });
            //   callApi();
        }catch (Exception e)
        {
            e.printStackTrace();
        }



    }


    private void callApi() {


        final ProgressBarCustom progressBarCustom=new ProgressBarCustom(DetialsCardActivity.this);
        progressBarCustom.showProgress();
        try {
            String token =UserPrefrence.getInstance(this).getAuthtoken();
            token="bearer " + token;
            RestClient.getInstance(this).get().getcarddetails( token,card_id, new retrofit.Callback<CardDetails_Model>() {

                @Override
                public void success(final CardDetails_Model res, Response response) {
                    progressBarCustom.cancelProgress();

                    if(res.getResponseMessage().equalsIgnoreCase(Constant.SUCCESS))
                    {

                        setData(res);
                        if(isaddiotnalapicall==true) {
                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                  //  callApiGetAddtionalInfo();
                                }
                            });
                        }
                    }else{
                        Toast.makeText(DetialsCardActivity.this, res.getMessage(), Toast.LENGTH_SHORT).show();
                    }

                }

                @Override
                public void failure(final RetrofitError error) {
                    progressBarCustom.cancelProgress();
                    error.printStackTrace();
                    if (error != null) {
                        utils.showError(error);
                    }

                }
            });

        }catch (Exception e)
        {
            progressBarCustom.cancelProgress();
            //progress.cancelProgress();
            e.printStackTrace();
        }


    }

    private void callApiGetDetailsAdditional() {
        final ProgressBarCustom progressBarCustom=new ProgressBarCustom(DetialsCardActivity.this);
        progressBarCustom.showProgress();
        try {
            String token =UserPrefrence.getInstance(this).getAuthtoken();
            token="bearer " + token;
            RestClient.getInstance(this).get().getReceivedCardDetails( token,UserPrefrence.getInstance(this).getUserid(),card_id, new retrofit.Callback<RecivedCard_model>() {

                @Override
                public void success(final RecivedCard_model res, Response response) {
                    progressBarCustom.cancelProgress();

                    if(res.getResponseMessage().equalsIgnoreCase(Constant.SUCCESS))
                    {
                        setData_dditional(res);
                    }else{
                        Toast.makeText(DetialsCardActivity.this, res.getMessage(), Toast.LENGTH_SHORT).show();
                    }

                }

                @Override
                public void failure(final RetrofitError error) {
                    progressBarCustom.cancelProgress();
                    error.printStackTrace();
                    if (error != null) {
                        utils.showError(error);
                    }

                }
            });

        }catch (Exception e)
        {
            progressBarCustom.cancelProgress();
            //progress.cancelProgress();
            e.printStackTrace();
        }

    }


    private void setData_dditional(RecivedCard_model res) {

        try {
            if (res.getResourceData() != null) {
                themecolor_server = res.getResourceData().getCardBackGroundColor();
                txt_phone.setText(res.getResourceData().getMobile());
              //  txt_company.setText(res.getResourceData().getCompanyName());
                txt_link.setText(res.getResourceData().getWebsite());
                txt_location.setText(res.getResourceData().getAddress());
                txt_desciption.setText(res.getResourceData().getSummary());
                txt_username.setText(res.getResourceData().getName());
                txt_email.setText(res.getResourceData().getEmail());
               // txt_desgination.setText(res.getResourceData().getDesignation());


                if(res.getResourceData().getDesignation()!=null&&res.getResourceData().getDesignation().length()>0)
                {
                    desingation_lay.setVisibility(View.VISIBLE);
                    txt_desgination.setText(res.getResourceData().getDesignation());
                }else{
                    desingation_lay.setVisibility(View.GONE);
                }

                if(res.getResourceData().getCompanyName()!=null&&res.getResourceData().getCompanyName().length()>0)
                {
                    address_lay.setVisibility(View.VISIBLE);
                    txt_company.setText(res.getResourceData().getCompanyName());
                }else{
                    address_lay.setVisibility(View.GONE);
                }


                if(res.getResourceData().getOcrImageOfManualCard()!=null)
                {
                    if(res.getResourceData().getOcrImageOfManualCard().length()>0)
                    {
                        view_img.setVisibility(View.VISIBLE);
                        ocr_img=res.getResourceData().getOcrImageOfManualCard();
                     //   new GetImage(res.getResourceData().getOcrImageOfManualCard()).execute();
                    }
                }
                if (res.getResourceData().getLogo() != null && res.getResourceData().getLogo().length() > 0) {
                    image_logo.setVisibility(View.VISIBLE);
                    txt_user_placeholder.setVisibility(View.GONE);
                    Glide.with(this)
                            .load(res.getResourceData().getLogo())
                            .apply(RequestOptions.circleCropTransform())
                            .into(image_logo);
                } else if (Utils.checkNull(res.getResourceData().getName().trim())) {
                    image_logo.setVisibility(View.GONE);
                    txt_user_placeholder.setVisibility(View.VISIBLE);
                    String name = "";
                    if (Utils.checkNull(res.getResourceData().getName().trim())) {
                        if (Utils.checkNull(res.getResourceData().getName().trim())) {
                            name = res.getResourceData().getName().trim();

                            txt_user_placeholder.setText(Utils.nameInital(name).trim() + "");
                        }

                    }
                }


                if (res.getResourceData().getBackgroundImage() != null
                        && res.getResourceData().getBackgroundImage().length() > 0) {
                    Glide.with(this)
                            .load(res.getResourceData().getBackgroundImage())
                            .into(banner_image);
                } else {
                    banner_image.setImageResource(R.drawable.bridege);
                }


                if (Utils.checkNull(res.getResourceData().getWebsite())) {
                    linear_link.setVisibility(View.VISIBLE);
                    txt_link.setText(res.getResourceData().getWebsite());
                } else {
                    linear_link.setVisibility(View.GONE);
                }
                if (Utils.checkNull(res.getResourceData().getAddress())) {
                    linear_location.setVisibility(View.VISIBLE);
                    txt_location.setText(res.getResourceData().getAddress());
                } else {
                    linear_location.setVisibility(View.GONE);
                }
                if (Utils.checkNull(res.getResourceData().getSummary())) {
                    linear_description.setVisibility(View.VISIBLE);
                    txt_desciption.setText(res.getResourceData().getSummary());
                } else {
                    linear_description.setVisibility(View.GONE);
                }


                if (res.getResourceData().getImpLinks().size() > 0) {
                    listview.setVisibility(View.VISIBLE);
                    txt_important_link.setVisibility(View.VISIBLE);
                    for (int k = 0; k < res.getResourceData().getImpLinks().size(); k++) {
                        Important_Model social_model = new Important_Model();
                        social_model.setLink(res.getResourceData().getImpLinks().get(k).getUrl());
                        social_model.setTitle(res.getResourceData().getImpLinks().get(k).getTitle());
                        important_list.add(social_model);
                    }
                    ImportantLink_Adpater importantLink_adpater = new ImportantLink_Adpater(DetialsCardActivity.this, important_list, "disable");
                    listview.setAdapter(importantLink_adpater);
                    importantLink_adpater.onlinkClick(new ImportantLink_Adpater.LinkClick() {
                        @Override
                        public void clickaction(int position) {
                            String links = important_list.get(position).getLink();
//                                Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(link));
//                                startActivity(browserIntent);

                            if (links.toString().length() > 0) {
                                String link = links.toString();
                                Uri webpage = Uri.parse(link);

                                if (!link.startsWith("http://") && !link.startsWith("https://")) {
                                    webpage = Uri.parse("http://" + link);
                                }

                                Intent intent = new Intent(Intent.ACTION_VIEW, webpage);
                                if (intent.resolveActivity(getPackageManager()) != null) {
                                    startActivity(intent);
                                }
                            }

                        }
                    });
                } else {
                    listview.setVisibility(View.GONE);
                    txt_important_link.setVisibility(View.GONE);
                }

                if (res.getResourceData().getImagesLinks().size() > 0) {
                    recylerview.setVisibility(View.VISIBLE);
                    txt_media.setVisibility(View.VISIBLE);
                    for (int j = 0; j < res.getResourceData().getImagesLinks().size(); j++) {
                        Media_Model media_model = new Media_Model();
                        media_model.setUrl_link(res.getResourceData().getImagesLinks().get(j).getUrl());
                        media_list.add(media_model);
                    }
                    new Handler(Looper.getMainLooper()).post(new Runnable() {
                        @Override
                        public void run() {

                            mediaAdpater = new MediaAdpater(DetialsCardActivity.this, media_list, "");
                            recylerview.setAdapter(mediaAdpater);
                            mediaAdpater.onlinkClick(new MediaAdpater.LinkClick() {
                                @Override
                                public void clickaction(int position) {

                                    String links = media_list.get(position).getUrl_link();
//                                Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(link));
//                                startActivity(browserIntent);

                                    if (links.toString().length() > 0) {
                                        String link = links.toString();
                                        Uri webpage = Uri.parse(link);

                                        if (!link.startsWith("http://") && !link.startsWith("https://")) {
                                            webpage = Uri.parse("http://" + link);
                                        }

                                        Intent intent = new Intent(Intent.ACTION_VIEW, webpage);
                                        if (intent.resolveActivity(getPackageManager()) != null) {
                                            startActivity(intent);
                                        }
                                    }

                                }
                            });

                            //   mediaAdpater.notifyDataSetChanged();
                        }
                    });
                } else {
                    txt_media.setVisibility(View.GONE);
                    recylerview.setVisibility(View.GONE);
                }

                if (res.getResourceData().getSocialLinks().size() > 0) {
                    txt_other_link.setVisibility(View.VISIBLE);
                    social_linklay.setVisibility(View.VISIBLE);
                    for (int i = 0; i < res.getResourceData().getSocialLinks().size(); i++) {
                        if (i == 0) {
                            int imagevalue = checkcase(res.getResourceData().getSocialLinks().get(i).getTitle());
                            image_social1.setImageResource(imagevalue);
                            managevisiblity(image_social1, 0);
                            image_social1.setTag(res.getResourceData().getSocialLinks().get(i).getUrl());
                        } else if (i == 1) {
                            int imagevalue = checkcase(res.getResourceData().getSocialLinks().get(i).getTitle());
                            image_social2.setImageResource(imagevalue);
                            managevisiblity(image_social2, 1);
                            image_social2.setTag(res.getResourceData().getSocialLinks().get(i).getUrl());
                        } else if (i == 2) {
                            int imagevalue = checkcase(res.getResourceData().getSocialLinks().get(i).getTitle());
                            image_social3.setImageResource(imagevalue);
                            managevisiblity(image_social3, 2);
                            image_social3.setTag(res.getResourceData().getSocialLinks().get(i).getUrl());
                        } else if (i == 3) {
                            int imagevalue = checkcase(res.getResourceData().getSocialLinks().get(i).getTitle());
                            image_social4.setImageResource(imagevalue);
                            managevisiblity(image_social4, 3);
                            image_social4.setTag(res.getResourceData().getSocialLinks().get(i).getUrl());
                        } else if (i == 4) {
                            int imagevalue = checkcase(res.getResourceData().getSocialLinks().get(i).getTitle());
                            image_social5.setImageResource(imagevalue);
                            managevisiblity(image_social5, 4);
                            image_social5.setTag(res.getResourceData().getSocialLinks().get(i).getUrl());
                        }
                    }
                } else {
                    txt_other_link.setVisibility(View.GONE);
                    social_linklay.setVisibility(View.GONE);
                }

                Utils utils = new Utils(DetialsCardActivity.this);
                if (res.getResourceData().getCardAdditionalInfoResp() != null) {
                    if (res.getResourceData().getAdditionalInfoAvailable() == true) {

                        additionalinfo_layout.setVisibility(View.VISIBLE);
                        txt_friends.setText(res.getResourceData().getCardAdditionalInfoResp().getLabel());
                        txt_add_notes.setText(res.getResourceData().getCardAdditionalInfoResp().getNotes());
                        txt_add_location.setText(res.getResourceData().getCardAdditionalInfoResp().getLocation());
                        if(res.getResourceData().getCardAdditionalInfoResp().getTime()!=null&&res.getResourceData().getCardAdditionalInfoResp().getTime().length()>0) {
                            adddinto_line.setVisibility(View.VISIBLE);
                            txt_add_time.setText(utils.convertTime(res.getResourceData().getCardAdditionalInfoResp().getTime()));
                        }

                        if(res.getResourceData().getCardAdditionalInfoResp().getLabel().length()>0)
                        {
                            adddinto_line.setVisibility(View.VISIBLE);
                        }else if(res.getResourceData().getCardAdditionalInfoResp().getNotes().length()>0)
                        {
                            adddinto_line.setVisibility(View.VISIBLE);
                        }
                        else if(res.getResourceData().getCardAdditionalInfoResp().getLocation().length()>0)
                        {
                            adddinto_line.setVisibility(View.VISIBLE);
                        }


                        if (res.getResourceData().getCardAdditionalInfoResp().getLead() == 1) {
                            txt_leadtype.setText(getResources().getString(R.string.warm_lead));
                            txt_leadtype.setTextColor(getResources().getColor(R.color.warm_lead_color));
                        } else if (res.getResourceData().getCardAdditionalInfoResp().getLead() == 2) {
                            txt_leadtype.setText(getResources().getString(R.string.hot_lead));
                            txt_leadtype.setTextColor(getResources().getColor(R.color.hot_lead_color));
                        } else if (res.getResourceData().getCardAdditionalInfoResp().getLead() == 3) {
                            txt_leadtype.setText(getResources().getString(R.string.connected));
                            txt_leadtype.setTextColor(getResources().getColor(R.color.connected_lead_color));
                        } else if (res.getResourceData().getCardAdditionalInfoResp().getLead() == 0) {
//                            txt_leadtype.setText(getResources().getString(R.string.connected));
//                            txt_leadtype.setTextColor(getResources().getColor(R.color.connected_lead_color));
                        }


                        if (res.getResourceData().getLinkAvailable() == true) {
                            if (res.getResourceData().getCardLinkResponse() != null) {
                                adddinto_line.setVisibility(View.VISIBLE);
                                txt_addlink.setVisibility(View.VISIBLE);
                                for (int a = 0; a < res.getResourceData().getCardLinkResponse().size(); a++) {
                                    AdditionalLinkModel additionalLinkModel = new AdditionalLinkModel();
                                    additionalLinkModel.setLink(res.getResourceData().getCardLinkResponse().get(a).getUrl());
                                    additionalLinkModel.setTitle(res.getResourceData().getCardLinkResponse().get(a).getTitle());
                                    additional_list.add(additionalLinkModel);
                                }
                                additionalLink_adapter = new AdditionalLink_Adapter(this, additional_list, "yes");
                                listview_addtional.setAdapter(additionalLink_adapter);
                                additionalLink_adapter.onCallBackReturn(new AdditionalLink_Adapter.Callback() {
                                    @Override
                                    public void clickaction(final int position) {


                                        new Handler(Looper.getMainLooper()).post(new Runnable() {
                                            @Override
                                            public void run() {
//                                            additional_list.remove(position);
//                                            additionalLink_adapter.notifyDataSetChanged();

                                                String links = additional_list.get(position).getLink();
                                                if (links.toString().length() > 0) {
                                                    String link = links.toString();
                                                    Uri webpage = Uri.parse(link);

                                                    if (!link.startsWith("http://") && !link.startsWith("https://")) {
                                                        webpage = Uri.parse("http://" + link);
                                                    }

                                                    Intent intent = new Intent(Intent.ACTION_VIEW, webpage);
                                                    if (intent.resolveActivity(getPackageManager()) != null) {
                                                        startActivity(intent);
                                                    }
                                                }

                                            }
                                        });

                                    }
                                });
                            }else{
                                txt_addlink.setVisibility(View.GONE);
                            }
                        }else{
                            txt_addlink.setVisibility(View.GONE);
                        }

                        boolean isvisibile=false;
                        if(Utils.checkNull(res.getResourceData().getCardAdditionalInfoResp().getLabel())){
                            linearfriends.setVisibility(View.VISIBLE);
                            isvisibile=true;
                            txt_friends.setText(res.getResourceData().getCardAdditionalInfoResp().getLabel());

                        }else {
                            linearfriends.setVisibility(View.GONE);
                        }
                        if(Utils.checkNull(res.getResourceData().getCardAdditionalInfoResp().getNotes())){
                            isvisibile=true;
                            linearAddNotes.setVisibility(View.VISIBLE);
                            txt_add_notes.setText(res.getResourceData().getCardAdditionalInfoResp().getNotes());

                        }else {
                            linearAddNotes.setVisibility(View.GONE);
                        }
                        if(Utils.checkNull(res.getResourceData().getCardAdditionalInfoResp().getLocation())){
                            isvisibile=true;
                            linaerLocation.setVisibility(View.VISIBLE);
                            txt_add_location.setText(res.getResourceData().getCardAdditionalInfoResp().getLocation());

                        }else {
                            linaerLocation.setVisibility(View.GONE);
                        }
                        if(Utils.checkNull(res.getResourceData().getCardAdditionalInfoResp().getTime())){
                            isvisibile=true;
                            linearTime.setVisibility(View.VISIBLE);
                            txt_add_time.setText(utils.convertTime(res.getResourceData().getCardAdditionalInfoResp().getTime()));

                        }else {
                            linearTime.setVisibility(View.GONE);
                        }

                        if(isvisibile==false)
                        {
                            txt_addtionalinfo.setVisibility(View.GONE);
                        }else{
                            txt_addtionalinfo.setVisibility(View.VISIBLE);
                        }


                    } else {
                        adddinto_line.setVisibility(View.GONE);
                        additionalinfo_layout.setVisibility(View.GONE);
                    }


                } else {
                   // txt_leadtype.setText(getResources().getString(R.string.connected));
                   // txt_leadtype.setTextColor(getResources().getColor(R.color.connected_lead_color));
                    adddinto_line.setVisibility(View.GONE);
                 additionalinfo_layout.setVisibility(View.GONE);
                }
            }else{
                ProgressBarCustom.customDialog(DetialsCardActivity.this,res.getMessage(),true);
            }
        }catch (Exception e)
        {
            e.printStackTrace();
        }
    }


    private void setData(CardDetails_Model res) {
        try {
            if (res.getResourceData() != null) {
                themecolor_server = res.getResourceData().getCardBackGroundColor();
                txt_phone.setText(res.getResourceData().getMobile());

                txt_link.setText(res.getResourceData().getWebsite());
                txt_location.setText(res.getResourceData().getAddress());
                txt_desciption.setText(res.getResourceData().getSummary());
                txt_username.setText(res.getResourceData().getName());
                txt_email.setText(res.getResourceData().getEmail());

                if(res.getResourceData().getDesignation()!=null&&res.getResourceData().getDesignation().length()>0)
                {
                    desingation_lay.setVisibility(View.VISIBLE);
                    txt_desgination.setText(res.getResourceData().getDesignation());
                }else{
                    desingation_lay.setVisibility(View.GONE);
                }

                if(res.getResourceData().getCompanyName()!=null&&res.getResourceData().getCompanyName().length()>0)
                {
                    address_lay.setVisibility(View.VISIBLE);
                    txt_company.setText(res.getResourceData().getCompanyName());
                }else{
                    address_lay.setVisibility(View.GONE);
                }



                if(res.getResourceData().getOcrImageOfManualCard()!=null)
                {
                    if(res.getResourceData().getOcrImageOfManualCard().length()>0)
                    {
                        view_img.setVisibility(View.VISIBLE);
                        ocr_img=res.getResourceData().getOcrImageOfManualCard();
                     //   new GetImage(res.getResourceData().getOcrImageOfManualCard()).execute();

                    }
                }

                if (res.getResourceData().getLogo() != null && res.getResourceData().getLogo().length() > 0) {
                    image_logo.setVisibility(View.VISIBLE);
                    txt_user_placeholder.setVisibility(View.GONE);
                    Glide.with(this)
                            .load(res.getResourceData().getLogo())
                            .apply(RequestOptions.circleCropTransform())
                            .into(image_logo);
                } else if (Utils.checkNull(res.getResourceData().getName().trim())) {
                    image_logo.setVisibility(View.GONE);
                    txt_user_placeholder.setVisibility(View.VISIBLE);
                    String name = "";
                    if (Utils.checkNull(res.getResourceData().getName().trim())) {
                        if (Utils.checkNull(res.getResourceData().getName().trim())) {
                            name = res.getResourceData().getName();

                            txt_user_placeholder.setText(Utils.nameInital(name).trim() + "");
                        }

                    }
                }


                if (res.getResourceData().getBackgroundImage() != null
                        && res.getResourceData().getBackgroundImage().length() > 0) {
                    Glide.with(this)
                            .load(res.getResourceData().getBackgroundImage())
                            .into(banner_image);
                } else {
                      banner_image.setImageResource(R.drawable.bridege);
                }

                if (Utils.checkNull(res.getResourceData().getWebsite())) {
                    linear_link.setVisibility(View.VISIBLE);
                    txt_link.setText(res.getResourceData().getWebsite());
                } else {
                    linear_link.setVisibility(View.GONE);
                }
                if (Utils.checkNull(res.getResourceData().getAddress())) {
                    linear_location.setVisibility(View.VISIBLE);
                    txt_location.setText(res.getResourceData().getAddress());
                } else {
                    linear_location.setVisibility(View.GONE);
                }
                if (Utils.checkNull(res.getResourceData().getSummary())) {
                    linear_description.setVisibility(View.VISIBLE);
                    txt_desciption.setText(res.getResourceData().getSummary());
                } else {
                    linear_description.setVisibility(View.GONE);
                }

                if (res.getResourceData().getImpLinks().size() > 0) {
                    listview.setVisibility(View.VISIBLE);
                    txt_important_link.setVisibility(View.VISIBLE);

                    for (int k = 0; k < res.getResourceData().getImpLinks().size(); k++) {
                        Important_Model social_model = new Important_Model();
                        social_model.setLink(res.getResourceData().getImpLinks().get(k).getUrl());
                        social_model.setTitle(res.getResourceData().getImpLinks().get(k).getTitle());
                        important_list.add(social_model);
                    }
                    ImportantLink_Adpater importantLink_adpater = new ImportantLink_Adpater(DetialsCardActivity.this, important_list, "disable");
                    listview.setAdapter(importantLink_adpater);
                    importantLink_adpater.onlinkClick(new ImportantLink_Adpater.LinkClick() {
                        @Override
                        public void clickaction(int position) {
                            String links = important_list.get(position).getLink();
                            if (links.toString().length() > 0) {
                                String link = links.toString();
                                Uri webpage = Uri.parse(link);

                                if (!link.startsWith("http://") && !link.startsWith("https://")) {
                                    webpage = Uri.parse("http://" + link);
                                }

                                Intent intent = new Intent(Intent.ACTION_VIEW, webpage);
                                if (intent.resolveActivity(getPackageManager()) != null) {
                                    startActivity(intent);
                                }

                   /* Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(link));
                    startActivity(browserIntent);*/
                            }


//                        Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(link));
//                        startActivity(browserIntent);
                        }
                    });
                } else {
                    listview.setVisibility(View.GONE);
                    txt_important_link.setVisibility(View.GONE);
                }

                if (res.getResourceData().getImagesLinks().size() > 0) {
                    txt_media.setVisibility(View.VISIBLE);
                    recylerview.setVisibility(View.VISIBLE);
                    for (int j = 0; j < res.getResourceData().getImagesLinks().size(); j++) {
                        Media_Model media_model = new Media_Model();
                        media_model.setUrl_link(res.getResourceData().getImagesLinks().get(j).getUrl());
                        media_list.add(media_model);
                    }
                    new Handler(Looper.getMainLooper()).post(new Runnable() {
                        @Override
                        public void run() {

                            mediaAdpater = new MediaAdpater(DetialsCardActivity.this, media_list, "");
                            recylerview.setAdapter(mediaAdpater);
                            mediaAdpater.onlinkClick(new MediaAdpater.LinkClick() {
                                @Override
                                public void clickaction(int position) {
                                    String links = media_list.get(position).getUrl_link();
//                                Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(link));
//                                startActivity(browserIntent);

                                    if (links.toString().length() > 0) {
                                        String link = links.toString();
                                        Uri webpage = Uri.parse(link);

                                        if (!link.startsWith("http://") && !link.startsWith("https://")) {
                                            webpage = Uri.parse("http://" + link);
                                        }

                                        Intent intent = new Intent(Intent.ACTION_VIEW, webpage);
                                        if (intent.resolveActivity(getPackageManager()) != null) {
                                            startActivity(intent);
                                        }

                   /* Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(link));
                    startActivity(browserIntent);*/
                                    }
                                }
                            });
                            //   mediaAdpater.notifyDataSetChanged();
                        }
                    });
                } else {
                    txt_media.setVisibility(View.GONE);
                    recylerview.setVisibility(View.GONE);
                }


                if (res.getResourceData().getSocialLinks().size() > 0) {
                    txt_other_link.setVisibility(View.VISIBLE);
                    social_linklay.setVisibility(View.VISIBLE);
                    for (int i = 0; i < res.getResourceData().getSocialLinks().size(); i++) {
                        if (i == 0) {
                            int imagevalue = checkcase(res.getResourceData().getSocialLinks().get(i).getTitle());
                            image_social1.setImageResource(imagevalue);
                            managevisiblity(image_social1, 0);
                            image_social1.setTag(res.getResourceData().getSocialLinks().get(i).getUrl());
                        } else if (i == 1) {
                            int imagevalue = checkcase(res.getResourceData().getSocialLinks().get(i).getTitle());
                            image_social2.setImageResource(imagevalue);
                            managevisiblity(image_social2, 1);
                            image_social2.setTag(res.getResourceData().getSocialLinks().get(i).getUrl());
                        } else if (i == 2) {
                            int imagevalue = checkcase(res.getResourceData().getSocialLinks().get(i).getTitle());
                            image_social3.setImageResource(imagevalue);
                            managevisiblity(image_social3, 2);
                            image_social3.setTag(res.getResourceData().getSocialLinks().get(i).getUrl());
                        } else if (i == 3) {
                            int imagevalue = checkcase(res.getResourceData().getSocialLinks().get(i).getTitle());
                            image_social4.setImageResource(imagevalue);
                            managevisiblity(image_social4, 3);
                            image_social4.setTag(res.getResourceData().getSocialLinks().get(i).getUrl());
                        } else if (i == 4) {
                            int imagevalue = checkcase(res.getResourceData().getSocialLinks().get(i).getTitle());
                            image_social5.setImageResource(imagevalue);
                            managevisiblity(image_social5, 4);
                            image_social5.setTag(res.getResourceData().getSocialLinks().get(i).getUrl());
                        }
                    }
                } else {
                    txt_other_link.setVisibility(View.GONE);
                    social_linklay.setVisibility(View.GONE);
                }
            }else{
                ProgressBarCustom.customDialog(DetialsCardActivity.this,res.getMessage(),true);
            }
        }catch (Exception e)
        {
            e.printStackTrace();
        }
    }

    public  void managevisiblity(ImageView image,int visible)
    {
        if(visible==0)
        {
            image_social1.setVisibility(View.GONE);
            image_social2.setVisibility(View.GONE);
            image_social3.setVisibility(View.GONE);
            image_social4.setVisibility(View.GONE);
            image_social5.setVisibility(View.GONE);
            image.setVisibility(View.VISIBLE);
        }
        else if(visible==1)
        {
            image_social2.setVisibility(View.GONE);
            image_social3.setVisibility(View.GONE);
            image_social4.setVisibility(View.GONE);
            image_social5.setVisibility(View.GONE);
            image.setVisibility(View.VISIBLE);
        }
        else if(visible==2)
        {

            image_social3.setVisibility(View.GONE);
            image_social4.setVisibility(View.GONE);
            image_social5.setVisibility(View.GONE);
            image.setVisibility(View.VISIBLE);
        }
        else if(visible==3)
        {
            image_social4.setVisibility(View.GONE);
            image_social5.setVisibility(View.GONE);
            image.setVisibility(View.VISIBLE);
        }
        else if(visible==4)
        {
            image_social5.setVisibility(View.GONE);
            image.setVisibility(View.VISIBLE);
        }


    }

    public int  checkcase(String type) {
        int image_type=0;
        if(type.equalsIgnoreCase(getResources().getString(R.string.facebook_txt)))
        {
            image_type=R.drawable.fb;
        }
        else if(type.equalsIgnoreCase(getResources().getString(R.string.twitter_txt)))
        {
            image_type=R.drawable.twitter_img;
        }
        else  if(type.equalsIgnoreCase(getResources().getString(R.string.linkein_txt)))
        {
            image_type=R.drawable.linkedin_img;
        }
        else if(type.equalsIgnoreCase(getResources().getString(R.string.intagram_txt)))
        {
            image_type=R.drawable.intagram;
        }
        else  if(type.equalsIgnoreCase(getResources().getString(R.string.pinterest_txt)))
        {
            image_type=R.drawable.pintrest;
        }
        return image_type;
    }


    private void setFonts() {
//        utils.setTextview(txt_username,2);
//        utils.setTextview(txt_email,1);
//      //  utils.setTextview(txt_phone,1);
//        utils.setTextview(txt_company,1);
//        utils.setTextview(txt_link,1);
//        utils.setTextview(txt_desciption,1);
//        utils.setTextview(txt_location,1);
       // utils.setTextview(txt_media,2);
        //utils.setTextview(txt_other_link,2);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId())
        {

            case R.id.txt_email:
                try {
                    Intent intent = new Intent(Intent.ACTION_SENDTO);
                    intent.setData(Uri.parse("mailto:" + txt_email.getText().toString())); // only email apps should handle this
                    intent.putExtra(Intent.EXTRA_EMAIL, txt_email.getText().toString());
                    intent.putExtra(Intent.EXTRA_SUBJECT, "Avoconnect");
                    if (intent.resolveActivity(getPackageManager()) != null) {
                        startActivity(intent);
                    }
                }catch (Exception e)
                {
                    e.printStackTrace();
                }
                break;
            case R.id.txt_phone:
                String[] values=null;
                String phonenumbers="";
                if(txt_phone.getText().toString().contains(","))
                {
                    values = txt_phone.getText().toString().split(",");
                    phonenumbers=values[0]+"";
                }else{
                    phonenumbers=txt_phone.getText().toString();
                }

                CallMessageFragment bottomSheetFragment = new CallMessageFragment();
                Bundle bundle=new Bundle();
                bundle.putString("phonenumber",phonenumbers+"");
                bottomSheetFragment.setArguments(bundle);
                //bottomSheetFragment.setTargetFragment(getParentFragment());
                bottomSheetFragment.show(getSupportFragmentManager(), bottomSheetFragment.getTag());

                break;

            case R.id.theme_selection:
                openbottomDialog();
                break;
            case R.id.edit_card:
                if(isotherusercard==false) {
                    Intent intent = new Intent(DetialsCardActivity.this, AddEditCardActitivty.class);
                    intent.putExtra("type", "update");
                    if(isaddiotnalapicall==true)
                    {
                        intent.putExtra("additional_info","yes");
                    }
                    intent.putExtra("card_id", card_id);
                    startActivityForResult(intent, 145);
                }else{
                    OpenAddDocuments();
                }
                break;
            case R.id.backimages:
                Intent intent11=new Intent();
                setResult(RESULT_OK,intent11);
                finish();
                break;
            case R.id.txt_link:
                if(txt_link.getText().toString().length()>0)
                {String link=txt_link.getText().toString();
                    Uri webpage = Uri.parse(link);

                    if (!link.startsWith("http://") && !link.startsWith("https://")) {
                        webpage = Uri.parse("http://" + link);
                    }

                    Intent intent = new Intent(Intent.ACTION_VIEW, webpage);
                    if (intent.resolveActivity(getPackageManager()) != null) {
                        startActivity(intent);
                    }

                   /* Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(link));
                    startActivity(browserIntent);*/
                }
                break;
            case R.id.image_social1:
                socialClick(view);
                break;
            case R.id.image_social2:
                socialClick(view);
                break;
            case R.id.image_social3:
                socialClick(view);
                break;
            case R.id.image_social4:
                socialClick(view);
                break;
            case R.id.image_social5:
                socialClick(view);
                break;

            case R.id.view_img:
                Intent intent=new Intent(DetialsCardActivity.this,ImagePopActivity.class);
                intent.putExtra("url",ocr_img);
                startActivity(intent);
//                view_img.setOnClickListener(new View.OnClickListener() {
//                    @Override
//                    public void onClick(View view) {
//                        Intent intent=new Intent(DetialsCardActivity.this,ImagePopActivity.class);
//                        intent.putExtra("url",ocr_img);
//                        startActivity(intent);
//
//                    //    imagepop(myBitmap);
//
//                    }
//                });
                break;

        }
    }
    private void imagepop( Bitmap profileBitmap) {
        AlertDialog.Builder dialog = new AlertDialog.Builder(this);
        Dialog settingsDialog=dialog.create();
        settingsDialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        int width = (int)(getResources().getDisplayMetrics().widthPixels*0.90);
        int height = (int)(getResources().getDisplayMetrics().heightPixels*0.90);
        settingsDialog.getWindow().setLayout(width, height);

        View view= LayoutInflater.from(this).inflate(R.layout.image_layout,null);
        PhotoView image_view=(PhotoView)view.findViewById(R.id.image_view);

        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        profileBitmap.compress(Bitmap.CompressFormat.PNG, 100, stream);

//        DisplayMetrics metrics = new DisplayMetrics();
//        getWindowManager().getDefaultDisplay().getMetrics(metrics);
//        int usableHeight = metrics.widthPixels;
//        getWindowManager().getDefaultDisplay().getRealMetrics(metrics);
//        int realHeight = metrics.heightPixels;

        int Measuredwidth = 0;
        int Measuredheight = 0;
        Point size = new Point();
        WindowManager w = getWindowManager();

        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB)    {
            w.getDefaultDisplay().getSize(size);
            Measuredwidth = size.x;
            Measuredheight = size.y;
        }else{
            Display d = w.getDefaultDisplay();
            Measuredwidth = d.getWidth();
            Measuredheight = d.getHeight();
        }


        Glide.with(this)
                .asBitmap()
                .load(stream.toByteArray())
                .apply(new RequestOptions().override(Measuredwidth, Measuredheight))
                .into(image_view);


   //     image_view.setImageBitmap(myBitmap);
        dialog.setView(view);
        dialog.show();
    }
    private void OpenAddDocuments() {
        AddInfoOtherUserFragment bottomSheetFragment = new AddInfoOtherUserFragment();
        Bundle bundle=new Bundle();
        bottomSheetFragment.show(this.getSupportFragmentManager(), bottomSheetFragment.getTag());
    }

    private void openbottomDialog() {

        BottomSheetFragment bottomSheetFragment = new BottomSheetFragment();
        Bundle bundle=new Bundle();
        bundle.putString("themecolor",themecolor_server);
        bottomSheetFragment.setArguments(bundle);
        bottomSheetFragment.show(getSupportFragmentManager(), bottomSheetFragment.getTag());
        bottomSheetFragment.onCallBackReturn(new BottomSheetFragment.Callback() {
            @Override
            public void clickaction(String colorcode) {
                themecolor_server=colorcode;
                callApiChnageColorCode(colorcode);
            }
        });
    //    mBottomSheetBehavior.setState(BottomSheetBehavior.STATE_EXPANDED);
    }

    private void callApiChnageColorCode(String colorcode) {

        final ProgressBarCustom progressBarCustom=new ProgressBarCustom(DetialsCardActivity.this);
        progressBarCustom.showProgress();
        try {
            String token =UserPrefrence.getInstance(this).getAuthtoken();
            token="bearer " + token;
            JSONObject jsonObject3 = new JSONObject();
            TypedInput in3 = new TypedByteArray("application/json", jsonObject3.toString().getBytes("UTF-8"));

            RestClient.getInstance(this).get().changecardBackgroundcolor( token,card_id,colorcode,in3, new retrofit.Callback<CardBackgroundColorChnage_Model>() {

                @Override
                public void success(final CardBackgroundColorChnage_Model res, Response response) {
                    progressBarCustom.cancelProgress();

                    if(res.getResponseMessage().equalsIgnoreCase(Constant.SUCCESS))
                    {
                        Toast.makeText(DetialsCardActivity.this, res.getMessage(), Toast.LENGTH_SHORT).show();
                    }else{
                        Toast.makeText(DetialsCardActivity.this, res.getMessage(), Toast.LENGTH_SHORT).show();
                    }

                }
                @Override
                public void failure(final RetrofitError error) {
                    progressBarCustom.cancelProgress();
                    error.printStackTrace();
                    if (error != null) {
                        utils.showError(error);
                    }

                }
            });

        }catch (Exception e)
        {
            progressBarCustom.cancelProgress();
            //progress.cancelProgress();
            e.printStackTrace();
        }

    }

    @Override
    public void onBackPressed() {
        Intent intent=new Intent();
        setResult(RESULT_OK,intent);
        finish();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        if(requestCode==145&&resultCode==RESULT_OK)
        {
            media_list.clear();
            important_list.clear();
            additional_list.clear();
            if(isaddiotnalapicall==true)
            {
                adddinto_line.setVisibility(View.GONE);
                callApiGetDetailsAdditional();

            }else{
                callApi();
            }
        }
    }

    private void socialClick(View view) {
        view.getTag();

        String links= String.valueOf(view.getTag());


        if (links.toString().length() > 0) {
            String link = links.toString();
            Uri webpage = Uri.parse(link);

            if (!link.startsWith("http://") && !link.startsWith("https://")) {
                webpage = Uri.parse("http://" + link);
            }

            Intent intent = new Intent(Intent.ACTION_VIEW, webpage);
            if (intent.resolveActivity(getPackageManager()) != null) {
                startActivity(intent);
            }

        }
    }

    public class GetImage extends AsyncTask<Void, Void, Void> {

        String url_image="";
        ProgressBarCustom progressBarCustom;
        public GetImage( String ocrImageOfManualCard) {
            this.url_image=ocrImageOfManualCard;
        }
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressBarCustom=new ProgressBarCustom(DetialsCardActivity.this);
            progressBarCustom.showProgress();;
        }


        @Override
        protected Void doInBackground(Void... voids) {
            try {

                if (android.os.Build.VERSION.SDK_INT > 9) {
                    StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
                    StrictMode.setThreadPolicy(policy);
                }

                URL url = new URL(url_image);
                HttpURLConnection connection =(HttpURLConnection)url.openConnection();
                connection.setDoInput(true);
                connection.connect();
                InputStream input = connection.getInputStream();
                myBitmap= BitmapFactory.decodeStream(input);


            } catch (Exception e) {

                e.printStackTrace();

            }

            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            if(progressBarCustom!=null)
            {
                progressBarCustom.cancelProgress();
            }
        }
    }




    

}
