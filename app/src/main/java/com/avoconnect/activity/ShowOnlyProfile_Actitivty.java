package com.avoconnect.activity;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.avoconnect.Constants.Constant;
import com.avoconnect.Prefrence.UserPrefrence;
import com.avoconnect.R;
import com.avoconnect.Util.NonScrollListView;
import com.avoconnect.Util.ProgressBarCustom;
import com.avoconnect.Util.Utils;
import com.avoconnect.adapter.MyExpereinceAdapter;
import com.avoconnect.adapter.MySkillaAdpater;
import com.avoconnect.api.RestClient;
import com.avoconnect.responsemodel.DeleteExpereince_Model;
import com.avoconnect.responsemodel.DeleteSkills_Model;
import com.avoconnect.responsemodel.GetUserProfile_Model;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

public class ShowOnlyProfile_Actitivty extends AppCompatActivity {

    NonScrollListView listview_myskills, listview_myexperience;
    LinearLayout tab_myskills, tab_media;
    ImageView edit_card;
    TextView txt_username, txt_email, txt_uerid, txt_qualification, txt_designnation;
    GetUserProfile_Model res_global;
    MySkillaAdpater mySkillaAdpater;
    MyExpereinceAdapter myExpereinceAdapter;
    Utils utils;
    ImageView backimage,card_view_image,image_profile;
    RelativeLayout notification_bar;
    TextView txt_user_placeholder,txt_alter_mobile,txt_mobile;
    LinearLayout mobile_lay,mobile_alter_lay;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.profile_fragment);

        listview_myskills = (NonScrollListView)findViewById(R.id.listview_myskills);
        listview_myexperience = (NonScrollListView)findViewById(R.id.listview_myexperience);

        tab_myskills = (LinearLayout)findViewById(R.id.tab_myskills);
        tab_media = (LinearLayout)findViewById(R.id.tab_media);
        edit_card = (ImageView)findViewById(R.id.edit_card);
        txt_mobile=(TextView)findViewById(R.id.txt_mobile);
        txt_qualification = (TextView)findViewById(R.id.txt_qualification);
        txt_designnation = (TextView)findViewById(R.id.txt_designnation);
        backimage=(ImageView)findViewById(R.id.backimage) ;
        txt_username = (TextView)findViewById(R.id.txt_username);
        txt_email = (TextView)findViewById(R.id.txt_email);
        txt_uerid = (TextView)findViewById(R.id.txt_uerid);
        utils = new Utils(ShowOnlyProfile_Actitivty.this);
        RelativeLayout edit_lay=(RelativeLayout)findViewById(R.id.edit_lay);
        ImageView add_expimg=(ImageView)findViewById(R.id.add_expimg);
        ImageView add_media_img=(ImageView)findViewById(R.id.add_media_img);
        mobile_lay=(LinearLayout) findViewById(R.id.mobile_lay);
        mobile_alter_lay=(LinearLayout)findViewById(R.id.mobile_alter_lay);
        txt_alter_mobile=(TextView)findViewById(R.id.txt_alter_mobile);

        txt_user_placeholder=(TextView)findViewById(R.id.txt_user_placeholder);
        card_view_image=(ImageView)findViewById(R.id.card_view_image);
        image_profile=(ImageView)findViewById(R.id.image_profile);
        notification_bar=(RelativeLayout)findViewById(R.id.notification_bar);
        notification_bar.setVisibility(View.GONE);
        edit_lay.setVisibility(View.GONE);
        add_expimg.setVisibility(View.GONE);
        add_media_img.setVisibility(View.GONE);

        backimage.setVisibility(View.VISIBLE);
        backimage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });


        Bundle bundle=getIntent().getExtras();
        if(bundle!=null)
        {
            String sender_id=bundle.getString("sender_id");
            callApi(sender_id);
        }

    }

    private void callApi(String sender_id) {

        final ProgressBarCustom progressBarCustom = new ProgressBarCustom(ShowOnlyProfile_Actitivty.this);
        progressBarCustom.showProgress();
        try {
            String token = UserPrefrence.getInstance(ShowOnlyProfile_Actitivty.this).getAuthtoken();
            token = "bearer " + token;
            RestClient.getInstance(ShowOnlyProfile_Actitivty.this).get().getuserprofile(token, sender_id, new Callback<GetUserProfile_Model>() {

                @Override
                public void success(final GetUserProfile_Model res, Response response) {
                    progressBarCustom.cancelProgress();

                    if (res.getResponseMessage().equalsIgnoreCase(Constant.SUCCESS)) {
                        res_global = res;
                        setData(res);
                    } else {
                        ProgressBarCustom.customDialog(ShowOnlyProfile_Actitivty.this, res.getMessage(), false);
                    }

                }

                @Override
                public void failure(final RetrofitError error) {
                    progressBarCustom.cancelProgress();
                    error.printStackTrace();
                    if (error != null) {
                        Utils utils = new Utils(ShowOnlyProfile_Actitivty.this);
                        utils.showError(error);
                    }
                }
            });

        } catch (Exception e) {
            progressBarCustom.cancelProgress();
            e.printStackTrace();
        }


    }

    private void setData(GetUserProfile_Model res) {

        txt_username.setText(res.getFirstName() + " " + res.getLastName());
        txt_email.setText(res.getEmail());
        if( res.getAvoId()!=null){
            txt_uerid.setText("User Id:" + res.getAvoId());
        }


        if (res.getDesignation() != null) {
            txt_designnation.setText(res.getDesignation());
        }

        if (res.getQualification() != null) {
            txt_qualification.setText(res.getQualification());
        }


        if(res.getAlternateMobile()!=null&&res.getAlternateMobile().length()>0)
        {
            mobile_alter_lay.setVisibility(View.VISIBLE);
            txt_alter_mobile.setText(res.getAlternateMobile());
        }else{
            mobile_alter_lay.setVisibility(View.GONE);
        }

        txt_mobile.setVisibility(View.VISIBLE);
        if(res.getMobile()!=null&&res.getMobile().length()>0)
        {
            mobile_lay.setVisibility(View.VISIBLE);
            txt_mobile.setText(res.getMobile()+"");
        }

//        if(res.getUserProfileImage()!=null
//                &&res.getUserProfileImage().length()>0)
//        {
//            Glide.with(this)
//                    .load(res.getUserProfileImage())
//                    .apply(RequestOptions.circleCropTransform())
//                    .into(image_profile);
//        }else{
//            image_profile.setImageResource(R.drawable.placeholder);
//        }

        if (res.getUserProfileImage()!= null && res.getUserProfileImage().length() > 0) {
            image_profile.setVisibility(View.VISIBLE);
            txt_user_placeholder.setVisibility(View.GONE);
            Glide.with(this)
                    .load(res.getUserProfileImage())
                    .apply(RequestOptions.circleCropTransform())
                    .into(image_profile);
        } else if (Utils.checkNull(res.getFirstName().trim())) {
            image_profile.setVisibility(View.GONE);
            txt_user_placeholder.setVisibility(View.VISIBLE);
            String name = "";
            if(Utils.checkNull(res.getFirstName())){
                if(Utils.checkNull(res.getLastName())){
                    name=res.getFirstName()+" "+res.getLastName();
                }else {
                    name=res.getFirstName();
                }
                txt_user_placeholder.setText(Utils.nameInital(name).trim() + "");
            }
        }



        if(res.getUserProfileBanner()!=null
                &&res.getUserProfileBanner().length()>0)
        {
            Glide.with(this)
                    .load(res.getUserProfileBanner())
                    .into(card_view_image);
        }else{
            //  banner_image.setImageResource(R.drawable.placeholder);
        }

        setAdapter(res);
    }

    private void setAdapter(final GetUserProfile_Model res) {

        mySkillaAdpater = new MySkillaAdpater(ShowOnlyProfile_Actitivty.this, res,"res");
        listview_myskills.setAdapter(mySkillaAdpater);

        mySkillaAdpater.onCallBackReturn(new MySkillaAdpater.Callback() {
            @Override
            public void clickaction(int position) {
              //  DeleteSkillsApi(res, res.getUserSkillResponse().get(position).getSkillId() + "", position);
            }
        });

        myExpereinceAdapter = new MyExpereinceAdapter(ShowOnlyProfile_Actitivty.this, res,"res");
        listview_myexperience.setAdapter(myExpereinceAdapter);

        myExpereinceAdapter.onCallBackReturn(new MyExpereinceAdapter.Callback() {
            @Override
            public void clickaction(int position) {
              //  DeleteExperinceApi(res, res.getUserExperienceResponse().get(position).getUserExperienceId() + "", position);
            }
        });
    }



}