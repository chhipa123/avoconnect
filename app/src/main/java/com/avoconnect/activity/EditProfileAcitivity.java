package com.avoconnect.activity;

import android.Manifest;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.FileProvider;

import android.os.Environment;
import android.provider.MediaStore;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.amazonaws.ClientConfiguration;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferListener;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferObserver;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferState;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferUtility;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.s3.AmazonS3Client;
import com.amazonaws.services.s3.model.CannedAccessControlList;
import com.avoconnect.BuildConfig;
import com.avoconnect.Constants.Constant;
import com.avoconnect.Prefrence.UserPrefrence;
import com.avoconnect.R;
import com.avoconnect.Util.CameraUtils;
import com.avoconnect.Util.FileUtilAbove19;
import com.avoconnect.Util.ProgressBarCustom;
import com.avoconnect.Util.RealPathUtil;
import com.avoconnect.Util.Utils;
import com.avoconnect.api.RestClient;
import com.avoconnect.responsemodel.GetQualificaation_Model;
import com.avoconnect.responsemodel.ProfileUpdate_Model;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.google.android.material.textfield.TextInputLayout;
import com.karumi.dexter.Dexter;
import com.karumi.dexter.MultiplePermissionsReport;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.DexterError;
import com.karumi.dexter.listener.PermissionRequest;
import com.karumi.dexter.listener.PermissionRequestErrorListener;
import com.karumi.dexter.listener.multi.MultiplePermissionsListener;
import com.squareup.picasso.Picasso;

import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import de.hdodenhof.circleimageview.CircleImageView;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;
import retrofit.mime.TypedByteArray;
import retrofit.mime.TypedInput;

public class EditProfileAcitivity extends AppCompatActivity implements View.OnClickListener{

    EditText et_fullname,et_email,et_mobile,et_mobile_alternate;
    TextView txt_save;
    ImageView backimage;
    TextView txt_username,txt_email,txt_uerid;
    EditText et_desingation;
    AutoCompleteTextView et_qualification;
    GetQualificaation_Model res_global;
    Utils utils;
    int qualification_id=0;

    CircleImageView image;
    TextView txt_user_placeholder;
    ImageView background_image;

    String clickfrom;
    private final int RESULT_CROP = 400;
    private static final int CAMERA_REQUEST = 0;
    private static final int GALLERY_PICTURE = 1;
    public static final int MEDIA_TYPE_IMAGE = 1;
    private static final int BITMAP_SAMPLE_SIZE = 8;
    String profilePath="";
    Bitmap profileBitmap=null;
    Context mcontext;
    AmazonS3Client s3;
    BasicAWSCredentials credentials;
    TransferUtility transferUtility;
    TransferObserver observer;
    String cropfilepath="";
    String user_image="",user_banner_image="";
     ImageView card_view_image;
    TextInputLayout et_mobile_float;
    String userImage="",bannerImage="";
    RelativeLayout profile_Lay;
    String logo_delete="",banner_delete="";
     CharSequence[] options;
     boolean isfirst_time=false;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_profile_acitivity);
        et_email=(EditText)findViewById(R.id.et_email);
        et_fullname=(EditText)findViewById(R.id.et_fullname);
        et_desingation=(EditText)findViewById(R.id.et_desingation);
        et_mobile=(EditText)findViewById(R.id.et_mobile);
        et_mobile_float=(TextInputLayout)findViewById(R.id.et_mobile_float);
        et_mobile_alternate=(EditText)findViewById(R.id.et_mobile_alternate);
        et_qualification=(AutoCompleteTextView)findViewById(R.id.et_qualification);
        txt_save=(TextView) findViewById(R.id.txt_save);
        backimage=(ImageView)findViewById(R.id.backimage);
        mcontext=this;
        image=(CircleImageView) findViewById(R.id.image);
        image.setTag("logo");
        profile_Lay=(RelativeLayout)findViewById(R.id.profile_Lay);
        txt_user_placeholder= (TextView) findViewById(R.id.txt_user_placeholder);
        background_image=(ImageView) findViewById(R.id.background_image);
        card_view_image=(ImageView) findViewById(R.id.card_view_image);
        card_view_image.setTag("remove");
        txt_username=(TextView) findViewById(R.id.txt_username);
        txt_email=(TextView) findViewById(R.id.txt_email);
        txt_uerid=(TextView) findViewById(R.id.txt_uerid);

        txt_save.setOnClickListener(this);
        profile_Lay.setOnClickListener(this);
        background_image.setOnClickListener(this);

        utils=new Utils(this);
        backimage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });


        et_mobile_float.setVisibility(View.VISIBLE);

        try {
            Bundle bundle = getIntent().getExtras();
            if (bundle != null) {
                if (bundle.getString("from_first_login") != null) {
                    et_mobile.setText(UserPrefrence.getInstance(this).getMobile());
                    et_mobile.setEnabled(false);
                    et_mobile.setClickable(false);
                    et_mobile.setFocusable(false);
                    backimage.setVisibility(View.GONE);
                    txt_uerid.setText("User Id: " + UserPrefrence.getInstance(this).getAvoid());
                    isfirst_time = true;
                } else {
                    et_fullname.setText(bundle.getString("name"));
                    userImage = (bundle.getString("userImage"));
                    bannerImage = (bundle.getString("bannerImage"));

                    if (bundle.getString("alternate") != null) {
                        et_mobile_alternate.setText(bundle.getString("alternate"));
                    }
                    if (userImage != null && userImage.length() > 0) {
                        image.setVisibility(View.VISIBLE);
                        txt_user_placeholder.setVisibility(View.GONE);

                        Picasso.with(mcontext).load(userImage).placeholder(R.drawable.placeholder_camera).into(image);

                        image.setTag("image");
                    } else if (et_fullname.length() > 0) {

                        image.setTag("logo");
                        image.setVisibility(View.GONE);
                        txt_user_placeholder.setVisibility(View.VISIBLE);
                        txt_user_placeholder.setText(Utils.nameInital(bundle.getString("name").trim()) + "");
                        //  holder. txt_user_placeholder.setText(new Utils(mContext).getFirstLastCharacter(resourceData.get(position).getFullName()));
                    }

                    if (bundle.getString("alternate_mobile") != null) {
                        et_mobile_alternate.setText(bundle.getString("alternate_mobile"));
                    }

                    if (bannerImage != null && bannerImage.length() > 0) {
                        Picasso.with(mcontext).load(bannerImage).placeholder(R.drawable.bridege).into(card_view_image);
                        card_view_image.setTag("card_view_image");
                    }
                    et_email.setText(bundle.getString("email"));
                    et_email.setEnabled(false);
                    et_email.setTextColor(getResources().getColor(R.color.editext_float_linecolor));

                    if (bundle.getString("qualification") != null) {
                        et_qualification.setText(bundle.getString("qualification"));
                        qualification_id = Integer.parseInt(bundle.getString("qualification_id"));
                    }

                    if (bundle.getString("mobile") != null) {
                        et_mobile.setText(bundle.getString("mobile"));
                        et_mobile.setEnabled(false);
                        et_mobile.setTextColor(getResources().getColor(R.color.editext_float_linecolor));
                    }
                    txt_username.setText(bundle.getString("name"));
                    txt_email.setText(bundle.getString("email"));
                    txt_uerid.setText(bundle.getString("userid"));
                }

                et_desingation.setText(bundle.getString("desingation"));

            }
        }catch (Exception e)
        {
            e.printStackTrace();
        }
        credentials = new BasicAWSCredentials(Constant.accessKey,Constant.secretKey);
        ClientConfiguration cc = new ClientConfiguration();
        cc.setSocketTimeout(120000);
        s3 = new AmazonS3Client(credentials,cc);
      //  s3.setRegion(com.amazonaws.regions.Region.getRegion(Regions.AP_SOUTH_1));
        transferUtility = new TransferUtility(s3, this);

        callApigetQualification();

//        String[] countries = getResources().getStringArray(R.array.qualification_arr);
//        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,android.R.layout.simple_list_item_1,countries);
//        et_qualification.setAdapter(adapter);

    }

    private void callApigetQualification() {
        final ProgressBarCustom progressBarCustom=new ProgressBarCustom(EditProfileAcitivity.this);
        progressBarCustom.showProgress();
        try {
            String token =UserPrefrence.getInstance(this).getAuthtoken();
            token="bearer " + token;
            RestClient.getInstance(this).get().getQalificationList( token, new Callback<GetQualificaation_Model>() {

                @Override
                public void success(final GetQualificaation_Model res, Response response) {
                    progressBarCustom.cancelProgress();
                    if(res.getResponseMessage().equalsIgnoreCase(Constant.SUCCESS))
                    {
                        res_global=res;
                        ArrayList<String> movies = new ArrayList<String>();
                        for(int a=0;a<res.getList().size();a++)
                        {
                            movies.add(res.getList().get(a).getName());
                        }

                        ArrayAdapter<String> adapter = new ArrayAdapter<String>(EditProfileAcitivity.this,R.layout.simple_textview,movies);
                        et_qualification.setAdapter(adapter);
                        et_qualification.setThreshold(1);

                    }else{

                    }


                }

                @Override
                public void failure(final RetrofitError error) {
                    progressBarCustom.cancelProgress();
                    error.printStackTrace();
                    if (error != null) {
                        utils.showError(error);
                    }

                }
            });

        }catch (Exception e)
        {
            progressBarCustom.cancelProgress();
            //progress.cancelProgress();
            e.printStackTrace();
        }

    }


    @Override
    public void onClick(View view) {
        switch (view.getId())
        {
            case R.id.txt_save:
                validation();
                break;

            case R.id.profile_Lay:
                clickfrom="logoClick";
                requestStoragePermission(clickfrom);
                break;
            case R.id.background_image:
                clickfrom="cameraClick";
                requestStoragePermission(clickfrom);
                break;
        }
    }

    private void validation() {
        if(et_fullname.getText().toString().length()==0)
        {
            ProgressBarCustom.customDialog(this,getResources().getString(R.string.enter_fulllname),false);
        }else if(utils.validateFirstName(et_fullname.getText().toString())==false)
        {
            ProgressBarCustom.customDialog(this,getResources().getString(R.string.valid_firstname),false);
        }
        else if(et_email.getText().toString().length()==0)
        {
            ProgressBarCustom.customDialog(this,getResources().getString(R.string.enter_email),false);
        }
        else if(new Utils(this).isvalidaEmail(et_email.getText().toString())==false)
        {
            ProgressBarCustom.customDialog(this,getResources().getString(R.string.enter_valid_email),false);
        }
       else  if(et_mobile.getText().toString().length()==0)
        {
            ProgressBarCustom.customDialog(this,getResources().getString(R.string.enter_mobile_number),false);
        }
        else if(et_mobile.getText().toString().length()<10)
        {
            ProgressBarCustom.customDialog(this,getResources().getString(R.string.invalid_mobile),false);
        }
        else if(et_mobile_alternate.getText().toString().length()!=0&&et_mobile_alternate.getText().toString().length()<10)
        {
            ProgressBarCustom.customDialog(this,getResources().getString(R.string.alternate_mobile),false);
        }
       else if(isfirst_time==false){
//            if(et_qualification.getText().toString().length()==0)
//            {
//                ProgressBarCustom.customDialog(this,getResources().getString(R.string.enter_qualification),false);
//            }
             if(!UserPrefrence.getInstance(EditProfileAcitivity.this).getUsertype().equalsIgnoreCase(Constant.USERTYPE_FREE))
            {
                if (res_global != null) {
                    for (int a = 0; a < res_global.getList().size(); a++) {
                        if (et_qualification.getText().toString().equalsIgnoreCase(res_global.getList().get(a).getName())) {
                            qualification_id = (res_global.getList().get(a).getQualificationId());
                            break;
                        }
                    }
                    callApiUpdateProfile();
                }
            }

            else{
                for(int a=0;a<res_global.getList().size();a++)
                {
                    if(et_qualification.getText().toString().equalsIgnoreCase(res_global.getList().get(a).getName()))
                    {
                        qualification_id=(res_global.getList().get(a).getQualificationId());
                        break;
                    }
                }
                callApiUpdateProfile();
            }
        }
        /*First Time new User*/
        else {
            if(et_qualification.getText().toString().length()>0)
            {
                if (res_global != null) {
                    for (int a = 0; a < res_global.getList().size(); a++) {
                        if (et_qualification.getText().toString().equalsIgnoreCase(res_global.getList().get(a).getName())) {
                            qualification_id = (res_global.getList().get(a).getQualificationId());
                            break;
                        }
                    }
                    callApiUpdateProfile();
                }
            }else{
                callApiUpdateProfile();
            }
        }


    }

    private void callApiUpdateProfile() {
        final ProgressBarCustom progressBarCustom=new ProgressBarCustom(EditProfileAcitivity.this);
        progressBarCustom.showProgress();
        try {

            String token = UserPrefrence.getInstance(EditProfileAcitivity.this).getAuthtoken();
            token="bearer " + token;
            JSONObject jsonObject3 = new JSONObject();
            jsonObject3.put("userId", UserPrefrence.getInstance(EditProfileAcitivity.this).getUserid());
            jsonObject3.put("name", et_fullname.getText().toString());
            if(isfirst_time==true){
                jsonObject3.put("email", et_email.getText().toString());
            }


            if(et_qualification.getText().toString().length()>0)
            {
                jsonObject3.put("qualificationId",qualification_id);
                jsonObject3.put("qualification",et_qualification.getText().toString());
            }
            else{
                jsonObject3.put("qualificationId",0);
                jsonObject3.put("qualification","");
            }
            jsonObject3.put("designation",et_desingation.getText().toString());
            if(!UserPrefrence.getInstance(EditProfileAcitivity.this).getUsertype().equalsIgnoreCase(Constant.USERTYPE_FREE))
            {
                jsonObject3.put("mobile",et_mobile.getText().toString());
            }

            if(user_image.length()>0) {
                jsonObject3.put("userProfileImage", Constant.AWS_URL + user_image);
            }else if(logo_delete.equalsIgnoreCase("DELETE")){
                jsonObject3.put("userProfileImage", "");
            }
            if(user_banner_image.length()>0) {
                jsonObject3.put("userProfileBanner", Constant.AWS_URL + user_banner_image);
            }else if(banner_delete.equalsIgnoreCase("DELETE")){
                jsonObject3.put("userProfileBanner", "");
            }

            if(et_mobile_alternate.getText().toString().length()>0)
            {
                jsonObject3.put("alternateMobile", et_mobile_alternate.getText().toString());
            }

            TypedInput in3 = new TypedByteArray("application/json", jsonObject3.toString().getBytes("UTF-8"));

            RestClient.getInstance(this).get().updateprofile( token,in3, new Callback<ProfileUpdate_Model>() {

                @Override
                public void success(final ProfileUpdate_Model res, Response response) {
                    progressBarCustom.cancelProgress();

                    if(res.getResponseMessage().equalsIgnoreCase(Constant.SUCCESS))
                    {
                        UserPrefrence.getInstance(EditProfileAcitivity.this)
                                .setUsername(et_fullname.getText().toString());


                            UserPrefrence.getInstance(EditProfileAcitivity.this)
                                    .setMobile(et_mobile.getText().toString());

                        UserPrefrence.getInstance(EditProfileAcitivity.this)
                                .setEmail(et_email.getText().toString());

                        UserPrefrence.getInstance(EditProfileAcitivity.this)
                                .setAlternatemobile(et_mobile_alternate.getText().toString());

                        Toast.makeText(EditProfileAcitivity.this, res.getMessage(), Toast.LENGTH_SHORT).show();

                        if(isfirst_time==true)
                        {
                            UserPrefrence.getInstance(EditProfileAcitivity.this)
                                    .setIsLogin(UserPrefrence.getInstance(EditProfileAcitivity.this).getUserid());

                            Intent intent=new Intent(EditProfileAcitivity.this,HomeActivity.class);
                            startActivity(intent);
                            finish();
                        }else {
                            Intent intent = new Intent();
                            setResult(RESULT_OK, intent);
                            finish();
                        }
                    }else{
                        Toast.makeText(EditProfileAcitivity.this, res.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                }

                @Override
                public void failure(final RetrofitError error) {
                    progressBarCustom.cancelProgress();
                    error.printStackTrace();
                    if (error != null) {
                        Utils utils=new Utils(EditProfileAcitivity.this);
                        utils.showError(error);
                    }

                }
            });

        }catch (Exception e)
        {
            progressBarCustom.cancelProgress();
            //progress.cancelProgress();
            e.printStackTrace();
        }
    }



    private void requestStoragePermission(final String clickfrom) {

        Dexter.withActivity(this)
                .withPermissions(
                        Manifest.permission.CAMERA,
                        Manifest.permission.READ_EXTERNAL_STORAGE,
                        Manifest.permission.WRITE_EXTERNAL_STORAGE)

                .withListener(new MultiplePermissionsListener() {
                    @Override
                    public void onPermissionsChecked(MultiplePermissionsReport report) {

                        if (report.areAllPermissionsGranted()) {
//                            //  openGallery();
                            selectImage(clickfrom);

                        }
                        if (report.isAnyPermissionPermanentlyDenied()) {

                            CameraUtils.showSettingsDialog(EditProfileAcitivity.this);
                        }
                    }

                    @Override
                    public void onPermissionRationaleShouldBeShown(List<PermissionRequest> permissions, PermissionToken token) {
                        token.continuePermissionRequest();
                    }
                }).
                withErrorListener(new PermissionRequestErrorListener() {
                    @Override
                    public void onError(DexterError error) {
                        Toast.makeText(getApplicationContext(), "Error occurred! ", Toast.LENGTH_SHORT).show();
                    }
                })
                .onSameThread()
                .check();
    }
    private void selectImage(final String clickfrom) {
        try {
            if(clickfrom.equalsIgnoreCase("logoClick"))
            {
                if (image.getTag().equals("logo")){
                    //Image doesn´t exist.
                    options = new CharSequence[]{"Take Photo", "Choose From Gallery", "Cancel"};
                }else{
                    options = new CharSequence[]{"Take Photo", "Choose From Gallery","Cancel","Remove"};
                    //Image Exists!.
                }
            }
            else if(clickfrom.equalsIgnoreCase("cameraClick")){
                if (card_view_image.getTag().equals("remove")){
                    //Image doesn´t exist.
                    options = new CharSequence[]{"Take Photo", "Choose From Gallery", "Cancel"};
                }else{
                    //Image Exists!.
                    options = new CharSequence[]{"Take Photo", "Choose From Gallery","Cancel","Remove"};
                }
            }
            androidx.appcompat.app.AlertDialog.Builder builder = new androidx.appcompat.app.AlertDialog.Builder(this);
            builder.setTitle("Select Option");
            builder.setItems(options, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int item) {
                    if (options[item].equals("Take Photo")) {
                        dialog.dismiss();

                        captureImage();
                    } else if (options[item].equals("Choose From Gallery")) {
                        dialog.dismiss();

                        galleryIntent();
                    } else if (options[item].equals("Cancel")) {

                        dialog.dismiss();
                    }else if(options[item].equals("Remove"))
                    {
                        if(clickfrom.equalsIgnoreCase("logoClick"))
                        {

                            logo_delete="DELETE";
                                if(et_fullname.getText().toString().length()>0)
                                {
                                    image.setTag("logo");
                                    image.setVisibility(View.GONE);
                                    txt_user_placeholder.setVisibility(View.VISIBLE);
                                    txt_user_placeholder.setText(Utils.nameInital(et_fullname.getText().toString().trim()) + "");
                                }else{
                                    image.setVisibility(View.VISIBLE);
                                    txt_user_placeholder.setVisibility(View.GONE);


                                    Picasso.with(mcontext).load(R.drawable.placeholder_camera).placeholder(R.drawable.placeholder_camera).into(image);

                                    image.setTag("logo");
                                }
                        }else if(clickfrom.equalsIgnoreCase("cameraClick"))
                        {
                            banner_delete="DELETE";
                            Picasso.with(mcontext).load(R.drawable.bridege).placeholder(R.drawable.bridege)
                                    .into(card_view_image);
                            card_view_image.setTag("remove");
                        }
                        dialog.cancel();
                    }
                }
            });
            builder.show();
        } catch (Exception e) {
            Toast.makeText(this, "Camera Permission error", Toast.LENGTH_SHORT).show();
            e.printStackTrace();
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        Bitmap bitmap=null;
        File imageFile=null;
        if (resultCode == RESULT_OK && requestCode == CAMERA_REQUEST) {

            if (resultCode == RESULT_OK) {

                CameraUtils.refreshGallery(getApplicationContext(), profilePath);
                profileBitmap = CameraUtils.optimizeBitmap(BITMAP_SAMPLE_SIZE, profilePath);
                if(profilePath.length()==0&&profilePath==null){
                    Toast.makeText(mcontext, "Something went wrong", Toast.LENGTH_SHORT).show();
                }else {
                    performCrop(profilePath);
                }

            }


        } else if (resultCode == RESULT_OK && requestCode == GALLERY_PICTURE) {
            if (data != null&& data.getData() != null) {
                try {
                    Uri uri= data.getData();
                    String filepath;
                    if (Build.VERSION.SDK_INT < 11) {
                        filepath = RealPathUtil.getRealPathFromURI_BelowAPI11(this, data.getData());
                        // SDK >= 11 && SDK < 19
                    }else if (Build.VERSION.SDK_INT < 19)
                        filepath = RealPathUtil.getRealPathFromURI_API11to18(this, data.getData());
                        // Lolipop
                    else if(Build.VERSION.SDK_INT<=22)
                        filepath = RealPathUtil.getRealPathFromURI_API19(this, data.getData());
                        /*Kitkat to Pie*/
                    else
                        filepath = FileUtilAbove19.getPath(this, data.getData());

                    if(filepath.length()==0&&filepath==null)
                    {
                        Toast.makeText(mcontext, "Something went wrong", Toast.LENGTH_SHORT).show();
                    }else {

                        profilePath=filepath;
                        performCrop(filepath);
                    }


                }catch (Exception e)
                {
                    e.printStackTrace();
                    Toast.makeText(mcontext, "Something went wrong", Toast.LENGTH_SHORT).show();
                }
            } else {
                Toast.makeText(getApplicationContext(), "Cancelled",
                        Toast.LENGTH_SHORT).show();
            }

        }else if (requestCode == RESULT_CROP) {
            if (resultCode == RESULT_OK) {
                Bundle extras = data.getExtras();
                try {
                    if (extras != null) {
                        if (data.getData() != null) {

                            Uri selectedImageUri = data.getData();
                            //  File file=new File(selectedImageUri);
                            if (selectedImageUri != null) {

                                profilePath = selectedImageUri.getPath();
                                profileBitmap = BitmapFactory.decodeFile(cropfilepath);
                                imageFile = new File(profilePath);

                                if (clickfrom.equals("logoClick")) {
                                    Log.e("profilePath 1", profilePath);
                                    txt_user_placeholder.setVisibility(View.GONE);
                                    image.setVisibility(View.VISIBLE);
                                    image.setImageResource(0);
                                    image.setImageDrawable(null);
                                    image.setImageBitmap(profileBitmap);
                                    image.setTag("image");
                                    callS3Bucket_Upload(cropfilepath,"logoClick");
                                } else {
                                    card_view_image.setImageDrawable(null);
                                    card_view_image.setImageBitmap(profileBitmap);
                                    card_view_image.setTag("card_view_image");
                                    callS3Bucket_Upload(cropfilepath,"");
                                }
                            /*if (networkDetector.isConnectingToInternet()) {
                                image.setImageBitmap(profileBitmap);
                                callS3Bucket_Upload(imageFile);
                                //uploadImageToS3(imageFile,"profile");
                            } else {
                                DialogService.customDialog(mcontext, getResources().getString(R.string.internet_connection), false, null);

                            }*/
                            }else{
                                profilePath = cropfilepath;
                                profileBitmap = BitmapFactory.decodeFile(cropfilepath);
                                imageFile = new File(profilePath);

                                if (clickfrom.equals("logoClick")) {
                                    Log.e("profilePath 1", profilePath);
                                    txt_user_placeholder.setVisibility(View.GONE);
                                    image.setVisibility(View.VISIBLE);
                                    image.setImageResource(0);
                                    image.setImageDrawable(null);
                                    image.setImageBitmap(profileBitmap);
                                    image.setTag("image");
                                    callS3Bucket_Upload(cropfilepath,"logoClick");
                                } else {
                                    card_view_image.setImageDrawable(null);
                                    card_view_image.setImageBitmap(profileBitmap);
                                    card_view_image.setTag("card_view_image");
                                    callS3Bucket_Upload(cropfilepath,"");
                                }
                            }
                        }
                        else {
                            profileBitmap = (Bitmap) data.getExtras().get("data");
                            if(clickfrom.equals("logoClick")){
                                Log.e("profilePath 2", profilePath);
                                txt_user_placeholder.setVisibility(View.GONE);
                                image.setVisibility(View.VISIBLE);
                                image.setImageResource(0);
                                image.setImageDrawable(null);
                                image.setImageBitmap(profileBitmap);
                                image.setTag("image");
                                callS3Bucket_Upload(cropfilepath,"logoClick");
                            }else {
                                card_view_image.setImageDrawable(null);
                                card_view_image.setImageBitmap(profileBitmap);
                                card_view_image.setTag("card_view_image");
                                callS3Bucket_Upload(cropfilepath,"");
                            }
                        }
                    } else {
                        Uri selectedImageUri = data.getData();
                        if (selectedImageUri != null) {
                            profilePath = selectedImageUri.getPath();
                            profileBitmap = BitmapFactory.decodeFile(cropfilepath);
                            imageFile = new File(profilePath);
                            if (clickfrom.equals("logoClick")) {
                                Log.e("profilePath 3", profilePath);

                                txt_user_placeholder.setVisibility(View.GONE);
                                image.setVisibility(View.VISIBLE);
                                image.setImageResource(0);
                                image.setImageDrawable(null);
                                image.setImageBitmap(profileBitmap);
                                image.setTag("image");
                                callS3Bucket_Upload(cropfilepath,"logoClick");
                            } else {
                                card_view_image.setImageDrawable(null);
                                card_view_image.setImageBitmap(profileBitmap);
                                card_view_image.setTag("card_view_image");
                                callS3Bucket_Upload(cropfilepath,"");
                            }
                        /*if (networkDetector.isConnectingToInternet()) {
                            callS3Bucket_Upload(imageFile);
                            //uploadImageToS3(imageFile,"profile");
                        } else {
                            DialogService.customDialog(mcontext, getResources().getString(R.string.internet_connection), false, null);
                        }*/
                            // personProfile.setImageBitmap(profileBitmap);
                        }else{
                            profilePath = cropfilepath;
                            profileBitmap = BitmapFactory.decodeFile(cropfilepath);
                            imageFile = new File(profilePath);

                            if (clickfrom.equals("logoClick")) {
                                Log.e("profilePath 1", profilePath);
                                txt_user_placeholder.setVisibility(View.GONE);
                                image.setVisibility(View.VISIBLE);
                                image.setImageResource(0);
                                image.setImageDrawable(null);
                                image.setImageBitmap(profileBitmap);
                                image.setTag("image");
                                callS3Bucket_Upload(cropfilepath,"logoClick");
                            } else {
                                card_view_image.setImageDrawable(null);
                                card_view_image.setImageBitmap(profileBitmap);
                                card_view_image.setTag("card_view_image");
                                callS3Bucket_Upload(cropfilepath,"");
                            }
                        }
                    }
                }catch (Exception e)
                {
                    e.printStackTrace();
                }
            }
        }
    }


    private void captureImage() {
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        File file = CameraUtils.getOutputMediaFile(MEDIA_TYPE_IMAGE);
        Log.i("==clickfrom",clickfrom);
        if (file != null) {
            profilePath = file.getAbsolutePath();
            ///  uploadPathMap.put("profile",file.getAbsolutePath());
        }


        Uri fileUri = CameraUtils.getOutputMediaFileUri(getApplicationContext(), file);
        intent.putExtra(MediaStore.EXTRA_OUTPUT, fileUri);
        // start the image capture Intent
        startActivityForResult(intent, CAMERA_REQUEST);
    }
    public void galleryIntent(){
        Intent pictureActionIntent = null;

        pictureActionIntent = new Intent(
                Intent.ACTION_PICK,
                MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        startActivityForResult(
                pictureActionIntent,
                GALLERY_PICTURE);
    }


    private void performCrop(String picUri) {
        Uri mCropImagedUri;
        try {

            //imageStoragePath = picUri;
            Intent cropIntent = new Intent("com.android.camera.action.CROP");
            File f = new File(picUri);
            Uri contentUri = FileProvider.getUriForFile(EditProfileAcitivity.this, BuildConfig.APPLICATION_ID + ".provider",f);
            //  Uri contentUri = Uri.fromFile(f);
            cropIntent.setDataAndType(contentUri, "image/*");
            cropIntent.putExtra("crop", "true");
            cropIntent.putExtra("aspectX", 1);
            cropIntent.putExtra("aspectY", 1);
            cropIntent.putExtra("outputX", 500);
            cropIntent.putExtra("outputY", 500);
            cropIntent.putExtra("return-data", true);
            cropIntent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
            File fileN = createNewFile("CROP_");
            try {
                f.createNewFile();
            } catch (IOException ex) {
                Log.e("io", ex.getMessage());
            }
            mCropImagedUri = Uri.fromFile(fileN);
            cropfilepath=mCropImagedUri.getPath();
            cropIntent.putExtra(MediaStore.EXTRA_OUTPUT, mCropImagedUri);

            startActivityForResult(cropIntent, RESULT_CROP);
        }

        catch (ActivityNotFoundException anfe) {
            String errorMessage = "your device doesn't support the crop action!";
            Toast toast = Toast.makeText(this, errorMessage, Toast.LENGTH_SHORT);
            toast.show();
        }
    }
    private File createNewFile(String prefix){



        if(prefix==null || "".equalsIgnoreCase(prefix)){
            prefix="IMG_";
        }
        File newDirectory = new File(Environment.getExternalStorageDirectory()+"/Avocard/");
        if(!newDirectory.exists()){
            if(newDirectory.mkdir()){
                Log.d(mcontext.getClass().getName(), newDirectory.getAbsolutePath()+" directory created");
            }
        }
        File file = new File(newDirectory,(prefix+System.currentTimeMillis()+".jpg"));
        if(file.exists()){
            //this wont be executed
            file.delete();
            try {
                file.createNewFile();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        return file;
    }

    private void callS3Bucket_Upload(final String filepath,String image_path) {
        //    final CustomProgress customProgress = CustomProgress.show(mcontext, "", false, false, null);
//
        final ProgressBarCustom progressBarCustom=new ProgressBarCustom(this);
        progressBarCustom.showProgress();

        String timeStamp = TimeUnit.MILLISECONDS.toSeconds(System.currentTimeMillis()) + "";
        System.out.println("timeStamp"+" "+timeStamp);

        String oririnalFile="Avoconnect_"+timeStamp+".jpg";
        String bucketPath="";
        if(image_path.equalsIgnoreCase("logoClick"))
        {
            bucketPath=Constant.PROFILE+"/"+UserPrefrence.getInstance(this).getUserid()+"/"+oririnalFile;
            user_image=bucketPath;
        }else{
            bucketPath=Constant.PROFILE+"/"+UserPrefrence.getInstance(this).getUserid()+"/"+oririnalFile;
            user_banner_image=bucketPath;
        }

        System.out.println("ORiginalpath"+" "+oririnalFile);

        File file=new File(filepath);
        observer = transferUtility.upload(Constant.BUCKETNAME, bucketPath, file, CannedAccessControlList.PublicRead);
        observer.setTransferListener(new TransferListener() {
            @Override
            public void onStateChanged(int id, TransferState state) {

                Log.e("status",state+" "+id);
                if (state.COMPLETED.equals(observer.getState())) {
                    progressBarCustom.cancelProgress();
                    s3.getUrl(Constant.BUCKETNAME,observer.getKey());
                    System.out.println("State"+" "+state+" "+id);
                    System.out.println("State"+" "+state+" "+id);
                    System.out.println("checking"+""+observer.getAbsoluteFilePath()+"\n "+observer.getKey()+"\n "+observer.getId()+"\n "+observer.getState()+"\n ");
                    System.out.println("responce_id"+" "+observer.getId()+" "+observer.getId());
                    System.out.println("responce_id"+" "+ s3.getUrl(Constant.BUCKETNAME,observer.getKey()));
                    String userImage=String.valueOf(s3.getUrl(Constant.BUCKETNAME,observer.getKey()));


//                    textViewProfile.setVisibility(View.GONE);
//
//                    if(filepath.exists())
//                    {
//                        personProfile.setImageURI(Uri.fromFile(filepath));
//                    }else
//                        personProfile.setImageBitmap(profileBitmap);
                }

            }

            @Override
            public void onProgressChanged(int id, long bytesCurrent, long bytesTotal) {

                long _bytesCurrent = bytesCurrent;
                long _bytesTotal = bytesTotal;
                float percentage =  ((float)_bytesCurrent /(float)_bytesTotal * 100);

                Log.e("percentage","" +percentage);

            }

            @Override
            public void onError(int id, Exception ex) {
               /* if (customProgress != null && customProgress.isShowing()) {
                    customProgress.dismiss();

                }*/
                progressBarCustom.cancelProgress();
                Toast.makeText(getApplicationContext(),R.string.error_message, Toast.LENGTH_SHORT).show();
                Log.e("error", ex.getMessage());
                final int sdk = Build.VERSION.SDK_INT;
                if(sdk < Build.VERSION_CODES.JELLY_BEAN) {
                    //   speakerphoto.setBackgroundDrawable(ContextCompat.getDrawable(mcontext, R.drawable.profile_icon_1) );
                } else {
                    //   speakerphoto.setBackground(ContextCompat.getDrawable(mcontext, R.drawable.profile_icon_1));
                }
            }
        });


    }


    @Override
    public void onBackPressed() {
        if(isfirst_time==true)
        {

        }else{
            finish();
        }
    }
}
