package com.avoconnect.activity;

import android.app.DatePickerDialog;
import android.content.Intent;
import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;
import android.view.View;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.avoconnect.Constants.Constant;
import com.avoconnect.Prefrence.UserPrefrence;
import com.avoconnect.R;
import com.avoconnect.Util.ProgressBarCustom;
import com.avoconnect.Util.Utils;
import com.avoconnect.api.RestClient;
import com.avoconnect.responsemodel.Expereince_Model;
import com.avoconnect.responsemodel.Skills_Model;

import org.json.JSONObject;

import java.util.Calendar;
import java.util.TimeZone;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;
import retrofit.mime.TypedByteArray;
import retrofit.mime.TypedInput;

public class UpdateProfile_Actitivty extends AppCompatActivity implements View.OnClickListener, DatePickerDialog.OnDateSetListener {

    String update_type="";
    TextView heading,txt_mytitle;
    EditText et_dsingation,et_company;
    TextView txt_startdate,txt_enddate;
    EditText et_addskills;
    RelativeLayout relay_end,rel_start;
    View view1,view2;
    CheckBox primary_checkbox;
    TextView txt_save;
    boolean isstartdate=false;
    private int _day;
    private int _month;
    private int _birthYear;
    ImageView backimage;
    Utils utils;
    String serverstartdate="",serverenddate="";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_update_profile__actitivty);
        heading=(TextView)findViewById(R.id.heading);
        txt_mytitle=(TextView)findViewById(R.id.txt_mytitle);

        et_addskills=(EditText) findViewById(R.id.et_addskills);
        et_dsingation=(EditText) findViewById(R.id.et_dsingation);
        et_company=(EditText) findViewById(R.id.et_company);

        txt_startdate=(TextView)findViewById(R.id.txt_startdate);
        txt_enddate=(TextView)findViewById(R.id.txt_enddate);

        txt_save=(TextView)findViewById(R.id.txt_save);
        relay_end=(RelativeLayout)findViewById(R.id.relay_end);
        rel_start=(RelativeLayout)findViewById(R.id.rel_start);
        primary_checkbox=(CheckBox) findViewById(R.id.primary_checkbox);
        view1=(View)findViewById(R.id.view1);
        view2=(View)findViewById(R.id.view2);
         utils=new Utils(UpdateProfile_Actitivty.this);
        backimage=(ImageView)findViewById(R.id.backimage);
        txt_startdate.setOnClickListener(this);
        txt_enddate.setOnClickListener(this);
        txt_save.setOnClickListener(this);

        Bundle bundle=getIntent().getExtras();
        if(bundle.getString("update_type")!=null){
            update_type=bundle.getString("update_type");
            heading.setText(update_type);
            if(update_type.equalsIgnoreCase("Add Experience"))
            {
                txt_mytitle.setText(getResources().getString(R.string.txt_exp));
                setVisiblity(0);
            }else if(update_type.equalsIgnoreCase("Add Skill"))
            {
                txt_mytitle.setText(getResources().getString(R.string.txt_myskills));
                setVisiblity(1);
            }
        }
        primary_checkbox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if(b==true)
                {
                    serverenddate="";
                    txt_enddate.setText("");
                    relay_end.setVisibility(View.GONE);
                }else{
                    serverenddate="";
                    txt_enddate.setText("");
                    relay_end.setVisibility(View.VISIBLE);
                }
            }
        });

        backimage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
    }

    private void setVisiblity(int status) {
        if(status==0)
        {
            et_dsingation.setVisibility(View.VISIBLE);
            et_company.setVisibility(View.VISIBLE);

            relay_end.setVisibility(View.VISIBLE);
            rel_start.setVisibility(View.VISIBLE);
            view1.setVisibility(View.VISIBLE);
            view2.setVisibility(View.VISIBLE);
            primary_checkbox.setVisibility(View.VISIBLE);
         //   et_dsingation.setVisibility(View.VISIBLE);
        }else if(status==1)
        {
            et_addskills.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId())
        {
            case R.id.txt_enddate:
                isstartdate=false;
                Calendar calendar = Calendar.getInstance(TimeZone.getDefault());
                DatePickerDialog dialog = new DatePickerDialog(UpdateProfile_Actitivty.this, this,
                        calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH),
                        calendar.get(Calendar.DAY_OF_MONTH));

                dialog.getDatePicker().setMaxDate(System.currentTimeMillis());
                dialog.show();
                break;
            case R.id.txt_startdate:
                isstartdate=true;
                Calendar calendar01 = Calendar.getInstance(TimeZone.getDefault());
                DatePickerDialog dialog01 = new DatePickerDialog(UpdateProfile_Actitivty.this, this,
                        calendar01.get(Calendar.YEAR), calendar01.get(Calendar.MONTH),
                        calendar01.get(Calendar.DAY_OF_MONTH));
                dialog01.getDatePicker().setMaxDate(System.currentTimeMillis());
                dialog01.show();
                break;

            case R.id.txt_save:
                if(update_type.equalsIgnoreCase("Add Experience"))
                {
                    validationExperience();
                }else if(update_type.equalsIgnoreCase("Add Skill")){
                    validationSkills();
                }
                break;
        }
    }

    private void validationExperience() {
            if(et_dsingation.getText().toString().length()==0)
            {
                ProgressBarCustom.customDialog(this,getResources().getString(R.string.enter_desgination),false);
            }
            else if(et_company.getText().toString().length()==0)
            {
                ProgressBarCustom.customDialog(this,getResources().getString(R.string.enter_companyname),false);
            }
            else if(txt_startdate.getText().toString().length()==0)
            {
                ProgressBarCustom.customDialog(this,getResources().getString(R.string.selectstartdate),false);
            }

            else if(primary_checkbox.isChecked()==true)
            {
                callApiAddexperience();
            }else{
                if(txt_enddate.getText().toString().length()==0)
                {
                    ProgressBarCustom.customDialog(this,getResources().getString(R.string.selectenddate),false);
                }
                else if(utils.CheckDates(serverstartdate,serverenddate)==0)
                {
                    ProgressBarCustom.customDialog(this,getResources().getString(R.string.date_validation),false);
                }
                else if(utils.CheckDates(serverstartdate,serverenddate)==2)
                {
                    ProgressBarCustom.customDialog(this,getResources().getString(R.string.datenotsame),false);
                }
                else{
                    callApiAddexperience();
                }
            }


    }

    private void callApiAddexperience() {

        final ProgressBarCustom progressBarCustom=new ProgressBarCustom(UpdateProfile_Actitivty.this);
        progressBarCustom.showProgress();
        try {

            String token =UserPrefrence.getInstance(UpdateProfile_Actitivty.this).getAuthtoken();
            token="bearer " + token;
            JSONObject jsonObject3 = new JSONObject();
            jsonObject3.put("userId", UserPrefrence.getInstance(UpdateProfile_Actitivty.this).getUserid());
            jsonObject3.put("company", et_company.getText().toString());
            jsonObject3.put("designation", et_dsingation.getText().toString());
            jsonObject3.put("startDate",serverstartdate);
            if(primary_checkbox.isChecked()==true)
            {

            }else{
                jsonObject3.put("endDate", serverenddate);
            }
            jsonObject3.put("workingTillDate",primary_checkbox.isChecked());
            TypedInput in3 = new TypedByteArray("application/json", jsonObject3.toString().getBytes("UTF-8"));

            RestClient.getInstance(this).get().addexperience( token,in3, new Callback<Expereince_Model>() {

                @Override
                public void success(final Expereince_Model res, Response response) {
                    progressBarCustom.cancelProgress();

                    if(res.getResponseMessage().equalsIgnoreCase(Constant.SUCCESS))
                    {
                        Intent intent=new Intent();
                        setResult(RESULT_OK,intent);
                        finish();
                    }else{
                        Toast.makeText(UpdateProfile_Actitivty.this, res.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                }

                @Override
                public void failure(final RetrofitError error) {
                    progressBarCustom.cancelProgress();
                    error.printStackTrace();
                    if (error != null) {
                        Utils utils=new Utils(UpdateProfile_Actitivty.this);
                        utils.showError(error);
                    }

                }
            });

        }catch (Exception e)
        {
            progressBarCustom.cancelProgress();
            //progress.cancelProgress();
            e.printStackTrace();
        }


    }

    private void validationSkills() {
        if(et_addskills.getText().toString().length()==0)
        {
            ProgressBarCustom.customDialog(this,getResources().getString(R.string.enterskills),false);
        }else{
            callApiAddSkills();
        }
    }

    private void callApiAddSkills() {
        final ProgressBarCustom progressBarCustom=new ProgressBarCustom(UpdateProfile_Actitivty.this);
        progressBarCustom.showProgress();
        try {

            String token =UserPrefrence.getInstance(UpdateProfile_Actitivty.this).getAuthtoken();
            token="bearer " + token;
            JSONObject jsonObject3 = new JSONObject();
            jsonObject3.put("userId", UserPrefrence.getInstance(UpdateProfile_Actitivty.this).getUserid());
            jsonObject3.put("skill", et_addskills.getText().toString());
            TypedInput in3 = new TypedByteArray("application/json", jsonObject3.toString().getBytes("UTF-8"));

            RestClient.getInstance(this).get().addskill( token,in3, new Callback<Skills_Model>() {

                @Override
                public void success(final Skills_Model res, Response response) {
                    progressBarCustom.cancelProgress();

                    if(res.getResponseMessage().equalsIgnoreCase(Constant.SUCCESS))
                    {
                        Intent intent=new Intent();
                        setResult(RESULT_OK,intent);
                        finish();
                    }else{
                        Toast.makeText(UpdateProfile_Actitivty.this, res.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                }

                @Override
                public void failure(final RetrofitError error) {
                    progressBarCustom.cancelProgress();
                    error.printStackTrace();
                    if (error != null) {
                        Utils utils=new Utils(UpdateProfile_Actitivty.this);
                        utils.showError(error);
                    }

                }
            });

        }catch (Exception e)
        {
            progressBarCustom.cancelProgress();
            //progress.cancelProgress();
            e.printStackTrace();
        }

    }

    @Override
    public void onDateSet(DatePicker datePicker, int year, int month, int day) {

        if(isstartdate==true)
        {
            serverstartdate=year+"-"+(month+1)+"-"+day;
            Utils utils=new Utils(UpdateProfile_Actitivty.this);
            String date= utils.dateConvert(year+"-"+(month+1)+"-"+day);
            txt_startdate.setText(date);
        }else{
            serverenddate=year+"-"+(month+1)+"-"+day;
            Utils utils=new Utils(UpdateProfile_Actitivty.this);
            String date= utils.dateConvert(year+"-"+(month+1)+"-"+day);
            txt_enddate.setText(date);
        }
    }



}
