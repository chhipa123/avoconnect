package com.avoconnect.activity;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.media.Image;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.avoconnect.Constants.Constant;
import com.avoconnect.Prefrence.UserPrefrence;
import com.avoconnect.R;
import com.avoconnect.Util.ProgressBarCustom;
import com.avoconnect.Util.Utils;
import com.avoconnect.api.RestClient;
import com.avoconnect.responsemodel.ShareCardTemplate_Model;
import com.avoconnect.responsemodel.ShareMyUserId_Model;

import retrofit.RetrofitError;
import retrofit.client.Response;

public class NavigationHandler_Actitivty extends AppCompatActivity implements View.OnClickListener{

    ImageView backimage;
    TextView txt_save,userid,txt_name;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_navigation_handler__actitivty);
        backimage=(ImageView)findViewById(R.id.backimage);
        txt_save=(TextView)findViewById(R.id.txt_save);
        userid=(TextView)findViewById(R.id.userid);
        backimage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
        txt_save.setOnClickListener(this);
        txt_name=(TextView)findViewById(R.id.txt_name);
        txt_name.setText(UserPrefrence.getInstance(this).getUsername());
        userid.setText(UserPrefrence.getInstance(this).getAvoid());
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.txt_save:
                callApiShareMyCard();
                break;

        }
    }

    private void callApiShareMyCard() {
        final ProgressBarCustom progressBarCustom=new ProgressBarCustom(NavigationHandler_Actitivty.this);
        progressBarCustom.showProgress();
        try {
            String token = UserPrefrence.getInstance(NavigationHandler_Actitivty.this).getAuthtoken();
            token="bearer " + token;
            RestClient.getInstance(NavigationHandler_Actitivty.this).get().sharemyuserid( token,UserPrefrence.getInstance(NavigationHandler_Actitivty.this).getUserid()+"",
                    new retrofit.Callback<ShareMyUserId_Model>() {

                        @Override
                        public void success(final ShareMyUserId_Model res, Response response) {
                            progressBarCustom.cancelProgress();

                            if(res.getResponseMessage().equalsIgnoreCase(Constant.SUCCESS))
                            {
                                if(res.getResourceData()!=null) {

                                    Intent intent = new Intent(Intent.ACTION_SEND);
                                    intent.setType("text/plain");
                                    intent.putExtra(Intent.EXTRA_SUBJECT, "Avoconnect");
                                    intent.putExtra(Intent.EXTRA_TEXT, res.getResourceData());
                                    startActivity(Intent.createChooser(intent, "Choose sharing method"));
                                }
                            }else{
                                // Toast.makeText(NavigationHandler_Actitivty.this, res.getMessage(), Toast.LENGTH_SHORT).show();
                            }

                        }
                        @Override
                        public void failure(final RetrofitError error) {
                            progressBarCustom.cancelProgress();
                            error.printStackTrace();
                            if (error != null) {
                                Utils utils=new Utils(NavigationHandler_Actitivty.this);
                                utils.showError(error);
                            }

                        }
                    });
        }catch (Exception e)
        {
            progressBarCustom.cancelProgress();
            //progress.cancelProgress();
            e.printStackTrace();
        }
    }
}
