package com.avoconnect.activity;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

import com.avoconnect.R;

import java.util.Date;

public class NotificationPushUserid extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_notification_push_userid);


        Intent intent=getIntent();
        if (intent != null ) {
            try {
                String card_id="";
                String userid="";
                Uri data = intent.getData();
                Log.e("===userid",data.getEncodedPath());
                if (data.getQueryParameter("senderId") != null) {
                    {
                        /*Share my user id*/
                        userid = intent.getStringExtra("senderId");
                        Log.e("===userid",userid.toString());
                        Toast.makeText(this, userid.toString(), Toast.LENGTH_SHORT).show();
                    }
                }


            }catch (Exception e)
            {
                e.printStackTrace();
            }
        }


    }
}
