package com.avoconnect.activity;

import android.Manifest;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

import com.avoconnect.Constants.Constant;
import com.avoconnect.Prefrence.UserPrefrence;
import com.avoconnect.R;

import com.avoconnect.Util.ProgressBarCustom;
import com.avoconnect.Util.Utils;
import com.avoconnect.api.RestClient;
import com.avoconnect.responsemodel.ShareCard_Model;
import com.google.api.client.repackaged.org.apache.commons.codec.binary.Base64;
import com.google.zxing.BarcodeFormat;
import com.google.zxing.WriterException;
import com.google.zxing.common.BitMatrix;
import com.google.zxing.integration.android.IntentIntegrator;
import com.google.zxing.integration.android.IntentResult;
import com.google.zxing.qrcode.QRCodeWriter;
import com.karumi.dexter.Dexter;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.PermissionDeniedResponse;
import com.karumi.dexter.listener.PermissionGrantedResponse;
import com.karumi.dexter.listener.PermissionRequest;
import com.karumi.dexter.listener.single.PermissionListener;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

public class Generateqrcode_Acitivity extends AppCompatActivity {

    Button btn_genertaeqr,btn_scanqr;
    static final String ACTION_SCAN = "com.google.zxing.client.android.SCAN";
    boolean isfirst=false;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_generateqrcode__acitivity);
        btn_genertaeqr=(Button)findViewById(R.id.btn_genertaeqr);
        btn_scanqr=(Button)findViewById(R.id.btn_scanqr);

        btn_genertaeqr.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                geraQR("http://www.youtube.com");
            }
        });
        btn_scanqr.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                askPermission();
            }
        });

        askPermission();


    }

    private void askPermission()
    {
        Dexter.withActivity(this)
                .withPermission(Manifest.permission.CAMERA)
                .withListener(new PermissionListener() {
                    @Override
                    public void onPermissionGranted(PermissionGrantedResponse response) {
                        // permission is granted, open the camera
                        IntentIntegrator integrator = new IntentIntegrator(Generateqrcode_Acitivity.this);
                        integrator.setOrientationLocked(true);
                        integrator.initiateScan();
                    }

                    @Override
                    public void onPermissionDenied(PermissionDeniedResponse response) {
                        // check for permanent denial of permission
                        if (response.isPermanentlyDenied()) {
                            // navigate user to app settings
                        }
                    }

                    @Override
                    public void onPermissionRationaleShouldBeShown(PermissionRequest permission, PermissionToken token) {
                        token.continuePermissionRequest();
                    }
                }).check();
    }

    private void geraQR(String texto){
        QRCodeWriter writer = new QRCodeWriter();
        ImageView qrCode = (ImageView)findViewById(R.id.imageView);
        try {
            BitMatrix bitMatrix = writer.encode(texto, BarcodeFormat.QR_CODE, 512, 512);
            int width = 512;
            int height = 512;
            Bitmap bmp = Bitmap.createBitmap(width, height, Bitmap.Config.RGB_565);
            for (int x = 0; x < width; x++) {
                for (int y = 0; y < height; y++) {
                    if (bitMatrix.get(x,y))
                        bmp.setPixel(x, y, Color.BLACK);
                    else
                        bmp.setPixel(x, y, Color.WHITE);
                }
            }
            qrCode.setImageBitmap(bmp);
        } catch (WriterException e) {
            Log.e("QR ERROR", e.toString());
        }
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        IntentResult result = IntentIntegrator.parseActivityResult(requestCode, resultCode, data);
        if(result != null) {
            if(result.getContents() == null) {
               // Toast.makeText(this, "Cancelled", Toast.LENGTH_LONG).show();
                finish();
            } else {
                if(isfirst==false)
                {
                    isfirst=true;
                    spiltstring(result.getContents());
                }

              //  Toast.makeText(this, "Scanned: " + result.getContents(), Toast.LENGTH_LONG).show();
            }
        } else {
            super.onActivityResult(requestCode, resultCode, data);
        }
    }

    private void spiltstring(String contents) {
        try {
            String localstring = contents.replace(Constant.QRSACN_URL, "");
            String[] values = localstring.split(",");
            String card_id = values[0];
            String userid = values[1];
            callApiReceiveCard(card_id, userid);
        }catch (Exception e)
        {
            Toast.makeText(this, "Invalid format", Toast.LENGTH_SHORT).show();
        }
    }

    private void callApiReceiveCard(String card_id,String sender_id) {
        final ProgressBarCustom progressBarCustom=new ProgressBarCustom(Generateqrcode_Acitivity.this);
        progressBarCustom.showProgress();
        try {



            String token = UserPrefrence.getInstance(Generateqrcode_Acitivity.this).getAuthtoken();
            token="bearer " + token;
            RestClient.getInstance(Generateqrcode_Acitivity.this).get().sharecard_id(token,sender_id,UserPrefrence.getInstance(Generateqrcode_Acitivity.this).getAvoid()
                    ,card_id,new Callback<ShareCard_Model>() {

                        @Override
                        public void success(final ShareCard_Model res, Response response) {
                            progressBarCustom.cancelProgress();

                            if(res.getResponseMessage().equalsIgnoreCase(Constant.SUCCESS))
                            {
                                Toast.makeText(Generateqrcode_Acitivity.this, res.getMessage(), Toast.LENGTH_SHORT).show();
                                Intent intent=new Intent();
                                setResult(RESULT_OK,intent);
                                finish();
                            }else{
                                Toast.makeText(Generateqrcode_Acitivity.this, res.getMessage(), Toast.LENGTH_SHORT).show();
                                Intent intent=new Intent();
                                setResult(RESULT_CANCELED,intent);
                                finish();
                            }

                        }

                        @Override
                        public void failure(final RetrofitError error) {
                            progressBarCustom.cancelProgress();
                            error.printStackTrace();
                            if (error != null) {
                                Utils utils=new Utils(Generateqrcode_Acitivity.this);
                                utils.showError(error);
                            }
                        }
                    });
        }catch (Exception e)
        {
            progressBarCustom.cancelProgress();
            e.printStackTrace();
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }
}
