package com.avoconnect.activity;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.provider.Settings;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.avoconnect.BuildConfig;
import com.avoconnect.Constants.Constant;
import com.avoconnect.Prefrence.UserPrefrence;
import com.avoconnect.R;
import com.avoconnect.Util.ProgressBarCustom;
import com.avoconnect.Util.Utils;
import com.avoconnect.api.RestClient;
import com.avoconnect.responsemodel.GetPaymentResponce;
import com.karumi.dexter.Dexter;
import com.karumi.dexter.MultiplePermissionsReport;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.DexterError;
import com.karumi.dexter.listener.PermissionRequest;
import com.karumi.dexter.listener.PermissionRequestErrorListener;
import com.karumi.dexter.listener.multi.MultiplePermissionsListener;
import com.razorpay.Checkout;
import com.razorpay.PaymentResultListener;

import org.json.JSONObject;

import java.util.List;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;
import retrofit.mime.TypedByteArray;
import retrofit.mime.TypedInput;

public class PaymentActivity extends Activity implements PaymentResultListener {
    private static final String TAG = PaymentActivity.class.getSimpleName();
    private static final String USER_EMAIL= "androidnirmala2015@gmail.com";
    private static final String USER_PHONE = "9876543210";
    EditText ed_amount;
    private String orderId = "ORDER001"; /// dummy its getting from api
    private int API_HIT_COUNT = 0;
    String name="",description="",amount="",order_id="";
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.payment_layout);
        /*
         To ensure faster loading of the Checkout form,
          call this method as early as possible in your checkout flow.
         */
        Checkout.preload(getApplicationContext());

        // Payment button created by you in XML layout
        Button button = (Button) findViewById(R.id.btn_pay);
        ed_amount = (EditText)findViewById(R.id.ed_amount);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                requestStoragePermission();
            }
        });
        Bundle bundle=getIntent().getExtras();
        if(bundle!=null)
        {
            if(bundle.getString("name")!=null)
            {
                name=bundle.getString("name");
            }
            if(bundle.getString("description")!=null)
            {
                description=bundle.getString("description");
            }
            if(bundle.getString("order_id")!=null)
            {
                order_id=bundle.getString("order_id");
            }
            if(bundle.getString("amount")!=null)
            {
                amount=bundle.getString("amount");
                ed_amount.setText(amount);
                ed_amount.setClickable(false);
                ed_amount.setEnabled(false);
            }
        }


    }

    public void startPayment() {
        /*
          You need to pass current activity in order to let Razorpay create CheckoutActivity
         */
        final Activity activity = this;
        final Checkout co = new Checkout();
      //  String amount = ed_amount.getText().toString().trim();
        if (amount!=null && !amount.isEmpty())
        {
            co.setImage(R.drawable.applogo);
            try {
                JSONObject options = new JSONObject();
                options.put("name", name);
                options.put("description", description);
               // options.put("image", "https://s3.amazonaws.com/rzp-mobile/images/rzp.png");

                // set oder id here generate your server
               options.put("order_id ", order_id);

                options.put("currency", "INR");
                // amount to be set like 100 = 1.00
                options.put("amount", Double.valueOf(amount));
                JSONObject preFill = new JSONObject();
                preFill.put("email", UserPrefrence.getInstance(this).getEmail());
                preFill.put("contact", UserPrefrence.getInstance(this).getMobile());
                options.put("prefill", preFill);
                co.open(activity, options);
            } catch (Exception e) {
                Toast.makeText(activity, "Error in payment: " + e.getMessage(), Toast.LENGTH_SHORT)
                        .show();
                e.printStackTrace();
            }
        }else{
            Toast.makeText(PaymentActivity.this,"Please enter amount",Toast.LENGTH_SHORT).show();
        }
    }

    /**
     * The name of the function has to be
     * onPaymentSuccess
     * Wrap your code in try catch, as shown, to ensure that this method runs correctly
     */
    @SuppressWarnings("unused")
    @Override
    public void onPaymentSuccess(String razorpayPaymentID) {
        try {
            Log.i("onPaymentSuccess : ","response : "+razorpayPaymentID.trim());
            callAPIforPaymentSuccess(razorpayPaymentID,orderId);
        } catch (Exception e) {
            Log.e(TAG, "Exception in onPaymentSuccess", e);
        }
    }

    /**
     * The name of the function has to be
     * onPaymentError
     * Wrap your code in try catch, as shown, to ensure that this method runs correctly
     */
    @SuppressWarnings("unused")
    @Override
    public void onPaymentError(int code, String response) {
        try {
            Log.i("onPaymentError : "+code,"response : "+response.trim());
            Toast.makeText(this, "Payment Failed: " + response, Toast.LENGTH_SHORT).show();
        } catch (Exception e) {
            Log.e(TAG, "Exception in onPaymentError", e);
        }
    }

    private void requestStoragePermission() {

        startPayment();

//        Dexter.withActivity(this)
//                .withPermissions(
//                        Manifest.permission.RECEIVE_SMS,
//                        Manifest.permission.READ_SMS)
//
//                .withListener(new MultiplePermissionsListener() {
//                    @Override
//                    public void onPermissionsChecked(MultiplePermissionsReport report) {
//                        if (report.areAllPermissionsGranted()) {
//                            startPayment();
//                        }
//                        if (report.isAnyPermissionPermanentlyDenied()) {
//
//                            showPermissionDialog();
//                        }
//                    }
//
//                    @Override
//                    public void onPermissionRationaleShouldBeShown(List<PermissionRequest> permissions, PermissionToken token) {
//                        token.continuePermissionRequest();
//                    }
//                }).
//                withErrorListener(new PermissionRequestErrorListener() {
//                    @Override
//                    public void onError(DexterError error) {
//                        Toast.makeText(PaymentActivity.this, "Error occurred! ", Toast.LENGTH_SHORT).show();
//                    }
//                })
//                .onSameThread()
//                .check();
    }

    public static void openSettings(Context context) {
        Intent intent = new Intent();
        intent.setAction(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
        intent.setData(Uri.fromParts("package", BuildConfig.APPLICATION_ID, null));
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        context.startActivity(intent);
    }

    private void showPermissionDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(PaymentActivity.this);
        builder.setTitle("Need Permissions");
        builder.setMessage("This app needs permission to use this feature. You can grant them in app settings.");
        builder.setPositiveButton("GOTO SETTINGS", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
                openSettings(PaymentActivity.this);
                //   openSettings();
            }
        });
        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });
        builder.show();
    }

    private void callAPIforPaymentSuccess(final String payment_id,String order_Id) {
        final ProgressBarCustom progressBarCustom=new ProgressBarCustom(PaymentActivity.this);
        progressBarCustom.showProgress();
        try {
            String token = UserPrefrence.getInstance(PaymentActivity.this).getAuthtoken();
            token="bearer " + token;

            JSONObject jsonObject3 = new JSONObject();
            jsonObject3.put("userId",UserPrefrence.getInstance(PaymentActivity.this).getUserid());
            jsonObject3.put("paymentId", payment_id);
            jsonObject3.put("orderId",order_Id);

            TypedInput in3 = new TypedByteArray("application/json", jsonObject3.toString().getBytes("UTF-8"));
            API_HIT_COUNT++;
            RestClient.getInstance(PaymentActivity.this).get().postPaymentIdOnServer(token,in3,new Callback<GetPaymentResponce>() {

                        @Override
                        public void success(final GetPaymentResponce res, Response response) {
                            progressBarCustom.cancelProgress();
                            if(res.getResponseMessage().equalsIgnoreCase("Success"))
                            {
                                Toast.makeText(PaymentActivity.this, res.getMessage(), Toast.LENGTH_SHORT).show();
                                UserPrefrence.getInstance(PaymentActivity.this).setUsertype(Constant.USERTYPE_PREMIUM);
                                API_HIT_COUNT = 0;
                                Intent intent=new Intent();
                                setResult(RESULT_OK,intent);
                                finish();
                            }else{
                                if (API_HIT_COUNT<=3)
                                {
                                   callAPIforPaymentSuccess(payment_id,orderId);
                                }else{
                                    Log.i("","try api 3 times but not connected to server");
                                    Toast.makeText(PaymentActivity.this, res.getMessage(), Toast.LENGTH_SHORT).show();
                                }
                            }

                        }
                        @Override
                        public void failure(final RetrofitError error) {
                            progressBarCustom.cancelProgress();
                            error.printStackTrace();
                            if (API_HIT_COUNT<=3)
                            {
                                callAPIforPaymentSuccess(payment_id,orderId);
                            }else{
                                Log.i("","try api 3 times but not connected to server");
                                if (error != null) {
                                    Utils utils=new Utils(PaymentActivity.this);
                                    utils.showError(error);
                                }
                            }
                        }
                    });
        }catch (Exception e)
        {
            progressBarCustom.cancelProgress();
            e.printStackTrace();
        }
    }

}
