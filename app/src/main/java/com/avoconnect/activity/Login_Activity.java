package com.avoconnect.activity;

import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.os.StrictMode;
import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.text.InputType;
import android.text.method.PasswordTransformationMethod;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.avoconnect.Constants.Constant;
import com.avoconnect.Prefrence.UserPrefrence;
import com.avoconnect.R;
import com.avoconnect.Util.ProgressBarCustom;
import com.avoconnect.Util.Utils;
import com.avoconnect.api.RestClient;
import com.avoconnect.responsemodel.LoginUser_Model;
import com.avoconnect.responsemodel.RegistrationOtp_Model;
import com.avoconnect.responsemodel.SocialLoginResponce;
import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.Profile;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.InstanceIdResult;
import com.twitter.sdk.android.core.Callback;
import com.twitter.sdk.android.core.Result;
import com.twitter.sdk.android.core.Twitter;
import com.twitter.sdk.android.core.TwitterException;
import com.twitter.sdk.android.core.TwitterSession;
import com.twitter.sdk.android.core.identity.TwitterAuthClient;
import com.twitter.sdk.android.core.identity.TwitterLoginButton;

import org.json.JSONObject;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Arrays;
import java.util.LinkedHashMap;

import okhttp3.RequestBody;
import retrofit.RetrofitError;
import retrofit.client.Response;
import retrofit.mime.TypedByteArray;
import retrofit.mime.TypedInput;

public class Login_Activity extends AppCompatActivity implements  GoogleApiClient.
        OnConnectionFailedListener,View.OnClickListener {

    private CallbackManager callbackManager;
    public static final String PACKAGE = "com.avoconnect";
    private static final String TAG = Login_Activity.class.getSimpleName();
    GoogleApiClient mGoogleApiClient;
    private static final int RC_SIGN_IN = 1;

    private TwitterLoginButton twitterLoginButton;
    private TwitterAuthClient client;

    EditText et_email;
    TextView txt_orlogin,txt_dontaccount;
    TextView txt_login;
    ImageView txt_facebook,txt_google;
    ImageView lock, hintEmailIcon;
    Utils utils;
    boolean ispassword_visisble=true;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        FacebookSdk.sdkInitialize(getApplicationContext());
        Twitter.initialize(this);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.login_screen);

        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);

        callbackManager = CallbackManager.Factory.create();
        client = new TwitterAuthClient();
        Log.e("==login",getResources().getString(R.string.internet_connection));
        utils=new Utils(this);
       inits();
    }

    private void inits() {




        if(UserPrefrence.getInstance(this).getFcmToken().length()==0)
        {
            FirebaseInstanceId.getInstance().getInstanceId().addOnSuccessListener(new OnSuccessListener<InstanceIdResult>() {
                @Override
                public void onSuccess(InstanceIdResult instanceIdResult) {
                    String newToken = instanceIdResult.getToken();
                    UserPrefrence.getInstance(Login_Activity.this).setFcmToken(newToken);
                    Log.e("===mytoken",newToken);
                }
            });
        }

        et_email=(EditText)findViewById(R.id.et_email);



        txt_dontaccount=(TextView) findViewById(R.id.txt_dontaccount);
        txt_dontaccount.setText(getResources().getString(R.string.copyrite));
        txt_orlogin=(TextView) findViewById(R.id.txt_orlogin);

        txt_login=(TextView) findViewById(R.id.txt_login);
        txt_facebook=(ImageView) findViewById(R.id.txt_facebook);
        txt_google=(ImageView)findViewById(R.id.txt_google);

        hintEmailIcon =(ImageView)findViewById(R.id.hintEmailIcon);



        txt_login.setOnClickListener(this);
        txt_facebook.setOnClickListener(this);
        txt_google.setOnClickListener(this);




//        Auth.GoogleSignInApi.signOut(mGoogleApiClient).setResultCallback(
//                new ResultCallback<Status>() {
//                    @Override
//                    public void onResult(Status status) {
//                        Toast.makeText(Login_Activity.this, "Logout Successfully!", Toast.LENGTH_SHORT).show();
//                    }
//                });



    //    twitterLoginButton = findViewById(R.id.login_button);

      //  defaultLoginTwitter();
        googlesignincheck();
        setLanguage();

        et_email.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View mView, boolean hasFocus) {
                if (hasFocus)
                {
                    addOrRemoveProperty(hintEmailIcon, RelativeLayout.CENTER_VERTICAL, false);
                }else{
                    if (et_email.getText().toString().trim().length()>0)
                    {
                        addOrRemoveProperty(hintEmailIcon, RelativeLayout.CENTER_VERTICAL, false);
                    }else{
                        addOrRemoveProperty(hintEmailIcon, RelativeLayout.CENTER_VERTICAL, true);
                    }
                }
            }
        });





    }

    private void addOrRemoveProperty(ImageView view ,int property, boolean flag){
        RelativeLayout.LayoutParams layoutParams = (RelativeLayout.LayoutParams) view.getLayoutParams();
        if(flag){
            view.setColorFilter(ContextCompat.getColor(Login_Activity.this, R.color.light_grey));
            layoutParams.addRule(property);
        }else {
            view.setColorFilter(ContextCompat.getColor(Login_Activity.this, R.color.background_color));
            layoutParams.removeRule(property);
        }
        view.setLayoutParams(layoutParams);
    }


    private void setLanguage() {

    }

    public void defaultLoginTwitter() {
        //check if user is already authenticated or not

            //if user is not authenticated start authenticating
            twitterLoginButton.setCallback(new Callback<TwitterSession>() {
                @Override
                public void success(Result<TwitterSession> result) {
                    // Do something with result, which provides a TwitterSession for making API calls
                    TwitterSession twitterSession = result.data;
                    //call fetch email only when permission is granted
                    fetchTwitterEmail(twitterSession);
                }

                @Override
                public void failure(TwitterException exception) {
                    // Do something on failure
                    Toast.makeText(Login_Activity.this, "Failed to authenticate. Please try again.", Toast.LENGTH_SHORT).show();
                }
            });

    }

    public void fetchTwitterEmail(final TwitterSession twitterSession) {
        client.requestEmail(twitterSession, new Callback<String>() {
            @Override
            public void success(Result<String> result) {
                //here it will give u only email and rest of other information u can get from TwitterSession
                Log.e("User Id : " , twitterSession.getUserId() + "\nScreen Name : " + twitterSession.getUserName() + "\nEmail Id : " + result.data);
            }

            @Override
            public void failure(TwitterException exception) {
                Toast.makeText(Login_Activity.this, "Failed to authenticate. Please try again.", Toast.LENGTH_SHORT).show();
            }
        });
    }
    private void googlesignincheck() {

        GoogleSignInOptions options = new
                GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestEmail()
                .build();


            mGoogleApiClient = new GoogleApiClient.Builder(this)
                    .enableAutoManage(this, Login_Activity.this)
                    .addApi(Auth.GOOGLE_SIGN_IN_API, options)
                    .build();


    }
    private void signIn() {
        Intent signInIntent = Auth.GoogleSignInApi.getSignInIntent(mGoogleApiClient);
        startActivityForResult(signInIntent, RC_SIGN_IN);
    }

    private void launchFacebook() {
        LoginManager.getInstance().logInWithReadPermissions(
                this,
                Arrays.asList( "email", "public_profile"));

        LoginManager.getInstance().registerCallback(callbackManager,
                new FacebookCallback<LoginResult>() {
                    @Override
                    public void onSuccess(LoginResult loginResult) {
                        setFacebookData(loginResult);
                    }

                    @Override
                    public void onCancel() {
                    }

                    @Override
                    public void onError(FacebookException exception) {
                    }
                });

    }

    private void setFacebookData(LoginResult loginResult) {
        GraphRequest request = GraphRequest.newMeRequest(
                loginResult.getAccessToken(),
                new GraphRequest.GraphJSONObjectCallback() {
                    @Override
                    public void onCompleted(JSONObject object, GraphResponse response) {
                        // Application code
                        try {
                            Log.e("Response",response.toString());
                            String email="",lastName="";

                            if(response.getJSONObject().has("email")) {
                                 email = response.getJSONObject().getString("email");
                            }

                            String firstName = response.getJSONObject().getString("first_name");

                            if(response.getJSONObject().has("last_name")) {
                                 lastName = response.getJSONObject().getString("last_name");
                            }



                            Profile profile = Profile.getCurrentProfile();
                            String id = profile.getId();
                            String link = profile.getLinkUri().toString();
                            Log.e("Link",link);
                            if (Profile.getCurrentProfile()!=null)
                            {
                                Log.e("Login", "ProfilePic" + Profile.getCurrentProfile().getProfilePictureUri(200, 200));
                            }

                            socialLogin(firstName+""+lastName,email,"",id,"FACEBOOK");
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                });
        Bundle parameters = new Bundle();
        parameters.putString("fields", "id,email,first_name,last_name,gender");
        request.setParameters(parameters);
        request.executeAsync();
    }



    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {

        try {
            if(requestCode==RC_SIGN_IN){
                GoogleSignInResult result = Auth.GoogleSignInApi.getSignInResultFromIntent(data);
                handleSignInResult(result);
            }
//            facoebook
            else if(requestCode==64206)
            {
                callbackManager.onActivityResult(requestCode, resultCode, data);
            }
            else{
                client.onActivityResult(requestCode, resultCode, data);
                twitterLoginButton.onActivityResult(requestCode, resultCode, data);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void handleSignInResult(GoogleSignInResult result){
        if(result.isSuccess()){
            GoogleSignInAccount account = result.getSignInAccount();
            String idToken = account.getId();
            String  name = account.getDisplayName();
            String email = account.getEmail();
            socialLogin(name,email,"",idToken,"GPLUS");
        }else{
            // Google Sign In failed, update UI appropriately
            Log.e(TAG, "Login Fialed. "+result);
            Toast.makeText(this, "Login Failed", Toast.LENGTH_SHORT).show();
        }
    }

    public void generateHashkey(){
        try {
            PackageInfo info = getPackageManager().getPackageInfo(
                    PACKAGE,
                    PackageManager.GET_SIGNATURES);
            for (Signature signature : info.signatures) {
                MessageDigest md = MessageDigest.getInstance("SHA");
                md.update(signature.toByteArray());

                Log.d("KeyHash:", Base64.encodeToString(md.digest(), Base64.DEFAULT));
            }
        } catch (PackageManager.NameNotFoundException e) {
            Log.d(TAG, e.getMessage(), e);
        } catch (NoSuchAlgorithmException e) {
            Log.d(TAG, e.getMessage(), e);
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        try {
            LoginManager.getInstance().logOut();
            AccessToken.setCurrentAccessToken(null);

        }catch (Exception e)
        {
            e.printStackTrace();
        }
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

    @Override
    public void onClick(View view) {
        switch (view.getId())
        {
            case  R.id.txt_forgot:
                Intent intent=new Intent(Login_Activity.this,ForgetPassword_Actitivty.class);
                startActivity(intent);
                break;
            case  R.id.txt_login:
                validation();
                break;
            case  R.id.txt_facebook:
                launchFacebook();
                break;
            case  R.id.txt_google:
                signIn();
                break;
            case  R.id.txt_signup:
                Intent intent01=new Intent(Login_Activity.this,Registration_Activity.class);
                startActivity(intent01);
                break;
        }
    }

    private void validation() {
        String getemail=et_email.getText().toString();
        if(getemail.length()==0)
        {
            ProgressBarCustom.customDialog(this,getResources().getString(R.string.enter_mobile_number),false);
        }else if(getemail.length()<10)
        {
            ProgressBarCustom.customDialog(this,getResources().getString(R.string.enter_mobile),false);
        }else{
            if(utils.isNetworkAvailable(this)==true) {
                callApi();

            }else{
                ProgressBarCustom.customDialog(this,getResources().getString(R.string.internet_connection),false);
            }
        }

    }



    private void callApi() {


        final ProgressBarCustom progressBarCustom=new ProgressBarCustom(Login_Activity.this);
        progressBarCustom.showProgress();
        try {

            RestClient.getInstance(this).get().registration_otp(et_email.getText().toString(),new retrofit.Callback<RegistrationOtp_Model>() {

                @Override
                public void success(final RegistrationOtp_Model res, Response response) {
                    progressBarCustom.cancelProgress();



                    if(res.getResponseMessage().equalsIgnoreCase(Constant.SUCCESS))
                    {

                        UserPrefrence.getInstance(Login_Activity.this)
                                .setMobile(res.getMobile().toString());


                        if(res.getNewUser()==true)
                        {
                            Intent intent=new Intent(Login_Activity.this,TermsCondition_Actitivity.class);
                            startActivity(intent);
                        }else{
                            Toast.makeText(Login_Activity.this, res.getMessage(), Toast.LENGTH_SHORT).show();
                            Intent intent=new Intent(Login_Activity.this,Otpverify_Actitivity.class);
                            startActivity(intent);
                        }




//                        UserPrefrence.getInstance(Login_Activity.this)
//                                .setUserid(res.getUserid().toString());
////
//                        UserPrefrence.getInstance(Login_Activity.this)
//                                .setIsLogin(res.getMessage());
//
//                        UserPrefrence.getInstance(Login_Activity.this)
//                                .setUsername(res.getFirstName().toString());
//
//                        UserPrefrence.getInstance(Login_Activity.this)
//                                .setEmail(res.getEmail().toString());
//
//                        UserPrefrence.getInstance(Login_Activity.this)
//                                .setAuthtoken(res.getAccessToken().toString());
//
//
//                        if(res.getUserType()!=null)
//                        {
//                            if(res.getUserType().length()==0)
//                            {
//                                UserPrefrence.getInstance(Login_Activity.this).setUsertype(Constant.USERTYPE_FREE);
//                            }else{
//                                UserPrefrence.getInstance(Login_Activity.this).setUsertype(res.getUserType().toString());
//                            }
//                        }
//
//                       //UserPrefrence.getInstance(Login_Activity.this).setUsertype(Constant.USERTYPE_FREE);
//
//                        if(res.getMobile()!=null)
//                        {
//                            UserPrefrence.getInstance(Login_Activity.this).setMobile(res.getMobile().toString());
//                        }
//                        try {
//                            UserPrefrence.getInstance(Login_Activity.this).setIsAskemail(res.getAskEmailOnViewCard() + "");
//                            UserPrefrence.getInstance(Login_Activity.this).setIsNotificationOn(res.getIsNotificationActive() + "");
//                            UserPrefrence.getInstance(Login_Activity.this).setIsAuthenticaltion(res.getIsTwoWayAuthEnabled() + "");
//                        }catch (Exception e)
//                        {
//                            e.printStackTrace();
//                        }
//
//                        Toast.makeText(Login_Activity.this, res.getMessage(), Toast.LENGTH_SHORT).show();
//                        Intent intent=new Intent(Login_Activity.this,HomeActivity.class);
//                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
//                        startActivity(intent);
//                        finish();

                    }else{
                        Toast.makeText(Login_Activity.this, res.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                }

                @Override
                public void failure(final RetrofitError error) {
                    progressBarCustom.cancelProgress();
                    if (error != null) {
                        utils.showError(error);
                    }
                }
            });

        }catch (Exception e)
        {
            progressBarCustom.cancelProgress();
            //progress.cancelProgress();
            e.printStackTrace();
        }
    }

    public void socialLogin(String name,String email,String mobile,String socialid,String socialLoginWith)
    {

        final ProgressBarCustom progressBarCustom=new ProgressBarCustom(Login_Activity.this);
        progressBarCustom.showProgress();
        try {
     //       String getpassword=et_password.getText().toString();
       //     String checking=utils.convertpasswordtobase64(getpassword);

            JSONObject jsonObject3 = new JSONObject();
            jsonObject3.put("name",name);
            jsonObject3.put("email", email);
            jsonObject3.put("mobile",mobile);
            jsonObject3.put("socialId",socialid);
            jsonObject3.put("socialLoginWith",socialLoginWith);
            jsonObject3.put("deviceId",UserPrefrence.getInstance(Login_Activity.this).getFcmToken());
            jsonObject3.put("deviceAgent","ANDROID");

            TypedInput in3 = new TypedByteArray("application/json", jsonObject3.toString().getBytes("UTF-8"));

            RestClient.getInstance(this).get().socialLogin( in3, new retrofit.Callback<SocialLoginResponce>() {

                @Override
                public void success(final SocialLoginResponce res, Response response) {
                    progressBarCustom.cancelProgress();
                    try {
                        if (res.getResponseMessage().equalsIgnoreCase("Success")) {
                            UserPrefrence.getInstance(Login_Activity.this)
                                    .setUserid(res.getUserId().toString());

                            UserPrefrence.getInstance(Login_Activity.this)
                                    .setIsLogin(res.getUserId().toString());

                            UserPrefrence.getInstance(Login_Activity.this)
                                    .setUsername(res.getFirstName().toString());

                            UserPrefrence.getInstance(Login_Activity.this)
                                    .setEmail(res.getUsername().toString());

                            UserPrefrence.getInstance(Login_Activity.this)
                                    .setAuthtoken(res.getAccessToken().toString());

                            if (res.getUserType()!= null) {
                                UserPrefrence.getInstance(Login_Activity.this).setUsertype(res.getUserType().toString());
                            }

                            try {
                                UserPrefrence.getInstance(Login_Activity.this).setIsAskemail(res.getAskEmailOnViewCard() + "");
                                UserPrefrence.getInstance(Login_Activity.this).setIsNotificationOn(res.getIsNotificationActive() + "");
                                UserPrefrence.getInstance(Login_Activity.this).setIsAuthenticaltion(res.getIsTwoWayAuthEnabled() + "");
                            }catch (Exception e)
                            {
                                e.printStackTrace();
                            }

                            Toast.makeText(Login_Activity.this, res.getMessage(), Toast.LENGTH_SHORT).show();
                            Intent intent = new Intent(Login_Activity.this, HomeActivity.class);
                            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                            startActivity(intent);
                            finish();
                        } else {
                            Toast.makeText(Login_Activity.this, res.getMessage(), Toast.LENGTH_SHORT).show();
                        }
                    }catch (Exception e)
                    {
                        e.printStackTrace();
                    }
                }

                @Override
                public void failure(final RetrofitError error) {
                    progressBarCustom.cancelProgress();
                    error.printStackTrace();
                    if (error != null) {
                        utils.showError(error);
                    }
                }
            });

        }catch (Exception e)
        {
            progressBarCustom.cancelProgress();
            //progress.cancelProgress();
            e.printStackTrace();
        }
    }
    @Override
    public void onBackPressed() {
        finishAffinity();
    }
}



