package com.avoconnect.activity;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.widget.NestedScrollView;

import android.app.ProgressDialog;
import android.graphics.Bitmap;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageView;
import android.widget.TextView;

import com.avoconnect.Constants.Constant;
import com.avoconnect.R;
import com.avoconnect.Util.ProgressBarCustom;

@RequiresApi(api = Build.VERSION_CODES.M)
public class WebView_Activity extends AppCompatActivity implements View.OnScrollChangeListener {


    WebView web;
    ImageView backimage;
    TextView heading;
    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_web_view_);

        web = (WebView) findViewById(R.id.privacyPolicyWebView);

        backimage=(ImageView)findViewById(R.id.backimage);
        heading=(TextView) findViewById(R.id.heading);
        web.getSettings().setJavaScriptEnabled(true);

        web.getSettings().setAppCacheEnabled(true);
        web.getSettings().setCacheMode(WebSettings.LOAD_CACHE_ELSE_NETWORK);
        web.getSettings().setRenderPriority(WebSettings.RenderPriority.HIGH);
        web.getSettings().setDomStorageEnabled(true);
        web.getSettings().setAppCacheEnabled(true);
        web.getSettings().setLoadsImagesAutomatically(true);
        web.getSettings().setPluginState(WebSettings.PluginState.ON);
        web.setWebViewClient(new WebViewController());
        web.setOnScrollChangeListener(this);


        Bundle bundle=getIntent().getExtras();

        if(bundle!=null)
        {
            if(bundle.getString("type")!=null)
            {
                heading.setText(bundle.getString("type"));
                if(bundle.getString("url")!=null)
                {
                    Log.e("=====URL====",bundle.getString("url"));
                    web.loadUrl(bundle.getString("url"));
                }
            }
        }



        backimage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
    }





    public class WebViewController extends WebViewClient
    {
        @Override
        public void onPageStarted(WebView view, String url, Bitmap favicon) {
            super.onPageStarted(view, url, favicon);
            new ProgressBarCustom(WebView_Activity.this);
            ProgressBarCustom.showProgress();
        }

        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            view.loadUrl(url);
            return true;

        }
        public void onPageFinished(WebView view, String url) {
            ProgressBarCustom.cancelProgress();
        }
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event)
    {
        if ((keyCode == KeyEvent.KEYCODE_BACK) && web.canGoBack()) {
            web.goBack();
            return true;
        }
        return super.onKeyDown(keyCode, event);
    }


    @Override
    public void onScrollChange(View view, int i, int i1, int i2, int i3) {
        int height = (int) Math.floor(web.getContentHeight() * web.getScale());
        int webViewHeight = web.getMeasuredHeight();
        if(web.getScrollY() + webViewHeight >= height){
            Log.e("THE END", "reached");
        }
    }


}
