package com.avoconnect.activity;

import android.Manifest;
import android.accounts.AccountManager;
import android.annotation.SuppressLint;
import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.provider.CalendarContract;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;


import com.avoconnect.Constants.Constant;
import com.avoconnect.Prefrence.UserPrefrence;
import com.avoconnect.R;
import com.avoconnect.Util.GPS_TRACKER;
import com.avoconnect.Util.ProgressBarCustom;
import com.avoconnect.Util.Utils;
import com.avoconnect.api.RestClient;
import com.avoconnect.responsemodel.AddEvent_Model;
import com.avoconnect.responsemodel.CardBackgroundColorChnage_Model;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.api.client.extensions.android.http.AndroidHttp;
import com.google.api.client.googleapis.extensions.android.gms.auth.GoogleAccountCredential;
import com.google.api.client.http.HttpTransport;
import com.google.api.client.json.JsonFactory;
import com.google.api.client.json.jackson2.JacksonFactory;
import com.google.api.client.util.ExponentialBackOff;
import com.google.api.services.calendar.CalendarScopes;
import com.karumi.dexter.Dexter;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.PermissionDeniedResponse;
import com.karumi.dexter.listener.PermissionGrantedResponse;
import com.karumi.dexter.listener.PermissionRequest;
import com.karumi.dexter.listener.single.PermissionListener;


import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.Duration;
import java.time.LocalTime;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;

import retrofit.RetrofitError;
import retrofit.client.Response;
import retrofit.mime.TypedByteArray;
import retrofit.mime.TypedInput;


public class EventaddupdateAcitivity extends AppCompatActivity implements View.OnClickListener, DatePickerDialog.OnDateSetListener{

    RelativeLayout rel_start;
    TextView txt_time;
    TextView txt_startdate,txt_choosecalendre,txt_save;
    String serverstartdate="",server_time="";

    private com.google.api.services.calendar.Calendar mService = null;
    GoogleAccountCredential credential;
    private static final String PREF_ACCOUNT_NAME = "accountName";
    private static final String[] SCOPES = {CalendarScopes.CALENDAR};
    final HttpTransport transport = AndroidHttp.newCompatibleTransport();
    final JsonFactory jsonFactory = JacksonFactory.getDefaultInstance();

    static final int REQUEST_ACCOUNT_PICKER = 1000;
    static final int REQUEST_AUTHORIZATION = 1001;
    ImageView backimage;
    EditText et_title,et_note;
    int selected_motnth=0;
    ImageView img_calender;
    DatePicker datepicker;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_update_acitivity);
        rel_start=(RelativeLayout)findViewById(R.id.rel_start);
        txt_startdate=(TextView)findViewById(R.id.txt_startdate);

        et_title=(EditText)findViewById(R.id.et_title) ;
        et_note=(EditText)findViewById(R.id.et_note) ;

        txt_time=(TextView)findViewById(R.id.txt_time);

        txt_choosecalendre=(TextView)findViewById(R.id.txt_choosecalendre);
        txt_save=(TextView)findViewById(R.id.txt_save);

        backimage=(ImageView)findViewById(R.id.backimage);
        img_calender=(ImageView)findViewById(R.id.img_calender);
        backimage.setOnClickListener(this);
        img_calender.setOnClickListener(this);

        txt_save.setOnClickListener(this);
        rel_start.setOnClickListener(this);
        txt_startdate.setOnClickListener(this);
        txt_time.setOnClickListener(this);

            Dexter.withActivity(this)
                    .withPermission(Manifest.permission.GET_ACCOUNTS)
                    .withListener(new PermissionListener() {
                        @Override public void onPermissionGranted(PermissionGrantedResponse response) {

                            runOnUiThread(new Runnable() {
                                public void run() {
                                    SharedPreferences settings = getPreferences(Context.MODE_PRIVATE);
                                    credential = GoogleAccountCredential.usingOAuth2(
                                            getApplicationContext(), Arrays.asList(SCOPES))
                                            .setBackOff(new ExponentialBackOff())
                                            .setSelectedAccountName(settings.getString(PREF_ACCOUNT_NAME, null));



                                    mService = new com.google.api.services.calendar.Calendar.Builder(
                                            transport, jsonFactory, credential)
                                            .setApplicationName("Mohmmed Altamash")
                                            .build();


                                    txt_choosecalendre.setOnClickListener(EventaddupdateAcitivity.this);
                                }
                            });



                        }
                        @Override public void onPermissionDenied(PermissionDeniedResponse response) {

                        }
                        @Override public void onPermissionRationaleShouldBeShown(PermissionRequest permission, PermissionToken token) {

                        }
                    }).check();

    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    @Override
    public void onClick(View view) {
        switch (view.getId())
        {
            case R.id.txt_startdate:

                Calendar calendar = Calendar.getInstance(TimeZone.getDefault());
                DatePickerDialog dialog = new DatePickerDialog(EventaddupdateAcitivity.this, this,
                        calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH),
                        calendar.get(Calendar.DAY_OF_MONTH));
                dialog.getDatePicker().setMinDate(System.currentTimeMillis());
                if (datepicker != null) {
                    dialog.updateDate(   datepicker.getYear(),    datepicker.getMonth() - 1,  datepicker.getDayOfMonth());

                }
                dialog.show();

//                Calendar calendar = Calendar.getInstance(TimeZone.getDefault());
//                DatePickerDialog dialog = new DatePickerDialog(EventaddupdateAcitivity.this, this,
//                        calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH),
//                        calendar.get(Calendar.DAY_OF_MONTH));
//                dialog.getDatePicker().setMinDate(System.currentTimeMillis());
//                dialog.show();
                break;
            case R.id.img_calender:
                Calendar calendar01 = Calendar.getInstance(TimeZone.getDefault());
                DatePickerDialog dialog01 = new DatePickerDialog(EventaddupdateAcitivity.this, this,
                        calendar01.get(Calendar.YEAR), calendar01.get(Calendar.MONTH),
                        calendar01.get(Calendar.DAY_OF_MONTH));
                dialog01.getDatePicker().setMinDate(System.currentTimeMillis());
                if (datepicker != null) {
                    dialog01.updateDate(   datepicker.getYear(),    datepicker.getMonth() - 1,  datepicker.getDayOfMonth());

                }
                dialog01.show();
                break;

            case R.id.txt_time:
                opentimepickerdialog();
                break;
            case R.id.txt_choosecalendre:
                if (isGooglePlayServicesAvailable()) {
                    refreshResults();
                } else {
                    System.out.println("Google Play Services required: " +
                            "after installing, close and relaunch this app.");
                }
                break;
            case R.id.txt_save:
                validation();
                break;
            case R.id.backimage:
                Intent intent=new Intent();
                setResult(RESULT_OK,intent);
                finish();
                break;


        }
    }


    @RequiresApi(api = Build.VERSION_CODES.O)
    private void validation() {
        Date date = new Date();
        String modifiedDate= new SimpleDateFormat("yyyy-MM-dd").format(date);

        SimpleDateFormat sdf = new SimpleDateFormat("HH:mm");
        String currenttime = sdf.format(new Date());

        Log.e("======date",modifiedDate+" "+serverstartdate);
        Log.e("======time",currenttime+" "+server_time);
        if(et_title.getText().toString().length()==0)
        {
            ProgressBarCustom.customDialog(this,getResources().getString(R.string.enter_title),false);
        }
        else if(txt_startdate.getText().toString().length()==0)
        {
            ProgressBarCustom.customDialog(this,getResources().getString(R.string.select_startdate),false);
        }
        else if(txt_choosecalendre.getText().toString().length()==0)
        {
            ProgressBarCustom.customDialog(this,getResources().getString(R.string.choose_calender),false);
        }else if(txt_time.getText().toString().length()==0)
        {
            ProgressBarCustom.customDialog(this,getResources().getString(R.string.event_time),false);
        }

       else if(modifiedDate.equalsIgnoreCase(serverstartdate))
        {

            if(checktimings(server_time,currenttime)==true) {
               callApi();
            }else{
                ProgressBarCustom.customDialog(this,getResources().getString(R.string.past_time),false);
            }
        }

        else{
           callApi();
        }

    }

    private boolean checktimings(String time, String endtime) {

        String pattern = "HH:mm";
        SimpleDateFormat sdf = new SimpleDateFormat(pattern);

        try {
            Date date1 = sdf.parse(time);
            Date date2 = sdf.parse(endtime);

            if(date1.after(date2)) {
                return true;
            }
            else if(date1.equals(date2)) {
                return true;
            }

            else {

                return false;
            }
        } catch (ParseException e){
            e.printStackTrace();
        }
        return false;
    }

    private void callApi() {


        final ProgressBarCustom progressBarCustom=new ProgressBarCustom(EventaddupdateAcitivity.this);
        progressBarCustom.showProgress();
        try {
            String token = UserPrefrence.getInstance(this).getAuthtoken();
            token="bearer " + token;
            JSONObject jsonObject3 = new JSONObject();
            jsonObject3.put("title", et_title.getText().toString());
            jsonObject3.put("email", txt_choosecalendre.getText().toString());
            jsonObject3.put("note",et_note.getText().toString() );
            jsonObject3.put("month",selected_motnth);
            jsonObject3.put("date",serverstartdate+" "+server_time);
            TypedInput in3 = new TypedByteArray("application/json", jsonObject3.toString().getBytes("UTF-8"));

            RestClient.getInstance(this).get().addevent( token,UserPrefrence.getInstance(this).getUserid(),in3, new retrofit.Callback<AddEvent_Model>() {

                @Override
                public void success(final AddEvent_Model res, Response response) {
                    progressBarCustom.cancelProgress();

                    if(res.getResponseMessage().equalsIgnoreCase(Constant.SUCCESS))
                    {
                        Toast.makeText(EventaddupdateAcitivity.this, res.getMessage(), Toast.LENGTH_SHORT).show();
                        Intent intent=new Intent();
                        setResult(RESULT_OK,intent);
                        finish();
                    }else{
                        Toast.makeText(EventaddupdateAcitivity.this, res.getMessage(), Toast.LENGTH_SHORT).show();
                    }

                }
                @Override
                public void failure(final RetrofitError error) {
                    progressBarCustom.cancelProgress();
                    error.printStackTrace();
                    if (error != null) {
                        Utils utils=new Utils(EventaddupdateAcitivity.this);
                        utils.showError(error);
                    }

                }
            });

        }catch (Exception e)
        {
            progressBarCustom.cancelProgress();
            //progress.cancelProgress();
            e.printStackTrace();
        }

    }


    private void opentimepickerdialog() {
        Calendar mcurrentTime = Calendar.getInstance();
      final   int hour = mcurrentTime.get(Calendar.HOUR_OF_DAY);
       final int minute = mcurrentTime.get(Calendar.MINUTE);
        TimePickerDialog mTimePicker;
        mTimePicker = new TimePickerDialog(EventaddupdateAcitivity.this, new TimePickerDialog.OnTimeSetListener() {
            @Override
            public void onTimeSet(TimePicker timePicker, int selectedHour, int selectedMinute) {
                if(selectedMinute<10)
                {
                    server_time=selectedHour + ":" + "0"+selectedMinute;
                }else{
                    server_time=selectedHour + ":" + selectedMinute;
                }
                Utils utils=new Utils(EventaddupdateAcitivity.this);
                String time=utils.convertTime(selectedHour + ":" + selectedMinute);
                txt_time.setText(time);

            }
        }, hour, minute,false);
        mTimePicker.setTitle("Select Time");
        mTimePicker.show();
    }


    private void addEvent() {

        Log.e("=====dat==",serverstartdate+" "+server_time);

        Calendar beginTime = Calendar.getInstance();
        beginTime.set(2019, 11, 19, 7, 30);

        Calendar endTime = Calendar.getInstance();
        endTime.set(2019, 12, 19, 8, 30);

        Intent intent = new Intent(Intent.ACTION_INSERT)
                .setData(CalendarContract.Events.CONTENT_URI)
                .putExtra(CalendarContract.EXTRA_EVENT_BEGIN_TIME,
                        beginTime.getTimeInMillis())
                .putExtra(CalendarContract.EXTRA_EVENT_END_TIME,
                        endTime.getTimeInMillis())
                .putExtra(CalendarContract.Events.TITLE, "Yoga")
                .putExtra(CalendarContract.Events.DESCRIPTION, "Group class")
                .putExtra(CalendarContract.Events.EVENT_LOCATION, "The gym")
                .putExtra(CalendarContract.Events.AVAILABILITY, CalendarContract.Events.AVAILABILITY_BUSY)
                .putExtra(Intent.EXTRA_EMAIL, "manjeetlms@gmail.com");

        startActivity(intent);

    }

    private boolean isGooglePlayServicesAvailable() {
        final int connectionStatusCode =
                GooglePlayServicesUtil.isGooglePlayServicesAvailable(this);
        if (GooglePlayServicesUtil.isUserRecoverableError(connectionStatusCode)) {
            Log.e("=eroor",connectionStatusCode+"");
            return false;
        } else if (connectionStatusCode != ConnectionResult.SUCCESS) {
            return false;
        }
        return true;
    }

    @SuppressLint("LongLogTag")
    private void refreshResults() {

        if (isDeviceOnline()) {
            chooseAccount();
        } else {
            Log.e("No network connection available.","=======");;
        }


    }
    private boolean isDeviceOnline() {
        ConnectivityManager connMgr =
                (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connMgr.getActiveNetworkInfo();
        return (networkInfo != null && networkInfo.isConnected());
    }


    private void chooseAccount() {
        startActivityForResult(
                credential.newChooseAccountIntent(), REQUEST_ACCOUNT_PICKER);
    }


    @Override
    protected void onActivityResult(
            int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {

            case REQUEST_ACCOUNT_PICKER:
                if (resultCode == RESULT_OK && data != null &&
                        data.getExtras() != null) {
                    String accountName =
                            data.getStringExtra(AccountManager.KEY_ACCOUNT_NAME);
                    if (accountName != null) {
                        credential.setSelectedAccountName(accountName);
                        txt_choosecalendre.setText(accountName);
                    }
                } else if (resultCode == RESULT_CANCELED) {
                    System.out.println("Account unspecified.");
                }
                break;
            case REQUEST_AUTHORIZATION:
                if (resultCode == RESULT_OK) {
                    refreshResults();
                } else {
                    chooseAccount();
                }
                break;
        }

        super.onActivityResult(requestCode, resultCode, data);
    }




    @Override
    public void onDateSet(DatePicker datePicker, int year, int month, int day) {
        selected_motnth=(month+1);

        int months_select=(month+1);
        int day_select=day;
        if(months_select<10)
        {
            serverstartdate=year+"-"+"0"+months_select+"-"+day;

            if(day_select<10)
            {
                serverstartdate=year+"-"+"0"+months_select+"-"+"0"+day;
            }else{
                serverstartdate=year+"-"+"0"+months_select+"-"+day;
            }

        }else{
            serverstartdate=year+"-"+months_select+"-"+day;

            if(day_select<10)
            {
                serverstartdate=year+"-"+months_select+"-"+"0"+day;
            }else{
                serverstartdate=year+"-"+months_select+"-"+day;
            }
        }




        Utils utils=new Utils(EventaddupdateAcitivity.this);
        String date= utils.dateConvert(year+"-"+(month+1)+"-"+day);
        txt_startdate.setText(date);

        datepicker = new DatePicker(EventaddupdateAcitivity.this);
        datepicker.init(year, month + 1, day, null);

    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Intent intent=new Intent();
        setResult(RESULT_OK,intent);
        finish();
    }
}
