package com.avoconnect.activity;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.media.Image;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewTreeObserver;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.avoconnect.Constants.Constant;
import com.avoconnect.R;
import com.avoconnect.Util.ProgressBarCustom;
import com.avoconnect.Util.Utils;
import com.avoconnect.api.RestClient;
import com.avoconnect.responsemodel.Signup_Model;

import org.json.JSONObject;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;
import retrofit.mime.TypedByteArray;
import retrofit.mime.TypedInput;

public class TermsCondition_Actitivity extends AppCompatActivity {

    String name="",email="",password="";
    WebView webview;
    CheckBox checkbox;
    RelativeLayout accept_lay;
    TextView txt_submit;
    ImageView backimage;
    Utils utils;
    Handler handler = new Handler();
    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_terms_condition__actitivity);
        webview=(WebView)findViewById(R.id.webview);
        checkbox=(CheckBox)findViewById(R.id.checkbox);
        accept_lay=(RelativeLayout)findViewById(R.id.accept_lay);
        txt_submit=(TextView)findViewById(R.id.txt_submit);
        backimage=(ImageView)findViewById(R.id.backimage);
        Bundle bundle=getIntent().getExtras();
        if(bundle!=null)
        {
            name=bundle.getString("name");
            email=bundle.getString("email");
            password=bundle.getString("password");
        }
         utils=new Utils(TermsCondition_Actitivity.this);
        webview.getSettings().setJavaScriptEnabled(true);
        webview.getSettings().setAppCacheEnabled(true);
        webview.getSettings().setCacheMode(WebSettings.LOAD_CACHE_ELSE_NETWORK);
        webview.getSettings().setRenderPriority(WebSettings.RenderPriority.HIGH);
        webview.getSettings().setDomStorageEnabled(true);
        webview.getSettings().setAppCacheEnabled(true);
        webview.getSettings().setLoadsImagesAutomatically(true);
        webview.getSettings().setPluginState(WebSettings.PluginState.ON);
        webview.setWebViewClient(new WebViewController());

       // webview.loadUrl("https://www.termsandconditionsgenerator.com/");

        webview.loadUrl(Constant.TERMS_POLICYURL);





//        webview.setWebViewClient(new WebViewClient() {
//            @Override
//            public boolean shouldOverrideUrlLoading(WebView view, String url) {
//                // The webView is about to navigate to the specified url.
//                Log.e("===webivew==",url+" "+view.getTitle());
//                return super.shouldOverrideUrlLoading(view, url);
//            }
//        });


        txt_submit.getBackground().setAlpha(128);
        txt_submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(checkbox.isChecked()==true)
                {
                    //callApi();
                    checkbox.setClickable(false);
                    checkbox.setEnabled(false);
                    Toast.makeText(TermsCondition_Actitivity.this, getResources().getString(R.string.otp_sent_succesfully), Toast.LENGTH_SHORT).show();
                    Intent  intent=new Intent(TermsCondition_Actitivity.this,Otpverify_Actitivity.class);
                    startActivity(intent);
                }else{

                }
            }
        });

        checkbox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if(b==true)
                {
                    txt_submit.getBackground().setAlpha(250);
                }else{
                    txt_submit.getBackground().setAlpha(128);
                }
            }
        });
        backimage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

    }


    private void callApi() {
        final ProgressBarCustom progressBarCustom=new ProgressBarCustom(TermsCondition_Actitivity.this);
        progressBarCustom.showProgress();
        try {

            String checking=utils.convertpasswordtobase64(password);

            JSONObject jsonObject3 = new JSONObject();
            jsonObject3.put("name", name);
            jsonObject3.put("email", email);
            jsonObject3.put("password",checking.trim());
            TypedInput in3 = new TypedByteArray("application/json", jsonObject3.toString().getBytes("UTF-8"));

            RestClient.getInstance(this).get().signup( in3, new Callback<Signup_Model>() {

                @Override
                public void success(final Signup_Model res, Response response) {
                    progressBarCustom.cancelProgress();

                    if(res.getResponseMessage().equalsIgnoreCase(Constant.SUCCESS))
                    {
                        generatepop(res.getMessage());
                       // ProgressBarCustom.customDialog(TermsCondition_Actitivity.this,res.getMessage(),true);
                    }else{
                        Toast.makeText(TermsCondition_Actitivity.this, res.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                }

                @Override
                public void failure(final RetrofitError error) {
                    progressBarCustom.cancelProgress();
                    error.printStackTrace();
                    if (error != null) {
                        utils.showError(error);
                    }
                }
            });
        }catch (Exception e)
        {
            progressBarCustom.cancelProgress();
            //progress.cancelProgress();
            e.printStackTrace();
        }


    }

    private void generatepop(String message) {
        AlertDialog.Builder builder=new AlertDialog.Builder(TermsCondition_Actitivity.this);
        View view= LayoutInflater.from(this).inflate(R.layout.logout_lay,null);
        TextView txtDialogMessage=(TextView)view.findViewById(R.id.txtDialogMessage);
        ImageView center_line=(ImageView)view.findViewById(R.id.center_line);
        center_line.setVisibility(View.GONE);
        txtDialogMessage.setText(message);
        Button btnDialogYes=(Button)view.findViewById(R.id.btnDialogYes);
        Button btnDialogNo=(Button)view.findViewById(R.id.btnDialogNo);
        btnDialogNo.setVisibility(View.GONE);
        builder.setView(view);
        final Dialog dialog=builder.create();
        dialog.setCancelable(false);
        dialog.setCanceledOnTouchOutside(false);

        btnDialogYes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
                Intent intent=new Intent();
                setResult(RESULT_OK,intent);
                finish();

            }
        });
        btnDialogNo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });
        dialog.show();

    }



    public class WebViewController extends WebViewClient
    {
        @Override
        public void onPageStarted(WebView view, String url, Bitmap favicon) {
            super.onPageStarted(view, url, favicon);
            new ProgressBarCustom(TermsCondition_Actitivity.this);
            ProgressBarCustom.showProgress();
        }

        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            Log.e("=====url",url);
            view.loadUrl(url);
            return true;

        }
        public void onPageFinished(WebView view, String url) {

            try {
                Thread.sleep(2000);
                ProgressBarCustom.cancelProgress();

                handler.post(new Runnable() {
                    @Override
                    public void run() {
                        webview.loadUrl("javascript:(function(){ document.body.style.paddingBottom = '200px'})();");
                    }
                });


                webview.setOnScrollChangeListener(new View.OnScrollChangeListener() {
                    @Override
                    public void onScrollChange(View view, int i,final int i1, int i2, int i3) {

                        handler.post(new Runnable() {
                            @Override
                            public void run() {
                                int height = (int) Math.floor(webview.getContentHeight() * webview.getScale());
                                int webViewHeight = webview.getHeight();
                                int cutoff = height - webViewHeight - 10; // Don't be too strict on the cutoff point
                                if (i1 >= cutoff) {
                                    accept_lay.setVisibility(View.VISIBLE);

                                }else{
                                    accept_lay.setVisibility(View.GONE);
                                }
                            }
                        });


                    }
                });

//                webview.getViewTreeObserver()
//                        .addOnScrollChangedListener(new ViewTreeObserver.OnScrollChangedListener() {
//                            @Override
//                            public void onScrollChanged() {
//                                if (webview.getChildAt(0).getBottom()
//                                        <= (webview.getHeight() + webview.getScrollY())) {
//                                    //scroll view is at bottom
//                                    accept_lay.setVisibility(View.VISIBLE);
//                                } else {
//                                    accept_lay.setVisibility(View.GONE);
//                                }
//                            }
//                        });


            }catch (Exception e)
            {
                e.printStackTrace();
            }
        }
    }
}
