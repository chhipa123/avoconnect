package com.avoconnect.activity;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.viewpager.widget.PagerAdapter;
import androidx.viewpager.widget.ViewPager;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.avoconnect.Constants.Constant;
import com.avoconnect.Prefrence.UserPrefrence;
import com.avoconnect.R;
import com.avoconnect.Util.ProgressBarCustom;
import com.avoconnect.Util.Utils;
import com.avoconnect.api.RestClient;
import com.avoconnect.responsemodel.GEtPlanDetails;
import com.avoconnect.responsemodel.GenrateOrderID_Model;
import com.avoconnect.responsemodel.ShareNotificationUserid_Model;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import retrofit.RetrofitError;
import retrofit.client.Response;
import retrofit.mime.TypedByteArray;
import retrofit.mime.TypedInput;

public class PlanUserSubcription_Act extends AppCompatActivity {

    ViewPager viewpager;
    List<GEtPlanDetails.ResourceDatum>list_arr=new ArrayList<>();
    TextView txt_save;
    ImageView backimage;
   int position=0;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_plan_user_subcription_);

        viewpager=(ViewPager)findViewById(R.id.viewpager);
        txt_save=(TextView)findViewById(R.id.txt_save);
        backimage=(ImageView) findViewById(R.id.backimage);
        callApiPlanDetails();


        viewpager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int positions) {
                position=positions;
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

        txt_save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(list_arr!=null)
                {
                    if(list_arr.size()>0)
                    {

                        callApigenrateOrderID(list_arr.get(position).getId()+"");

                    }
                }
            }
        });

        backimage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

    }

    private void callApigenrateOrderID(String planid) {
        final ProgressBarCustom progressBarCustom = new ProgressBarCustom(PlanUserSubcription_Act.this);
        progressBarCustom.showProgress();
        try {

            String token = UserPrefrence.getInstance(PlanUserSubcription_Act.this).getAuthtoken();
            token="bearer " + token;

            JSONObject jsonObject3 = new JSONObject();
            jsonObject3.put("userId", UserPrefrence.getInstance(this).getUserid());
            jsonObject3.put("planId",planid);

            TypedInput in3 = new TypedByteArray("application/json", jsonObject3.toString().getBytes("UTF-8"));

            RestClient.getInstance(PlanUserSubcription_Act.this).get().generateorderid( token,in3,
                    new retrofit.Callback<GenrateOrderID_Model>() {

                        @Override
                        public void success(final GenrateOrderID_Model res, Response response) {
                            progressBarCustom.cancelProgress();
                            if(res.getResponseMessage().equalsIgnoreCase(Constant.SUCCESS))
                            {
                               Intent intent11=new Intent(PlanUserSubcription_Act.this,PaymentActivity.class);
                                intent11.putExtra("name",list_arr.get(position).getPlanName());
                                intent11.putExtra("description",list_arr.get(position).getDescription());
                                intent11.putExtra("amount",list_arr.get(position).getPlanAmount()+"");
                                if(res.getResourceData()!=null)
                                {
                                    intent11.putExtra("order_id",res.getResourceData());
                                }
                                startActivityForResult(intent11,155);
                            }else{
                                Toast.makeText(PlanUserSubcription_Act.this, res.getMessage(), Toast.LENGTH_SHORT).show();
                            }

                        }
                        @Override
                        public void failure(final RetrofitError error) {
                            progressBarCustom.cancelProgress();
                            error.printStackTrace();
                            if (error != null) {
                                Utils utils=new Utils(PlanUserSubcription_Act.this);
                                utils.showError(error);
                            }

                        }
                    });
        }catch (Exception e)
        {
            progressBarCustom.cancelProgress();
            //progress.cancelProgress();
            e.printStackTrace();
        }
    }

    private void callApiPlanDetails() {
        final ProgressBarCustom progressBarCustom = new ProgressBarCustom(PlanUserSubcription_Act.this);
        progressBarCustom.showProgress();
        try {

            String token = UserPrefrence.getInstance(PlanUserSubcription_Act.this).getAuthtoken();
            token="bearer " + token;
            RestClient.getInstance(PlanUserSubcription_Act.this).get().getplans( token,
                    new retrofit.Callback<GEtPlanDetails>() {

                        @Override
                        public void success(final GEtPlanDetails res, Response response) {
                            progressBarCustom.cancelProgress();
                            if(res.getResponseMessage().equalsIgnoreCase(Constant.SUCCESS))
                            {
                               if(res.getResourceData()!=null)
                               {
                                   if(res.getResourceData().size()>0)
                                   {
                                       list_arr.clear();
                                       list_arr.addAll(res.getResourceData());
                                       setAdapter(list_arr);
                                   }
                               }
                            }else{
                                Toast.makeText(PlanUserSubcription_Act.this, res.getMessage(), Toast.LENGTH_SHORT).show();
                            }

                        }
                        @Override
                        public void failure(final RetrofitError error) {
                            progressBarCustom.cancelProgress();
                            error.printStackTrace();
                            if (error != null) {
                                Utils utils=new Utils(PlanUserSubcription_Act.this);
                                utils.showError(error);
                            }

                        }
                    });
        }catch (Exception e)
        {
            progressBarCustom.cancelProgress();
            //progress.cancelProgress();
            e.printStackTrace();
        }
    }

    private void setAdapter(List<GEtPlanDetails.ResourceDatum> list_arr) {
        viewpagerAdapter adapter=new viewpagerAdapter(this,list_arr);

        if(list_arr.size()==1)
        {
            // viewpager.setPadding(0, 0, 0, 0);
        }else{
            viewpager.setPadding(50, 0, 50, 0);
            viewpager.setClipToPadding(false);
        }
        viewpager.setAdapter(adapter);
    }


    public class viewpagerAdapter extends PagerAdapter {

        Context context;
        List<GEtPlanDetails.ResourceDatum> list_arr;
        LayoutInflater inflater;

        viewpagerAdapter(Context contexts, List<GEtPlanDetails.ResourceDatum> list_arr){
            this.list_arr=list_arr;
            this.context=contexts;
        }
        @Override
        public int getCount() {
            return list_arr.size();
        }
        @Override
        public boolean isViewFromObject(View view, Object object) {
            return view==((LinearLayout) object);
        }
        @Override
        public void destroyItem(ViewGroup container, int position, Object object) {
            ((ViewPager) container).removeView((LinearLayout) object);
        }
        @Override
        public Object instantiateItem(ViewGroup container, int position) {
            inflater= (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View v=inflater.inflate(R.layout.viewpageritem,container,false);
             TextView  txt_planname=v.findViewById(R.id.txt_planname);
            TextView  txt_price=v.findViewById(R.id.txt_price);
            TextView  txt_monthyear=v.findViewById(R.id.txt_monthyear);
            TextView  txt_desctiption=v.findViewById(R.id.txt_desctiption);
            txt_planname.setText(list_arr.get(position).getPlanName());
            txt_monthyear.setText("/"+list_arr.get(position).getPlanType());
            txt_price.setText(list_arr.get(position).getPlanAmount()+"");
            txt_desctiption.setText(list_arr.get(position).getDescription());
            container.addView(v);
            return v;

        }

    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        if(requestCode==155&&resultCode==RESULT_OK)
        {
            Intent intent=new Intent();
            setResult(RESULT_OK,intent);
            finish();
        }
    }

}
