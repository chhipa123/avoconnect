package com.avoconnect.activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.avoconnect.Constants.Constant;
import com.avoconnect.Prefrence.UserPrefrence;
import com.avoconnect.R;
import com.avoconnect.Util.Utils;

import org.json.JSONArray;

public class FilterScreen_Recieved extends AppCompatActivity implements View.OnClickListener {

    ImageView backimage;
    TextView txt_reset,txt_submit;
    CardView cardview_warm,cardview_hot,cardview_connected;
    int select_warm=0,select_hot=0,select_connected=0;
    EditText et_link;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_filter_screen__recieved);

        cardview_warm=(CardView) findViewById(R.id.cardview_warm);
        cardview_hot=(CardView)findViewById(R.id.cardview_hot);
        cardview_connected=(CardView)findViewById(R.id.cardview_connected);
        txt_reset=(TextView) findViewById(R.id.txt_reset);
        txt_submit=(TextView) findViewById(R.id.txt_submit);
        et_link=(EditText)findViewById(R.id.et_link);

        cardview_warm.setOnClickListener(this);
        cardview_hot.setOnClickListener(this);
        txt_reset.setOnClickListener(this);
        txt_submit.setOnClickListener(this);
        cardview_connected.setOnClickListener(this);

        backimage=(ImageView)findViewById(R.id.backimage) ;
        backimage.setOnClickListener(this);

        Bundle bundle=getIntent().getExtras();
        try {
            if (bundle != null) {
                if (bundle.getString("warm_lead") != null) {
                    if(bundle.getString("warm_lead").length()>0)
                    {
                        select_warm = 1;
                        cardview_warm.setCardBackgroundColor(getResources().getColor(R.color.background_color));
                    }else{

                    }

                }
                if (bundle.getString("hot_lead") != null) {
                    if(bundle.getString("hot_lead").length()>0)
                    {
                        select_hot = 2;
                        cardview_hot.setCardBackgroundColor(getResources().getColor(R.color.background_color));
                    }else{

                    }
                }
                if (bundle.getString("cold_lead") != null) {
                    if(bundle.getString("cold_lead").length()>0)
                    {
                        select_connected=3;
                        cardview_connected.setCardBackgroundColor(getResources().getColor(R.color.background_color));
                    }else{

                    }
                }

                if (bundle.getString("label") != null) {
                    if(bundle.getString("label").length()>0)
                    {
                        et_link.setText(bundle.getString("label"));
                    }else{

                    }
                }
            }
        }catch (Exception e){
            e.printStackTrace();
        }

    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {

            case R.id.txt_reset:
                resetfilters();
                break;
            case R.id.txt_submit:

                submitRessponce();

                break;
            case R.id.backimage:
                finish();
                break;
            case R.id.cardview_warm:

                if(select_warm==0)
                {
                    select_warm = 1;
                    cardview_warm.setCardBackgroundColor(getResources().getColor(R.color.background_color));
                }else{
                    select_warm = 0;
                    cardview_warm.setCardBackgroundColor(getResources().getColor(R.color.light_grey));
                }

                break;
            case R.id.cardview_hot:

                if(select_hot==0)
                {
                    select_hot = 2;
                    cardview_hot.setCardBackgroundColor(getResources().getColor(R.color.background_color));
                }else{
                    select_hot = 0;
                    cardview_hot.setCardBackgroundColor(getResources().getColor(R.color.light_grey));
                }

                break;
            case R.id.cardview_connected:
                if(select_connected==0)
                {
                    select_connected=3;
                    cardview_connected.setCardBackgroundColor(getResources().getColor(R.color.background_color));
                }else{
                    select_connected=0;
                    cardview_connected.setCardBackgroundColor(getResources().getColor(R.color.light_grey));
                }

                break;
        }
    }

    private void submitRessponce() {
        Intent intent=new Intent();
        intent.putExtra("warm_type",select_warm+"");
        intent.putExtra("hot_type",select_hot+"");
        intent.putExtra("connected",select_connected+"");
        intent.putExtra("label",et_link.getText().toString());
        setResult(RESULT_OK,intent);
        finish();

    }

    private void resetfilters() {
        select_warm=0;
        select_hot=0;
        select_connected=0;
        cardview_warm.setCardBackgroundColor(getResources().getColor(R.color.light_grey));
        cardview_hot.setCardBackgroundColor(getResources().getColor(R.color.light_grey));
        cardview_connected.setCardBackgroundColor(getResources().getColor(R.color.light_grey));
        et_link.setText("");
    }



}
