package com.avoconnect.activity;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.text.Editable;
import android.text.InputType;
import android.text.TextWatcher;
import android.text.method.PasswordTransformationMethod;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.avoconnect.Constants.Constant;
import com.avoconnect.Prefrence.UserPrefrence;
import com.avoconnect.R;
import com.avoconnect.Util.ProgressBarCustom;
import com.avoconnect.Util.Utils;
import com.avoconnect.api.RestClient;
import com.avoconnect.responsemodel.AutoShare_Model;
import com.avoconnect.responsemodel.ChangePassword_Model;
import com.avoconnect.responsemodel.RequestOtp_Model;
import com.avoconnect.responsemodel.Submitotp_model;
import com.avoconnect.responsemodel.TwoWayAuth_Model;
import com.avoconnect.responsemodel.UpdateMobile_Modele;

import org.json.JSONObject;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;
import retrofit.mime.TypedByteArray;
import retrofit.mime.TypedInput;

public class ChangePassword_Actitivty extends AppCompatActivity implements TextWatcher{

    EditText et_oldpassword, et_newpass, et_confirmpass;
    ImageView eye_image1, eye_image2, eye_image3,backimage;
    TextView txt_reg;
    boolean iselectedfirst = true, isIselectedsecond = true, isIselectedthird = true;
      EditText editText_one,editText_two,editText_three,editText_four;
     Dialog dialog;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_change_password__actitivty);

        et_oldpassword = (EditText) findViewById(R.id.et_oldpassword);
        et_newpass = (EditText) findViewById(R.id.et_newpass);
        et_confirmpass = (EditText) findViewById(R.id.et_confirmpass);
        eye_image1 = (ImageView) findViewById(R.id.eye_image1);
        eye_image2 = (ImageView) findViewById(R.id.eye_image2);
        eye_image3 = (ImageView) findViewById(R.id.eye_image3);
        backimage = (ImageView) findViewById(R.id.backimage);
        txt_reg = (TextView) findViewById(R.id.txt_reg);
        et_oldpassword.setInputType(InputType.TYPE_CLASS_TEXT |
                InputType.TYPE_TEXT_VARIATION_PASSWORD);

        et_newpass.setInputType(InputType.TYPE_CLASS_TEXT |
                InputType.TYPE_TEXT_VARIATION_PASSWORD);

        et_confirmpass.setInputType(InputType.TYPE_CLASS_TEXT |
                InputType.TYPE_TEXT_VARIATION_PASSWORD);

        eye_image1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (iselectedfirst == true) {
                    iselectedfirst = false;
                    et_oldpassword.setTransformationMethod(null);
                    eye_image1.setImageResource(R.drawable.eye_open);
                    et_oldpassword.setSelection(et_oldpassword.getText().toString().length());
                } else {
                    iselectedfirst = true;
                    et_oldpassword.setTransformationMethod(new PasswordTransformationMethod());
                    eye_image1.setImageResource(R.drawable.eye_close);
                    et_oldpassword.setSelection(et_oldpassword.getText().toString().length());
                }
            }
        });

        eye_image2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (isIselectedsecond == true) {
                    isIselectedsecond = false;
                    et_newpass.setTransformationMethod(null);
                    eye_image2.setImageResource(R.drawable.eye_open);
                    et_newpass.setSelection(et_newpass.getText().toString().length());
                } else {
                    isIselectedsecond = true;
                    et_newpass.setTransformationMethod(new PasswordTransformationMethod());
                    eye_image2.setImageResource(R.drawable.eye_close);
                    et_newpass.setSelection(et_newpass.getText().toString().length());
                }
            }
        });

        eye_image3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (isIselectedthird == true) {
                    isIselectedthird = false;
                    et_confirmpass.setTransformationMethod(null);
                    eye_image3.setImageResource(R.drawable.eye_open);
                    et_confirmpass.setSelection(et_confirmpass.getText().toString().length());
                } else {
                    isIselectedthird = true;
                    et_confirmpass.setTransformationMethod(new PasswordTransformationMethod());
                    eye_image3.setImageResource(R.drawable.eye_close);
                    et_confirmpass.setSelection(et_confirmpass.getText().toString().length());
                }
            }
        });

        txt_reg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                validation();
            }
        });


        backimage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });


    }

    private void validation() {
        String getpassword = et_newpass.getText().toString();
        String get_confirmpass = et_confirmpass.getText().toString();
        if (et_oldpassword.getText().toString().length() == 0) {
            ProgressBarCustom.customDialog(this, getResources().getString(R.string.old_enter_password), false);
        } else if (et_oldpassword.getText().toString().length() < 8) {
            ProgressBarCustom.customDialog(this, getResources().getString(R.string.password_length), false);
        } else if (et_newpass.getText().toString().length() == 0) {
            ProgressBarCustom.customDialog(this, getResources().getString(R.string.new_enter_password), false);
        } else if (et_newpass.getText().toString().length() < 8) {
            ProgressBarCustom.customDialog(this, getResources().getString(R.string.new_password_length), false);
        } else if (et_confirmpass.getText().toString().length() == 0) {
            ProgressBarCustom.customDialog(this, getResources().getString(R.string.enter_con_password), false);
        } else if (!getpassword.equalsIgnoreCase(get_confirmpass)) {
            ProgressBarCustom.customDialog(this, getResources().getString(R.string.password_not_match), false);
        } else {

            if (!UserPrefrence.getInstance(this).getUsertype().equalsIgnoreCase(Constant.USERTYPE_FREE)) {

                if(UserPrefrence.getInstance(ChangePassword_Actitivty.this).getMobile().length()==0)
                {
                    updateMobileNumber();
                }else{
                    callApiRequestOtp();
                }
            } else {
                callApi();
            }

        }


    }


    private void updateMobileNumber() {

        AlertDialog.Builder builder=new AlertDialog.Builder(ChangePassword_Actitivty.this);
        View view= LayoutInflater.from(this).inflate(R.layout.asknumber,null);
        TextView txt_save=(TextView)view.findViewById(R.id.txt_save);
        final EditText et_mobile=(EditText)view.findViewById(R.id.et_mobile);
        ImageView cut_img=(ImageView)view.findViewById(R.id.cut_img);
        builder.setView(view);
        final Dialog dialog=builder.create();
        dialog.setCancelable(false);
        dialog.setCanceledOnTouchOutside(false);

        txt_save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(et_mobile.getText().toString().length()==0)
                {
                    ProgressBarCustom.customDialog(ChangePassword_Actitivty.this,getResources().getString(R.string.enter_mobile_number),false);
                }
                else if(et_mobile.getText().toString().length()<10)
                {
                    ProgressBarCustom.customDialog(ChangePassword_Actitivty.this,getResources().getString(R.string.invalid_mobile),false);
                }
                else{
                    callApiUpdatemobileNumber(et_mobile.getText().toString());
                    dialog.dismiss();
                }
                // callApiLgout();
            }
        });
        cut_img.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });
        dialog.show();

    }

    private void callApiUpdatemobileNumber(final String mobile) {
        final ProgressBarCustom progressBarCustom=new ProgressBarCustom(ChangePassword_Actitivty.this);
        progressBarCustom.showProgress();
        try {
            String token = UserPrefrence.getInstance(this).getAuthtoken();
            token="bearer " + token;
            JSONObject jsonObject3 = new JSONObject();

            TypedInput in3 = new TypedByteArray("application/json", jsonObject3.toString().getBytes("UTF-8"));

            RestClient.getInstance(this).get().updatemobilenumber( token,UserPrefrence.getInstance(this).getUserid(),
                    mobile+"",in3,
                    new retrofit.Callback<UpdateMobile_Modele>() {

                        @Override
                        public void success(final UpdateMobile_Modele res, Response response) {
                            progressBarCustom.cancelProgress();

                            if(res.getResponseMessage().equalsIgnoreCase(Constant.SUCCESS))
                            {
                                UserPrefrence.getInstance(ChangePassword_Actitivty.this).setMobile(mobile);
                                callApiRequestOtp();
                            }else{
                                Toast.makeText(ChangePassword_Actitivty.this, res.getMessage(), Toast.LENGTH_SHORT).show();
                            }

                        }
                        @Override
                        public void failure(final RetrofitError error) {
                            progressBarCustom.cancelProgress();
                            error.printStackTrace();
                            if (error != null) {
                                Utils utils=new Utils(ChangePassword_Actitivty.this);
                                utils.showError(error);
                            }

                        }
                    });
        }catch (Exception e)
        {
            progressBarCustom.cancelProgress();
            //progress.cancelProgress();
            e.printStackTrace();
        }
    }


    private void callApiRequestOtp() {
        final ProgressBarCustom progressBarCustom = new ProgressBarCustom(ChangePassword_Actitivty.this);
        progressBarCustom.showProgress();
        try {
            String token = UserPrefrence.getInstance(ChangePassword_Actitivty.this).getAuthtoken();
            token = "bearer " + token;
            JSONObject jsonObject3 = new JSONObject();
            TypedInput in3 = new TypedByteArray("application/json", jsonObject3.toString().getBytes("UTF-8"));
            RestClient.getInstance(this).get().requestotp_premium(token, UserPrefrence.getInstance(this).getUserid(), UserPrefrence.getInstance(this).getMobile(), in3, new retrofit.Callback<RequestOtp_Model>() {

                @Override
                public void success(final RequestOtp_Model res, Response response) {
                    progressBarCustom.cancelProgress();

                    if (res.getResponseMessage().equalsIgnoreCase(Constant.SUCCESS)) {
                        if(dialog!=null&&dialog.isShowing()==true){

                        }else{
                            openpopsubmitotp();
                        }
                        Toast.makeText(ChangePassword_Actitivty.this, res.getMessage(), Toast.LENGTH_SHORT).show();
                    } else {
                        ProgressBarCustom.customDialog(ChangePassword_Actitivty.this,res.getMessage(),false);
                    }

                }

                @Override
                public void failure(final RetrofitError error) {
                    progressBarCustom.cancelProgress();
                    error.printStackTrace();
                    if (error != null) {
                        Utils utils = new Utils(ChangePassword_Actitivty.this);
                        utils.showError(error);
                    }

                }
            });


        } catch (Exception e) {
            progressBarCustom.cancelProgress();
            //progress.cancelProgress();
            e.printStackTrace();
        }
    }

    private void openpopsubmitotp() {
        AlertDialog.Builder builder = new AlertDialog.Builder(ChangePassword_Actitivty.this);
        View view = LayoutInflater.from(this).inflate(R.layout.custom_otp, null);
        editText_one=(EditText)view.findViewById(R.id.editTextone);
        editText_two=(EditText)view.findViewById(R.id.editTexttwo);
        editText_three=(EditText)view.findViewById(R.id.editTextthree);
        editText_four=(EditText)view.findViewById(R.id.editTextfour);

       TextView txt_reg=(TextView)view.findViewById(R.id.txt_reg);
        TextView txt_resend=(TextView)view.findViewById(R.id.txt_resend);
        editText_one.addTextChangedListener(this);
        editText_two.addTextChangedListener(this);
        editText_three.addTextChangedListener(this);
        editText_four.addTextChangedListener(this);

        txt_reg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                validation_check();
            }
        });

        txt_resend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                editText_one.setText("");
                editText_two.setText("");
                editText_three.setText("");
                editText_four.setText("");
                callApiRequestOtp();
            }
        });
        builder.setView(view);
        dialog = builder.create();
        dialog.setCancelable(false);
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();
    }

    private void validation_check() {
        if(editText_one.getText().length()==0||editText_two.getText().length()==0||editText_three.getText().length()==0||editText_four.getText().length()==0)
        {
            ProgressBarCustom.customDialog(ChangePassword_Actitivty.this,getResources().getString(R.string.enter_valid_otp),false);
        }else{
              callApiSendOTp();
        }

    }

    private void callApiSendOTp() {
        final ProgressBarCustom progressBarCustom = new ProgressBarCustom(ChangePassword_Actitivty.this);
        progressBarCustom.showProgress();
        try {

            String token = UserPrefrence.getInstance(ChangePassword_Actitivty.this).getAuthtoken();
            token = "bearer " + token;
            JSONObject jsonObject3 = new JSONObject();
            String old_pass_get = et_oldpassword.getText().toString();
            String new_pass_get = et_newpass.getText().toString();
            String otp=editText_one.getText().toString()+""+editText_two.getText().toString()+""+
                    editText_three.getText().toString()+""+editText_four.getText().toString();
            jsonObject3.put("oldPassword", new Utils(this).convertpasswordtobase64(old_pass_get).trim());
            jsonObject3.put("newPassword", new Utils(this).convertpasswordtobase64(new_pass_get).trim());

            TypedInput in3 = new TypedByteArray("application/json", jsonObject3.toString().getBytes("UTF-8"));
            RestClient.getInstance(ChangePassword_Actitivty.this).get().submitotp(token, UserPrefrence.getInstance(this).getUserid(),otp, in3,
                    new Callback<Submitotp_model>() {

                        @Override
                        public void success(final Submitotp_model res, Response response) {
                            progressBarCustom.cancelProgress();
                            if (response.getStatus() == 200) {
                                if (res.getResponseMessage().equalsIgnoreCase(Constant.SUCCESS)) {
                                    if(dialog!=null)
                                    {
                                        dialog.dismiss();
                                    }
                                    ProgressBarCustom.customDialog(ChangePassword_Actitivty.this, res.getMessage(), true);
                                } else {
                                    if(dialog!=null)
                                    {
                                        dialog.dismiss();
                                    }
                                    Toast.makeText(ChangePassword_Actitivty.this, res.getMessage(), Toast.LENGTH_SHORT).show();
                                }
                            }
                        }

                        @Override
                        public void failure(final RetrofitError error) {
                            progressBarCustom.cancelProgress();
                            error.printStackTrace();
                            if (error != null) {
                                Utils utils = new Utils(ChangePassword_Actitivty.this);
                                utils.showError(error);
                            }
                        }
                    });

        } catch (Exception e) {
            progressBarCustom.cancelProgress();
            e.printStackTrace();
        }
    }

    private void callApi() {

        final ProgressBarCustom progressBarCustom = new ProgressBarCustom(ChangePassword_Actitivty.this);
        progressBarCustom.showProgress();
        try {

            String token = UserPrefrence.getInstance(ChangePassword_Actitivty.this).getAuthtoken();
            token = "bearer " + token;
            JSONObject jsonObject3 = new JSONObject();
            String old_pass_get = et_oldpassword.getText().toString();
            String new_pass_get = et_newpass.getText().toString();
            jsonObject3.put("oldPassword", new Utils(this).convertpasswordtobase64(old_pass_get).trim());
            jsonObject3.put("newPassword", new Utils(this).convertpasswordtobase64(new_pass_get).trim());
            TypedInput in3 = new TypedByteArray("application/json", jsonObject3.toString().getBytes("UTF-8"));
            RestClient.getInstance(ChangePassword_Actitivty.this).get().changepassword_settings(token, UserPrefrence.getInstance(this).getUserid(), in3,
                    new Callback<ChangePassword_Model>() {

                        @Override
                        public void success(final ChangePassword_Model res, Response response) {
                            progressBarCustom.cancelProgress();
                            if (response.getStatus() == 200) {
                                if (res.getResponseMessage().equalsIgnoreCase(Constant.SUCCESS)) {
                                    ProgressBarCustom.customDialog(ChangePassword_Actitivty.this, res.getMessage(), true);

                                } else {
                                    Toast.makeText(ChangePassword_Actitivty.this, res.getMessage(), Toast.LENGTH_SHORT).show();
                                }
                            }
                        }

                        @Override
                        public void failure(final RetrofitError error) {
                            progressBarCustom.cancelProgress();
                            error.printStackTrace();
                            if (error != null) {
                                Utils utils = new Utils(ChangePassword_Actitivty.this);
                                utils.showError(error);
                            }
                        }
                    });

        } catch (Exception e) {
            progressBarCustom.cancelProgress();
            e.printStackTrace();
        }
    }

    @Override
    public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

    }

    @Override
    public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

    }

    @Override
    public void afterTextChanged(Editable editable) {
        if (editable.length() == 1) {
            if (editText_one.length() == 1) {
                editText_two.requestFocus();
            }

            if (editText_two.length() == 1) {
                editText_three.requestFocus();
            }
            if (editText_three.length() == 1) {
                editText_four.requestFocus();
            }
        } else if (editable.length() == 0) {
            if (editText_four.length() == 0) {
                editText_three.requestFocus();
            }
            if (editText_three.length() == 0) {
                editText_two.requestFocus();
            }
            if (editText_two.length() == 0) {
                editText_one.requestFocus();
            }
        }
    }




}
