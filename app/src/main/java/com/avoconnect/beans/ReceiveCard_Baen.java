package com.avoconnect.beans;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ReceiveCard_Baen {

    @SerializedName("cardId")
    @Expose
    private Integer cardId;
    @SerializedName("fullName")
    @Expose
    private String name;
    @SerializedName("mobile")
    @Expose
    private String mobile;
    @SerializedName("email")
    @Expose
    private String email;
    @SerializedName("isPrimary")
    @Expose
    private Integer isPrimary;
    @SerializedName("autoShare")
    @Expose
    private Integer autoShare;
    @SerializedName("backgroundColor")
    @Expose
    private String cardBackGroundColor;


    @SerializedName("leadType")
    @Expose
    private Integer leadType;

    @SerializedName("logo")
    @Expose
    private String logo;
    @SerializedName("backgroundImage")
    @Expose
    private String backgroundImage;


    @SerializedName("cardAddInContact")
    @Expose
    private Boolean cardAddInContact;

    public Boolean getCardAddInContact() {
        return cardAddInContact;
    }

    public void setCardAddInContact(Boolean cardAddInContact) {
        this.cardAddInContact = cardAddInContact;
    }



    public String getLogo() {
        return logo;
    }

    public void setLogo(String logo) {
        this.logo = logo;
    }

    public String getBackgroundImage() {
        return backgroundImage;
    }

    public void setBackgroundImage(String backgroundImage) {
        this.backgroundImage = backgroundImage;
    }


    public Integer getLeadType() {
        return leadType;
    }

    public void setLeadType(Integer leadType) {
        this.leadType = leadType;
    }

    public Integer getCardId() {
        return cardId;
    }

    public void setCardId(Integer cardId) {
        this.cardId = cardId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Integer getIsPrimary() {
        return isPrimary;
    }

    public void setIsPrimary(Integer isPrimary) {
        this.isPrimary = isPrimary;
    }

    public Integer getAutoShare() {
        return autoShare;
    }

    public void setAutoShare(Integer autoShare) {
        this.autoShare = autoShare;
    }

    public String getCardBackGroundColor() {
        return cardBackGroundColor;
    }

    public void setCardBackGroundColor(String cardBackGroundColor) {
        this.cardBackGroundColor = cardBackGroundColor;
    }



}
