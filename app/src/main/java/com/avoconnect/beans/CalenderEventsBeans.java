package com.avoconnect.beans;

public class CalenderEventsBeans {

    String event_name;
    String event_time;
    String event_title;
    String event_date;

    public String getEvent_email() {
        return event_email;
    }

    public void setEvent_email(String event_email) {
        this.event_email = event_email;
    }

    String event_email;

    public String getEvent_name() {
        return event_name;
    }

    public void setEvent_name(String event_name) {
        this.event_name = event_name;
    }

    public String getEvent_time() {
        return event_time;
    }

    public void setEvent_time(String event_time) {
        this.event_time = event_time;
    }

    public String getEvent_title() {
        return event_title;
    }

    public void setEvent_title(String event_description) {
        this.event_title = event_description;
    }

    public String getEvent_date() {
        return event_date;
    }

    public void setEvent_date(String event_date) {
        this.event_date = event_date;
    }
}
