package com.avoconnect.adapter;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.Typeface;
import androidx.cardview.widget.CardView;
import androidx.core.graphics.drawable.RoundedBitmapDrawable;
import androidx.core.graphics.drawable.RoundedBitmapDrawableFactory;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.avoconnect.R;
import com.avoconnect.Util.Utils;
import com.avoconnect.responsemodel.ResourceDatum;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.bumptech.glide.request.target.BitmapImageViewTarget;

import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

public class MyCardRecyclerViewAdapter extends RecyclerView.Adapter<MyCardRecyclerViewAdapter.MyViewHolder> {
    private Context mContext;
    Callback callback;

    List<ResourceDatum> resourceData;

    public MyCardRecyclerViewAdapter(Context mContext,List<ResourceDatum> resourceData) {
        this.mContext = mContext;
        this.resourceData = resourceData;

    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        //load layout you write,look R.layout.item to see detail.
        View view = (LayoutInflater.from(mContext).inflate(R.layout.item_mycard, viewGroup, false));
        MyViewHolder holder = new MyViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {
        // populate list with daya
        if (resourceData.get(position).getIsPrimary() == 1) {
            holder.primary_txt.setVisibility(View.VISIBLE);
            holder.checkbox.setVisibility(View.VISIBLE);
            if(resourceData.size()>1)
            {
                if(position==0) {
                    holder.othercard_txt.setVisibility(View.VISIBLE);
                }else{
                    holder.othercard_txt.setVisibility(View.GONE);
                }
            }else{
                holder.othercard_txt.setVisibility(View.GONE);
            }

            if(resourceData.get(position).getAutoShare() == 1)
            {
                holder.checkbox.setChecked(true);
            }else{
                holder.checkbox.setChecked(false);
            }

            holder.mainCard1.setCardBackgroundColor(mContext.getResources().getColor(R.color.background_color));
            holder.mainCard1.setCardElevation(5);
        } else {
            holder.primary_txt.setVisibility(View.GONE);
            holder.othercard_txt.setVisibility(View.GONE);
            holder.checkbox.setVisibility(View.GONE);
            holder.mainCard1.setCardBackgroundColor(mContext.getResources().getColor(R.color.cardview_border));
            holder.mainCard1.setCardElevation(0);
        }

        try {
            if (resourceData.get(position).getLogo() != null && resourceData.get(position).getLogo().length() > 0) {
                holder.img_userimg.setVisibility(View.VISIBLE);
                holder.txt_user_placeholder.setVisibility(View.GONE);
                Glide.with(mContext)
                        .load(resourceData.get(position).getLogo())
                        .apply(RequestOptions.circleCropTransform())
                        .into(holder.img_userimg);
            } else if (Utils.checkNull(resourceData.get(position).getFullName())) {
                holder.img_userimg.setVisibility(View.GONE);
                holder.txt_user_placeholder.setVisibility(View.VISIBLE);
                holder.txt_user_placeholder.setText(Utils.nameInital(resourceData.get(position).getFullName().trim()) + "");
                //  holder. txt_user_placeholder.setText(new Utils(mContext).getFirstLastCharacter(resourceData.get(position).getFullName()));
            }

        if(resourceData.get(position).getBackgroundColor().contains("#"))
        {
            holder.maincard.setCardBackgroundColor(Color.parseColor(resourceData.get(position).getBackgroundColor()));
        }else{
            String colorcode="#"+resourceData.get(position).getBackgroundColor();
            holder.maincard.setCardBackgroundColor(Color.parseColor(colorcode));
        }
        }catch (Exception e)
        {
            e.printStackTrace();
        }
        // username_txt
        holder.username_txt.setText(resourceData.get(position).getFullName());
        holder.useremail.setText(resourceData.get(position).getEmail());
        holder.phonenumber_txt.setText(resourceData.get(position).getMobile());
        // mentaion click event
        holder.maincard.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (callback != null) {
                    callback.clickaction(position);
               }
            }
        });
        // share click event
        holder.img_share.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (callback != null) {
                    callback.clickaction(position, "share", "" + resourceData.get(position).getId());
                }
            }
        });
        holder.rldeletecard.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (callback != null) {
                    callback.clickaction(position, "delete", "" + resourceData.get(position).getId());
                }
            }
        });
       // holder.username_txt.setTypeface(type_bold);
      //  holder.useremail.setTypeface(type_normal);
     //   holder.phonenumber_txt.setTypeface(type_normal);
      //  holder.primary_txt.setTypeface(type_normal);
      //  holder.othercard_txt.setTypeface(type_normal);
     //   holder.checkbox.setTypeface(type_normal);
        if (resourceData.get(position).isDeleteButtonShow()) {
            holder.rldeletecard.setVisibility(View.VISIBLE);
            setMargins(holder.rl_cardlayout, -200, 0, 0, 0);
        } else {
            holder.rldeletecard.setVisibility(View.GONE);
            setMargins(holder.rl_cardlayout, 0, 0, 0, 0);
        }

        holder.checkbox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (callback != null) {
                    callback.clickcheckbox(position,b,resourceData.get(position).getId());
                }
            }
        });

        holder.phonenumber_txt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (callback != null) {
                    callback.addcontact(resourceData.get(position).getFullName(),resourceData.get(position).getMobile(),position);
                }
            }
        });

        holder.useremail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (callback != null) {
                    callback.sendemail(resourceData.get(position).getEmail());
                }
            }
        });

    }
    @Override
    public int getItemCount() {
        return resourceData.size();
    }

    class MyViewHolder extends RecyclerView.ViewHolder {
        CardView mainCard1, maincard;
        private ImageView img_userimg;
        private TextView username_txt, useremail, phonenumber_txt, primary_txt, othercard_txt;
        RelativeLayout parentlay, img_share, rl_cardlayout, rldeletecard;
        TextView txt_user_placeholder;
        CheckBox checkbox;
        public MyViewHolder(View view) {
            super(view);
            img_userimg = (ImageView) view.findViewById(R.id.img_userimg);
            parentlay = (RelativeLayout) view.findViewById(R.id.parentlay);
            username_txt = (TextView) view.findViewById(R.id.username_txt);
            useremail = (TextView) view.findViewById(R.id.useremail);
            phonenumber_txt = (TextView) view.findViewById(R.id.phonenumber_txt);
            txt_user_placeholder= (TextView) view.findViewById(R.id.txt_user_placeholder);
            primary_txt = (TextView) view.findViewById(R.id.primary_txt);
            othercard_txt = (TextView) view.findViewById(R.id.othercard_txt);
            checkbox = (CheckBox) view.findViewById(R.id.checkbox);
            mainCard1 = (CardView) view.findViewById(R.id.maincard1);
            maincard = (CardView) view.findViewById(R.id.maincard);
            img_share = (RelativeLayout) view.findViewById(R.id.img_share);
            rl_cardlayout = (RelativeLayout) view.findViewById(R.id.rl_cardlayout);
            rldeletecard = (RelativeLayout) view.findViewById(R.id.rldeletecard);
        }
    }

    public void onCallBackReturn(Callback callback) {
        this.callback = callback;
    }

    public interface Callback {
        void clickaction(int position);

        void clickaction(int position, String type, String cardId);

        void clickcheckbox(int position, boolean status, int card_id);

        void addcontact(String name, String phonenumber,int position);
        void sendemail(String email);
    }

    private void setMargins(View view, int left, int top, int right, int bottom) {
        if (view.getLayoutParams() instanceof ViewGroup.MarginLayoutParams) {
            ViewGroup.MarginLayoutParams p = (ViewGroup.MarginLayoutParams) view.getLayoutParams();
            p.setMargins(left, top, right, bottom);
            view.requestLayout();
        }
    }
}
