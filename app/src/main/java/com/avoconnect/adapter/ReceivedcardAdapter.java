package com.avoconnect.adapter;

import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;
import androidx.cardview.widget.CardView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.avoconnect.Constants.Constant;
import com.avoconnect.Prefrence.UserPrefrence;
import com.avoconnect.R;
import com.avoconnect.Util.Utils;
import com.avoconnect.beans.ReceiveCard_Baen;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;

import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

public class ReceivedcardAdapter extends BaseAdapter {

    Context context;
    Callback callback;
    Typeface type_normal,type_bold;
    private List<ReceiveCard_Baen> mDisplayedValues;
    List<ReceiveCard_Baen> res;

   // List<ResourceDatum> resourceData ;
    public ReceivedcardAdapter(Context context, List<ReceiveCard_Baen> res) {
        this.context=context;
        this.res=res;
        this.mDisplayedValues=res;
//        type_normal= Typeface.createFromAsset(context.getAssets(), "font/Montserrat_Regular.ttf");
//        type_bold = Typeface.createFromAsset(context.getAssets(), "font/Montserrat_SemiBold.ttf");
    }

    @Override
    public int getCount() {
        return res.size();
    }

    @Override
    public Object getItem(int i) {
        return null;
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup viewGroup) {

        View view = convertView;
        final ViewHolder holder;
        if (view == null) {
            view = LayoutInflater.from(context).inflate(R.layout.item_receivecard, viewGroup, false);
            holder = new ViewHolder();
            holder.img_userimg = (CircleImageView) view.findViewById(R.id.img_userimg);
            holder.parentlay = (RelativeLayout) view.findViewById(R.id.parentlay);
            holder.img_share=(RelativeLayout)view.findViewById(R.id.img_share);
            holder.username_txt = (TextView) view.findViewById(R.id.username_txt);
            holder.useremail = (TextView) view.findViewById(R.id.useremail);
            holder.phonenumber_txt = (TextView) view.findViewById(R.id.phonenumber_txt);

            holder.mainCard1 = (CardView)view.findViewById(R.id.maincard1);
            holder.maincard=(CardView)view.findViewById(R.id.maincard);
            holder.rl_cardlayout = (RelativeLayout) view.findViewById(R.id.rl_cardlayout);
            holder.lead_img=(ImageView)view.findViewById(R.id.lead_img);
            holder.img_banner=(RelativeLayout)view.findViewById(R.id.img_banner);
            holder.txt_user_placeholder=(TextView)view.findViewById(R.id.txt_user_placeholder);
            holder.add_contact=(ImageView)view.findViewById(R.id.add_contact);
          //  holder.rldeletecard = (RelativeLayout) view.findViewById(R.id.rldeletecard);

            view.setTag(holder);
        } else {
            holder = (ViewHolder) view.getTag();
        }
        holder.add_contact.setVisibility(View.VISIBLE);
        if(position==0)
        {

            holder.mainCard1.setCardBackgroundColor(context.getResources().getColor(R.color.cardview_border));
        }else{

            holder.mainCard1.setCardBackgroundColor(context.getResources().getColor(R.color.cardview_border));
        }

        try {
            if (res.get(position).getLogo() != null && res.get(position).getLogo().length() > 0) {
                holder.img_userimg.setVisibility(View.VISIBLE);
                holder.txt_user_placeholder.setVisibility(View.GONE);
                Glide.with(context)
                        .load(res.get(position).getLogo())
                        .apply(RequestOptions.circleCropTransform())
                        .into(holder.img_userimg);
            } else if (Utils.checkNull(res.get(position).getName().trim())) {
                holder.img_userimg.setVisibility(View.GONE);
                holder.txt_user_placeholder.setVisibility(View.VISIBLE);
                holder.txt_user_placeholder.setText(Utils.nameInital(res.get(position).getName().trim()) + "");
                //  holder. txt_user_placeholder.setText(new Utils(mContext).getFirstLastCharacter(resourceData.get(position).getFullName()));
            }
        }catch (Exception e)
        {
            e.printStackTrace();
        }

        if(res.get(position).getCardBackGroundColor()!=null) {
            if (res.get(position).getCardBackGroundColor().contains("#")) {
                holder.maincard.setCardBackgroundColor(Color.parseColor(res.get(position).getCardBackGroundColor()));
            } else {
                String colorcode = "#" + res.get(position).getCardBackGroundColor();
                holder.maincard.setCardBackgroundColor(Color.parseColor(colorcode));
            }
        }

        if(res.get(position).getCardAddInContact()==true)
        {
            holder.add_contact.setColorFilter(context.getColor(R.color.green_dark));
        }else{
            holder.add_contact.setColorFilter(context.getColor(R.color.black));
        }

        if(!UserPrefrence.getInstance(context).getUsertype().equalsIgnoreCase(Constant.USERTYPE_FREE)) {
            holder.img_banner.setVisibility(View.VISIBLE);
            if (res.get(position).getLeadType() == 1) {
                holder.lead_img.setImageResource(R.drawable.tag_warm_lead);
            } else if (res.get(position).getLeadType() == 2) {
                holder.lead_img.setImageResource(R.drawable.tag_hot_lead);
            } else if (res.get(position).getLeadType() == 3) {
                holder.lead_img.setImageResource(R.drawable.tag_connected);
            } else {
                holder.lead_img.setImageResource(0);
            }
        }else{
            holder.img_banner.setVisibility(View.GONE);
        }

        holder.parentlay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (callback != null) {
                    callback.clickaction(position);
                }
            }
        });
        holder.img_share.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (callback != null) {
                    callback.share_action(position);
                }
            }
        });
        holder.phonenumber_txt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (callback != null) {
                    callback.addcontact(res.get(position).getName(),res.get(position).getMobile(),position);
                }
            }
        });

        holder.add_contact.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (callback != null) {
                    if(res.get(position).getCardAddInContact()==false) {
                        callback.addcontact_number(res.get(position).getName(), res.get(position).getMobile(), position);
                    }
                }
            }
        });
        holder.useremail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (callback != null) {
                    callback.sendemail(res.get(position).getEmail());
                }
            }
        });


        holder.username_txt.setText(res.get(position).getName());
        holder.useremail.setText(res.get(position).getEmail());
        holder.phonenumber_txt.setText(res.get(position).getMobile());

//        holder.username_txt.setTypeface(type_bold);
//        holder.useremail.setTypeface(type_normal);
//        holder.phonenumber_txt.setTypeface(type_normal);




        return view;
    }


    class ViewHolder {
        CardView mainCard1;
        private CircleImageView img_userimg;
        TextView txt_user_placeholder;
        private TextView username_txt,useremail,phonenumber_txt;
        RelativeLayout parentlay,rl_cardlayout,img_share,img_banner;;
        CardView maincard;
        ImageView lead_img,add_contact;

    }

    public  void onCallBackReturn(Callback callback)
    {
        this.callback=callback;
    }

    public interface Callback{
        void clickaction(int position);
        void share_action(int position);
        void addcontact(String name, String phonenumber,int position);
        void addcontact_number(String name, String phonenumber,int position);
        void sendemail(String email);


    }






}
