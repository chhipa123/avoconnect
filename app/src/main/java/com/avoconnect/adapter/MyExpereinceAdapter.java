package com.avoconnect.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.avoconnect.R;
import com.avoconnect.responsemodel.GetUserProfile_Model;

public class MyExpereinceAdapter extends BaseAdapter {

    Context context;
    Callback callback;
    String check;
    GetUserProfile_Model res;
    public MyExpereinceAdapter(Context context, GetUserProfile_Model res, String s)
    {
        this.context=context;
        this.res=res;
        this.check=s;
    }

    @Override
    public int getCount() {
        return res.getUserExperienceResponse().size();
    }

    @Override
    public Object getItem(int i) {
        return null;
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(final int i, View convertView, ViewGroup viewGroup) {

        View view = convertView;
        final ViewHolder holder;
        if (view == null) {
            view = LayoutInflater.from(context).inflate(R.layout.item_myexpereice, viewGroup, false);
            holder = new ViewHolder();
            holder.txt_username=(TextView)view.findViewById(R.id.txt_username);
            holder.txt_companyname=(TextView)view.findViewById(R.id.txt_companyname);
            holder.txt_date=(TextView)view.findViewById(R.id.txt_date);
            holder.icon_type=(ImageView) view.findViewById(R.id.icon_type);
            holder.cut_img=(ImageView)view.findViewById(R.id.cut_img);
            view.setTag(holder);
        } else {
            holder = (ViewHolder) view.getTag();
        }


        holder.txt_username.setText(res.getUserExperienceResponse().get(i).getDesignation());
        holder.txt_companyname.setText(res.getUserExperienceResponse().get(i).getCompany());

        holder.txt_date.setText(res.getUserExperienceResponse().get(i).getStartDate()+"-"+res.getUserExperienceResponse().get(i).getEndDate());

        if(check.length()>0)
        {
            holder.cut_img.setVisibility(View.GONE);
        }else{
            holder.cut_img.setVisibility(View.VISIBLE);
        }

        holder.cut_img.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(callback!=null)
                {
                    callback.clickaction(i);
                }
            }
        });

        return view;
    }

    class ViewHolder {
        TextView txt_username,txt_companyname,txt_date;
        ImageView icon_type,cut_img;
    }

    public  void onCallBackReturn(Callback callback)
    {
        this.callback=callback;
    }

    public interface Callback{
        void clickaction(int position);
    }
}
