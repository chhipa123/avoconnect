package com.avoconnect.adapter;

import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;
import android.text.SpannableString;
import android.text.TextUtils;
import android.text.style.AbsoluteSizeSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.avoconnect.R;
import com.avoconnect.Util.CustomTypefaceSpan;
import com.avoconnect.Util.Utils;
import com.avoconnect.beans.CalenderEventsBeans;
import com.avoconnect.responsemodel.GetAllEvent_Model;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.google.android.gms.vision.text.Line;

import java.util.ArrayList;
import java.util.List;

import static android.text.Spanned.SPAN_INCLUSIVE_INCLUSIVE;

public class EventListAdapter extends  RecyclerView.Adapter<EventListAdapter.MyViewHolder> {


    Context context;
   Callback callback;
    GetAllEvent_Model res;
    public EventListAdapter(Context context, GetAllEvent_Model res)
    {
        this.context=context;
        this.res=res;

    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        //load layout you write,look R.layout.item to see detail.
        View view = (LayoutInflater.from(context).inflate(R.layout.item_eventlist, viewGroup, false));
        MyViewHolder holder = new MyViewHolder(view);
        return holder;
    }



    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int i) {
        // populate list with daya

        String lineOfCurrencies =res.getResourceData().get(i).getDate();
        String[] splited = lineOfCurrencies.split("\\s+");

        //holder.event_date.setText(splited[0]+"\n"+splited[1]+"\n"+splited[2]);
        holder.event_title.setText(res.getResourceData().get(i).getTitle());
        holder.event_time.setText(new Utils(context).convertTime(res.getResourceData().get(i).getTime()));
        holder.event_desc.setText(res.getResourceData().get(i).getNote());
        if(res.getResourceData().get(i).getEmail()!=null)
        {
            holder.event_email.setVisibility(View.VISIBLE);
            holder.event_email.setText("Calendar ID:"+""+res.getResourceData().get(i).getEmail());
        }else{
            holder.event_email.setVisibility(View.INVISIBLE);
        }


        if(res.getResourceData().get(i).getPublish()==true)
        {
            holder.unsyn_calender.setVisibility(View.VISIBLE);
            holder.syn_calender.setVisibility(View.GONE);
        }else{
            holder.unsyn_calender.setVisibility(View.GONE);
            holder.syn_calender.setVisibility(View.VISIBLE);
        }

        int textSize1 = context.getResources().getDimensionPixelSize(R.dimen.text_size_1);
        int textSize2 = context.getResources().getDimensionPixelSize(R.dimen.text_size_2);

        SpannableString span1 = new SpannableString(splited[0]);
        span1.setSpan(new AbsoluteSizeSpan(textSize1), 0, splited[0].length(), SPAN_INCLUSIVE_INCLUSIVE);

        SpannableString span2 = new SpannableString(splited[1]);
        span2.setSpan(new AbsoluteSizeSpan(textSize2), 0, splited[1].length(), SPAN_INCLUSIVE_INCLUSIVE);
        Typeface type_bold = Typeface.createFromAsset(context.getAssets(), "font/Montserrat_SemiBold.ttf");
        span2.setSpan (new CustomTypefaceSpan(type_bold), 0,  splited[1].length(), SPAN_INCLUSIVE_INCLUSIVE);

        SpannableString span3 = new SpannableString(splited[2]);
        span3.setSpan(new AbsoluteSizeSpan(textSize1), 0, splited[2].length(), SPAN_INCLUSIVE_INCLUSIVE);
        // let's put both spans together with a separator and all
        CharSequence finalText = TextUtils.concat(span1, "\n", span2,"\n",span3);
        holder.event_date.setText(finalText);
        holder.syn_calender.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (callback != null) {
                    callback.clickaction(i,res.getResourceData().get(i).getId());
                }
            }
        });

        if (res.getResourceData().get(i).isDeleteButtonShow()) {
            holder.rldeletecard.setVisibility(View.VISIBLE);
            setMargins(holder.card_swipw, -200, 0, 0, 0);
        } else {
            holder.rldeletecard.setVisibility(View.GONE);
            setMargins(holder.card_swipw, 0, 0, 0, 0);
        }

        holder.rldeletecard.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (callback != null) {
                    callback.deleteevent(i,res.getResourceData().get(i).getId());
                }
            }
        });

    }

    @Override
    public int getItemCount() {
        return res.getResourceData().size();
    }


    class MyViewHolder extends RecyclerView.ViewHolder {
        LinearLayout parenlay;
        TextView event_date,event_title,event_time,event_desc,event_email;
        RelativeLayout syn_calender,unsyn_calender,rldeletecard;
        LinearLayout card_swipw;

        public MyViewHolder(View view) {
            super(view);
            parenlay=(LinearLayout)view.findViewById(R.id.parenlay);
            event_date=(TextView)view.findViewById(R.id.event_date);
           event_title=(TextView)view.findViewById(R.id.event_title);
           event_time=(TextView)view.findViewById(R.id.event_time);
            event_desc=(TextView)view.findViewById(R.id.event_desc);
           event_email=(TextView)view.findViewById(R.id.event_email);
            syn_calender=(RelativeLayout)view.findViewById(R.id.syn_calender);
           unsyn_calender=(RelativeLayout)view.findViewById(R.id.unsyn_calender);
            card_swipw=(LinearLayout)view.findViewById(R.id.card_swipw);
            rldeletecard=(RelativeLayout)view.findViewById(R.id.rldeletecard);
        }
    }
    @Override
    public long getItemId(int i) {
        return 0;
    }

    public  void onCallBackReturn(Callback callback)
    {
        this.callback=callback;
    }

    public interface Callback{
        void clickaction(int position,int eventid);
        void deleteevent(int position,int eventid);
    }

    private void setMargins(View view, int left, int top, int right, int bottom) {
        if (view.getLayoutParams() instanceof ViewGroup.MarginLayoutParams) {
            ViewGroup.MarginLayoutParams p = (ViewGroup.MarginLayoutParams) view.getLayoutParams();
            p.setMargins(left, top, right, bottom);
            view.requestLayout();
        }
    }

}
