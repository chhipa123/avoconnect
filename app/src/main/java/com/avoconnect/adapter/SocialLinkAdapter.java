package com.avoconnect.adapter;

import android.content.Context;
import android.graphics.Paint;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.avoconnect.R;
import com.avoconnect.beans.Social_Model;

import java.util.List;

public class SocialLinkAdapter extends BaseAdapter {

    Context context;
    List<Social_Model> social_list;
   Callback callback;
    String iscrossvisible;
    public SocialLinkAdapter(Context context, List<Social_Model> social_list,String iscrossvisible)
    {
        this.context=context;
        this.social_list=social_list;
        this.iscrossvisible=iscrossvisible;
    }
    @Override
    public int getCount() {
        return social_list.size();
    }

    @Override
    public Object getItem(int i) {
        return null;
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(final int i, View convertView, ViewGroup viewGroup) {

        View view = convertView;
        final ViewHolder holder;
        if (view == null) {
            view = LayoutInflater.from(context).inflate(R.layout.item_sociallink, viewGroup, false);
            holder = new ViewHolder();
            holder.title_link=(TextView)view.findViewById(R.id.title_link);
            holder.link=(TextView)view.findViewById(R.id.link);
            holder.icon_type=(ImageView) view.findViewById(R.id.icon_type);
            holder.cut_img=(ImageView)view.findViewById(R.id.cut_img);
            view.setTag(holder);
        } else {
            holder = (ViewHolder) view.getTag();
        }
        holder.title_link.setText(social_list.get(i).getTitle());
        holder.link.setText(social_list.get(i).getLink());
        holder.icon_type.setImageResource(checkcase(social_list.get(i).getTitle()));

        if(iscrossvisible.length()>0)
        {
            holder.cut_img.setVisibility(View.GONE);
        }
        holder.cut_img.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (callback != null) {
                    callback.clickaction(i);
                }
            }
        });

        holder.link.setPaintFlags(holder.link.getPaintFlags()| Paint.UNDERLINE_TEXT_FLAG);

        return view;
    }

    class ViewHolder {
        TextView title_link,link;
        ImageView icon_type,cut_img;
    }

    public int  checkcase(String type) {
        int image_type=0;
        if(type.equalsIgnoreCase(context.getResources().getString(R.string.facebook_txt)))
        {
            image_type=R.drawable.fb;
        }
        else if(type.equalsIgnoreCase(context.getResources().getString(R.string.twitter_txt)))
        {
            image_type=R.drawable.twitter_img;
        }
        else  if(type.equalsIgnoreCase(context.getResources().getString(R.string.linkein_txt)))
        {
            image_type=R.drawable.linkedin_img;
        }
        else if(type.equalsIgnoreCase(context.getResources().getString(R.string.intagram_txt)))
        {
            image_type=R.drawable.intagram;
        }
        else  if(type.equalsIgnoreCase(context.getResources().getString(R.string.pinterest_txt)))
        {
            image_type=R.drawable.pintrest;
        }
        return image_type;
    }



    public  void onCallBackReturn(Callback callback)
    {
        this.callback=callback;
    }

    public interface Callback{
        void clickaction(int position);
    }
}

