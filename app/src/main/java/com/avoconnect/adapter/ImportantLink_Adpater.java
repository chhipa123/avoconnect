package com.avoconnect.adapter;

import android.content.Context;
import android.graphics.Paint;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.avoconnect.R;
import com.avoconnect.beans.Important_Model;

import java.util.List;

public class ImportantLink_Adpater extends BaseAdapter {

    Context context;
    List<Important_Model> important_list;
    Callback callback;
    LinkClick linkClick;
    String enable;
    public ImportantLink_Adpater(Context context, List<Important_Model> important_list,String enable)
    {
        this.context=context;
        this.important_list=important_list;
        this.enable=enable;
    }
    @Override
    public int getCount() {
        return important_list.size();
    }

    @Override
    public Object getItem(int i) {
        return null;
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(final int i, View convertView, ViewGroup viewGroup) {

        View view = convertView;
        final ViewHolder holder;
        if (view == null) {
            view = LayoutInflater.from(context).inflate(R.layout.item_importantlink, viewGroup, false);
            holder = new ViewHolder();
            holder.title_link=(TextView)view.findViewById(R.id.title_link);
            holder.link=(TextView)view.findViewById(R.id.link);
            holder.cut_img=(ImageView)view.findViewById(R.id.cut_img);
            view.setTag(holder);
        } else {
            holder = (ViewHolder) view.getTag();
        }
        holder.title_link.setText(important_list.get(i).getTitle());
        holder.link.setText(important_list.get(i).getLink());
        if(enable.length()==0)
        {
            holder.cut_img.setVisibility(View.VISIBLE);
        }else{
            holder.cut_img.setVisibility(View.GONE);
        }

        holder.cut_img.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (callback != null) {
                    callback.clickaction(i);
                }
            }
        });
        holder.link.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (linkClick != null) {
                    linkClick.clickaction(i);
                }
            }
        });
        holder.link.setPaintFlags(holder.link.getPaintFlags()| Paint.UNDERLINE_TEXT_FLAG);
        return view;
    }

    class ViewHolder {

    TextView title_link,link;
    ImageView cut_img;
    }

    public  void onCallBackReturn(Callback callback)
    {
        this.callback=callback;
    }

    public interface Callback{
        void clickaction(int position);
    }
    public  void onlinkClick(LinkClick linkClick)
    {
        this.linkClick=linkClick;
    }

    public interface LinkClick{
        void clickaction(int position);
    }
}
