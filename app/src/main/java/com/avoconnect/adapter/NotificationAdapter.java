package com.avoconnect.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.avoconnect.R;
import com.avoconnect.Util.Utils;
import com.avoconnect.responsemodel.ListNotification_Model;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;

import de.hdodenhof.circleimageview.CircleImageView;

public class NotificationAdapter extends BaseAdapter {

    Context context;
    Callback callback;
    ListNotification_Model res;
    public NotificationAdapter(Context context, ListNotification_Model res) {
        this.context=context;
        this.res=res;
    }

    @Override
    public int getCount() {
        return res.getResourceData().size();
    }

    @Override
    public Object getItem(int i) {
        return null;
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(final int i, View convertView, ViewGroup viewGroup) {

        View view = convertView;
        final ViewHolder holder;
        if (view == null) {
            view = LayoutInflater.from(context).inflate(R.layout.item_notification, viewGroup, false);
            holder = new ViewHolder();
            holder.parentlay=(LinearLayout)view.findViewById(R.id.parentlay);
            holder.title=(TextView)view.findViewById(R.id.title);
            holder.time_hours=(TextView)view.findViewById(R.id.time_hours);
            holder.user_img=(CircleImageView)view.findViewById(R.id.img_userimg);
            holder.img_dot=(ImageView)view.findViewById(R.id.img_dot);
            holder.txt_user_placeholder=(TextView)view.findViewById(R.id.txt_user_placeholder);
            view.setTag(holder);
        } else {
            holder = (ViewHolder) view.getTag();
        }

        if(res.getResourceData().get(i).getIsRead()==1)
        {
            holder.parentlay.setBackgroundColor(context.getResources().getColor(R.color.white));
            holder.img_dot.setVisibility(View.GONE);

        }else{
            holder.parentlay.setBackgroundColor(context.getResources().getColor(R.color.event_background));
            holder.img_dot.setVisibility(View.VISIBLE);
        }

        try {
            if (res.getResourceData().get(i).getLogo() != null && res.getResourceData().get(i).getLogo().length() > 0) {
                holder.user_img.setVisibility(View.VISIBLE);
                holder.txt_user_placeholder.setVisibility(View.GONE);
                Glide.with(context)
                        .load(res.getResourceData().get(i).getLogo())
                        .apply(RequestOptions.circleCropTransform())
                        .into( holder.user_img);
            } else if (Utils.checkNull(res.getResourceData().get(i).getSenderName().trim())) {
                holder.user_img.setVisibility(View.GONE);
                holder.txt_user_placeholder.setVisibility(View.VISIBLE);
                holder.txt_user_placeholder.setText(Utils.nameInital(res.getResourceData().get(i).getSenderName().trim()) + "");
                //  holder. txt_user_placeholder.setText(new Utils(mContext).getFirstLastCharacter(resourceData.get(position).getFullName()));
            }
        }catch (Exception e)
        {
            e.printStackTrace();
        }

        holder.title.setText( res.getResourceData().get(i).getNotification());
        holder.time_hours.setText(" "+res.getResourceData().get(i).getTime());

        holder.parentlay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(callback!=null)
                {
                  if(res.getResourceData().get(i).getIsRead()==0)
                  {
                      /*Unread*/
                      if(res.getResourceData().get(i).getAction().equalsIgnoreCase("view_profile_request"))
                      {
                          callback.requestview(i,"view_profile_request");
                      }else{
                          callback.clickaction(i, res.getResourceData().get(i).getId());
                      }

                  }else {
                      /*Read*/
                      if(res.getResourceData().get(i).getAction().equalsIgnoreCase("card_received"))
                      {
                          callback.requestview(i,"card_received");
                      }else if(res.getResourceData().get(i).getAction().equalsIgnoreCase("accept_request"))
                      {
                          callback.requestview(i,"accept_request");
                      }
                  }

//                    if(res.getResourceData().get(i).getIsRead()==0) {
//                        callback.clickaction(i, res.getResourceData().get(i).getId());
//                    }
                }
            }
        });
        return view;
    }

    class ViewHolder {

        TextView title,time_hours;
        CircleImageView user_img;
        ImageView img_dot;
        TextView txt_user_placeholder;
        LinearLayout parentlay;
    }

    public  void onCallBackReturn(Callback callback)
    {
        this.callback=callback;
    }

    public interface Callback{
        void clickaction(int position,int notficationid);
        void requestview(int position,String type);
    }
}
