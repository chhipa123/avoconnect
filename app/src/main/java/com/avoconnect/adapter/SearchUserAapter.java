package com.avoconnect.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.avoconnect.R;
import com.avoconnect.Util.Utils;
import com.avoconnect.responsemodel.SearchUser_Model;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;

import java.util.ArrayList;

import de.hdodenhof.circleimageview.CircleImageView;

public class SearchUserAapter extends BaseAdapter {

    Context context;
    Callback callback;
    ArrayList<SearchUser_Model.UserResponce> arrayList;
    public SearchUserAapter(Context context, ArrayList<SearchUser_Model.UserResponce> arrayList)
    {
        this.context=context;
        this.arrayList=arrayList;
    }

    @Override
    public int getCount() {
        return arrayList.size();
    }

    @Override
    public Object getItem(int i) {
        return null;
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup viewGroup) {

        View view = convertView;
        final ViewHolder holder;
        if (view == null) {
            view = LayoutInflater.from(context).inflate(R.layout.item_searchuser, viewGroup, false);
            holder = new ViewHolder();
            holder.parentlay=(LinearLayout)view.findViewById(R.id.parentlay);
            holder.txt_username=(TextView) view.findViewById(R.id.txt_username);
            holder.img_userimg=(CircleImageView) view.findViewById(R.id.img_userimg);
            holder.txt_user_placeholder=(TextView) view.findViewById(R.id.txt_user_placeholder);

            view.setTag(holder);
        } else {
            holder = (ViewHolder) view.getTag();
        }
        holder.txt_username.setText(arrayList.get(position).getDisplayName());


//        if (Utils.checkNull(arrayList.get(position).getDisplayName().toString())) {
//            holder.img_userimg.setVisibility(View.GONE);
//            holder.txt_user_placeholder.setVisibility(View.VISIBLE);
//            holder.txt_user_placeholder.setText(Utils.nameInital(arrayList.get(position).getDisplayName().trim()) + "");
//        }

        if (arrayList.get(position).getUserProfileImage() != null && arrayList.get(position).getUserProfileImage().length() > 0) {
            holder.img_userimg.setVisibility(View.VISIBLE);
            holder.txt_user_placeholder.setVisibility(View.GONE);
            Glide.with(context)
                    .load(arrayList.get(position).getUserProfileImage())
                    .apply(RequestOptions.circleCropTransform())
                    .into(holder.img_userimg);
        }
        else if (Utils.checkNull(arrayList.get(position).getDisplayName().toString())) {
            holder.img_userimg.setVisibility(View.GONE);
            holder.txt_user_placeholder.setVisibility(View.VISIBLE);
            holder.txt_user_placeholder.setText(Utils.nameInital(arrayList.get(position).getDisplayName().trim()) + "");
        }


        holder.parentlay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (callback != null) {
                    callback.clickaction(position);
                }
            }
        });
        return view;
    }

    class ViewHolder {

        TextView txt_username,link,txt_user_placeholder;
        ImageView cut_img;
        CircleImageView img_userimg;
        LinearLayout parentlay;
    }

    public  void onCallBackReturn(Callback callback)
    {
        this.callback=callback;
    }

    public interface Callback{
        void clickaction(int position);
    }
}
