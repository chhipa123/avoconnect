package com.avoconnect.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.avoconnect.R;
import com.avoconnect.responsemodel.GetUserProfile_Model;

public class MySkillaAdpater  extends BaseAdapter {


    Context context;
   Callback callback;
    GetUserProfile_Model res;
    String check;
    public MySkillaAdpater(Context context, GetUserProfile_Model res, String s) {

        this.context = context;
        this.res=res;
        this.check=s;
    }
    @Override
    public int getCount() {
        return res.getUserSkillResponse().size();
    }

    @Override
    public Object getItem(int i) {
        return null;
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(final int i, View convertView, ViewGroup viewGroup) {
        View view = convertView;
        final ViewHolder holder;
        if (view == null) {
            view = LayoutInflater.from(context).inflate(R.layout.item_media, viewGroup, false);
            holder = new ViewHolder();
            holder.txt_url=(TextView)view.findViewById(R.id.txt_url);
            holder.image_cut=(LinearLayout)view.findViewById(R.id.image_cut);
            view.setTag(holder);
        } else {
            holder = (ViewHolder) view.getTag();
        }
        holder.txt_url.setText(res.getUserSkillResponse().get(i).getSkill());

        if(check.length()>0)
        {
            holder.image_cut.setVisibility(View.GONE);
        }else{
            holder.image_cut.setVisibility(View.VISIBLE);
        }
        holder.image_cut.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(callback!=null)
                {
                    callback.clickaction(i);
                }
            }
        });
        return view;
    }

    class ViewHolder {
        TextView txt_url;
        LinearLayout image_cut;
    }

    public  void onCallBackReturn(Callback callback)
    {
        this.callback=callback;
    }

    public interface Callback{
        void clickaction(int position);
    }
}
