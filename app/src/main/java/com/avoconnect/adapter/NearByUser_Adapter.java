package com.avoconnect.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.avoconnect.R;
import com.avoconnect.Util.Utils;
import com.avoconnect.responsemodel.GetAllEvent_Model;
import com.avoconnect.responsemodel.NearByUser_Model;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;

import de.hdodenhof.circleimageview.CircleImageView;

public class NearByUser_Adapter extends BaseAdapter {
    Context context;
    Callback callback;
    NearByUser_Model res;
    public NearByUser_Adapter(Context context, NearByUser_Model res)
    {
        this.context=context;
        this.res=res;

    }

    @Override
    public int getCount() {
        return res.getResourceData().size();
    }

    @Override
    public Object getItem(int i) {
        return null;
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup viewGroup) {

        View view = convertView;
        final ViewHolder holder;
        if (view == null) {
            view = LayoutInflater.from(context).inflate(R.layout.item_searchuser, viewGroup, false);
            holder = new ViewHolder();
            holder.parentlay=(LinearLayout)view.findViewById(R.id.parentlay);
            holder.txt_username=(TextView) view.findViewById(R.id.txt_username);
            holder.img_userimg=(CircleImageView) view.findViewById(R.id.img_userimg);
            holder.txt_user_placeholder=(TextView) view.findViewById(R.id.txt_user_placeholder);
            view.setTag(holder);
        } else {
            holder = (ViewHolder) view.getTag();
        }
        holder.txt_username.setText(res.getResourceData().get(position).getDisplayName());

        holder.parentlay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (callback != null) {
                    callback.clickaction(position);
                }
            }
        });

         if (res.getResourceData().get(position).getProfileImage() != null && res.getResourceData().get(position).getProfileImage().length() > 0) {
             holder.img_userimg.setVisibility(View.VISIBLE);
             holder.txt_user_placeholder.setVisibility(View.GONE);
            Glide.with(context)
                    .load(res.getResourceData().get(position).getProfileImage())
                    .apply(RequestOptions.circleCropTransform())
                    .into(holder.img_userimg);
        } else if (Utils.checkNull(res.getResourceData().get(position).getDisplayName())) {
             holder.img_userimg.setVisibility(View.GONE);
             holder.txt_user_placeholder.setVisibility(View.VISIBLE);
             holder.txt_user_placeholder.setText(Utils.nameInital(res.getResourceData().get(position).getDisplayName().trim()));
            //  holder. txt_user_placeholder.setText(new Utils(mContext).getFirstLastCharacter(resourceData.get(position).getFullName()));
        }

        return view;
    }

    class ViewHolder {

        TextView txt_username,link,txt_user_placeholder;
        ImageView cut_img;
        LinearLayout parentlay;
        CircleImageView img_userimg;

    }

    public  void onCallBackReturn(Callback callback)
    {
        this.callback=callback;
    }

    public interface Callback{
        void clickaction(int position);
    }
}
