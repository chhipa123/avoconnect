package com.avoconnect.adapter;

import android.content.Context;
import androidx.recyclerview.widget.RecyclerView;

import android.graphics.Paint;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.avoconnect.R;
import com.avoconnect.beans.Media_Model;

import java.util.List;

public class MediaAdpater extends RecyclerView.Adapter<MediaAdpater.GroceryViewHolder> {

    Context context;
    List<Media_Model> media_list;
    String status="";
    Callback callback;
    LinkClick linkClick;
    public MediaAdpater(Context context, List<Media_Model> media_list,String status) {

        this.context = context;
        this.media_list=media_list;
        this.status=status;
    }

    @Override
    public GroceryViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        //inflate the layout file
        View groceryProductView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_media, parent, false);

        GroceryViewHolder gvh = new GroceryViewHolder(groceryProductView);
        return gvh;
    }

    @Override
    public void onBindViewHolder(GroceryViewHolder holder, final int position) {

        holder.txt_url.setText(media_list.get(position).getUrl_link());
        holder.txt_url.setPaintFlags(holder.txt_url.getPaintFlags()| Paint.UNDERLINE_TEXT_FLAG);
        if(status.length()>0)
        {
            holder.image_cut.setVisibility(View.VISIBLE);
        }else{
            holder.image_cut.setVisibility(View.GONE);
        }
        holder.image_cut.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (callback != null) {
                    callback.clickaction(position);
                }
            }
        });
        holder.txt_url.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (linkClick != null) {
                    linkClick.clickaction(position);
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return media_list.size();
    }

    public class GroceryViewHolder extends RecyclerView.ViewHolder {
        TextView txt_url;
        LinearLayout image_cut;
        public GroceryViewHolder(View view) {
            super(view);
            txt_url=(TextView)view.findViewById(R.id.txt_url);
            image_cut=(LinearLayout)view.findViewById(R.id.image_cut);
        }

    }

    public  void onCallBackReturn(Callback callback)
    {
        this.callback=callback;
    }

    public interface Callback{
        void clickaction(int position);
    }
    public  void onlinkClick(LinkClick linkClick)
    {
        this.linkClick=linkClick;
    }

    public interface LinkClick{
        void clickaction(int position);
    }
}
