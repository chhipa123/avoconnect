package com.avoconnect.Util;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.TextView;


import com.avoconnect.R;
import com.lms.customprogress.CustomProgress;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

import retrofit.client.Response;


public class ProgressBarCustom {
    static Context mContext;
    public static Dialog customProgress;
    public static Dialog yesNoDialog;
    public ProgressBarCustom(Context mContext) {
        this.mContext = mContext;
    }

    public static void showProgress() {

        cancelProgress();
        if (customProgress != null) {
            if (!customProgress.isShowing()) {
                Log.i("LOADER", "============LOADER  ");
                customProgress = CustomProgress.show(mContext)
                ;
            } else {
                cancelProgress();
            }
        } else {
            customProgress = CustomProgress.show(mContext);
        }
    }

    public static void cancelProgress() {
//
        try {
            if ((customProgress != null) && customProgress.isShowing()) {
                customProgress.dismiss();
            }
        } catch (final IllegalArgumentException e) {
            // Handle or log or ignore
        } catch (final Exception e) {
            // Handle or log or ignore
        } finally {
            customProgress = null;
        }


    }

    public static Dialog getProgress() {
        return customProgress;
    }

    public static boolean customDialog(final Context dialogContext,
                                       String message, final boolean flag) {
        final Dialog dialog;
        dialog = new Dialog(dialogContext);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.getWindow().setBackgroundDrawableResource(
                android.R.color.transparent);
        // dialog.getWindow().getAttributes().windowAnimations =
        // R.style.DialogAnimation;
        dialog.setContentView(R.layout.dialog_condition);
        TextView btn = ((TextView) dialog.findViewById(R.id.btnDialogOk));
        btn.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View arg0) {
                // TODO Auto-generated method stub
                dialog.dismiss();
                if (flag) {
                    Activity act = (Activity) dialogContext;
                    act.finish();

                }
            }
        });

        ((TextView) dialog.findViewById(R.id.txtDialogMessage))
                .setText(message);

        dialog.show();
        return true;
    }



    public static String getResponseInStr(Response response) {
        BufferedReader reader = null;
        StringBuilder sb = new StringBuilder();
        try {
            reader = new BufferedReader(new InputStreamReader(response.getBody().in()));
            String line;
            try {
                while ((line = reader.readLine()) != null) {
                    sb.append(line);
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        String result = sb.toString();

        return result;
    }

    public static void drawRouteOnMap(String destinationLatitude, String destinationLongitude, String sourceLatitude, String sourceLongitude) {
        String uri = "http://maps.google.com/maps?saddr=" + sourceLatitude + "," + sourceLongitude + "&daddr=" + destinationLatitude + "," + destinationLongitude;
        Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(uri));
        mContext.startActivity(intent);
    }
}
