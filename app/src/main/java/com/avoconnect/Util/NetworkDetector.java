package com.avoconnect.Util;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

/**
 * Created by VIVEK on 27-Mar-19.
 */

public class NetworkDetector {


        private Context _context;

        public NetworkDetector(Context context) {
            this._context = context;
        }

        /**
         * Checking for all possible internet providers
         * **/
        public boolean isConnectingToInternet() {
            ConnectivityManager connectivity = (ConnectivityManager) _context
                    .getSystemService(Context.CONNECTIVITY_SERVICE);
            if (connectivity != null) {
                NetworkInfo[] info = connectivity.getAllNetworkInfo();
                if (info != null)
                    for (int i = 0; i < info.length; i++)
                        if (info[i].getState() == NetworkInfo.State.CONNECTED) {
                            return true;
                        }

            }
            return false;
        }


}
