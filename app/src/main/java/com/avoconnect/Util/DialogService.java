package com.avoconnect.Util;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Paint;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Handler;

import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.view.Window;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;


import com.avoconnect.R;

import java.util.ArrayList;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

import static com.avoconnect.Util.ProgressBarCustom.yesNoDialog;


/**
 * Created by vicky on 4/28/2016.
 */

public class DialogService {

    Context context;
    public void DialogService(Context context)
    {
        this.context=context;
    }
    public static void customYesDialog(View.OnClickListener clickListener,Context dialogContext,String message){
        yesNoDialog = new Dialog(dialogContext);
        yesNoDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        yesNoDialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        //yesNoDialog.getWindow().getAttributes().windowAnimations = R.style.DialogAnimation;
        yesNoDialog.setContentView(R.layout.logout_lay);
        yesNoDialog.setCanceledOnTouchOutside(true);
//        if(title!=null && title.length()>0){
//            if(title.equalsIgnoreCase("login")){
//                ((Button) yesNoDialog.findViewById(R.id.btnDialogYes)).setText("Login");
//                ((Button) yesNoDialog.findViewById(R.id.btnDialogNo)).setText("Cancel");
//            }
//            if(title.equalsIgnoreCase("cart")){
//
//            }
//        }


        //((Button) yesNoDialog.findViewById(R.id.btnDialogYes)).setText("Ok");
        //((Button) yesNoDialog.findViewById(R.id.btnDialogNo)).setText("Cancel");
        //((Button) yesNoDialog.findViewById(R.id.btnDialogNo)).setTextColor(ContextCompat.getColor(dialogContext, R.color.colorAppRed));

        ((Button) yesNoDialog.findViewById(R.id.btnDialogYes)).setOnClickListener(clickListener);
        ((Button) yesNoDialog.findViewById(R.id.btnDialogNo)).setOnClickListener(clickListener);
        ((TextView) yesNoDialog.findViewById(R.id.txtDialogMessage)).setText(message);
        //   ((TextView) yesNoDialog.findViewById(R.id.txtDialogTitle)).setText("");
        yesNoDialog.show();
    }

}
