package com.avoconnect.Util;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.net.ConnectivityManager;
import android.os.Handler;
import android.os.Looper;
import android.util.Base64;
import android.util.Log;
import android.util.Patterns;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;
import androidx.cardview.widget.CardView;

import com.avoconnect.Prefrence.UserPrefrence;
import com.avoconnect.R;
import com.avoconnect.activity.HomeActivity;
import com.avoconnect.activity.Login_Activity;
import com.twitter.sdk.android.core.models.User;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import retrofit.RetrofitError;
import retrofit.mime.TypedByteArray;

public class Utils {

    Context context;
    String error_handler="",error_description="";
   public static Dialog yesNoDialog;
   public Utils(Context context)
    {
        this.context=context;
    }

    public boolean isvalidaEmail(String email)
    {
        boolean isvalid=false;
        String expression = "^[\\w\\.-]+@([\\w\\-]+\\.)+[A-Z]{2,4}$";
        Pattern pattern = Pattern.compile(expression, Pattern.CASE_INSENSITIVE);
        Matcher matcher = pattern.matcher(email);
        return matcher.matches();
    }


    public String convertpasswordtobase64(String input)
    {
        byte[] data;
        String str = input.trim();
        String base64="";
        try {
            data = str.getBytes("UTF-8");
            base64 = Base64.encodeToString(data, Base64.DEFAULT);
            Log.e("Base 64 ", base64);
        }catch (Exception e)
        {
            e.printStackTrace();
        }
        return base64;
    }
    public void setTextview(TextView textview, int fontType) {
        Typeface type;
        switch (fontType) {
            case 1:
                type = Typeface.createFromAsset(context.getAssets(), "font/Montserrat_Regular.ttf");
                textview.setTypeface(type);
                break;

            case 2:
                type = Typeface.createFromAsset(context.getAssets(), "font/Montserrat_SemiBold.ttf");
                textview.setTypeface(type);
                break;
        }
    }
    public void setEditview(EditText textview, int fontType) {
        Typeface type;
        switch (fontType) {
            case 1:
                type = Typeface.createFromAsset(context.getAssets(), "font/Montserrat_Regular.ttf");
                textview.setTypeface(type);
                break;

            case 2:
                type = Typeface.createFromAsset(context.getAssets(), "font/Montserrat_SemiBold.ttf");
                textview.setTypeface(type);
                break;
        }


    }

    public String convertTime(String _24HourTime)
    {
        String time="";
       try {
           SimpleDateFormat _24HourSDF = new SimpleDateFormat("HH:mm");
           SimpleDateFormat _12HourSDF = new SimpleDateFormat("hh:mm a");
           Date _24HourDt = _24HourSDF.parse(_24HourTime);
            time=_12HourSDF.format(_24HourDt);

       }catch (Exception e)
       {
           e.printStackTrace();
       }
        return time;
    }

    public boolean isNetworkAvailable(Context context) {
        ConnectivityManager connectivityManager = ((ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE));
        return connectivityManager.getActiveNetworkInfo() != null && connectivityManager.getActiveNetworkInfo().isConnected();
    }

    public  boolean validateFirstName( String firstName )
    {
        return firstName.matches( "[a-zA-Z. ]*" );
    }

    public boolean validwebsite(String urlAddress)
    {

        boolean matches=android.util.Patterns.WEB_URL.matcher(urlAddress).matches();
        return matches;
    }


    public String dateConvert(String D){

        SimpleDateFormat format1 = new SimpleDateFormat("yyyy-MM-dd");
        SimpleDateFormat format2 = new SimpleDateFormat("dd-MMM-yyyy");
        Date date = null;
        try {
            date = format1.parse(D);
        } catch (ParseException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        String dateString = format2.format(date);

       String tempArray[] = dateString.split("-");
        dateString=tempArray[1]+" "+tempArray[0]+", "+tempArray[2];
        System.out.println(dateString);
        return ((dateString));
    }

    public  int CheckDates(String d1, String d2)    {

        SimpleDateFormat dfDate  = new SimpleDateFormat("yyyy-MM-dd");
        int b = 0;
        try {
            if(dfDate.parse(d1).before(dfDate.parse(d2)))
            {
                b = 1;//If start date is before end date
            }
            else if(dfDate.parse(d1).equals(dfDate.parse(d2)))
            {
                b = 2;//If two dates are equal
            }
            else
            {
                b = 0; //If start date is after the end date
            }
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return b;
    }

    public static Boolean checkNull(String value){
        if(value!=null&& !value.equals("")){
            return true;
        }
        return false;
    }

    public void showError(RetrofitError error)
    {
        if (error.getMessage().equalsIgnoreCase("500")) {
            Toast.makeText(context, context.getResources().getString(R.string.internal_server), Toast.LENGTH_SHORT).show();
        } else {
            if (error.getMessage().toString().trim().length() > 3) {
                Toast.makeText(context, context.getResources().getString(R.string.unable_connect), Toast.LENGTH_SHORT).show();
            } else {
                if (error.getMessage() != null) {
                    if (error.getResponse().getBody() != null) {
                        String json = new String(((TypedByteArray) error.getResponse().getBody()).getBytes());
                          JSONObject jsonObject = null;
                        Log.e("=500","==ekse=========="+json);
                        try {
                            if (json.toString() != null) {
                                jsonObject = new JSONObject(json.toString());

                                if(jsonObject.has("error_description"))
                                {
                                    error_description= jsonObject.getString("error_description");
                                }
                                if(jsonObject.has("error"))
                                {
                                    if(jsonObject.getString("error").equalsIgnoreCase("invalid_token"))
                                    {

                                        new Handler(Looper.getMainLooper()).post(new Runnable() {
                                            @Override
                                            public void run() {
                                                try {
                                                    Toast.makeText(context, context.getResources().getString(R.string.session_expire), Toast.LENGTH_SHORT).show();
                                                    UserPrefrence.getInstance(context).logoutDone();
                                                    Intent intent = new Intent(context, Login_Activity.class);
                                                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                                    context.startActivity(intent);
                                                }catch (Exception e)
                                                {
                                                    e.printStackTrace();
                                                }
                                            }
                                        });

                                    }else{
                                        try {

                                            error_handler="";

                                            if(jsonObject.getString("error_description")!=null){
                                                error_handler=jsonObject.getString("error_description");
                                            }
                                            else if(jsonObject.getString("error")!=null)
                                            {
                                                if(jsonObject.getString("error").equalsIgnoreCase("unauthorized_user"))
                                                {
                                                    if(jsonObject.getString("error_description")!=null)
                                                    {
                                                        error_handler=jsonObject.getString("error_description");
                                                    }

                                                }else{
                                                    error_handler=jsonObject.getString("error");
                                                }

                                            }
                                            new Handler(Looper.getMainLooper()).post(new Runnable() {
                                                @Override
                                                public void run() {

                                                   ProgressBarCustom.customDialog(context,error_handler,false);
                                                }
                                            });
                                        }catch (Exception e)
                                        {
                                            e.printStackTrace();
                                        }

                                    }

                                }
                               else if (jsonObject.has("error_description")) {

                                    new Handler(Looper.getMainLooper()).post(new Runnable() {
                                        @Override
                                        public void run() {

                                            ProgressBarCustom.customDialog(context,error_description,false);
                                        }
                                    });


                                }
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }

                }
            }
        }
    }


        public static String getFirstLastCharacter(String name)
        {
            String[] values = name.split(" ");
            String finalname="";
            if(values[0]!=null)
            {
                char first = values[0].charAt(0);
                finalname= String.valueOf(first);
            }

            if(values[1]!=null)
            {
                char first = values[1].charAt(0);
                finalname=finalname+" "+String.valueOf(first);
            }
            return finalname;
        }

    public static String nameInital(String fullname){
        String name = "";
       try {
           String lastName = "";
           String firstName = "";
           if (fullname.split("\\w+").length > 1) {

               lastName = String.valueOf(fullname.substring(fullname.lastIndexOf(" ") + 1).charAt(0));
               firstName = String.valueOf(fullname.substring(0, fullname.lastIndexOf(' ')).charAt(0));
               name = firstName + lastName;
           } else {
               firstName = String.valueOf(fullname.charAt(0));
               name = firstName;
           }
       } catch (Exception e)
       {
           e.printStackTrace();
       }
        return name.toUpperCase();
    }


    public static void customYesDialog(View.OnClickListener clickListener,Context dialogContext){
        yesNoDialog = new Dialog(dialogContext);
        yesNoDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        //yesNoDialog.getWindow().getAttributes().windowAnimations = R.style.DialogAnimation;
        yesNoDialog.setContentView(R.layout.premiumuser_pop);

        ((TextView) yesNoDialog.findViewById(R.id.txt_upgrade)).setOnClickListener(clickListener);

        yesNoDialog.show();
    }

    public int getMonth(String selected_month,int years)
    {
        int a=0;
        if(selected_month.equalsIgnoreCase("Jan"+","+years))
        {
            a=1;
        }
        else if(selected_month.equalsIgnoreCase("Feb"+","+years))
        {
            a=2;
        }
        else if(selected_month.equalsIgnoreCase("Mar"+","+years))
        {
            a=3;
        }
        else  if(selected_month.equalsIgnoreCase("Apr"+","+years))
        {
            a=4;
        }
        else  if(selected_month.equalsIgnoreCase("May"+","+years))
        {
            a=5;
        }
        else  if(selected_month.equalsIgnoreCase("Jun"+","+years))
        {
            a=6;
        }
        else   if(selected_month.equalsIgnoreCase("Jul"+","+years))
        {
            a=7;
        }
        else if(selected_month.equalsIgnoreCase("Aug"+","+years))
        {
            a=8;
        }
        else   if(selected_month.equalsIgnoreCase("Sep"+","+years))
        {
            a=9;
        }
        else  if(selected_month.equalsIgnoreCase("Oct"+","+years))
        {
            a=10;
        }
        else if(selected_month.equalsIgnoreCase("Nov"+","+years))
        {
            a=11;
        }
        else if(selected_month.equalsIgnoreCase("Dec"+","+years))
        {
            a=12;
        }
    return a;
    }





 }


