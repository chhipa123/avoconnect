package com.avoconnect.BottomSheetFragments;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.avoconnect.activity.ScanBusinessCardActivity;
import com.avoconnect.testingcard.TextActivity;
import com.google.android.material.bottomsheet.BottomSheetBehavior;
import com.google.android.material.bottomsheet.BottomSheetDialogFragment;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.avoconnect.R;
import com.avoconnect.activity.AddEditCardActitivty;
import com.avoconnect.activity.Generateqrcode_Acitivity;

import static android.app.Activity.RESULT_OK;

public class AddManualScanFragment extends BottomSheetDialogFragment implements View.OnClickListener {

    Dialog dialog_global;
   Callback callback;
   boolean isfromqr_scanner=false;
    private BottomSheetBehavior.BottomSheetCallback mBottomSheetBehaviorCallback = new BottomSheetBehavior.BottomSheetCallback() {

        @Override
        public void onStateChanged(@NonNull View bottomSheet, int newState) {
//            if (newState == BottomSheetBehavior.STATE_HIDDEN) {
//                dismiss();
//            }

        }

        @Override
        public void onSlide(@NonNull View bottomSheet, float slideOffset) {

        }
    };

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);


    }

    @SuppressLint("RestrictedApi")
    @Override
    public void setupDialog(final Dialog dialog, int style) {
        super.setupDialog(dialog, style);
        //Get the content View
        dialog_global=dialog;

        View contentView = View.inflate(getContext(), R.layout.item_addmanual_scan, null);
        TextView txt_scan=(TextView)contentView.findViewById(R.id.txt_scan);
        TextView txt_addmaually=(TextView)contentView.findViewById(R.id.txt_addmaually);
        TextView txt_scan_qrcode=(TextView)contentView.findViewById(R.id.txt_scan_qrcode);
        ImageView cross_img = (ImageView) contentView.findViewById(R.id.cross_img);
        dialog.setContentView(contentView);

        txt_scan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                Intent intent=new Intent(getActivity(), ScanBusinessCardActivity.class);
//                startActivityForResult(intent,144);
                isfromqr_scanner=false;
                Intent intent=new Intent(getActivity(), TextActivity.class);
                startActivityForResult(intent,144);
             //   dialog.dismiss();
            }
        });
        txt_addmaually.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                isfromqr_scanner=false;
                Intent intent=new Intent(getActivity(), AddEditCardActitivty.class);
                intent.putExtra("additional_info","yes");
                intent.putExtra("manual","MANUAL");
                startActivityForResult(intent,144);
            }
        });
        txt_scan_qrcode.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                isfromqr_scanner=true;
                Intent intent=new Intent(getActivity(), Generateqrcode_Acitivity.class);
                startActivityForResult(intent,144);
            }
        });
        cross_img.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.cancel();
            }
        });



        CoordinatorLayout.LayoutParams params = (CoordinatorLayout.LayoutParams) ((View) contentView.getParent()).getLayoutParams();
        CoordinatorLayout.Behavior behavior = params.getBehavior();

        //Set callback
        if (behavior != null && behavior instanceof BottomSheetBehavior) {
            ((BottomSheetBehavior) behavior).setBottomSheetCallback(mBottomSheetBehaviorCallback);
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 144 && resultCode == RESULT_OK) {
            if (callback != null) {
                if(isfromqr_scanner==false) {
                    callback.clickaction(0);
                }
            }
            dialog_global.dismiss();
            //getActivity().finish();
        }else if(requestCode==154){
            dialog_global.dismiss();
        }
    }

    public  void onCallBackReturn(Callback callback)
    {
        this.callback=callback;
    }

    public interface Callback{
        void clickaction(int position);
    }

    @Override
    public void onClick(View view) {

    }
}