package com.avoconnect.BottomSheetFragments;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.avoconnect.Fragments.Profile_Fragment;
import com.google.android.material.bottomsheet.BottomSheetBehavior;
import com.google.android.material.bottomsheet.BottomSheetDialogFragment;

import androidx.appcompat.app.AlertDialog;
import androidx.coordinatorlayout.widget.CoordinatorLayout;

import android.os.Environment;
import android.os.Handler;
import android.os.StrictMode;
import android.provider.Settings;
import android.util.Log;
import android.view.View;
import android.webkit.URLUtil;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.avoconnect.R;
import com.avoconnect.activity.SearchNearByActivity;
import com.avoconnect.activity.SharingActitivtity;
import com.karumi.dexter.Dexter;
import com.karumi.dexter.MultiplePermissionsReport;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.DexterError;
import com.karumi.dexter.listener.PermissionDeniedResponse;
import com.karumi.dexter.listener.PermissionGrantedResponse;
import com.karumi.dexter.listener.PermissionRequest;
import com.karumi.dexter.listener.PermissionRequestErrorListener;
import com.karumi.dexter.listener.multi.MultiplePermissionsListener;
import com.karumi.dexter.listener.single.PermissionListener;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.util.List;

import static android.app.Activity.RESULT_OK;
import static android.content.Context.LOCATION_SERVICE;

public class ShareCardFragment extends BottomSheetDialogFragment implements View.OnClickListener{

    boolean isclick = false;
    EditText linktitle_lay, et_link;
    String name="",email="",mobile="",card_id="",share_url="",logo_url="",backgroundcolor="";
   Callback callback;
   CutCallback cutCallback;
    Dialog dialog_global;
    Uri uri;
    private BottomSheetBehavior.BottomSheetCallback mBottomSheetBehaviorCallback = new BottomSheetBehavior.BottomSheetCallback() {

        @Override
        public void onStateChanged(@NonNull View bottomSheet, int newState) {
//            if (newState == BottomSheetBehavior.STATE_HIDDEN) {
//                dismiss();
//            }

        }

        @Override
        public void onSlide(@NonNull View bottomSheet, float slideOffset) {

        }
    };

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        StrictMode.VmPolicy.Builder builder = new StrictMode.VmPolicy.Builder();
        StrictMode.setVmPolicy(builder.build());

    }

    @SuppressLint("RestrictedApi")
    @Override
    public void setupDialog(final Dialog dialog, int style) {
        super.setupDialog(dialog, style);
        //Get the content View
        dialog_global=dialog;
        View contentView = View.inflate(getContext(), R.layout.item_shared_card, null);
        TextView txt_shareuserid=(TextView)contentView.findViewById(R.id.txt_shareuserid);
        TextView txt_shareemail=(TextView)contentView.findViewById(R.id.txt_shareemail);
        TextView txt_sharesms=(TextView)contentView.findViewById(R.id.txt_sharesms);
        TextView txt_shareother=(TextView)contentView.findViewById(R.id.txt_shareother);
        TextView txt_shareqrcode=(TextView)contentView.findViewById(R.id.txt_shareqrcode);
        TextView txt_sharenearby=(TextView)contentView.findViewById(R.id.txt_sharenearby);

       ImageView cross_img = (ImageView) contentView.findViewById(R.id.cross_img);

        dialog.setContentView(contentView);
        cross_img.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (cutCallback != null) {
                    cutCallback.cut_clickaction(0);
                }
                dialog.cancel();
//                Intent intent = new Intent();
//                getActivity().setResult(Activity.RESULT_CANCELED, intent);
//                getActivity().finish();
            }
        });

         txt_shareuserid.setOnClickListener(this);
         txt_shareemail.setOnClickListener(this);
         txt_sharesms.setOnClickListener(this);
         txt_shareother.setOnClickListener(this);
         txt_shareqrcode.setOnClickListener(this);
         txt_sharenearby.setOnClickListener(this);

        Bundle bundle = this.getArguments();
        if (bundle != null) {
             name = bundle.getString("name", "");
             email = bundle.getString("email", "");
             mobile = bundle.getString("mobile", "");
             card_id=bundle.getString("card_id", "");
            share_url=bundle.getString("share_url", "");
            backgroundcolor=bundle.getString("backgroundcolor", "");
            if(bundle.getString("logo")!=null)
            {
                logo_url=bundle.getString("logo", "");
                Log.e("=========",logo_url);
            }
            Log.e("========data",name+" "+email+" "+mobile+" "+share_url);
        }


        Dexter.withActivity(getActivity())
                .withPermissions(
                        Manifest.permission.WRITE_EXTERNAL_STORAGE,
                        Manifest.permission.READ_EXTERNAL_STORAGE
                ).withListener(new MultiplePermissionsListener() {
            @Override
            public void onPermissionsChecked(MultiplePermissionsReport report) {

                getUriToResource(getActivity(),R.drawable.applogo);
            }

            @Override
            public void onPermissionRationaleShouldBeShown(List<PermissionRequest> permissions, PermissionToken token) {
            }
        }).withErrorListener(new PermissionRequestErrorListener() {

            @Override
            public void onError(DexterError error) {
                Log.e("=eroor", error.toString());
            }

        }).check();


        CoordinatorLayout.LayoutParams params = (CoordinatorLayout.LayoutParams) ((View) contentView.getParent()).getLayoutParams();
        CoordinatorLayout.Behavior behavior = params.getBehavior();

        //Set callback
        if (behavior != null && behavior instanceof BottomSheetBehavior) {
            ((BottomSheetBehavior) behavior).setBottomSheetCallback(mBottomSheetBehaviorCallback);
        }
    }

    private void validation() {
        String getText = linktitle_lay.getText().toString();
        String getlink = et_link.getText().toString();
        if (getText.length() == 0) {
            Toast.makeText(getActivity(), getResources().getString(R.string.enter_link_title), Toast.LENGTH_SHORT).show();
        } else if (getlink.length() == 0) {
            Toast.makeText(getActivity(), getResources().getString(R.string.enter_link), Toast.LENGTH_SHORT).show();
        } else if (URLUtil.isValidUrl(getlink) == false) {
            Toast.makeText(getActivity(), getResources().getString(R.string.enter_valid_url), Toast.LENGTH_SHORT).show();
        } else {
            Intent intent = new Intent();
            intent.putExtra("value", getText);
            intent.putExtra("link", getlink);
            intent.putExtra("type", "important");
            getActivity().setResult(Activity.RESULT_OK, intent);
            getActivity().finish();
        }

    }

    @Override
    public void onClick(View view) {
        switch (view.getId())
        {
            case R.id.txt_shareuserid:
                Intent intent=new Intent(getActivity(), SharingActitivtity.class);
                intent.putExtra("sharing_type","userid");
                intent.putExtra("name",name);
                intent.putExtra("email",email);
                intent.putExtra("mobile",mobile);
                intent.putExtra("card_id",card_id);
                intent.putExtra("share_url",share_url);
                intent.putExtra("logo_url",logo_url);
                intent.putExtra("backgroundcolor",backgroundcolor);
               startActivityForResult(intent,144);
                break;
            case R.id.txt_shareemail:
                Intent intent01=new Intent(getActivity(), SharingActitivtity.class);
                intent01.putExtra("sharing_type","email");
                intent01.putExtra("name",name);
                intent01.putExtra("email",email);
                intent01.putExtra("mobile",mobile);
                intent01.putExtra("card_id",card_id);
                intent01.putExtra("share_url",share_url);
                intent01.putExtra("logo_url",logo_url);
                intent01.putExtra("backgroundcolor",backgroundcolor);
                startActivityForResult(intent01,144);
                break;
            case R.id.txt_sharesms:

                sendMMS();
//                Intent intent02=new Intent(getActivity(), SharingActitivtity.class);
//                intent02.putExtra("sharing_type","sms");
//                intent02.putExtra("name",name);
//                intent02.putExtra("email",email);
//                intent02.putExtra("mobile",mobile);
//                intent02.putExtra("card_id",card_id);
//                intent02.putExtra("share_url",share_url);
//                intent02.putExtra("logo_url",logo_url);
//                intent02.putExtra("backgroundcolor",backgroundcolor);
//                startActivityForResult(intent02,144);
                break;
            case R.id.txt_shareother:
                Intent share = new Intent(android.content.Intent.ACTION_SEND);
                share.setType("text/plain");
                share.addFlags(Intent.FLAG_ACTIVITY_CLEAR_WHEN_TASK_RESET);
                share.putExtra(Intent.EXTRA_SUBJECT, "Avonnect Card Share");
                share.putExtra(Intent.EXTRA_TEXT, share_url);
                startActivity(Intent.createChooser(share, "Share link!"));
                break;
            case R.id.txt_shareqrcode:
                Intent intent03=new Intent(getActivity(), SharingActitivtity.class);
                intent03.putExtra("sharing_type","qrocode");
                intent03.putExtra("name",name);
                intent03.putExtra("email",email);
                intent03.putExtra("mobile",mobile);
                intent03.putExtra("card_id",card_id);
                intent03.putExtra("logo_url",logo_url);
                intent03.putExtra("backgroundcolor",backgroundcolor);
                startActivity(intent03);
                break;
            case R.id.txt_sharenearby:

                LocationManager service = (LocationManager) getActivity().getSystemService(LOCATION_SERVICE);
                boolean enabled = service.isProviderEnabled(LocationManager.GPS_PROVIDER);
                // Check if enabled and if not send user to the GPS settings
                if (!enabled) {
                    showAlert();
                }else {
                    Dexter.withActivity(getActivity())
                            .withPermission(Manifest.permission.ACCESS_FINE_LOCATION)
                            .withListener(new PermissionListener() {
                                @Override
                                public void onPermissionGranted(PermissionGrantedResponse response) {

                                    Intent intent10 = new Intent(getActivity(), SearchNearByActivity.class);
                                    intent10.putExtra("card_id", card_id);
                                    startActivity(intent10);

                                }

                                @Override
                                public void onPermissionDenied(PermissionDeniedResponse response) {

                                }

                                @Override
                                public void onPermissionRationaleShouldBeShown(PermissionRequest permission, PermissionToken token) {

                                }
                            }).check();
                }

                break;

        }
    }

    private void showAlert() {
        final AlertDialog.Builder dialog = new AlertDialog.Builder(getActivity());
        dialog.setTitle("Enable Location")
                .setMessage("Your Locations Settings is set to 'Off'.\nPlease Enable Location to " +
                        "use this app")
                .setPositiveButton("Location Settings", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface paramDialogInterface, int paramInt) {
                        Intent myIntent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                        startActivity(myIntent);
                    }
                })
                .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface paramDialogInterface, int paramInt) {
                    }
                });
        dialog.show();
    }
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 144 && resultCode == RESULT_OK) {
            if (callback != null) {
                callback.clickaction(0);
            }
            dialog_global.dismiss();
            //getActivity().finish();
        }
    }

    public  void onCallBackReturn(Callback callback)
    {
        this.callback=callback;
    }

    public interface Callback{
        void clickaction(int position);
    }



    public  void onCutCallBackReturn(CutCallback callback)
    {
        this.cutCallback=callback;
    }

    public interface CutCallback{
        void cut_clickaction(int position);
    }

    public   void getUriToResource(Context context, int resId){
        try {
            Bitmap icon = BitmapFactory.decodeResource(context.getResources(),
                    R.drawable.applogo);

            ByteArrayOutputStream bytes = new ByteArrayOutputStream();
            icon.compress(Bitmap.CompressFormat.JPEG, 60, bytes);
            File f = new File(Environment.getExternalStorageDirectory()
                    + File.separator + "testimage.jpg");
            f.createNewFile();
            FileOutputStream fo = new FileOutputStream(f);
            fo.write(bytes.toByteArray());
            fo.close();

            uri = Uri.fromFile(f);


            /** return uri */

        }catch (Exception e)
        {
            e.printStackTrace();
        }
    }

    public void sendMMS() {
        try {

            Intent smsIntent = new Intent(Intent.ACTION_VIEW);
            smsIntent.setType("vnd.android-dir/mms-sms");
            smsIntent.putExtra("address", "");
            smsIntent.putExtra("sms_body",share_url);
            startActivity(smsIntent);

//        Intent intentsms = new Intent(Intent.ACTION_VIEW, Uri.parse("sms:"+"9806503950"));
//        intentsms.putExtra("sms_body",  share_url);
//        startActivity(intentsms);

//        Intent intent = new Intent(Intent.ACTION_SEND);
//        intent.putExtra("sms_body", share_url);
//        intent.putExtra("address", Uri.parse("sms:"+""));
//       intent.putExtra(Intent.EXTRA_STREAM, uri);
//        intent.setType("image/gif");
//         startActivity(Intent.createChooser(intent,"Send"));

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}