package com.avoconnect.BottomSheetFragments;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.google.android.material.bottomsheet.BottomSheetBehavior;
import com.google.android.material.bottomsheet.BottomSheetDialogFragment;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import android.view.View;
import android.webkit.URLUtil;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.avoconnect.R;

public class AddtionalLinkFragment extends BottomSheetDialogFragment {

    boolean isclick = false;
    EditText linktitle_lay, et_link;
    private BottomSheetBehavior.BottomSheetCallback mBottomSheetBehaviorCallback = new BottomSheetBehavior.BottomSheetCallback() {

        @Override
        public void onStateChanged(@NonNull View bottomSheet, int newState) {
//            if (newState == BottomSheetBehavior.STATE_HIDDEN) {
//                dismiss();
//            }

        }

        @Override
        public void onSlide(@NonNull View bottomSheet, float slideOffset) {

        }
    };

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }

    @SuppressLint("RestrictedApi")
    @Override
    public void setupDialog(Dialog dialog, int style) {
        super.setupDialog(dialog, style);
        //Get the content View
        View contentView = View.inflate(getContext(), R.layout.addtionallink_layout, null);
        final LinearLayout dropdown_lay = (LinearLayout) contentView.findViewById(R.id.dropdown_lay);
        linktitle_lay = (EditText) contentView.findViewById(R.id.linktitle_lay);
        et_link = (EditText) contentView.findViewById(R.id.et_link);
        ImageView cross_img = (ImageView) contentView.findViewById(R.id.cross_img);
        TextView txt_submit = (TextView) contentView.findViewById(R.id.txt_submit);

        dialog.setContentView(contentView);
        cross_img.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent();
                getActivity().setResult(Activity.RESULT_CANCELED, intent);
                getActivity().finish();
            }
        });
        txt_submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                validation();
            }
        });
        //Set the coordinator layout behavior
        CoordinatorLayout.LayoutParams params = (CoordinatorLayout.LayoutParams) ((View) contentView.getParent()).getLayoutParams();
        CoordinatorLayout.Behavior behavior = params.getBehavior();

        //Set callback
        if (behavior != null && behavior instanceof BottomSheetBehavior) {
            ((BottomSheetBehavior) behavior).setBottomSheetCallback(mBottomSheetBehaviorCallback);
        }
    }

    private void validation() {
        String getText = linktitle_lay.getText().toString();
        String getlink = et_link.getText().toString();
        if (getText.length() == 0) {
            Toast.makeText(getActivity(), getResources().getString(R.string.enter_link_title), Toast.LENGTH_SHORT).show();
        } else if (getlink.length() == 0) {
            Toast.makeText(getActivity(), getResources().getString(R.string.enter_link), Toast.LENGTH_SHORT).show();
        }
//        else if (URLUtil.isValidUrl(getlink) == false) {
//            Toast.makeText(getActivity(), getResources().getString(R.string.enter_valid_url), Toast.LENGTH_SHORT).show();
//        }
        else {
            Intent intent = new Intent();
            intent.putExtra("value", getText);
            intent.putExtra("link", getlink);
            intent.putExtra("type", "additional");
            getActivity().setResult(Activity.RESULT_OK, intent);
            getActivity().finish();
        }

    }
}
