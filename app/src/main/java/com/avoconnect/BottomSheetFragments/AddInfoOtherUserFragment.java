package com.avoconnect.BottomSheetFragments;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.avoconnect.activity.PlanUserSubcription_Act;
import com.google.android.material.bottomsheet.BottomSheetBehavior;
import com.google.android.material.bottomsheet.BottomSheetDialogFragment;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import androidx.appcompat.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.webkit.URLUtil;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.avoconnect.Constants.Constant;
import com.avoconnect.Prefrence.UserPrefrence;
import com.avoconnect.R;
import com.avoconnect.Util.ProgressBarCustom;
import com.avoconnect.Util.Utils;
import com.avoconnect.activity.Otheruserinfoadd_Activity;
import com.avoconnect.api.RestClient;
import com.avoconnect.responsemodel.DeleteRecivedCard_Modle;

import retrofit.RetrofitError;
import retrofit.client.Response;

import static android.app.Activity.RESULT_OK;

public class AddInfoOtherUserFragment extends BottomSheetDialogFragment implements View.OnClickListener {

    boolean isclick = false;
    EditText linktitle_lay, et_link;
    String name = "", email = "", mobile = "", card_id = "",card_username="",lead_type="";
    Dialog dialog_global;
   Callback callback;


    private BottomSheetBehavior.BottomSheetCallback mBottomSheetBehaviorCallback = new BottomSheetBehavior.BottomSheetCallback() {

        @Override
        public void onStateChanged(@NonNull View bottomSheet, int newState) {
//            if (newState == BottomSheetBehavior.STATE_HIDDEN) {
//                dismiss();
//            }

        }

        @Override
        public void onSlide(@NonNull View bottomSheet, float slideOffset) {

        }
    };




    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);


    }

    @SuppressLint("RestrictedApi")
    @Override
    public void setupDialog(final Dialog dialog, int style) {
        super.setupDialog(dialog, style);
        //Get the content View
        dialog_global=dialog;
        View contentView = View.inflate(getContext(), R.layout.item_addinfo_card, null);
        TextView txt_addtag = (TextView) contentView.findViewById(R.id.txt_addtag);
        TextView txt_label = (TextView) contentView.findViewById(R.id.txt_label);
        TextView txt_addnote = (TextView) contentView.findViewById(R.id.txt_addnote);
        TextView txt_location = (TextView) contentView.findViewById(R.id.txt_location);
        TextView txt_addlink = (TextView) contentView.findViewById(R.id.txt_addlink);
        TextView txt_removecard = (TextView) contentView.findViewById(R.id.txt_removecard);
        ImageView cross_img = (ImageView) contentView.findViewById(R.id.cross_img);
        dialog.setContentView(contentView);
        cross_img.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.cancel();
//                Intent intent = new Intent();
//                getActivity().setResult(Activity.RESULT_CANCELED, intent);
//                getActivity().finish();
            }
        });

        txt_addtag.setOnClickListener(this);
        txt_label.setOnClickListener(this);
        txt_addnote.setOnClickListener(this);
        txt_location.setOnClickListener(this);
        txt_addlink.setOnClickListener(this);
        txt_removecard.setOnClickListener(this);

        Bundle bundle = this.getArguments();
        if (bundle != null) {
//            name = bundle.getString("name", "");
//            email = bundle.getString("email", "");
//            mobile = bundle.getString("mobile", "");
            card_id = bundle.getString("card_id", "");
            card_username = bundle.getString("card_username", "");

            if(bundle.getString("lead_type")!=null)
            {
                lead_type=bundle.getString("lead_type");
            }
            Log.e("========data", card_id);
        }


        CoordinatorLayout.LayoutParams params = (CoordinatorLayout.LayoutParams) ((View) contentView.getParent()).getLayoutParams();
        CoordinatorLayout.Behavior behavior = params.getBehavior();

        //Set callback
        if (behavior != null && behavior instanceof BottomSheetBehavior) {
            ((BottomSheetBehavior) behavior).setBottomSheetCallback(mBottomSheetBehaviorCallback);
        }
    }

    private void validation() {
        String getText = linktitle_lay.getText().toString();
        String getlink = et_link.getText().toString();
        if (getText.length() == 0) {
            Toast.makeText(getActivity(), getResources().getString(R.string.enter_link_title), Toast.LENGTH_SHORT).show();
        } else if (getlink.length() == 0) {
            Toast.makeText(getActivity(), getResources().getString(R.string.enter_link), Toast.LENGTH_SHORT).show();
        } else if (URLUtil.isValidUrl(getlink) == false) {
            Toast.makeText(getActivity(), getResources().getString(R.string.enter_valid_url), Toast.LENGTH_SHORT).show();
        } else {
            Intent intent = new Intent();
            intent.putExtra("value", getText);
            intent.putExtra("link", getlink);
            intent.putExtra("type", "important");
            getActivity().setResult(RESULT_OK, intent);
            getActivity().finish();
        }

    }





    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.txt_addtag:
              //  dialog_global.cancel();
                Log.e("========",UserPrefrence.getInstance(getActivity()).getUsertype());
                if(UserPrefrence.getInstance(getActivity()).getUsertype().equalsIgnoreCase(Constant.USERTYPE_PREMIUM)
               ||UserPrefrence.getInstance(getActivity()).getUsertype().equalsIgnoreCase(Constant.USERTYPE_CORPORATE)
                ){
                    Intent intent=new Intent(getActivity(), Otheruserinfoadd_Activity.class);
                    intent.putExtra("add_type",getActivity().getResources().getString(R.string.add_tag));
                    intent.putExtra("card_id",card_id);
                    intent.putExtra("lead_type",lead_type);
                    intent.putExtra("card_username",card_username);
                    startActivityForResult(intent, 144);
                }else{
                    Utils.customYesDialog(this,getContext());
                 }

                break;

            case R.id.txt_upgrade:
                if(Utils.yesNoDialog!=null&&Utils.yesNoDialog.isShowing()==true)
                {
                    Intent intent11=new Intent(getActivity(), PlanUserSubcription_Act.class);
                    startActivityForResult(intent11,155);
                    Utils.yesNoDialog.dismiss();
                }
                break;
            case R.id.txt_label:
              //  dialog_global.cancel();
                if(UserPrefrence.getInstance(getActivity()).getUsertype().equalsIgnoreCase(Constant.USERTYPE_PREMIUM)
                        ||UserPrefrence.getInstance(getActivity()).getUsertype().equalsIgnoreCase(Constant.USERTYPE_CORPORATE)
                ){
                    Intent intent01=new Intent(getActivity(), Otheruserinfoadd_Activity.class);
                    intent01.putExtra("add_type",getActivity().getResources().getString(R.string.add_label));
                    intent01.putExtra("card_id",card_id);
                    intent01.putExtra("card_username",card_username);
                    startActivityForResult(intent01, 144);
                }else{
                    Utils.customYesDialog(this,getContext());
                }


                break;
            case R.id.txt_addnote:
              //  dialog_global.cancel();
                Intent intent02=new Intent(getActivity(), Otheruserinfoadd_Activity.class);
                intent02.putExtra("add_type",getActivity().getResources().getString(R.string.add_note));
                intent02.putExtra("card_id",card_id);
                intent02.putExtra("card_username",card_username);
                startActivityForResult(intent02, 144);
                break;
            case R.id.txt_location:
             //   dialog_global.cancel();
                Intent intent03=new Intent(getActivity(), Otheruserinfoadd_Activity.class);
                intent03.putExtra("add_type",getActivity().getResources().getString(R.string.add_locatiotime));
                intent03.putExtra("card_id",card_id);
                intent03.putExtra("card_username",card_username);
                startActivityForResult(intent03, 144);
                break;
            case R.id.txt_addlink:
              //  dialog_global.cancel();
                Intent intent04=new Intent(getActivity(), Otheruserinfoadd_Activity.class);
                intent04.putExtra("add_type",getActivity().getResources().getString(R.string.add_links));
                intent04.putExtra("card_id",card_id);
                intent04.putExtra("card_username",card_username);
                startActivityForResult(intent04, 144);
                break;
            case R.id.txt_removecard:
                generatepop(card_id);
//                Intent intent05=new Intent(getActivity(), Otheruserinfoadd_Activity.class);
//                intent05.putExtra("add_type",getActivity().getResources().getString(R.string.add_removecard));
//                startActivity(intent05);
                break;

        }
    }

    private void generatepop(final String cardid) {
        AlertDialog.Builder builder=new AlertDialog.Builder(getActivity());
        View view= LayoutInflater.from(getActivity()).inflate(R.layout.logout_lay,null);
        TextView txtDialogMessage=(TextView)view.findViewById(R.id.txtDialogMessage);
        txtDialogMessage.setText(getResources().getString(R.string.delete_Card));
        Button btnDialogYes=(Button)view.findViewById(R.id.btnDialogYes);
        Button btnDialogNo=(Button)view.findViewById(R.id.btnDialogNo);
        builder.setView(view);
        final Dialog dialog=builder.create();
        dialog.setCancelable(false);
        dialog.setCanceledOnTouchOutside(false);

        btnDialogYes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();

                DeleteCardApi(cardid);

            }
        });
        btnDialogNo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });
        dialog.show();

    }
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 144 && resultCode == RESULT_OK) {
            if (callback != null) {
                callback.clickaction(0);
            }
            dialog_global.dismiss();
            //getActivity().finish();
        }
    }

    public  void onCallBackReturn(Callback callback)
    {
        this.callback=callback;
    }

    public interface Callback{
        void clickaction(int position);
    }

    public void DeleteCardApi(final String card_id) {
        final ProgressBarCustom progressBarCustom = new ProgressBarCustom(getActivity());
        progressBarCustom.showProgress();
        try {
            String token = UserPrefrence.getInstance(getActivity()).getAuthtoken();
            token = "bearer " + token;
            RestClient.getInstance(getActivity()).get().deletereceivedcard(token, UserPrefrence.getInstance(getActivity()).getUserid(),card_id, new retrofit.Callback<DeleteRecivedCard_Modle>() {

                @Override
                public void success(final DeleteRecivedCard_Modle res, Response response) {
                    progressBarCustom.cancelProgress();
                    if (response.getStatus() == 200) {
                        if (res.getResponseMessage().equalsIgnoreCase(Constant.SUCCESS)) {
                            Toast.makeText(getContext(), res.getMessage(), Toast.LENGTH_SHORT).show();
                            if (callback != null) {
                                callback.clickaction(0);
                            }
                            dialog_global.dismiss();
                        } else {
                            Toast.makeText(getContext(), res.getMessage(), Toast.LENGTH_SHORT).show();
                        }
                    }
                }

                @Override
                public void failure(final RetrofitError error) {
                    progressBarCustom.cancelProgress();
                    error.printStackTrace();
                    if (error != null) {
                        Utils utils = new Utils(getActivity());
                        utils.showError(error);
                    }
                }
            });

        } catch (Exception e) {
            progressBarCustom.cancelProgress();
            e.printStackTrace();
        }
    }

}