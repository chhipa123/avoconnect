package com.avoconnect.api;

import android.content.Context;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.avoconnect.R;
import com.avoconnect.Util.NetworkDetector;
import com.avoconnect.Util.ProgressBarCustom;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Protocol;

import java.util.Arrays;
import java.util.concurrent.TimeUnit;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.SSLSession;

import retrofit.ErrorHandler;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.OkClient;
import retrofit.client.Response;

public class RestClient2_Ocr extends AppCompatActivity {
    private static RestClient2_Ocr sInstance;

    private static APIServices REST_CLIENT;
    private Context mContext;
    //line server
    private static String ROOT = "https://avoconnect.com/api/py";

    /*dev server*/
    // private static String ROOT= http://qa1.lmsin.com:8787/api/v1/

    private RestClient2_Ocr(Context context) {
        mContext = context.getApplicationContext();
        init();
    }

    public static RestClient2_Ocr getInstance(Context context) {
        if (sInstance == null) {
            sInstance = new RestClient2_Ocr(context);
        }
        return sInstance;
    }


    private void init() {

        final OkHttpClient okHttpClient = new OkHttpClient();
        okHttpClient.setReadTimeout(60, TimeUnit.SECONDS);
        okHttpClient.setConnectTimeout(60, TimeUnit.SECONDS);
        okHttpClient.setProtocols(Arrays.asList(Protocol.HTTP_1_1));

        okHttpClient.setHostnameVerifier(new HostnameVerifier() {
            @Override
            public boolean verify(String hostname, SSLSession session) {

                return true;
            }
        });

        RestAdapter restAdapter = new RestAdapter.Builder()
                .setLogLevel(RestAdapter.LogLevel.FULL)
                .setEndpoint(ROOT)
                .setClient(new OkClient(okHttpClient))
                //.setErrorHandler(new CustomErrorHandler(mContext))
                .build();


        REST_CLIENT = restAdapter.create(APIServices.class);
    }

    public static APIServices get() {
        return REST_CLIENT;
    }

    private  class CustomErrorHandler implements ErrorHandler {
        private final Context ctx;

        public CustomErrorHandler(Context ctx) {
            this.ctx = ctx;
        }

        @Override
        public Throwable handleError(RetrofitError cause) {
            String errorDescription = "sd";
            try {
                Response r = cause.getResponse();
                if(new NetworkDetector(ctx).isConnectingToInternet()){

                    if (r.getStatus()==500) {
                        errorDescription = "Server Error";

                    } else if (r != null && r.getStatus() == 401) {
                        errorDescription = "Unauthorized";
                    } else {
                        errorDescription = "Unable to communicate to server. Please try again.";
                        if (r.getStatus() == 200) {
                            errorDescription = "Rejected";
                        }
                    }
                }else{
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            new ProgressBarCustom(ctx).cancelProgress();
                            Toast.makeText(ctx, ctx.getResources().getString(R.string.internet_connection), Toast.LENGTH_SHORT).show();
                        }
                    });

                }

            }
            catch (Exception e)
            {
                e.printStackTrace();
            }
            return new Exception(errorDescription);
        }
    }

}
