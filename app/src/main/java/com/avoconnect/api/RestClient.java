package com.avoconnect.api;

import android.content.Context;

import android.widget.Toast;


import androidx.appcompat.app.AppCompatActivity;

import com.avoconnect.R;
import com.avoconnect.Util.NetworkDetector;
import com.avoconnect.Util.ProgressBarCustom;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Protocol;

import java.io.InputStream;
import java.security.KeyStore;
import java.security.cert.CertificateFactory;
import java.util.Arrays;
import java.util.concurrent.TimeUnit;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.TrustManagerFactory;

import retrofit.ErrorHandler;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.OkClient;
import retrofit.client.Response;
import java.security.cert.Certificate;
import java.security.cert.CertificateFactory;

@SuppressWarnings("deprecation")
public class RestClient extends AppCompatActivity {

    private static RestClient sInstance;

    private static APIServices REST_CLIENT;
    private Context mContext;


    //    mayank sir
  // private static String ROOT = "http://172.16.0.56:8787/api/v1";

    // abhishek sir
 //   private static String ROOT = "http://172.16.0.140:8576/api/v1";

    /*Live url="*/
private static String ROOT="https://avoconnect.com/api/v1/";

    /*Testing url="*/
// private static String ROOT="http://qa1.lmsin.com:8787/api/v1/";


    private RestClient(Context context) {
        mContext = context.getApplicationContext();
        init();
    }

    public static RestClient getInstance(Context context) {
            if (sInstance == null) {
                sInstance = new RestClient(context);
            }
        return sInstance;
    }



    private void init() {

        try {
//            CertificateFactory cf = CertificateFactory.getInstance("X.509");
//            InputStream cert = mContext.getResources().openRawResource(R.raw.uhpeer);
//            Certificate ca;
//            ca = cf.generateCertificate(cert);
//            // creating a KeyStore containing our trusted CAs
//            String keyStoreType = KeyStore.getDefaultType();
//            KeyStore keyStore = KeyStore.getInstance(keyStoreType);
//
//            keyStore.load(null, null);
//            keyStore.setCertificateEntry("ca", ca);
//
//            // creating a TrustManager that trusts the CAs in our KeyStore
//            String tmfAlgorithm = TrustManagerFactory.getDefaultAlgorithm();
//            TrustManagerFactory tmf = TrustManagerFactory.getInstance(tmfAlgorithm);
//            tmf.init(keyStore);
//
//            SSLContext sslContext = SSLContext.getInstance("TLS");
//            sslContext.init(null, tmf.getTrustManagers(), null);
//
            // creating an OkHttpClient that uses our SSLSocketFactory
            OkHttpClient okHttpClient = new OkHttpClient();
            okHttpClient.setProtocols(Arrays.asList(Protocol.HTTP_1_1));
            okHttpClient.setReadTimeout(60, TimeUnit.SECONDS);
            okHttpClient.setConnectTimeout(60, TimeUnit.SECONDS);

            okHttpClient.setHostnameVerifier(new HostnameVerifier() {
                @Override
                public boolean verify(String hostname, SSLSession session) {

                    return true;
                }
            });

            RestAdapter restAdapter = new RestAdapter.Builder()
                    .setLogLevel(RestAdapter.LogLevel.FULL)
                    .setEndpoint(ROOT)
                    .setClient(new OkClient(okHttpClient))
                  //  .setClient(new OkClient(okHttpClient))
                    // .setErrorHandler(new CustomErrorHandler(mContext))
                    .build();

            REST_CLIENT = restAdapter.create(APIServices.class);

//        final OkHttpClient okHttpClient = new OkHttpClient();
//        okHttpClient.setReadTimeout(60, TimeUnit.SECONDS);
//        okHttpClient.setConnectTimeout(60, TimeUnit.SECONDS);
//
//        RestAdapter restAdapter = new RestAdapter.Builder()
//                .setLogLevel(RestAdapter.LogLevel.FULL)
//                .setEndpoint(ROOT)
//                .setClient(new OkClient(okHttpClient))
//                //.setErrorHandler(new CustomErrorHandler(mContext))
//                .build();
//
//
//        REST_CLIENT = restAdapter.create(APIServices.class);
        }catch (Exception e)
        {
            e.printStackTrace();
        }
    }

    public static APIServices get() {
        return REST_CLIENT;
    }

    private  class CustomErrorHandler implements ErrorHandler {
        private final Context ctx;

        public CustomErrorHandler(Context ctx) {
            this.ctx = ctx;
        }

        @Override
        public Throwable handleError(RetrofitError cause) {
            String errorDescription = "sd";
            try {
                Response r = cause.getResponse();
                if(new NetworkDetector(ctx).isConnectingToInternet()){

                    if (r.getStatus()==500) {
                        errorDescription = "Server Error";

                    } else if (r != null && r.getStatus() == 401) {
                        errorDescription = "Unauthorized";
                    } else {
                        errorDescription = "Unable to communicate to server. Please try again.";
                        if (r.getStatus() == 200) {
                            errorDescription = "Rejected";
                        }
                    }
                }else{
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            new ProgressBarCustom(ctx).cancelProgress();
                            Toast.makeText(ctx, ctx.getResources().getString(R.string.internet_connection), Toast.LENGTH_SHORT).show();
                        }
                    });

                }

            }
            catch (Exception e)
            {
                e.printStackTrace();
            }
            return new Exception(errorDescription);
        }
    }


}
