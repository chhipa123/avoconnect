package com.avoconnect.responsemodel;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class NearByUser_Model {

    @SerializedName("statusCode")
    @Expose
    private Integer statusCode;
    @SerializedName("responseMessage")
    @Expose
    private String responseMessage;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("resourceData")
    @Expose
    private List<ResourceDatum> resourceData = null;

    public Integer getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(Integer statusCode) {
        this.statusCode = statusCode;
    }

    public String getResponseMessage() {
        return responseMessage;
    }

    public void setResponseMessage(String responseMessage) {
        this.responseMessage = responseMessage;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<ResourceDatum> getResourceData() {
        return resourceData;
    }

    public void setResourceData(List<ResourceDatum> resourceData) {
        this.resourceData = resourceData;
    }


    public class ResourceDatum {

        @SerializedName("userId")
        @Expose
        private Integer userId;
        @SerializedName("displayName")
        @Expose
        private String displayName;
        @SerializedName("distance")
        @Expose
        private Double distance;
        @SerializedName("profileImage")
        @Expose
        private String profileImage;

        @SerializedName("avoId")
        @Expose
        private String avoId;

        public String getAvoId() {
            return avoId;
        }

        public void setAvoId(String avoId) {
            this.avoId = avoId;
        }

        public Integer getUserId() {
            return userId;
        }

        public void setUserId(Integer userId) {
            this.userId = userId;
        }

        public String getDisplayName() {
            return displayName;
        }

        public void setDisplayName(String displayName) {
            this.displayName = displayName;
        }

        public Double getDistance() {
            return distance;
        }

        public void setDistance(Double distance) {
            this.distance = distance;
        }

        public String getProfileImage() {
            return profileImage;
        }

        public void setProfileImage(String profileImage) {
            this.profileImage = profileImage;
        }

    }

}
