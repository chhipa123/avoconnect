package com.avoconnect.responsemodel;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class ListNotification_Model {

    @SerializedName("statusCode")
    @Expose
    private Integer statusCode;
    @SerializedName("responseMessage")
    @Expose
    private String responseMessage;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("resourceData")
    @Expose
    private List<ResourceDatum> resourceData = null;

    public Integer getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(Integer statusCode) {
        this.statusCode = statusCode;
    }

    public String getResponseMessage() {
        return responseMessage;
    }

    public void setResponseMessage(String responseMessage) {
        this.responseMessage = responseMessage;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<ResourceDatum> getResourceData() {
        return resourceData;
    }

    public void setResourceData(List<ResourceDatum> resourceData) {
        this.resourceData = resourceData;
    }

    public class ResourceDatum {

        @SerializedName("id")
        @Expose
        private Integer id;
        @SerializedName("title")
        @Expose
        private String title;
        @SerializedName("notification")
        @Expose
        private String notification;
        @SerializedName("senderId")
        @Expose
        private Integer senderId;
        @SerializedName("time")
        @Expose
        private String time;
        @SerializedName("senderName")
        @Expose
        private String senderName;

        @SerializedName("action")
        @Expose
        private String action;



        @SerializedName("isRead")
        @Expose
        private Integer isRead;

        @SerializedName("cardId")
        @Expose
        private Integer cardId;

        @SerializedName("requestSent")
        @Expose
        private Boolean requestSent;
        @SerializedName("accept")
        @Expose
        private Boolean accept;

        @SerializedName("logo")
        @Expose
        private String logo;

        @SerializedName("requestViewProfileId")
        @Expose
        private Integer requestViewProfileId;

        public Integer getRequestViewProfileId() {
            return requestViewProfileId;
        }

        public void setRequestViewProfileId(Integer requestViewProfileId) {
            this.requestViewProfileId = requestViewProfileId;
        }


        public String getAction() {
            return action;
        }

        public void setAction(String action) {
            this.action = action;
        }

        public String getLogo() {
            return logo;
        }

        public void setLogo(String logo) {
            this.logo = logo;
        }

        public Boolean getRequestSent() {
            return requestSent;
        }

        public void setRequestSent(Boolean requestSent) {
            this.requestSent = requestSent;
        }

        public Boolean getAccept() {
            return accept;
        }

        public void setAccept(Boolean accept) {
            this.accept = accept;
        }

        public Integer getIsRead() {
            return isRead;
        }

        public void setIsRead(Integer id) {
            this.isRead = id;
        }


        public Integer getId() {
            return id;
        }

        public void setId(Integer id) {
            this.id = id;
        }

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public String getNotification() {
            return notification;
        }

        public void setNotification(String notification) {
            this.notification = notification;
        }

        public Integer getSenderId() {
            return senderId;
        }

        public void setSenderId(Integer senderId) {
            this.senderId = senderId;
        }

        public String getTime() {
            return time;
        }

        public void setTime(String time) {
            this.time = time;
        }

        public String getSenderName() {
            return senderName;
        }

        public void setSenderName(String senderName) {
            this.senderName = senderName;
        }

        public Integer getCardId() {
            return cardId;
        }

        public void setCardId(Integer cardId) {
            this.cardId = cardId;
        }

    }
}
