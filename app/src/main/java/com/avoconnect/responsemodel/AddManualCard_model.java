package com.avoconnect.responsemodel;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class AddManualCard_model {

    @SerializedName("statusCode")
    @Expose
    private Integer statusCode;
    @SerializedName("responseMessage")
    @Expose
    private String responseMessage;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("resourceData")
    @Expose
    private ResourceData resourceData;

    public Integer getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(Integer statusCode) {
        this.statusCode = statusCode;
    }

    public String getResponseMessage() {
        return responseMessage;
    }

    public void setResponseMessage(String responseMessage) {
        this.responseMessage = responseMessage;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public ResourceData getResourceData() {
        return resourceData;
    }

    public void setResourceData(ResourceData resourceData) {
        this.resourceData = resourceData;
    }

    public class ResourceData {

        @SerializedName("cardId")
        @Expose
        private Integer cardId;
        @SerializedName("isPrimary")
        @Expose
        private Integer isPrimary;
        @SerializedName("autoShare")
        @Expose
        private Integer autoShare;
        @SerializedName("linkAvailable")
        @Expose
        private Boolean linkAvailable;
        @SerializedName("additionalInfoAvailable")
        @Expose
        private Boolean additionalInfoAvailable;

        public Integer getCardId() {
            return cardId;
        }

        public void setCardId(Integer cardId) {
            this.cardId = cardId;
        }

        public Integer getIsPrimary() {
            return isPrimary;
        }

        public void setIsPrimary(Integer isPrimary) {
            this.isPrimary = isPrimary;
        }

        public Integer getAutoShare() {
            return autoShare;
        }

        public void setAutoShare(Integer autoShare) {
            this.autoShare = autoShare;
        }

        public Boolean getLinkAvailable() {
            return linkAvailable;
        }

        public void setLinkAvailable(Boolean linkAvailable) {
            this.linkAvailable = linkAvailable;
        }

        public Boolean getAdditionalInfoAvailable() {
            return additionalInfoAvailable;
        }

        public void setAdditionalInfoAvailable(Boolean additionalInfoAvailable) {
            this.additionalInfoAvailable = additionalInfoAvailable;
        }
    }
}
