package com.avoconnect.responsemodel;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class GetCompany_Model {

    @SerializedName("statusCode")
    @Expose
    private Integer statusCode;
    @SerializedName("responseMessage")
    @Expose
    private String responseMessage;
    @SerializedName("list")
    @Expose
    private List<CompanyName> list = null;

    public Integer getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(Integer statusCode) {
        this.statusCode = statusCode;
    }

    public String getResponseMessage() {
        return responseMessage;
    }

    public void setResponseMessage(String responseMessage) {
        this.responseMessage = responseMessage;
    }

    public List<CompanyName> getList() {
        return list;
    }

    public void setList(List<CompanyName> list) {
        this.list = list;
    }

    public class CompanyName {

        @SerializedName("id")
        @Expose
        private Integer id;
        @SerializedName("name")
        @Expose
        private String name;
        @SerializedName("website")
        @Expose
        private String website;
        @SerializedName("contactNumber")
        @Expose
        private String contactNumber;
        @SerializedName("address")
        @Expose
        private String address;
        @SerializedName("noOfUsers")
        @Expose
        private Long noOfUsers;
        @SerializedName("planType")
        @Expose
        private String planType;
        @SerializedName("createdBy")
        @Expose
        private String createdBy;
        @SerializedName("createdOn")
        @Expose
        private String createdOn;
        @SerializedName("lastUpdatedOn")
        @Expose
        private String lastUpdatedOn;
        @SerializedName("available")
        @Expose
        private Boolean available;
        @SerializedName("displayName")
        @Expose
        private String displayName;

        public Integer getId() {
            return id;
        }

        public void setId(Integer id) {
            this.id = id;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getWebsite() {
            return website;
        }

        public void setWebsite(String website) {
            this.website = website;
        }

        public String getContactNumber() {
            return contactNumber;
        }

        public void setContactNumber(String contactNumber) {
            this.contactNumber = contactNumber;
        }

        public String getAddress() {
            return address;
        }

        public void setAddress(String address) {
            this.address = address;
        }

        public Long getNoOfUsers() {
            return noOfUsers;
        }

        public void setNoOfUsers(Long noOfUsers) {
            this.noOfUsers = noOfUsers;
        }

        public String getPlanType() {
            return planType;
        }

        public void setPlanType(String planType) {
            this.planType = planType;
        }

        public String getCreatedBy() {
            return createdBy;
        }

        public void setCreatedBy(String createdBy) {
            this.createdBy = createdBy;
        }

        public String getCreatedOn() {
            return createdOn;
        }

        public void setCreatedOn(String createdOn) {
            this.createdOn = createdOn;
        }

        public String getLastUpdatedOn() {
            return lastUpdatedOn;
        }

        public void setLastUpdatedOn(String lastUpdatedOn) {
            this.lastUpdatedOn = lastUpdatedOn;
        }

        public Boolean getAvailable() {
            return available;
        }

        public void setAvailable(Boolean available) {
            this.available = available;
        }

        public String getDisplayName() {
            return displayName;
        }

        public void setDisplayName(String displayName) {
            this.displayName = displayName;
        }

    }
}
