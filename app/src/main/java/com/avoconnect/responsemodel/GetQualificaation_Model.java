package com.avoconnect.responsemodel;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class GetQualificaation_Model {

    @SerializedName("noOfRecords")
    @Expose
    private Integer noOfRecords;
    @SerializedName("statusCode")
    @Expose
    private Integer statusCode;
    @SerializedName("responseMessage")
    @Expose
    private String responseMessage;

    @SerializedName("list")
    @Expose
    private List<Qualification> qualification = null;

    public Integer getNoOfRecords() {
        return noOfRecords;
    }

    public void setNoOfRecords(Integer noOfRecords) {
        this.noOfRecords = noOfRecords;
    }

    public Integer getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(Integer statusCode) {
        this.statusCode = statusCode;
    }

    public String getResponseMessage() {
        return responseMessage;
    }

    public void setResponseMessage(String responseMessage) {
        this.responseMessage = responseMessage;
    }

    public List<Qualification> getList() {
        return qualification;
    }

    public void setList(List<Qualification> list) {
        this.qualification = list;
    }




    public class Qualification {

        @SerializedName("qualificationId")
        @Expose
        private Integer qualificationId;
        @SerializedName("name")
        @Expose
        private String name;

        public Integer getQualificationId() {
            return qualificationId;
        }

        public void setQualificationId(Integer qualificationId) {
            this.qualificationId = qualificationId;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

    }

}
