package com.avoconnect.responsemodel;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class GetUserProfile_Model {

    @SerializedName("statusCode")
    @Expose
    private Integer statusCode;
    @SerializedName("responseMessage")
    @Expose
    private String responseMessage;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("userId")
    @Expose
    private Integer userId;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("firstName")
    @Expose
    private String firstName;
    @SerializedName("lastName")
    @Expose
    private String lastName;
    @SerializedName("email")
    @Expose
    private String email;
    @SerializedName("mobile")
    @Expose
    private String mobile;

    @SerializedName("designation")
    @Expose
    private String designation;

    public String getDesignation() {
        return designation;
    }

    public void setDesignation(String designation) {
        this.designation = designation;
    }




    @SerializedName("qualification")
    @Expose
    private String qualification;
    @SerializedName("qualificationId")
    @Expose
    private Integer qualificationId;


    @SerializedName("userProfileImage")
    @Expose
    private String userProfileImage;
    @SerializedName("userProfileBanner")
    @Expose
    private String userProfileBanner;

    @SerializedName("alternateMobile")
    @Expose
    private String alternateMobile;

    public String getAlternateMobile() {
        return alternateMobile;
    }

    @SerializedName("avoId")
    @Expose
    private String avoId;

    public String getAvoId() {
        return avoId;
    }

    public void setAvoId(String avoId) {
        this.avoId = avoId;
    }


    public void setAlternateMobile(String alternateMobile) {
        this.alternateMobile = alternateMobile;
    }

    public String getUserProfileImage() {
        return userProfileImage;
    }

    public void setUserProfileImage(String userProfileImage) {
        this.userProfileImage = userProfileImage;
    }

    public String getUserProfileBanner() {
        return userProfileBanner;
    }

    public void setUserProfileBanner(String userProfileBanner) {
        this.userProfileBanner = userProfileBanner;
    }

    public String getQualification() {
        return qualification;
    }

    public void setQualification(String qualification) {
        this.qualification = qualification;
    }

    public Integer getQualificationId() {
        return qualificationId;
    }

    public void setQualificationId(Integer qualificationId) {
        this.qualificationId = qualificationId;
    }

    @SerializedName("userExperienceResponse")
    @Expose
    private List<UserExperienceResponse> userExperienceResponse = null;
    @SerializedName("userSkillResponse")
    @Expose
    private List<UserSkills> userSkillResponse = null;

    public Integer getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(Integer statusCode) {
        this.statusCode = statusCode;
    }

    public String getResponseMessage() {
        return responseMessage;
    }

    public void setResponseMessage(String responseMessage) {
        this.responseMessage = responseMessage;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public List<UserExperienceResponse> getUserExperienceResponse() {
        return userExperienceResponse;
    }

    public void setUserExperienceResponse(List<UserExperienceResponse> userExperienceResponse) {
        this.userExperienceResponse = userExperienceResponse;
    }

    public List<UserSkills> getUserSkillResponse() {
        return userSkillResponse;
    }

    public void setUserSkillResponse(List<UserSkills> userSkillResponse) {
        this.userSkillResponse = userSkillResponse;
    }


    public class UserSkills {

        @SerializedName("skillId")
        @Expose
        private Integer skillId;
        @SerializedName("skill")
        @Expose
        private String skill;

        public Integer getSkillId() {
            return skillId;
        }

        public void setSkillId(Integer skillId) {
            this.skillId = skillId;
        }

        public String getSkill() {
            return skill;
        }

        public void setSkill(String skill) {
            this.skill = skill;
        }

    }

    public class UserExperienceResponse {

        @SerializedName("userExperienceId")
        @Expose
        private Integer userExperienceId;
        @SerializedName("designation")
        @Expose
        private String designation;
        @SerializedName("company")
        @Expose
        private String company;
        @SerializedName("startDate")
        @Expose
        private String startDate;
        @SerializedName("endDate")
        @Expose
        private String endDate;
        @SerializedName("workingTillDate")
        @Expose
        private Boolean workingTillDate;

        public Integer getUserExperienceId() {
            return userExperienceId;
        }

        public void setUserExperienceId(Integer userExperienceId) {
            this.userExperienceId = userExperienceId;
        }

        public String getDesignation() {
            return designation;
        }

        public void setDesignation(String designation) {
            this.designation = designation;
        }

        public String getCompany() {
            return company;
        }

        public void setCompany(String company) {
            this.company = company;
        }

        public String getStartDate() {
            return startDate;
        }

        public void setStartDate(String startDate) {
            this.startDate = startDate;
        }

        public String getEndDate() {
            return endDate;
        }

        public void setEndDate(String endDate) {
            this.endDate = endDate;
        }

        public Boolean getWorkingTillDate() {
            return workingTillDate;
        }

        public void setWorkingTillDate(Boolean workingTillDate) {
            this.workingTillDate = workingTillDate;
        }

    }
}
