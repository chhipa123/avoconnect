package com.avoconnect.responsemodel;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class AditonalInfo_Modele {

    @SerializedName("statusCode")
    @Expose
    private Integer statusCode;
    @SerializedName("responseMessage")
    @Expose
    private String responseMessage;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("resourceData")
    @Expose
    private ResourceData resourceData;

    public Integer getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(Integer statusCode) {
        this.statusCode = statusCode;
    }

    public String getResponseMessage() {
        return responseMessage;
    }

    public void setResponseMessage(String responseMessage) {
        this.responseMessage = responseMessage;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public ResourceData getResourceData() {
        return resourceData;
    }

    public void setResourceData(ResourceData resourceData) {
        this.resourceData = resourceData;
    }
    public class ResourceData {

        @SerializedName("id")
        @Expose
        private Integer id;
        @SerializedName("userId")
        @Expose
        private Integer userId;
        @SerializedName("cardId")
        @Expose
        private Integer cardId;
        @SerializedName("notes")
        @Expose
        private String notes;
        @SerializedName("label")
        @Expose
        private String label;
        @SerializedName("lead")
        @Expose
        private Integer lead;
        @SerializedName("location")
        @Expose
        private String location;
        @SerializedName("time")
        @Expose
        private String time;
        @SerializedName("updatedOn")
        @Expose
        private String updatedOn;

        @SerializedName("cardLinkResponse")
        @Expose
        private List<CardLinkResponse> cardLinkResponse = null;

        @SerializedName("linkAvailable")
        @Expose
        private Boolean linkAvailable;

        public Boolean getLinkAvailable() {
            return linkAvailable;
        }

        public void setLinkAvailable(Boolean linkAvailable) {
            this.linkAvailable = linkAvailable;
        }


        public Integer getId() {
            return id;
        }

        public void setId(Integer id) {
            this.id = id;
        }

        public Integer getUserId() {
            return userId;
        }

        public void setUserId(Integer userId) {
            this.userId = userId;
        }

        public Integer getCardId() {
            return cardId;
        }

        public void setCardId(Integer cardId) {
            this.cardId = cardId;
        }

        public String getNotes() {
            return notes;
        }

        public void setNotes(String notes) {
            this.notes = notes;
        }

        public String getLabel() {
            return label;
        }

        public void setLabel(String label) {
            this.label = label;
        }

        public Integer getLead() {
            return lead;
        }

        public void setLead(Integer lead) {
            this.lead = lead;
        }

        public String getLocation() {
            return location;
        }

        public void setLocation(String location) {
            this.location = location;
        }

        public String getTime() {
            return time;
        }

        public void setTime(String time) {
            this.time = time;
        }

        public String getUpdatedOn() {
            return updatedOn;
        }

        public List<CardLinkResponse> getCardLinkResponse() {
            return cardLinkResponse;
        }

        public void setCardLinkResponse(List<CardLinkResponse> cardLinkResponse) {
            this.cardLinkResponse = cardLinkResponse;
        }

        public void setUpdatedOn(String updatedOn) {
            this.updatedOn = updatedOn;
        }
    }

    public class CardLinkResponse {

        @SerializedName("title")
        @Expose
        private String title;
        @SerializedName("url")
        @Expose
        private String url;

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public String getUrl() {
            return url;
        }

        public void setUrl(String url) {
            this.url = url;
        }

    }

}
