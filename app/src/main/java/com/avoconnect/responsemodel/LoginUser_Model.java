package com.avoconnect.responsemodel;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class LoginUser_Model {

    @SerializedName("error")
    @Expose
    private String error;
    @SerializedName("error_description")
    @Expose
    private String errorDescription;

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }

    public String getErrorDescription() {
        return errorDescription;
    }

    public void setErrorDescription(String errorDescription) {
        this.errorDescription = errorDescription;
    }


    @SerializedName("access_token")
    @Expose
    private String accessToken;

    @SerializedName("isEmailExists")
    @Expose
    private Boolean isEmailExists;

    @SerializedName("token_type")
    @Expose
    private String tokenType;
    @SerializedName("refresh_token")
    @Expose
    private String refreshToken;
    @SerializedName("expires_in")
    @Expose
    private Integer expiresIn;
    @SerializedName("scope")
    @Expose
    private String scope;
    @SerializedName("firstName")
    @Expose
    private String firstName;
    @SerializedName("lastName")
    @Expose
    private String lastName;
    @SerializedName("role_id")
    @Expose
    private Integer roleId;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("responseMessage")
    @Expose
    private String responseMessage;
    @SerializedName("userId")
    @Expose
    private Integer userid;
    @SerializedName("username")
    @Expose
    private String username;
    @SerializedName("statusCode")
    @Expose
    private Integer statusCode;
    @SerializedName("jti")
    @Expose
    private String jti;

    @SerializedName("avoId")
    @Expose
    private String avoId;

    public String getAvoId() {
        return avoId;
    }

    public void setAvoId(String avoId) {
        this.avoId = avoId;
    }



    @SerializedName("userType")
    @Expose
    private String userType;

    @SerializedName("askEmailOnViewCard")
    @Expose
    private Boolean askEmailOnViewCard;
    @SerializedName("isTwoWayAuthEnabled")
    @Expose
    private Boolean isTwoWayAuthEnabled;
    @SerializedName("isNotificationActive")
    @Expose
    private Boolean isNotificationActive;


    @SerializedName("mobile")
    @Expose
    private String mobile;


    @SerializedName("permission")
    @Expose
    private String permission;

    public String getPermission() {
        return permission;
    }

    public void setPermission(String permission) {
        this.permission = permission;
    }

    public Boolean getIsEmailExists() {
        return isEmailExists;
    }

    public void setIsEmailExists(Boolean isEmailExists) {
        this.isEmailExists = isEmailExists;
    }


    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public Boolean getAskEmailOnViewCard() {
        return askEmailOnViewCard;
    }

    public void setAskEmailOnViewCard(Boolean askEmailOnViewCard) {
        this.askEmailOnViewCard = askEmailOnViewCard;
    }

    public Boolean getIsTwoWayAuthEnabled() {
        return isTwoWayAuthEnabled;
    }

    public void setIsTwoWayAuthEnabled(Boolean isTwoWayAuthEnabled) {
        this.isTwoWayAuthEnabled = isTwoWayAuthEnabled;
    }

    public Boolean getIsNotificationActive() {
        return isNotificationActive;
    }

    public void setIsNotificationActive(Boolean isNotificationActive) {
        this.isNotificationActive = isNotificationActive;
    }


    public String getUserType() {
        return userType;
    }

    public void setUserType(String userType) {
        this.userType = userType;
    }

    public String getAccessToken() {
        return accessToken;
    }

    public void setAccessToken(String accessToken) {
        this.accessToken = accessToken;
    }

    public String getTokenType() {
        return tokenType;
    }

    public void setTokenType(String tokenType) {
        this.tokenType = tokenType;
    }

    public String getRefreshToken() {
        return refreshToken;
    }

    public void setRefreshToken(String refreshToken) {
        this.refreshToken = refreshToken;
    }

    public Integer getExpiresIn() {
        return expiresIn;
    }

    public void setExpiresIn(Integer expiresIn) {
        this.expiresIn = expiresIn;
    }

    public String getScope() {
        return scope;
    }

    public void setScope(String scope) {
        this.scope = scope;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public Integer getRoleId() {
        return roleId;
    }

    public void setRoleId(Integer roleId) {
        this.roleId = roleId;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getResponseMessage() {
        return responseMessage;
    }

    public void setResponseMessage(String responseMessage) {
        this.responseMessage = responseMessage;
    }

    public Integer getUserid() {
        return userid;
    }

    public void setUserid(Integer userid) {
        this.userid = userid;
    }

    public String getEmail() {
        return username;
    }

    public void setEmail(String email) {
        this.username = email;
    }

    public Integer getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(Integer statusCode) {
        this.statusCode = statusCode;
    }

    public String getJti() {
        return jti;
    }

    public void setJti(String jti) {
        this.jti = jti;
    }


}
