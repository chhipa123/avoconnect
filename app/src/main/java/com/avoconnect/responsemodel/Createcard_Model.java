package com.avoconnect.responsemodel;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Createcard_Model {
    @SerializedName("statusCode")
    @Expose
    private Integer statusCode;
    @SerializedName("responseMessage")
    @Expose
    private String responseMessage;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("resourceData")
    @Expose
    private ResourceData resourceData;

    public Integer getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(Integer statusCode) {
        this.statusCode = statusCode;
    }

    public String getResponseMessage() {
        return responseMessage;
    }

    public void setResponseMessage(String responseMessage) {
        this.responseMessage = responseMessage;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public ResourceData getResourceData() {
        return resourceData;
    }

    public void setResourceData(ResourceData resourceData) {
        this.resourceData = resourceData;
    }

    public class ResourceData {

        @SerializedName("id")
        @Expose
        private Integer id;
        @SerializedName("fullName")
        @Expose
        private String fullName;
        @SerializedName("designation")
        @Expose
        private String designation;
        @SerializedName("mobile")
        @Expose
        private String mobile;
        @SerializedName("email")
        @Expose
        private String email;
        @SerializedName("companyName")
        @Expose
        private Object companyName;
        @SerializedName("website")
        @Expose
        private String website;
        @SerializedName("summary")
        @Expose
        private String summary;
        @SerializedName("isPrimary")
        @Expose
        private Integer isPrimary;
        @SerializedName("autoShare")
        @Expose
        private Integer autoShare;
        @SerializedName("createdOn")
        @Expose
        private Object createdOn;
        @SerializedName("lastUpdatedOn")
        @Expose
        private Object lastUpdatedOn;
        @SerializedName("createdBy")
        @Expose
        private Object createdBy;
        @SerializedName("userId")
        @Expose
        private Integer userId;
        @SerializedName("listOfImpLinks")
        @Expose
        private List<ListOfImpLink> listOfImpLinks = null;
        @SerializedName("listOfSocialLinks")
        @Expose
        private List<ListOfSocialLink> listOfSocialLinks = null;
        @SerializedName("listOfImageLinks")
        @Expose
        private List<Object> listOfImageLinks = null;
        @SerializedName("isDeleted")
        @Expose
        private Integer isDeleted;
        @SerializedName("backgroundColor")
        @Expose
        private String backgroundColor;

        public Integer getId() {
            return id;
        }

        public void setId(Integer id) {
            this.id = id;
        }

        public String getFullName() {
            return fullName;
        }

        public void setFullName(String fullName) {
            this.fullName = fullName;
        }

        public String getDesignation() {
            return designation;
        }

        public void setDesignation(String designation) {
            this.designation = designation;
        }

        public String getMobile() {
            return mobile;
        }

        public void setMobile(String mobile) {
            this.mobile = mobile;
        }

        public String getEmail() {
            return email;
        }

        public void setEmail(String email) {
            this.email = email;
        }

        public Object getCompanyName() {
            return companyName;
        }

        public void setCompanyName(Object companyName) {
            this.companyName = companyName;
        }

        public String getWebsite() {
            return website;
        }

        public void setWebsite(String website) {
            this.website = website;
        }

        public String getSummary() {
            return summary;
        }

        public void setSummary(String summary) {
            this.summary = summary;
        }

        public Integer getIsPrimary() {
            return isPrimary;
        }

        public void setIsPrimary(Integer isPrimary) {
            this.isPrimary = isPrimary;
        }

        public Integer getAutoShare() {
            return autoShare;
        }

        public void setAutoShare(Integer autoShare) {
            this.autoShare = autoShare;
        }

        public Object getCreatedOn() {
            return createdOn;
        }

        public void setCreatedOn(Object createdOn) {
            this.createdOn = createdOn;
        }

        public Object getLastUpdatedOn() {
            return lastUpdatedOn;
        }

        public void setLastUpdatedOn(Object lastUpdatedOn) {
            this.lastUpdatedOn = lastUpdatedOn;
        }

        public Object getCreatedBy() {
            return createdBy;
        }

        public void setCreatedBy(Object createdBy) {
            this.createdBy = createdBy;
        }

        public Integer getUserId() {
            return userId;
        }

        public void setUserId(Integer userId) {
            this.userId = userId;
        }

        public List<ListOfImpLink> getListOfImpLinks() {
            return listOfImpLinks;
        }

        public void setListOfImpLinks(List<ListOfImpLink> listOfImpLinks) {
            this.listOfImpLinks = listOfImpLinks;
        }

        public List<ListOfSocialLink> getListOfSocialLinks() {
            return listOfSocialLinks;
        }

        public void setListOfSocialLinks(List<ListOfSocialLink> listOfSocialLinks) {
            this.listOfSocialLinks = listOfSocialLinks;
        }

        public List<Object> getListOfImageLinks() {
            return listOfImageLinks;
        }

        public void setListOfImageLinks(List<Object> listOfImageLinks) {
            this.listOfImageLinks = listOfImageLinks;
        }

        public Integer getIsDeleted() {
            return isDeleted;
        }

        public void setIsDeleted(Integer isDeleted) {
            this.isDeleted = isDeleted;
        }

        public String getBackgroundColor() {
            return backgroundColor;
        }

        public void setBackgroundColor(String backgroundColor) {
            this.backgroundColor = backgroundColor;
        }
    }

    public class ListOfSocialLink {

        @SerializedName("id")
        @Expose
        private Integer id;
        @SerializedName("title")
        @Expose
        private String title;
        @SerializedName("resourceType")
        @Expose
        private String resourceType;
        @SerializedName("fileType")
        @Expose
        private String fileType;
        @SerializedName("url")
        @Expose
        private String url;
        @SerializedName("state")
        @Expose
        private Object state;
        @SerializedName("cardId")
        @Expose
        private Integer cardId;

        public Integer getId() {
            return id;
        }

        public void setId(Integer id) {
            this.id = id;
        }

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public String getResourceType() {
            return resourceType;
        }

        public void setResourceType(String resourceType) {
            this.resourceType = resourceType;
        }

        public String getFileType() {
            return fileType;
        }

        public void setFileType(String fileType) {
            this.fileType = fileType;
        }

        public String getUrl() {
            return url;
        }

        public void setUrl(String url) {
            this.url = url;
        }

        public Object getState() {
            return state;
        }

        public void setState(Object state) {
            this.state = state;
        }

        public Integer getCardId() {
            return cardId;
        }

        public void setCardId(Integer cardId) {
            this.cardId = cardId;
        }

    }

    public class ListOfImpLink {

        @SerializedName("id")
        @Expose
        private Integer id;
        @SerializedName("title")
        @Expose
        private String title;
        @SerializedName("resourceType")
        @Expose
        private String resourceType;
        @SerializedName("fileType")
        @Expose
        private String fileType;
        @SerializedName("url")
        @Expose
        private String url;
        @SerializedName("state")
        @Expose
        private Object state;
        @SerializedName("cardId")
        @Expose
        private Integer cardId;

        public Integer getId() {
            return id;
        }

        public void setId(Integer id) {
            this.id = id;
        }

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public String getResourceType() {
            return resourceType;
        }

        public void setResourceType(String resourceType) {
            this.resourceType = resourceType;
        }

        public String getFileType() {
            return fileType;
        }

        public void setFileType(String fileType) {
            this.fileType = fileType;
        }

        public String getUrl() {
            return url;
        }

        public void setUrl(String url) {
            this.url = url;
        }

        public Object getState() {
            return state;
        }

        public void setState(Object state) {
            this.state = state;
        }

        public Integer getCardId() {
            return cardId;
        }

        public void setCardId(Integer cardId) {
            this.cardId = cardId;
        }

    }
}
