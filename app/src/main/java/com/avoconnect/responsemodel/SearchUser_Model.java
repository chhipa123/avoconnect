package com.avoconnect.responsemodel;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class SearchUser_Model {

    @SerializedName("pageNumber")
    @Expose
    private Integer pageNumber;
    @SerializedName("pageSize")
    @Expose
    private Integer pageSize;
    @SerializedName("noOfRecords")
    @Expose
    private Integer noOfRecords;
    @SerializedName("totalRecords")
    @Expose
    private Integer totalRecords;
    @SerializedName("statusCode")
    @Expose
    private Integer statusCode;
    @SerializedName("responseMessage")
    @Expose
    private String responseMessage;
    @SerializedName("list")
    @Expose
    private java.util.List<UserResponce> list = null;

    public Integer getPageNumber() {
        return pageNumber;
    }

    public void setPageNumber(Integer pageNumber) {
        this.pageNumber = pageNumber;
    }

    public Integer getPageSize() {
        return pageSize;
    }

    public void setPageSize(Integer pageSize) {
        this.pageSize = pageSize;
    }

    public Integer getNoOfRecords() {
        return noOfRecords;
    }

    public void setNoOfRecords(Integer noOfRecords) {
        this.noOfRecords = noOfRecords;
    }

    public Integer getTotalRecords() {
        return totalRecords;
    }

    public void setTotalRecords(Integer totalRecords) {
        this.totalRecords = totalRecords;
    }

    public Integer getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(Integer statusCode) {
        this.statusCode = statusCode;
    }

    public String getResponseMessage() {
        return responseMessage;
    }

    public void setResponseMessage(String responseMessage) {
        this.responseMessage = responseMessage;
    }

    public List<UserResponce> getList() {
        return list;
    }

    public void setList(List<UserResponce> list) {
        this.list = list;
    }

    public class UserResponce {

        @SerializedName("userId")
        @Expose
        private Integer userId;
        @SerializedName("id")
        @Expose
        private Integer id;
        @SerializedName("firstName")
        @Expose
        private String firstName;
        @SerializedName("lastName")
        @Expose
        private String lastName;
        @SerializedName("email")
        @Expose
        private String email;
        @SerializedName("gender")
        @Expose
        private String gender;
        @SerializedName("mobile")
        @Expose
        private String mobile;
        @SerializedName("displayName")
        @Expose
        private String displayName;
        @SerializedName("dateOfBirth")
        @Expose
        private String dateOfBirth;
        @SerializedName("designation")
        @Expose
        private String designation;
        @SerializedName("companyName")
        @Expose
        private String companyName;
        @SerializedName("employeeId")
        @Expose
        private String employeeId;
        @SerializedName("userType")
        @Expose
        private String userType;
        @SerializedName("language")
        @Expose
        private String language;
        @SerializedName("createdBy")
        @Expose
        private String createdBy;
        @SerializedName("roleId")
        @Expose
        private Integer roleId;
        @SerializedName("lastUpdatedOn")
        @Expose
        private String lastUpdatedOn;
        @SerializedName("gplusId")
        @Expose
        private String gplusId;
        @SerializedName("socialPrimaryLogin")
        @Expose
        private String socialPrimaryLogin;
        @SerializedName("createdOn")
        @Expose
        private String createdOn;


        @SerializedName("userProfileImage")
        @Expose
        private String userProfileImage;

        @SerializedName("userProfileBanner")
        @Expose
        private String userProfileBanner;

        public String getUserProfileBanner() {
            return userProfileBanner;
        }

        public void setUserProfileBanner(String userProfileBanner) {
            this.userProfileBanner = userProfileBanner;
        }

        public String getUserProfileImage() {
            return userProfileImage;
        }

        public void setUserProfileImage(String userProfileImage) {
            this.userProfileImage = userProfileImage;
        }


        public Integer getUserId() {
            return userId;
        }

        public void setUserId(Integer userId) {
            this.userId = userId;
        }

        public Integer getId() {
            return id;
        }

        public void setId(Integer id) {
            this.id = id;
        }

        public String getFirstName() {
            return firstName;
        }

        public void setFirstName(String firstName) {
            this.firstName = firstName;
        }

        public String getLastName() {
            return lastName;
        }

        public void setLastName(String lastName) {
            this.lastName = lastName;
        }

        public String getEmail() {
            return email;
        }

        public void setEmail(String email) {
            this.email = email;
        }

        public String getGender() {
            return gender;
        }

        public void setGender(String gender) {
            this.gender = gender;
        }

        public String getMobile() {
            return mobile;
        }

        public void setMobile(String mobile) {
            this.mobile = mobile;
        }

        public String getDisplayName() {
            return displayName;
        }

        public void setDisplayName(String displayName) {
            this.displayName = displayName;
        }

        public String getDateOfBirth() {
            return dateOfBirth;
        }

        public void setDateOfBirth(String dateOfBirth) {
            this.dateOfBirth = dateOfBirth;
        }

        public String getDesignation() {
            return designation;
        }

        public void setDesignation(String designation) {
            this.designation = designation;
        }

        public String getCompanyName() {
            return companyName;
        }

        public void setCompanyName(String companyName) {
            this.companyName = companyName;
        }

        public String getEmployeeId() {
            return employeeId;
        }

        public void setEmployeeId(String employeeId) {
            this.employeeId = employeeId;
        }

        public String getUserType() {
            return userType;
        }

        public void setUserType(String userType) {
            this.userType = userType;
        }

        public String getLanguage() {
            return language;
        }

        public void setLanguage(String language) {
            this.language = language;
        }

        public String getCreatedBy() {
            return createdBy;
        }

        public void setCreatedBy(String createdBy) {
            this.createdBy = createdBy;
        }

        public Integer getRoleId() {
            return roleId;
        }

        public void setRoleId(Integer roleId) {
            this.roleId = roleId;
        }

        public String getLastUpdatedOn() {
            return lastUpdatedOn;
        }

        public void setLastUpdatedOn(String lastUpdatedOn) {
            this.lastUpdatedOn = lastUpdatedOn;
        }

        public String getGplusId() {
            return gplusId;
        }

        public void setGplusId(String gplusId) {
            this.gplusId = gplusId;
        }

        public String getSocialPrimaryLogin() {
            return socialPrimaryLogin;
        }

        public void setSocialPrimaryLogin(String socialPrimaryLogin) {
            this.socialPrimaryLogin = socialPrimaryLogin;
        }

        public String getCreatedOn() {
            return createdOn;
        }

        public void setCreatedOn(String createdOn) {
            this.createdOn = createdOn;
        }
    }
}
