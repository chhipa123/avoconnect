package com.avoconnect.responsemodel;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class RecivedCard_model {
    @SerializedName("statusCode")
    @Expose
    private Integer statusCode;
    @SerializedName("responseMessage")
    @Expose
    private String responseMessage;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("resourceData")
    @Expose
    private ResourceData resourceData;

    public Integer getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(Integer statusCode) {
        this.statusCode = statusCode;
    }

    public String getResponseMessage() {
        return responseMessage;
    }

    public void setResponseMessage(String responseMessage) {
        this.responseMessage = responseMessage;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public ResourceData getResourceData() {
        return resourceData;
    }

    public void setResourceData(ResourceData resourceData) {
        this.resourceData = resourceData;
    }

    public class ImagesLink {

        @SerializedName("title")
        @Expose
        private String title;
        @SerializedName("resourceType")
        @Expose
        private String resourceType;
        @SerializedName("fileType")
        @Expose
        private String fileType;
        @SerializedName("url")
        @Expose
        private String url;
        @SerializedName("cardId")
        @Expose
        private Integer cardId;
        @SerializedName("id")
        @Expose
        private Integer id;


        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public String getResourceType() {
            return resourceType;
        }

        public void setResourceType(String resourceType) {
            this.resourceType = resourceType;
        }

        public String getFileType() {
            return fileType;
        }

        public void setFileType(String fileType) {
            this.fileType = fileType;
        }

        public String getUrl() {
            return url;
        }

        public void setUrl(String url) {
            this.url = url;
        }

        public Integer getCardId() {
            return cardId;
        }

        public void setCardId(Integer cardId) {
            this.cardId = cardId;
        }

        public Integer getId() {
            return id;
        }

        public void setId(Integer id) {
            this.id = id;
        }

    }

    public class ImpLink {

        @SerializedName("title")
        @Expose
        private String title;
        @SerializedName("resourceType")
        @Expose
        private String resourceType;
        @SerializedName("fileType")
        @Expose
        private String fileType;
        @SerializedName("url")
        @Expose
        private String url;
        @SerializedName("cardId")
        @Expose
        private Integer cardId;
        @SerializedName("id")
        @Expose
        private Integer id;

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public String getResourceType() {
            return resourceType;
        }

        public void setResourceType(String resourceType) {
            this.resourceType = resourceType;
        }

        public String getFileType() {
            return fileType;
        }

        public void setFileType(String fileType) {
            this.fileType = fileType;
        }

        public String getUrl() {
            return url;
        }

        public void setUrl(String url) {
            this.url = url;
        }

        public Integer getCardId() {
            return cardId;
        }

        public void setCardId(Integer cardId) {
            this.cardId = cardId;
        }

        public Integer getId() {
            return id;
        }

        public void setId(Integer id) {
            this.id = id;
        }

    }


    public class ResourceData {

        @SerializedName("cardId")
        @Expose
        private Integer cardId;
        @SerializedName("name")
        @Expose
        private String name;
        @SerializedName("designation")
        @Expose
        private String designation;
        @SerializedName("mobile")
        @Expose
        private String mobile;
        @SerializedName("email")
        @Expose
        private String email;
        @SerializedName("companyName")
        @Expose
        private String companyName;
        @SerializedName("website")
        @Expose
        private String website;
        @SerializedName("summary")
        @Expose
        private String summary;
        @SerializedName("impLinks")
        @Expose
        private List<ImpLink> impLinks = null;
        @SerializedName("socialLinks")
        @Expose
        private List<SocialLink> socialLinks = null;
        @SerializedName("imagesLinks")
        @Expose
        private List<ImagesLink> imagesLinks = null;
        @SerializedName("isPrimary")
        @Expose
        private Integer isPrimary;
        @SerializedName("autoShare")
        @Expose
        private Integer autoShare;
        @SerializedName("cardBackGroundColor")
        @Expose
        private String cardBackGroundColor;
        @SerializedName("cardAdditionalInfoResp")
        @Expose
        private CardAdditionalInfoResp cardAdditionalInfoResp;

        @SerializedName("cardLinkResponse")
        @Expose
        private List<CardLinkResponse> cardLinkResponse = null;

        @SerializedName("logo")
        @Expose
        private String logo;
        @SerializedName("backgroundImage")
        @Expose
        private String backgroundImage;


        @SerializedName("ocrImageOfManualCard")
        @Expose
        private String ocrImageOfManualCard;

        public String getOcrImageOfManualCard() {
            return ocrImageOfManualCard;
        }

        public void setOcrImageOfManualCard(String ocrImageOfManualCard) {
            this.ocrImageOfManualCard = ocrImageOfManualCard;
        }


        public String getLogo() {
            return logo;
        }

        public void setLogo(String logo) {
            this.logo = logo;
        }

        public String getBackgroundImage() {
            return backgroundImage;
        }

        public void setBackgroundImage(String backgroundImage) {
            this.backgroundImage = backgroundImage;
        }


        @SerializedName("linkAvailable")
        @Expose
        private Boolean linkAvailable;

        @SerializedName("address")
        @Expose
        private String address;

        public String getAddress() {
            return address;
        }


        @SerializedName("leadType")
        @Expose
        private Integer lead;

        @SerializedName("additionalInfoAvailable")
        @Expose
        private Boolean additionalInfoAvailable;

        public Integer getCardId() {
            return cardId;
        }

        public void setCardId(Integer cardId) {
            this.cardId = cardId;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getDesignation() {
            return designation;
        }

        public void setDesignation(String designation) {
            this.designation = designation;
        }

        public String getMobile() {
            return mobile;
        }

        public void setMobile(String mobile) {
            this.mobile = mobile;
        }

        public String getEmail() {
            return email;
        }

        public void setEmail(String email) {
            this.email = email;
        }

        public String getCompanyName() {
            return companyName;
        }

        public void setCompanyName(String companyName) {
            this.companyName = companyName;
        }

        public String getWebsite() {
            return website;
        }

        public void setWebsite(String website) {
            this.website = website;
        }

        public String getSummary() {
            return summary;
        }

        public void setSummary(String summary) {
            this.summary = summary;
        }

        public List<ImpLink> getImpLinks() {
            return impLinks;
        }

        public void setImpLinks(List<ImpLink> impLinks) {
            this.impLinks = impLinks;
        }

        public List<SocialLink> getSocialLinks() {
            return socialLinks;
        }

        public void setSocialLinks(List<SocialLink> socialLinks) {
            this.socialLinks = socialLinks;
        }

        public List<ImagesLink> getImagesLinks() {
            return imagesLinks;
        }

        public void setImagesLinks(List<ImagesLink> imagesLinks) {
            this.imagesLinks = imagesLinks;
        }

        public Integer getIsPrimary() {
            return isPrimary;
        }

        public void setIsPrimary(Integer isPrimary) {
            this.isPrimary = isPrimary;
        }

        public Integer getAutoShare() {
            return autoShare;
        }

        public void setAutoShare(Integer autoShare) {
            this.autoShare = autoShare;
        }

        public String getCardBackGroundColor() {
            return cardBackGroundColor;
        }

        public void setCardBackGroundColor(String cardBackGroundColor) {
            this.cardBackGroundColor = cardBackGroundColor;
        }

        public CardAdditionalInfoResp getCardAdditionalInfoResp() {
            return cardAdditionalInfoResp;
        }

        public void setCardAdditionalInfoResp(CardAdditionalInfoResp cardAdditionalInfoResp) {
            this.cardAdditionalInfoResp = cardAdditionalInfoResp;
        }

        public Boolean getAdditionalInfoAvailable() {
            return additionalInfoAvailable;
        }

        public void setAdditionalInfoAvailable(Boolean additionalInfoAvailable) {
            this.additionalInfoAvailable = additionalInfoAvailable;
        }

        public List<CardLinkResponse> getCardLinkResponse() {
            return cardLinkResponse;
        }

        public void setCardLinkResponse(List<CardLinkResponse> cardLinkResponse) {
            this.cardLinkResponse = cardLinkResponse;
        }

        public Boolean getLinkAvailable() {
            return linkAvailable;
        }

        public void setLinkAvailable(Boolean linkAvailable) {
            this.linkAvailable = linkAvailable;
        }

        public Integer getLead() {
            return lead;
        }

        public void setLead(Integer lead) {
            this.lead = lead;
        }

    }


    public class SocialLink {

        @SerializedName("title")
        @Expose
        private String title;
        @SerializedName("resourceType")
        @Expose
        private String resourceType;
        @SerializedName("fileType")
        @Expose
        private String fileType;
        @SerializedName("url")
        @Expose
        private String url;
        @SerializedName("cardId")
        @Expose
        private Integer cardId;
        @SerializedName("id")
        @Expose
        private Integer id;

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public String getResourceType() {
            return resourceType;
        }

        public void setResourceType(String resourceType) {
            this.resourceType = resourceType;
        }

        public String getFileType() {
            return fileType;
        }

        public void setFileType(String fileType) {
            this.fileType = fileType;
        }

        public String getUrl() {
            return url;
        }

        public void setUrl(String url) {
            this.url = url;
        }

        public Integer getCardId() {
            return cardId;
        }

        public void setCardId(Integer cardId) {
            this.cardId = cardId;
        }

        public Integer getId() {
            return id;
        }

        public void setId(Integer id) {
            this.id = id;
        }

    }

    public class CardAdditionalInfoResp {

        @SerializedName("id")
        @Expose
        private Integer id;
        @SerializedName("cardId")
        @Expose
        private Integer cardId;
        @SerializedName("notes")
        @Expose
        private String notes;
        @SerializedName("label")
        @Expose
        private String label;
        @SerializedName("lead")
        @Expose
        private Integer lead;
        @SerializedName("location")
        @Expose
        private String location;
        @SerializedName("time")
        @Expose
        private String time;

        public Integer getId() {
            return id;
        }

        public void setId(Integer id) {
            this.id = id;
        }

        public Integer getCardId() {
            return cardId;
        }

        public void setCardId(Integer cardId) {
            this.cardId = cardId;
        }

        public String getNotes() {
            return notes;
        }

        public void setNotes(String notes) {
            this.notes = notes;
        }

        public String getLabel() {
            return label;
        }

        public void setLabel(String label) {
            this.label = label;
        }

        public Integer getLead() {
            return lead;
        }

        public void setLead(Integer lead) {
            this.lead = lead;
        }

        public String getLocation() {
            return location;
        }

        public void setLocation(String location) {
            this.location = location;
        }

        public String getTime() {
            return time;
        }

        public void setTime(String time) {
            this.time = time;
        }

    }

    public class CardLinkResponse {

        @SerializedName("title")
        @Expose
        private String title;
        @SerializedName("url")
        @Expose
        private String url;

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public String getUrl() {
            return url;
        }

        public void setUrl(String url) {
            this.url = url;
        }

    }

}
