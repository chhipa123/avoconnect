package com.avoconnect.responsemodel;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class GetAllEvent_Model {

    @SerializedName("statusCode")
    @Expose
    private Integer statusCode;
    @SerializedName("responseMessage")
    @Expose
    private String responseMessage;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("resourceData")
    @Expose
    private List<ResourceDatum> resourceData = null;

    public Integer getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(Integer statusCode) {
        this.statusCode = statusCode;
    }

    public String getResponseMessage() {
        return responseMessage;
    }

    public void setResponseMessage(String responseMessage) {
        this.responseMessage = responseMessage;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<ResourceDatum> getResourceData() {
        return resourceData;
    }

    public void setResourceData(List<ResourceDatum> resourceData) {
        this.resourceData = resourceData;
    }

    public class ResourceDatum {

        @SerializedName("id")
        @Expose
        private Integer id;

        public Integer getId() {
            return id;
        }

        public void setId(Integer id) {
            this.id = id;
        }



        @SerializedName("title")
        @Expose
        private String title;
        @SerializedName("email")
        @Expose
        private String email;
        @SerializedName("note")
        @Expose
        private String note;
        @SerializedName("date")
        @Expose
        private String date;
        @SerializedName("time")
        @Expose
        private String time;
        @SerializedName("serverDate")
        @Expose
        private String serverDate;
        @SerializedName("totalInvites")
        @Expose
        private Integer totalInvites;
        @SerializedName("publish")
        @Expose
        private Boolean publish;

        private boolean isDeleteButtonShow = false;

        public boolean isDeleteButtonShow() {
            return isDeleteButtonShow;
        }

        public void setDeleteButtonShow(boolean deleteButtonShow) {
            isDeleteButtonShow = deleteButtonShow;
        }

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public String getEmail() {
            return email;
        }

        public void setEmail(String email) {
            this.email = email;
        }

        public String getNote() {
            return note;
        }

        public void setNote(String note) {
            this.note = note;
        }

        public String getDate() {
            return date;
        }

        public void setDate(String date) {
            this.date = date;
        }

        public String getTime() {
            return time;
        }

        public void setTime(String time) {
            this.time = time;
        }

        public String getServerDate() {
            return serverDate;
        }

        public void setServerDate(String serverDate) {
            this.serverDate = serverDate;
        }

        public Integer getTotalInvites() {
            return totalInvites;
        }

        public void setTotalInvites(Integer totalInvites) {
            this.totalInvites = totalInvites;
        }

        public Boolean getPublish() {
            return publish;
        }

        public void setPublish(Boolean publish) {
            this.publish = publish;
        }

    }

}
