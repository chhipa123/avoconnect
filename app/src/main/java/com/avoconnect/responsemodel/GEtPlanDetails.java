package com.avoconnect.responsemodel;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class GEtPlanDetails {


    @SerializedName("statusCode")
    @Expose
    private Integer statusCode;
    @SerializedName("responseMessage")
    @Expose
    private String responseMessage;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("resourceData")
    @Expose
    private List<ResourceDatum> resourceData = null;
    @SerializedName("requestSent")
    @Expose
    private Boolean requestSent;
    @SerializedName("accept")
    @Expose
    private Boolean accept;

    public Integer getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(Integer statusCode) {
        this.statusCode = statusCode;
    }

    public String getResponseMessage() {
        return responseMessage;
    }

    public void setResponseMessage(String responseMessage) {
        this.responseMessage = responseMessage;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<ResourceDatum> getResourceData() {
        return resourceData;
    }

    public void setResourceData(List<ResourceDatum> resourceData) {
        this.resourceData = resourceData;
    }

    public Boolean getRequestSent() {
        return requestSent;
    }

    public void setRequestSent(Boolean requestSent) {
        this.requestSent = requestSent;
    }

    public Boolean getAccept() {
        return accept;
    }

    public void setAccept(Boolean accept) {
        this.accept = accept;
    }

    public class ResourceDatum {

        @SerializedName("id")
        @Expose
        private Integer id;
        @SerializedName("planName")
        @Expose
        private String planName;
        @SerializedName("planType")
        @Expose
        private String planType;
        @SerializedName("planAmount")
        @Expose
        private Double planAmount;
        @SerializedName("description")
        @Expose
        private String description;
        @SerializedName("creationTime")
        @Expose
        private String creationTime;

        public Integer getId() {
            return id;
        }

        public void setId(Integer id) {
            this.id = id;
        }

        public String getPlanName() {
            return planName;
        }

        public void setPlanName(String planName) {
            this.planName = planName;
        }

        public String getPlanType() {
            return planType;
        }

        public void setPlanType(String planType) {
            this.planType = planType;
        }

        public Double getPlanAmount() {
            return planAmount;
        }

        public void setPlanAmount(Double planAmount) {
            this.planAmount = planAmount;
        }

        public String getDescription() {
            return description;
        }

        public void setDescription(String description) {
            this.description = description;
        }

        public String getCreationTime() {
            return creationTime;
        }

        public void setCreationTime(String creationTime) {
            this.creationTime = creationTime;
        }

    }
}
