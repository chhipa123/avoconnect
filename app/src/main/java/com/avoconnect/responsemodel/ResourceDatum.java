package com.avoconnect.responsemodel;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class ResourceDatum {
    @SerializedName("cardId")
    @Expose
    private Integer id;
    @SerializedName("name")
    @Expose
    private String fullName;
    @SerializedName("designation")
    @Expose
    private String designation;
    @SerializedName("mobile")
    @Expose
    private String mobile;
    @SerializedName("email")
    @Expose
    private String email;
    @SerializedName("companyName")
    @Expose
    private Object companyName;
    @SerializedName("website")
    @Expose
    private String website;
    @SerializedName("summary")
    @Expose
    private String summary;
    @SerializedName("isPrimary")
    @Expose
    private Integer isPrimary;
    @SerializedName("autoShare")
    @Expose
    private Integer autoShare;
    @SerializedName("createdOn")
    @Expose
    private Object createdOn;
    @SerializedName("lastUpdatedOn")
    @Expose
    private Object lastUpdatedOn;
    @SerializedName("createdBy")
    @Expose
    private Object createdBy;
    @SerializedName("userId")
    @Expose
    private Integer userId;
    @SerializedName("listOfImpLinks")
    @Expose
    private List<ListOfImpLink> listOfImpLinks = null;
    @SerializedName("listOfSocialLinks")
    @Expose
    private List<Object> listOfSocialLinks = null;
    @SerializedName("listOfImageLinks")
    @Expose
    private List<Object> listOfImageLinks = null;
    @SerializedName("isDeleted")
    @Expose
    private Integer isDeleted;
    @SerializedName("cardBackGroundColor")
    @Expose
    private String backgroundColor;


    @SerializedName("shareCardUrl")
    @Expose
    private String shareCardUrl;

    @SerializedName("logo")
    @Expose
    private String logo;
    @SerializedName("backgroundImage")
    @Expose
    private String backgroundImage;

    public String getLogo() {
        return logo;
    }

    public void setLogo(String logo) {
        this.logo = logo;
    }

    public String getBackgroundImage() {
        return backgroundImage;
    }

    public void setBackgroundImage(String backgroundImage) {
        this.backgroundImage = backgroundImage;
    }



    public String getShareCardUrl() {
        return shareCardUrl;
    }

    public void setShareCardUrl(String shareCardUrl) {
        this.shareCardUrl = shareCardUrl;
    }


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getDesignation() {
        return designation;
    }

    public void setDesignation(String designation) {
        this.designation = designation;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Object getCompanyName() {
        return companyName;
    }

    public void setCompanyName(Object companyName) {
        this.companyName = companyName;
    }

    public String getWebsite() {
        return website;
    }

    public void setWebsite(String website) {
        this.website = website;
    }

    public String getSummary() {
        return summary;
    }

    public void setSummary(String summary) {
        this.summary = summary;
    }

    public Integer getIsPrimary() {
        return isPrimary;
    }

    public void setIsPrimary(Integer isPrimary) {
        this.isPrimary = isPrimary;
    }

    public Integer getAutoShare() {
        return autoShare;
    }

    public void setAutoShare(Integer autoShare) {
        this.autoShare = autoShare;
    }

    public Object getCreatedOn() {
        return createdOn;
    }

    public void setCreatedOn(Object createdOn) {
        this.createdOn = createdOn;
    }

    public Object getLastUpdatedOn() {
        return lastUpdatedOn;
    }

    public void setLastUpdatedOn(Object lastUpdatedOn) {
        this.lastUpdatedOn = lastUpdatedOn;
    }

    public Object getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(Object createdBy) {
        this.createdBy = createdBy;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public List<ListOfImpLink> getListOfImpLinks() {
        return listOfImpLinks;
    }

    public void setListOfImpLinks(List<ListOfImpLink> listOfImpLinks) {
        this.listOfImpLinks = listOfImpLinks;
    }

    public List<Object> getListOfSocialLinks() {
        return listOfSocialLinks;
    }

    public void setListOfSocialLinks(List<Object> listOfSocialLinks) {
        this.listOfSocialLinks = listOfSocialLinks;
    }

    public List<Object> getListOfImageLinks() {
        return listOfImageLinks;
    }

    public void setListOfImageLinks(List<Object> listOfImageLinks) {
        this.listOfImageLinks = listOfImageLinks;
    }

    public Integer getIsDeleted() {
        return isDeleted;
    }

    public void setIsDeleted(Integer isDeleted) {
        this.isDeleted = isDeleted;
    }

    public String getBackgroundColor() {
        return backgroundColor;
    }

    public void setBackgroundColor(String backgroundColor) {
        this.backgroundColor = backgroundColor;
    }

    private boolean isDeleteButtonShow = false;

    public boolean isDeleteButtonShow() {
        return isDeleteButtonShow;
    }

    public void setDeleteButtonShow(boolean deleteButtonShow) {
        isDeleteButtonShow = deleteButtonShow;
    }
}