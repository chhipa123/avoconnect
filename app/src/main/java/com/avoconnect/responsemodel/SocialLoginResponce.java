package com.avoconnect.responsemodel;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class SocialLoginResponce {
    @SerializedName("statusCode")
    @Expose
    private Integer statusCode;
    @SerializedName("responseMessage")
    @Expose
    private String responseMessage;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("access_token")
    @Expose
    private String accessToken;
    @SerializedName("token_type")
    @Expose
    private String tokenType;
    @SerializedName("refresh_token")
    @Expose
    private String refreshToken;
    @SerializedName("expires_in")
    @Expose
    private Integer expiresIn;
    @SerializedName("scope")
    @Expose
    private String scope;
    @SerializedName("userId")
    @Expose
    private String userId;
    @SerializedName("username")
    @Expose
    private String username;
    @SerializedName("jti")
    @Expose
    private String jti;
    @SerializedName("firstName")
    @Expose
    private String firstName;
    @SerializedName("lastName")
    @Expose
    private String lastName;
    @SerializedName("roleId")
    @Expose
    private String roleId;

    @SerializedName("userType")
    @Expose
    private String userType;

    @SerializedName("askEmailOnViewCard")
    @Expose
    private Boolean askEmailOnViewCard;
    @SerializedName("isTwoWayAuthEnabled")
    @Expose
    private Boolean isTwoWayAuthEnabled;
    @SerializedName("isNotificationActive")
    @Expose
    private Boolean isNotificationActive;

    public Boolean getAskEmailOnViewCard() {
        return askEmailOnViewCard;
    }

    public void setAskEmailOnViewCard(Boolean askEmailOnViewCard) {
        this.askEmailOnViewCard = askEmailOnViewCard;
    }

    public Boolean getIsTwoWayAuthEnabled() {
        return isTwoWayAuthEnabled;
    }

    public void setIsTwoWayAuthEnabled(Boolean isTwoWayAuthEnabled) {
        this.isTwoWayAuthEnabled = isTwoWayAuthEnabled;
    }

    public Boolean getIsNotificationActive() {
        return isNotificationActive;
    }

    public void setIsNotificationActive(Boolean isNotificationActive) {
        this.isNotificationActive = isNotificationActive;
    }


    public String getUserType() {
        return userType;
    }

    public void setUserType(String userType) {
        this.userType = userType;
    }

    public Integer getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(Integer statusCode) {
        this.statusCode = statusCode;
    }

    public String getResponseMessage() {
        return responseMessage;
    }

    public void setResponseMessage(String responseMessage) {
        this.responseMessage = responseMessage;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getAccessToken() {
        return accessToken;
    }

    public void setAccessToken(String accessToken) {
        this.accessToken = accessToken;
    }

    public String getTokenType() {
        return tokenType;
    }

    public void setTokenType(String tokenType) {
        this.tokenType = tokenType;
    }

    public String getRefreshToken() {
        return refreshToken;
    }

    public void setRefreshToken(String refreshToken) {
        this.refreshToken = refreshToken;
    }

    public Integer getExpiresIn() {
        return expiresIn;
    }

    public void setExpiresIn(Integer expiresIn) {
        this.expiresIn = expiresIn;
    }

    public String getScope() {
        return scope;
    }

    public void setScope(String scope) {
        this.scope = scope;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getJti() {
        return jti;
    }

    public void setJti(String jti) {
        this.jti = jti;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getRoleId() {
        return roleId;
    }

    public void setRoleId(String roleId) {
        this.roleId = roleId;
    }

}