package com.avoconnect.Prefrence;


import android.content.Context;
import android.content.SharedPreferences;
import android.provider.ContactsContract;

public class UserPrefrence {
    private static final String PREF_NAME = "Avocard";
    private static UserPrefrence yourPreference;
    private SharedPreferences sharedPreferences;
    private static SharedPreferences.Editor editor;
    public static final String IS_LOGIN = "isLogin";
    public static final String FCM_TOKEN = "FCM_TOKEN";
    public static final String CURRENT_LANGUAGE="CURRENT_LANGUAGE";
    public static final String USERID = "USERID";
    public static final String USERNAME = "USERNAME";
    public static final String EMAIL = "EMAIL";
    public static final String USERTYPE = "USERTYPE";
    public static final String AUTHTOKEN = "AUTHTOKEN";
    public static final String MOBILE = "MOBILE";
    public static final String IS_ASKEMAIL = "IS_ASKEMAIL";
    public static final String IS_NOTIFICATION_ON = "IS_NOTIFICATION_ON";
    public static final String IS_BLOCK = "IS_BLOCK";
    public static final String IS_AUTHENTICALTION = "IS_AUTHENTICALTION";
    public static final String AVOID = "AVOID";
    public static final String NOTIFICATION_UNREAD = "NOTIFICATION_UNREAD";
    public static final String ALTERNATEMOBILE = "ALTERNATEMOBILE";
    public static final String OTPNUMNBER="OTPNUMNBER";

    public static final String EXPIRESIN="EXPIRESIN";
    public static final String JTI="JTI";
    public static final String REFRESHTOKEN="REFRESHTOKEN";
    public static final String PERMISSION="PERMISSION";



    public static UserPrefrence getInstance(Context context) {
        if (yourPreference == null) {
            yourPreference = new UserPrefrence(context);
        }
        return yourPreference;
    }

    public UserPrefrence(Context context) {
        sharedPreferences = context.getSharedPreferences(PREF_NAME,Context.MODE_PRIVATE);
        editor = sharedPreferences.edit();
    }

    public  void setIsLogin(String auth) {
        editor.putString(IS_LOGIN, auth);
        editor.commit();
    }

    public  boolean isLogin() {
        boolean auth = false;
        String value = sharedPreferences.getString(IS_LOGIN, null);
        if (value != null) {
            auth = true;

        }
        return auth;
    }

    public  void setFcmToken(String fcmtoken) {
        editor.putString(FCM_TOKEN, fcmtoken);
        editor.commit();
    }

    public  String getFcmToken() {
        String auth = "";
        String value = sharedPreferences.getString(FCM_TOKEN, null);
        if (value != null) {
            auth = value;
        }
        return auth;
    }

    public  void setAvoid(String usertype) {
        editor.putString(AVOID, usertype);
        editor.commit();
    }

    public  String getAvoid() {
        String auth = "";
        String value = sharedPreferences.getString(AVOID, null);
        if (value != null) {
            auth = value;
        }
        return auth;
    }

    public  void setUsertype(String usertype) {
        editor.putString(USERTYPE, usertype);
        editor.commit();
    }

    public  String getUsertype() {
        String auth = "";
        String value = sharedPreferences.getString(USERTYPE, null);
        if (value != null) {
            auth = value;
        }
        return auth;
    }

    public  void setUserid(String userid) {
        editor.putString(USERID, userid);
        editor.commit();
    }

    public  String getUserid() {
        String auth = "";
        String value = sharedPreferences.getString(USERID, null);
        if (value != null) {
            auth = value;
        }
        return auth;
    }


    public  void setCurrentLanguage(String currentLanguage) {
        editor.putString(CURRENT_LANGUAGE, currentLanguage);
        editor.commit();
    }

    public  String getCurrentLanguage() {
        String auth = "";
        String value = sharedPreferences.getString(CURRENT_LANGUAGE, null);
        if (value != null) {
            auth = value;
        }
        return auth;
    }



    public  void setUsername(String username) {
        editor.putString(USERNAME, username);
        editor.commit();
    }

    public  String getUsername() {
        String auth = "";
        String value = sharedPreferences.getString(USERNAME, null);
        if (value != null) {
            auth = value;
        }
        return auth;
    }

    public  void setEmail(String username) {
        editor.putString(EMAIL, username);
        editor.commit();
    }

    public  String getEmail() {
        String auth = "";
        String value = sharedPreferences.getString(EMAIL, null);
        if (value != null) {
            auth = value;
        }
        return auth;
    }

    public  void setAuthtoken(String authtoken) {
        editor.putString(AUTHTOKEN, authtoken);
        editor.commit();
    }

    public  String getAuthtoken() {
        String auth = "";
        String value = sharedPreferences.getString(AUTHTOKEN, null);
        if (value != null) {
            auth = value;
        }
        return auth;
    }


    public  void logoutDone() {
        editor.putString(IS_LOGIN, null);
        editor.clear();
        editor.commit();
    }


    public  String getIsAskemail() {
        String auth = "";
        String value = sharedPreferences.getString(IS_ASKEMAIL, null);
        if (value != null) {
            auth = value;
        }
        return auth;
    }
    public  void setIsAskemail(String askemail) {
        editor.putString(IS_ASKEMAIL, askemail);
        editor.commit();
    }

    public  String getIsNotificationOn() {
        String auth = "";
        String value = sharedPreferences.getString(IS_NOTIFICATION_ON, null);
        if (value != null) {
            auth = value;
        }
        return auth;

    }

    public  void setIsNotificationOn(String isnotifica) {
        editor.putString(IS_NOTIFICATION_ON, isnotifica);
        editor.commit();
    }

    public  String getIsBlock() {

        String auth = "";
        String value = sharedPreferences.getString(IS_BLOCK, null);
        if (value != null) {
            auth = value;
        }
        return auth;

    }

    public  void setIsBlock(String isblockuser) {
        editor.putString(IS_BLOCK, isblockuser);
        editor.commit();
    }

    public  String getIsAuthenticaltion() {
        String auth = "";
        String value = sharedPreferences.getString(IS_AUTHENTICALTION, null);
        if (value != null) {
            auth = value;
        }
        return auth;

    }

    public  void setIsAuthenticaltion(String isatuh) {
        editor.putString(IS_AUTHENTICALTION, isatuh);
        editor.commit();
    }


    public  String getNotificationUnread() {
        String auth = "";
        String value = sharedPreferences.getString(NOTIFICATION_UNREAD, null);
        if (value != null) {
            auth = value;
        }
        return auth;

    }

    public  void setNotificationUnread(String isatuh) {
        editor.putString(NOTIFICATION_UNREAD, isatuh);
        editor.commit();
    }

    public  String getMobile() {
        String auth = "";
        String value = sharedPreferences.getString(MOBILE, null);
        if (value != null) {
            auth = value;
        }
        return auth;

    }

    public  void setMobile(String isatuh) {
        editor.putString(MOBILE, isatuh);
        editor.commit();
    }

    public  String getAlternatemobile() {
        String auth = "";
        String value = sharedPreferences.getString(ALTERNATEMOBILE, null);
        if (value != null) {
            auth = value;
        }
        return auth;

    }

    public  void setAlternatemobile(String alternatemobile) {
        editor.putString(ALTERNATEMOBILE, alternatemobile);
        editor.commit();
    }


    public  String getOtpnumnber() {
        String auth = "";
        String value = sharedPreferences.getString(OTPNUMNBER, null);
        if (value != null) {
            auth = value;
        }
        return auth;

    }

    public  void setOtpnumnber(String otpnumnber) {
        editor.putString(OTPNUMNBER, otpnumnber);
        editor.commit();
    }

    public  String getExpiresin() {
        String auth = "";
        String value = sharedPreferences.getString(EXPIRESIN, null);
        if (value != null) {
            auth = value;
        }
        return auth;

    }

    public  void setExpiresin(String expiresin) {
        editor.putString(EXPIRESIN, expiresin);
        editor.commit();
    }

    public  String getJti() {
        String auth = "";
        String value = sharedPreferences.getString(JTI, null);
        if (value != null) {
            auth = value;
        }
        return auth;

    }

    public  void setJti(String expiresin) {
        editor.putString(JTI, expiresin);
        editor.commit();
    }

    public  String getRefreshtoken() {
        String auth = "";
        String value = sharedPreferences.getString(REFRESHTOKEN, null);
        if (value != null) {
            auth = value;
        }
        return auth;

    }

    public  void setRefreshtoken(String expiresin) {
        editor.putString(REFRESHTOKEN, expiresin);
        editor.commit();
    }

    public  String getPermission() {
        String auth = "";
        String value = sharedPreferences.getString(PERMISSION, null);
        if (value != null) {
            auth = value;
        }
        return auth;

    }

    public  void setPermission(String permission) {
        editor.putString(PERMISSION, permission);
        editor.commit();
    }


}

