package com.avoconnect.Common_TextView_Editext;

import android.content.Context;
import android.graphics.Typeface;
import androidx.appcompat.widget.AppCompatEditText;
import android.util.AttributeSet;


public class EditextRegular extends AppCompatEditText {

    public EditextRegular(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init(context);
    }

    public EditextRegular(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context);
    }

    public EditextRegular(Context context) {
        super(context);
        init(context);
    }

    private void init(Context context) {
            Typeface type_normal= Typeface.createFromAsset(context.getAssets(), "font/candaraz.ttf");
            setTypeface(type_normal);
    }
}
