package com.avoconnect.Common_TextView_Editext;

import android.content.Context;
import android.graphics.Typeface;
import androidx.appcompat.widget.AppCompatEditText;
import androidx.appcompat.widget.AppCompatTextView;
import android.util.AttributeSet;

/**
 * Created by VIVEK on 17-May-19.
 */


public class TextViewRegular extends AppCompatTextView {

   public TextViewRegular(Context context, AttributeSet attrs, int defStyle) {
            super(context, attrs, defStyle);
            init(context);
        }

  public TextViewRegular(Context context, AttributeSet attrs) {
            super(context, attrs);
            init(context);
        }

  public TextViewRegular(Context context) {
            super(context);
            init(context);
        }

  private void init(Context context) {
         Typeface type_normal= Typeface.createFromAsset(context.getAssets(), "font/candaraz.ttf");
         setTypeface(type_normal);
        }

}
