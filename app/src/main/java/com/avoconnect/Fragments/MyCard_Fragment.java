package com.avoconnect.Fragments;

import android.Manifest;
import android.app.Dialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.avoconnect.BottomSheetFragments.AddCardsOcrManual;
import com.avoconnect.BottomSheetFragments.AddInfoOtherUserFragment;
import com.avoconnect.BottomSheetFragments.CallMessageFragment;
import com.avoconnect.Util.GPS_TRACKER;
import com.avoconnect.activity.SearchNearByActivity;
import com.avoconnect.responsemodel.AutoShare_Model;
import com.google.android.material.bottomsheet.BottomSheetBehavior;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import androidx.fragment.app.Fragment;
import androidx.appcompat.app.AlertDialog;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.ItemTouchHelper;

import android.os.Handler;
import android.provider.Settings;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.avoconnect.BottomSheetFragments.ShareCardFragment;
import com.avoconnect.Constants.Constant;
import com.avoconnect.Prefrence.UserPrefrence;
import com.avoconnect.R;
import com.avoconnect.Util.ProgressBarCustom;
import com.avoconnect.Util.Utils;
import com.avoconnect.activity.AddEditCardActitivty;
import com.avoconnect.activity.DetialsCardActivity;
import com.avoconnect.adapter.MyCardRecyclerViewAdapter;
import com.avoconnect.api.RestClient;
import com.avoconnect.responsemodel.CardModel;
import com.avoconnect.responsemodel.DeleteCard_Model;
import com.avoconnect.responsemodel.ResourceDatum;
import com.karumi.dexter.Dexter;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.PermissionDeniedResponse;
import com.karumi.dexter.listener.PermissionGrantedResponse;
import com.karumi.dexter.listener.PermissionRequest;
import com.karumi.dexter.listener.single.PermissionListener;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;
import retrofit.mime.TypedByteArray;
import retrofit.mime.TypedInput;

import static android.content.Context.LOCATION_SERVICE;

public class MyCard_Fragment extends Fragment {

    RecyclerView other_card_list;
    FloatingActionButton card_add;
    LinearLayout ll_blankCard;
    RelativeLayout rl_myCard;
    TextView txt_addcard, userId;
    List<ResourceDatum> resourceData = new ArrayList<>();
    private BottomSheetBehavior mBottomSheetBehavior;
    MyCardRecyclerViewAdapter mycardAdapter = null;
    int value=0;
    ImageView nearbyIcon;
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = LayoutInflater.from(getActivity()).inflate(R.layout.mycard_fragment, null);
        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        ll_blankCard = (LinearLayout) getActivity().findViewById(R.id.ll_blankCard);
        rl_myCard = (RelativeLayout) getActivity().findViewById(R.id.rl_myCard);
        nearbyIcon=(ImageView)getActivity().findViewById(R.id.nearbyIcon);
        other_card_list = (RecyclerView) getActivity().findViewById(R.id.other_card_list_mycard);
        txt_addcard = (TextView) getActivity().findViewById(R.id.txt_addcard);
        userId = (TextView) getActivity().findViewById(R.id.userId);
        card_add = (FloatingActionButton) getActivity().findViewById(R.id.card_add);
        card_add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                AddCardsOcrManual bottomSheetFragment = new AddCardsOcrManual();
                bottomSheetFragment.show(getActivity().getSupportFragmentManager(), bottomSheetFragment.getTag());

                bottomSheetFragment.onCallBackReturn(new AddCardsOcrManual.Callback() {
                    @Override
                    public void clickaction(int position) {
                        Log.e("======my card","====my card");
                        callApi();
                    }
                });

//                Intent intent = new Intent(getActivity(), AddEditCardActitivty.class);
//                startActivityForResult(intent, 144);
            }
        });
        txt_addcard.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                AddCardsOcrManual bottomSheetFragment = new AddCardsOcrManual();
                Bundle bundle=new Bundle();
                bundle.putString("myfirstcard","yes");
                bottomSheetFragment.setArguments(bundle);
                bottomSheetFragment.show(getActivity().getSupportFragmentManager(), bottomSheetFragment.getTag());

                bottomSheetFragment.onCallBackReturn(new AddCardsOcrManual.Callback() {
                    @Override
                    public void clickaction(int position) {
                        Log.e("======my card","====my card");
                        callApi();
                    }
                });


//                Intent intent = new Intent(getActivity(), AddEditCardActitivty.class);
//                intent.putExtra("myfirstcard","yes");
//                startActivityForResult(intent, 144);
            }
        });
        userId.setText(UserPrefrence.getInstance(getActivity()).getAvoid());

        LinearLayout bottomSheet = (LinearLayout) getActivity().findViewById(R.id.bottom_sheet);
        mBottomSheetBehavior = BottomSheetBehavior.from(bottomSheet);
        mBottomSheetBehavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
        mBottomSheetBehavior.setPeekHeight(0);

        nearbyIcon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                LocationManager service = (LocationManager) getActivity().getSystemService(LOCATION_SERVICE);
                boolean enabled = service.isProviderEnabled(LocationManager.GPS_PROVIDER);
                // Check if enabled and if not send user to the GPS settings
                if (!enabled) {
                    showAlert();
                }else {
                    Dexter.withActivity(getActivity())
                            .withPermission(Manifest.permission.ACCESS_FINE_LOCATION)
                            .withListener(new PermissionListener() {
                                @Override
                                public void onPermissionGranted(PermissionGrantedResponse response) {

                                    Intent intent = new Intent(getActivity(), SearchNearByActivity.class);
                                    intent.putExtra("is_enable", "yes");
                                    startActivity(intent);

                                }

                                @Override
                                public void onPermissionDenied(PermissionDeniedResponse response) {

                                }

                                @Override
                                public void onPermissionRationaleShouldBeShown(PermissionRequest permission, PermissionToken token) {

                                }
                            }).check();
                }
            }

        });

        callApi();
    }

    private void showAlert() {
        final AlertDialog.Builder dialog = new AlertDialog.Builder(getActivity());
        dialog.setTitle("Enable Location")
                .setMessage("Your Locations Settings is set to 'Off'.\nPlease Enable Location to " +
                        "use this app")
                .setPositiveButton("Location Settings", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface paramDialogInterface, int paramInt) {
                        Intent myIntent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                        startActivity(myIntent);
                    }
                })
                .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface paramDialogInterface, int paramInt) {
                    }
                });
        dialog.show();
    }

    private void callApi() {
        final ProgressBarCustom progressBarCustom = new ProgressBarCustom(getActivity());
        progressBarCustom.showProgress();
        try {
            String token = UserPrefrence.getInstance(getActivity()).getAuthtoken();
            token = "bearer " + token;
            RestClient.getInstance(getActivity()).get().getCardList(token, UserPrefrence.getInstance(getActivity()).getUserid(), new Callback<CardModel>() {

                @Override
                public void success(final CardModel res, Response response) {
                    progressBarCustom.cancelProgress();
                    if (response.getStatus() == 200) {
                        if (res.getResponseMessage().equalsIgnoreCase(Constant.SUCCESS)) {
                            resourceData.clear();
                            resourceData = res.getResourceData();
                            isBlankCardView(resourceData);
                        } else {
                            resourceData.clear();
                            resourceData = res.getResourceData();
                            isBlankCardView(resourceData);
                        }
                    }
                }

                @Override
                public void failure(final RetrofitError error) {
                    progressBarCustom.cancelProgress();
                    error.printStackTrace();
                    if (error != null) {
                        Utils utils = new Utils(getActivity());
                        utils.showError(error);
                    }
                }
            });

        } catch (Exception e) {
            progressBarCustom.cancelProgress();
            e.printStackTrace();
        }


    }

    //
    private void setAdapter() {
        mycardAdapter = new MyCardRecyclerViewAdapter(getContext(), resourceData);
        other_card_list.setAdapter(mycardAdapter);
        other_card_list.setLayoutManager(new LinearLayoutManager(getContext()));
        other_card_list.setNestedScrollingEnabled(false);
        other_card_list.setHasFixedSize(false);
        mycardAdapter.onCallBackReturn(new MyCardRecyclerViewAdapter.Callback() {
            @Override
            public void clickaction(int position) {
                Intent intent = new Intent(getActivity(), DetialsCardActivity.class);
                intent.putExtra("card_id", resourceData.get(position).getId() + "");
                startActivityForResult(intent, 144);
            }
            @Override
            public void clickaction(int position, String type, String cardId) {
                if (type != null && !type.isEmpty() && type.equalsIgnoreCase("share")) {
                    //                opensharing pop()
                    ShareCardFragment bottomSheetFragment = new ShareCardFragment();
                    Bundle bundle=new Bundle();
                    bundle.putString("name",resourceData.get(position).getFullName());
                    bundle.putString("email",resourceData.get(position).getEmail());
                    bundle.putString("mobile",resourceData.get(position).getMobile());
                    bundle.putString("share_url",resourceData.get(position).getShareCardUrl());
                    bundle.putString("backgroundcolor",resourceData.get(position).getBackgroundColor());
                    if(resourceData.get(position).getLogo()!=null&&resourceData.get(position).getLogo().length()>0)
                    {

                        bundle.putString("logo",resourceData.get(position).getLogo());
                    }

                    bundle.putString("card_id",cardId+"");

                    bottomSheetFragment.setArguments(bundle);
                    bottomSheetFragment.show(getActivity().getSupportFragmentManager(), bottomSheetFragment.getTag());

                } else if (type != null && !type.isEmpty() && type.equalsIgnoreCase("delete")) {
                    if (cardId != null && !cardId.isEmpty() && cardId.length() > 0) {
                        generatepop(cardId);

                    }
                }
            }

            @Override
            public void clickcheckbox(int position,boolean status,int card_id) {
                Log.e("=print cheange","=======");
                if(status==true)
                {
                   callupdateAutoShareAtstus(status,card_id,position);
                }else if(status==false)
                {
                    callupdateAutoShareAtstus(status,card_id,position);
                }
            }
            @Override
            public void addcontact(String name,String phonenumber,int position) {
                try {

                    String[] values=null;
                    String phonenumbers="";
                    if(phonenumber.contains(","))
                    {
                        values = phonenumber.split(",");
                        phonenumbers=values[0]+"";
                    }else{
                        phonenumbers=phonenumber;
                    }

                    CallMessageFragment bottomSheetFragment = new CallMessageFragment();
                    Bundle bundle=new Bundle();
                    bundle.putString("phonenumber",phonenumbers+"");
                    bottomSheetFragment.setArguments(bundle);
                    //bottomSheetFragment.setTargetFragment(getParentFragment());
                    bottomSheetFragment.show(getActivity().getSupportFragmentManager(), bottomSheetFragment.getTag());


//                    String[] values=null;
//
//                    if(phonenumber.contains(","))
//                    {
//                       values = phonenumber.split(",");
//                    }
//
//                    if(values!=null&&values.length>0){
//                        Intent intent = new Intent(ContactsContract.Intents.Insert.ACTION);
//                        intent.setType(ContactsContract.Contacts.CONTENT_TYPE);
//                        ArrayList<ContentValues> data = new ArrayList<ContentValues>();
//
//                        //Filling data with phone numbers
//                        for (int i = 0; i < values.length; i++) {
//                            ContentValues row = new ContentValues();
//                            row.put(ContactsContract.Contacts.Data.MIMETYPE, ContactsContract.CommonDataKinds.Phone.CONTENT_ITEM_TYPE);
//                            row.put(ContactsContract.CommonDataKinds.Phone.NUMBER, values[i].toString());
//                            row.put(ContactsContract.CommonDataKinds.Phone.TYPE, ContactsContract.RawContacts.CONTENT_TYPE);
//                            data.add(row);
//                        }
//
//                        intent.putExtra(ContactsContract.Intents.Insert.NAME, name);
//                        intent.putParcelableArrayListExtra(ContactsContract.Intents.Insert.DATA, data);
//                        startActivity(intent);
//                    }else{
//                        Intent contactIntent = new Intent(ContactsContract.Intents.Insert.ACTION);
//                        contactIntent.setType(ContactsContract.RawContacts.CONTENT_TYPE);
//                        contactIntent.putExtra(ContactsContract.Intents.Insert.NAME, name)
//                                .putExtra(ContactsContract.Intents.Insert.PHONE, phonenumber);
//                        startActivity(contactIntent);
//                    }




                }catch (Exception e)
                {
                    e.printStackTrace();
                }
            }

            @Override
            public void sendemail(String email) {
                try {
                    Intent intent = new Intent(Intent.ACTION_SENDTO);
                    intent.setData(Uri.parse("mailto:" + email)); // only email apps should handle this
                    intent.putExtra(Intent.EXTRA_EMAIL, email);
                    intent.putExtra(Intent.EXTRA_SUBJECT, "Avoconnect");
                    if (intent.resolveActivity(getActivity().getPackageManager()) != null) {
                        startActivity(intent);
                    }
                }catch (Exception e)
                {
                    e.printStackTrace();
                }
            }

        });
        enableSwipe();
    }

    private void callupdateAutoShareAtstus(final boolean status, final int card_id,final int position) {

        final ProgressBarCustom progressBarCustom = new ProgressBarCustom(getActivity());
        progressBarCustom.showProgress();
        try {

            if(status==true)
            {
                value=1;
            }else{
                value=0;
            }
            String token = UserPrefrence.getInstance(getActivity()).getAuthtoken();
            token = "bearer " + token;
            JSONObject jsonObject3 = new JSONObject();
            TypedInput in3 = new TypedByteArray("application/json", jsonObject3.toString().getBytes("UTF-8"));
            RestClient.getInstance(getActivity()).get().autosharecard(token, card_id+"",value+"",in3, new Callback<AutoShare_Model>() {

                @Override
                public void success(final AutoShare_Model res, Response response) {
                    progressBarCustom.cancelProgress();
                    if (response.getStatus() == 200) {
                        if (res.getResponseMessage().equalsIgnoreCase(Constant.SUCCESS)) {
                            Toast.makeText(getActivity(), res.getMessage(), Toast.LENGTH_SHORT).show();

                            Handler handler = new Handler();
                            handler.post(new Runnable() {
                                @Override
                                public void run() {
                                    resourceData.get(position).setAutoShare(value);
                                }
                            });

                        } else {
                          //  Toast.makeText(getActivity(), res.getMessage(), Toast.LENGTH_SHORT).show();
                        }
                    }
                }

                @Override
                public void failure(final RetrofitError error) {
                    progressBarCustom.cancelProgress();
                    error.printStackTrace();
                    if (error != null) {
                        Utils utils = new Utils(getActivity());
                        utils.showError(error);
                    }
                }
            });

        } catch (Exception e) {
            progressBarCustom.cancelProgress();
            e.printStackTrace();
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        LocalBroadcastManager.getInstance(getActivity()).registerReceiver(notificationCounterBroadcast
                , new IntentFilter("callapi"));
    }

    public BroadcastReceiver notificationCounterBroadcast = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent != null) {
                String click_action = intent.getStringExtra("callapi");
                callApi();
            }
        }

    };

    @Override
    public void onDestroy() {
        LocalBroadcastManager.getInstance(getActivity()).unregisterReceiver(notificationCounterBroadcast);
        super.onDestroy();

    }


    private void generatepop(final String cardid) {
        AlertDialog.Builder builder=new AlertDialog.Builder(getActivity());
        View view= LayoutInflater.from(getActivity()).inflate(R.layout.logout_lay,null);
        TextView txtDialogMessage=(TextView)view.findViewById(R.id.txtDialogMessage);
        txtDialogMessage.setText(getResources().getString(R.string.delete_Card));
        Button btnDialogYes=(Button)view.findViewById(R.id.btnDialogYes);
        Button btnDialogNo=(Button)view.findViewById(R.id.btnDialogNo);
        builder.setView(view);
        final Dialog dialog=builder.create();
        dialog.setCancelable(false);
        dialog.setCanceledOnTouchOutside(false);

        btnDialogYes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
                DeleteCardApi(cardid);

            }
        });
        btnDialogNo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });
        dialog.show();

    }

    private void isBlankCardView(List<ResourceDatum> resourceData) {
        if (resourceData.size() > 0) {
            ll_blankCard.setVisibility(View.GONE);
            rl_myCard.setVisibility(View.VISIBLE);
            card_add.show();
            setAdapter();
        } else {
            ll_blankCard.setVisibility(View.VISIBLE);
            rl_myCard.setVisibility(View.GONE);
            card_add.hide();
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {

        if (requestCode == 144 && resultCode == getActivity().RESULT_OK) {
            callApi();
        }
    }

    public void DeleteCardApi(final String card_id) {
        final ProgressBarCustom progressBarCustom = new ProgressBarCustom(getActivity());
        progressBarCustom.showProgress();
        try {
            String token = UserPrefrence.getInstance(getActivity()).getAuthtoken();
            token = "bearer " + token;
            RestClient.getInstance(getActivity()).get().deletecard(token, card_id, new Callback<DeleteCard_Model>() {

                @Override
                public void success(final DeleteCard_Model res, Response response) {
                    progressBarCustom.cancelProgress();
                    if (response.getStatus() == 200) {
                        if (res.getResponseMessage().equalsIgnoreCase(Constant.SUCCESS)) {
                            if (resourceData.size() > 0) {
                                int removepos = 0;
                                for (int i = 0; i < resourceData.size(); i++) {
                                    ResourceDatum resourceDatum = resourceData.get(i);
                                    resourceDatum.setDeleteButtonShow(false);
                                    resourceData.set(i, resourceDatum);
                                    if (String.valueOf(resourceData.get(i).getId()).equalsIgnoreCase(card_id)) {
                                        removepos = i;
                                    }
                                }
                                resourceData.remove(removepos);
                                if (mycardAdapter != null) {
                                    mycardAdapter.notifyDataSetChanged();
                                }
                            }
                        } else {
                            Toast.makeText(getContext(), res.getMessage(), Toast.LENGTH_SHORT).show();
                        }
                    }
                }

                @Override
                public void failure(final RetrofitError error) {
                    progressBarCustom.cancelProgress();
                    error.printStackTrace();
                    if (error != null) {
                        Utils utils = new Utils(getActivity());
                        utils.showError(error);
                    }
                }
            });

        } catch (Exception e) {
            progressBarCustom.cancelProgress();
            e.printStackTrace();
        }
    }

    private void enableSwipe() {
        ItemTouchHelper.SimpleCallback simpleItemTouchCallback = new ItemTouchHelper.SimpleCallback(0, ItemTouchHelper.LEFT | ItemTouchHelper.RIGHT) {
            @Override
            public boolean onMove(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder, RecyclerView.ViewHolder target) {
                return false;
            }
            @Override
            public void onSwiped(RecyclerView.ViewHolder viewHolder, int direction) {
                int position = viewHolder.getAdapterPosition();
                if (resourceData.get(position).getIsPrimary() == 1 ) {
                    Log.i("onSwiped","primary card not delete");
                }else{
                    if (direction == ItemTouchHelper.LEFT) {
                        final int deletedPosition = position;
                        if (resourceData.size() > 0) {
                            for (int i = 0; i < resourceData.size(); i++) {
                                ResourceDatum resourceDatum = resourceData.get(i);
                                if (i == deletedPosition) {
                                    resourceDatum.setDeleteButtonShow(true);
                                } else {
                                    resourceDatum.setDeleteButtonShow(false);
                                }
                                resourceData.set(i,resourceDatum);
                            }
                            if (mycardAdapter != null) {
                                mycardAdapter.notifyDataSetChanged();
                            }
                        }
                    } else {
                        if (resourceData.size() > 0) {
                            for (int i = 0; i < resourceData.size(); i++) {
                                ResourceDatum resourceDatum = resourceData.get(i);
                                resourceDatum.setDeleteButtonShow(false);
                                resourceData.set(i,resourceDatum);
                            }
                            if (mycardAdapter != null) {
                                mycardAdapter.notifyDataSetChanged();
                            }
                        }
                    }
                }
            }
            @Override
            public int getMovementFlags(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder) {
                int position = viewHolder.getAdapterPosition();
                if (resourceData.get(position).getIsPrimary() == 1 ) {
                    return 0;
                }else{
                    return makeMovementFlags(0, ItemTouchHelper.LEFT | ItemTouchHelper.RIGHT);
                }
            }
        };
        ItemTouchHelper itemTouchHelper = new ItemTouchHelper(simpleItemTouchCallback);
        itemTouchHelper.attachToRecyclerView(other_card_list);
    }
}
