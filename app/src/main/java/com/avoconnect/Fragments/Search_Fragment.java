package com.avoconnect.Fragments;

import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.content.res.ResourcesCompat;
import androidx.fragment.app.Fragment;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.EditText;
import android.widget.PopupMenu;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.avoconnect.Constants.Constant;
import com.avoconnect.Prefrence.UserPrefrence;
import com.avoconnect.R;
import com.avoconnect.Util.EndlessScrollListener;
import com.avoconnect.Util.NonScrollListView;
import com.avoconnect.Util.ProgressBarCustom;
import com.avoconnect.Util.Utils;
import com.avoconnect.activity.EditProfileAcitivity;
import com.avoconnect.activity.EventsListActitivty;
import com.avoconnect.activity.PlanUserSubcription_Act;
import com.avoconnect.activity.RequestoView_Actitivty;
import com.avoconnect.activity.ShowOnlyProfile_Actitivty;
import com.avoconnect.adapter.SearchUserAapter;
import com.avoconnect.api.RestClient;
import com.avoconnect.beans.SearchUser;
import com.avoconnect.responsemodel.CheckRequested_Model;
import com.avoconnect.responsemodel.GetCompany_Model;
import com.avoconnect.responsemodel.GetQualificaation_Model;
import com.avoconnect.responsemodel.SearchUser_Model;
import com.avoconnect.responsemodel.SendNotificationRequest_Model;
import com.libaml.android.view.chip.ChipLayout;

import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;

import retrofit.RetrofitError;
import retrofit.client.Response;
import retrofit.mime.TypedByteArray;
import retrofit.mime.TypedInput;

public class Search_Fragment extends Fragment implements View.OnClickListener {

    NonScrollListView listview_searchuser;
    AutoCompleteTextView et_dsgination,et_qualification;
    TextView txt_search;
    RelativeLayout footerlay;
    View listfooter;
    int pageno=0;
    int pagedata=10;
    boolean isFirst=true;

    SearchUserAapter searchUserAapter;
    ArrayList<SearchUser_Model.UserResponce>arrayList=new ArrayList<>();
    GetQualificaation_Model res_global;
    int qualification_id=0,company_id=0;
    EditText et_fullname,et_skill;
    AutoCompleteTextView et_company;
    GetCompany_Model company_model_Res;
     ProgressBarCustom progressBarCustom_loading;
     TextView norecord_txt;
    TextView et_expereience;
    String year_count="";
    Spinner spinner;
    TextView et_expereience_label;
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
       View view=LayoutInflater.from(getActivity()).inflate(R.layout.search_fragment,null);
       return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        listview_searchuser=(NonScrollListView)getActivity().findViewById(R.id.listview_searchuser);
        et_dsgination = (AutoCompleteTextView)getActivity().findViewById(R.id.et_dsgination);
        et_qualification=(AutoCompleteTextView)getActivity().findViewById(R.id.et_qualification);
        txt_search=(TextView)getActivity().findViewById(R.id.txt_search);

        et_skill=(EditText) getActivity().findViewById(R.id.et_skill);
        et_expereience=(TextView) getActivity().findViewById(R.id.et_expereience);
        et_expereience_label=(TextView) getActivity().findViewById(R.id.et_expereience_label);
        spinner=(Spinner)getActivity().findViewById(R.id.spinner);
        progressBarCustom_loading=new ProgressBarCustom(getActivity());

        et_fullname=(EditText)getActivity().findViewById(R.id.et_fullname);
        norecord_txt=(TextView)getActivity().findViewById(R.id.norecord_txt);
     //   et_company=(AutoCompleteTextView)getActivity().findViewById(R.id.et_company);
        listfooter=LayoutInflater.from(getActivity()).inflate(R.layout.listviewfooter,null);
        footerlay=(RelativeLayout)listfooter.findViewById(R.id.footerlay);
        listview_searchuser.addFooterView(listfooter);
        footerlay.setVisibility(View.GONE);


        txt_search.setOnClickListener(this);
        et_expereience_label.setOnClickListener(this);

        callApigetQualification();

     //   et_expereience.setOnClickListener(this);

        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {

                view.setPadding(0, view.getPaddingTop(), view.getPaddingRight(), view.getPaddingBottom());

                final Typeface typeface = ResourcesCompat.getFont(getActivity(), R.font.montserrat_regular);
                ((TextView) adapterView.getChildAt(0)).setTypeface(typeface);

                year_count=firstTwo(adapterView.getItemAtPosition(i).toString());

                if(year_count.contains("-"))
                {
                    year_count=year_count.replaceAll("-","");
                    et_expereience.setVisibility(View.VISIBLE);
                    et_expereience_label.setVisibility(View.GONE);
                }else if(adapterView.getItemAtPosition(i).toString().length()==0){
                    year_count="";
                    et_expereience.setVisibility(View.GONE);
                    et_expereience_label.setVisibility(View.VISIBLE);
                }else{
                    et_expereience.setVisibility(View.VISIBLE);
                    et_expereience_label.setVisibility(View.GONE);
                }

                Log.e("========",year_count+""+"=="+adapterView.getItemAtPosition(i).toString().length());
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }


        });

        try{
            ArrayAdapter<CharSequence> qual_adapter = ArrayAdapter.createFromResource(getActivity(), R.array.professional_qualification_array,R.layout.simple_textview_spinner);
            qual_adapter.setDropDownViewResource(R.layout.drpdn_qual);
            spinner.setAdapter(qual_adapter);
        }catch (Exception e)
        {
            e.printStackTrace();
        }

    }

    private void checkvalidation() {

        try {
            boolean isreadycall = false;
            if (new Utils(getActivity()).isNetworkAvailable(getActivity()) == true) {

                if (et_fullname.getText().toString().length() != 0) {
                    isreadycall = true;
                }
//            else if(et_company.getText().toString().length()!=0)
//            {
//                isreadycall=true;
//            }
                else if (et_qualification.getText().toString().length() != 0) {
                    isreadycall = true;
                }
            else if(et_dsgination.getText().toString().length()!=0)
            {
                isreadycall=true;
            }
                else if (et_skill.getText().toString().length() != 0) {
                    isreadycall = true;
                } else if (year_count.length() != 0) {
                    isreadycall = true;
                }

                if (isreadycall == true) {
                    isFirst = true;
                    pageno = 0;
                    arrayList.clear();

                    if (res_global != null) {
                        for (int a = 0; a < res_global.getList().size(); a++) {
                            if (et_qualification.getText().toString().equalsIgnoreCase(res_global.getList().get(a).getName())) {
                                qualification_id = (res_global.getList().get(a).getQualificationId());
                                break;
                            }
                        }
                        callApiSearching(true);
//                        for (int k = 0; k < company_model_Res.getList().size(); k++) {
//                            if (et_company.getText().toString().equalsIgnoreCase(company_model_Res.getList().get(k).getName())) {
//                                company_id = (company_model_Res.getList().get(k).getId());
//                                break;
//                            }
//                        }
                    } else {
                        if (et_qualification.getText().toString().length() > 0) {
                            if (res_global != null) {
                                for (int a = 0; a < res_global.getList().size(); a++) {
                                    if (et_qualification.getText().toString().equalsIgnoreCase(res_global.getList().get(a).getName())) {
                                        qualification_id = (res_global.getList().get(a).getQualificationId());
                                        break;
                                    }
                                }
                            }
                        }
                        callApiSearching(true);
                    }

                } else {
                    ProgressBarCustom.customDialog(getActivity(), getResources().getString(R.string.please_selectone), false);
                }

            } else {
                ProgressBarCustom.customDialog(getActivity(), getResources().getString(R.string.internet_connection), false);
            }
        }catch (Exception e)
        {
            e.printStackTrace();
        }
    }

    private void callApiSearching(boolean showloader) {
        final ProgressBarCustom progressBarCustom = new ProgressBarCustom(getActivity());
        if(showloader==true) {
            progressBarCustom.showProgress();
        }else{

        }

        try {
            String token = UserPrefrence.getInstance(getActivity()).getAuthtoken();
            token="bearer " + token;

            JSONObject jsonObject3 = new JSONObject();
            if(et_qualification.getText().toString().length()!=0)
            {
                jsonObject3.put("qualificationId",qualification_id);
            }
//            if(et_company.getText().toString().length()!=0)
//            {
//                jsonObject3.put("companyId",company_id);
//            }
            if(et_skill.getText().toString().length()!=0)
            {
                jsonObject3.put("skill",et_skill.getText().toString());
            }
            if(year_count.length()!=0)
            {
                jsonObject3.put("experience",year_count.toString());
            }
            jsonObject3.put("userId",UserPrefrence.getInstance(getActivity()).getUserid());
           jsonObject3.put("designation",et_dsgination.getText().toString());
            jsonObject3.put("name",et_fullname.getText().toString());
            jsonObject3.put("pageNumber",pageno);
            jsonObject3.put("pageSize",pagedata);
            TypedInput in3 = new TypedByteArray("application/json", jsonObject3.toString().getBytes("UTF-8"));
            RestClient.getInstance(getActivity()).get().serachuserapi( token,in3, new retrofit.Callback<SearchUser_Model>() {

                @Override
                public void success(final SearchUser_Model res, Response response) {
                    progressBarCustom.cancelProgress();
                    footerlay.setVisibility(View.GONE);
                    if(res.getResponseMessage().equalsIgnoreCase(Constant.SUCCESS))
                    {
                            if(res.getList().size()>0)
                            {
                                norecord_txt.setVisibility(View.GONE);
                                listview_searchuser.setVisibility(View.VISIBLE);
                                if(isFirst==true)
                                {
                                    arrayList.addAll(res.getList());
                                    isFirst=false;
                                    setAdapter(arrayList);
                                }else{
                                    new Handler(Looper.getMainLooper()).post(new Runnable() {
                                        @Override
                                        public void run() {
                                            arrayList.addAll(res.getList());
                                            if(searchUserAapter!=null)
                                            {
                                                searchUserAapter.notifyDataSetChanged();
                                            }
                                        }
                                    });


                                }
                            }else{
                                if(isFirst==true) {
                                    norecord_txt.setVisibility(View.VISIBLE);
                                    listview_searchuser.setVisibility(View.GONE);
                                  //  Toast.makeText(getActivity(), "No Record found", Toast.LENGTH_SHORT).show();
                                }
                            }

                    }else{
                        if(isFirst==true) {
                            norecord_txt.setVisibility(View.VISIBLE);
                            listview_searchuser.setVisibility(View.GONE);
                        }
                    }


                }

                @Override
                public void failure(final RetrofitError error) {
                    progressBarCustom.cancelProgress();
                    error.printStackTrace();
                    if(isFirst==true) {
                        norecord_txt.setVisibility(View.VISIBLE);
                        listview_searchuser.setVisibility(View.GONE);
                        //  Toast.makeText(getActivity(), "No Record found", Toast.LENGTH_SHORT).show();
                    }
                    if (error != null) {
                        Utils utils=new Utils(getActivity());
                        utils.showError(error);
                    }

                }
            });

        }catch (Exception e)
        {
            progressBarCustom.cancelProgress();
            //progress.cancelProgress();
            e.printStackTrace();
        }

    }

    private void setAdapter(final  ArrayList<SearchUser_Model.UserResponce> arrayList) {
        searchUserAapter=new SearchUserAapter(getActivity(),arrayList);
        listview_searchuser.setAdapter(searchUserAapter);

        searchUserAapter.onCallBackReturn(new SearchUserAapter.Callback() {
            @Override
            public void clickaction(final int position) {


                new Handler(Looper.getMainLooper()).post(new Runnable() {
                    @Override
                    public void run() {
//
                        callApi(arrayList.get(position).getUserId()+"",position);
                    }
                });

            }
        });


        listview_searchuser.setOnScrollListener(new EndlessScrollListener() {
            @Override
            public boolean onLoadMore(int page, int totalItemsCount) {
                pageno++;
                //pagedata=pageno+pagedata;
                footerlay.setVisibility(View.VISIBLE);
                callApiSearching(false);
                return true;
            }
        });

    }

    private void callApi(String receiver_id, final int position) {
        final ProgressBarCustom progressBarCustom=new ProgressBarCustom(getActivity());
        progressBarCustom.showProgress();
        try {
            String token = UserPrefrence.getInstance(getActivity()).getAuthtoken();
            token="bearer " + token;
            RestClient.getInstance(getActivity()).get().checkAlreadyRequestedOrNotForSearchUserProfile( token,UserPrefrence.getInstance(getActivity()).getUserid()+"",
                    receiver_id+"",
                    new retrofit.Callback<CheckRequested_Model>() {

                        @Override
                        public void success(final CheckRequested_Model res, Response response) {
                            progressBarCustom.cancelProgress();

                            if(res.getResponseMessage().equalsIgnoreCase(Constant.SUCCESS))
                            {

                                if(res.getAccept()==true)
                                {
                                    if(res.getRequestSent()==true)
                                    {
                                        Intent  intent=new Intent(getActivity(), ShowOnlyProfile_Actitivty.class);
                                        intent.putExtra("sender_id",arrayList.get(position).getId()+"");
                                        //   intent.putExtra("name",arrayList.get(position).getDisplayName());
                                        startActivity(intent);
                                    }else{
                                        Intent  intent=new Intent(getActivity(), RequestoView_Actitivty.class);
                                        intent.putExtra("receiver_id",arrayList.get(position).getId()+"");
                                        if (arrayList.get(position).getUserProfileImage() != null &&
                                                arrayList.get(position).getUserProfileImage().length() > 0) {
                                            intent.putExtra("image_url",arrayList.get(position).getUserProfileImage());
                                        }
                                        if (arrayList.get(position).getUserProfileBanner() != null &&
                                                arrayList.get(position).getUserProfileBanner().length() > 0) {
                                            intent.putExtra("banner_url",arrayList.get(position).getUserProfileBanner());
                                        }
                                        intent.putExtra("name",arrayList.get(position).getDisplayName());
                                        startActivity(intent);
                                    }
                                }else{
                                    if(res.getRequestSent()==true)
                                    {
                                        ProgressBarCustom.customDialog(getActivity(),res.getMessage(),false);
                                    }else{
                                        Intent  intent=new Intent(getActivity(), RequestoView_Actitivty.class);
                                        intent.putExtra("receiver_id",arrayList.get(position).getId()+"");
                                        if (arrayList.get(position).getUserProfileImage() != null &&
                                                arrayList.get(position).getUserProfileImage().length() > 0) {
                                            intent.putExtra("image_url",arrayList.get(position).getUserProfileImage());
                                        }
                                        if (arrayList.get(position).getUserProfileBanner() != null &&
                                                arrayList.get(position).getUserProfileBanner().length() > 0) {
                                            intent.putExtra("banner_url",arrayList.get(position).getUserProfileBanner());
                                        }
                                        intent.putExtra("name",arrayList.get(position).getDisplayName());
                                        startActivity(intent);
                                    }
                                }
//                                if(res.getAccept()==false&&res.getRequestSent()==false)
//                                {
//                                   Intent  intent=new Intent(getActivity(), RequestoView_Actitivty.class);
//                                   intent.putExtra("receiver_id",arrayList.get(position).getId()+"");
//                                  intent.putExtra("name",arrayList.get(position).getDisplayName());
//                                  startActivity(intent);
//                                }
//                              else  if(res.getAccept()==true&&res.getRequestSent()==true)
//                                {
//                                    Intent  intent=new Intent(getActivity(), ShowOnlyProfile_Actitivty.class);
//                                    intent.putExtra("sender_id",arrayList.get(position).getId()+"");
//                                 //   intent.putExtra("name",arrayList.get(position).getDisplayName());
//                                    startActivity(intent);
//                                }
//                                else{
//                                 ProgressBarCustom.customDialog(getActivity(),"Request already send",false);
//                                }

                            }else{

                                if(res.getAccept()==true)
                                {
                                    if(res.getRequestSent()==true)
                                    {
                                        Intent  intent=new Intent(getActivity(), ShowOnlyProfile_Actitivty.class);
                                        intent.putExtra("sender_id",arrayList.get(position).getId()+"");
                                        //   intent.putExtra("name",arrayList.get(position).getDisplayName());
                                        startActivity(intent);
                                    }else{
                                        Intent  intent=new Intent(getActivity(), RequestoView_Actitivty.class);
                                        intent.putExtra("receiver_id",arrayList.get(position).getId()+"");
                                        if (arrayList.get(position).getUserProfileImage() != null &&
                                                arrayList.get(position).getUserProfileImage().length() > 0) {
                                            intent.putExtra("image_url",arrayList.get(position).getUserProfileImage());
                                        }
                                        if (arrayList.get(position).getUserProfileBanner() != null &&
                                                arrayList.get(position).getUserProfileBanner().length() > 0) {
                                            intent.putExtra("banner_url",arrayList.get(position).getUserProfileBanner());
                                        }
                                        intent.putExtra("name",arrayList.get(position).getDisplayName());
                                        startActivity(intent);
                                    }
                                }else{
                                    if(res.getRequestSent()==true)
                                    {
                                        ProgressBarCustom.customDialog(getActivity(),res.getMessage(),false);
                                    }else{
                                        Intent  intent=new Intent(getActivity(), RequestoView_Actitivty.class);
                                        intent.putExtra("receiver_id",arrayList.get(position).getId()+"");
                                        if (arrayList.get(position).getUserProfileImage() != null &&
                                                arrayList.get(position).getUserProfileImage().length() > 0) {
                                            intent.putExtra("image_url",arrayList.get(position).getUserProfileImage());
                                        }
                                        if (arrayList.get(position).getUserProfileBanner() != null &&
                                                arrayList.get(position).getUserProfileBanner().length() > 0) {
                                            intent.putExtra("banner_url",arrayList.get(position).getUserProfileBanner());
                                        }
                                        intent.putExtra("name",arrayList.get(position).getDisplayName());
                                        startActivity(intent);
                                    }
                                }

                            }

                        }
                        @Override
                        public void failure(final RetrofitError error) {
                            progressBarCustom.cancelProgress();
                            error.printStackTrace();
                            if (error != null) {
                                Utils utils=new Utils(getActivity());
                                utils.showError(error);
                            }

                        }
                    });
        }catch (Exception e)
        {
            progressBarCustom.cancelProgress();
            //progress.cancelProgress();
            e.printStackTrace();
        }
    }


    private void callApigetQualification() {
        progressBarCustom_loading.showProgress();
        try {
            String token =UserPrefrence.getInstance(getActivity()).getAuthtoken();
            token="bearer " + token;
            RestClient.getInstance(getActivity()).get().getQalificationList( token, new retrofit.Callback<GetQualificaation_Model>() {

                @Override
                public void success(final GetQualificaation_Model res, Response response) {
                    progressBarCustom_loading.cancelProgress();
                    if(res.getResponseMessage().equalsIgnoreCase(Constant.SUCCESS))
                    {

                        res_global=res;
                        ArrayList<String> movies = new ArrayList<String>();
                        for(int a=0;a<res.getList().size();a++)
                        {
                            movies.add(res.getList().get(a).getName());
                        }

                            try {

                                ArrayAdapter<String> adapter = new ArrayAdapter<String>(getActivity(), R.layout.simple_textview, movies);
                                et_qualification.setAdapter(adapter);
                                et_qualification.setThreshold(1);

                              //  getCompanyList();
                            }catch (Exception e)
                            {
                                progressBarCustom_loading.cancelProgress();
                                e.printStackTrace();
                            }

                    }else{
                        progressBarCustom_loading.cancelProgress();
                    }


                }

                @Override
                public void failure(final RetrofitError error) {
                    progressBarCustom_loading.cancelProgress();
                    error.printStackTrace();
                    if (error != null) {
                        Utils utils=new Utils(getActivity());
                        utils.showError(error);
                    }

                }
            });

        }catch (Exception e)
        {
            progressBarCustom_loading.cancelProgress();
            //progress.cancelProgress();
            e.printStackTrace();
        }

    }

    private void getCompanyList() {

        try {
            String token =UserPrefrence.getInstance(getActivity()).getAuthtoken();
            token="bearer " + token;
            RestClient.getInstance(getActivity()).get().getCompany( token, new retrofit.Callback<GetCompany_Model>() {

                @Override
                public void success(final GetCompany_Model res, Response response) {
                    progressBarCustom_loading.cancelProgress();
                    if(res.getResponseMessage().equalsIgnoreCase(Constant.SUCCESS))
                    {
                        company_model_Res=res;
                        ArrayList<String> movies = new ArrayList<String>();
                        for(int a=0;a<res.getList().size();a++)
                        {
                            if(res.getList().get(a).getDisplayName()!=null) {
                                if (res.getList().get(a).getDisplayName().length() > 0) {
                                    movies.add(res.getList().get(a).getDisplayName());
                                }
                            }
                        }

                        try {
                            ArrayAdapter<String> adapter = new ArrayAdapter<String>(getActivity(),R.layout.simple_textview, movies);
                            et_company.setAdapter(adapter);



//                            ArrayList<String> movies_month = new ArrayList<String>();
//                            movies_month.add("0-1 year");
//                            movies_month.add("1-2 years");
//                            movies_month.add("2-3 years");
//                            movies_month.add("3-4 years");
//                            movies_month.add("4-5 years");
//                            movies_month.add("5-6 years");
//                            movies_month.add("6-7 years");
//                            movies_month.add("7-8 years");
//                            movies_month.add("8-9 years");
//                            movies_month.add("9-10 years");
//                            movies_month.add("10 & More years");

//                            ArrayAdapter adapter_spinner = new ArrayAdapter(getActivity(), R.layout.simple_textview_spinner, movies_month);
//                            adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
//                            spinner.setAdapter(adapter_spinner);


                        }catch (Exception e)
                        {
                            e.printStackTrace();
                        }
                    }else{

                    }


                }

                @Override
                public void failure(final RetrofitError error) {
                    progressBarCustom_loading.cancelProgress();
                    error.printStackTrace();
                    if (error != null) {
                        Utils utils=new Utils(getActivity());
                        utils.showError(error);
                    }

                }
            });
        }catch (Exception e)
        {
            progressBarCustom_loading.cancelProgress();
            //progress.cancelProgress();
            e.printStackTrace();
        }

    }


    @Override
    public void onClick(View view) {
        switch (view.getId())
        {
            case R.id.txt_upgrade:
                if(Utils.yesNoDialog!=null&&Utils.yesNoDialog.isShowing()==true)
                {
                    Intent intent11=new Intent(getActivity(), PlanUserSubcription_Act.class);
                    startActivityForResult(intent11,155);
                    Utils.yesNoDialog.dismiss();
                }
                break;

            case R.id.txt_search:
            //    checkvalidation();
                if(UserPrefrence.getInstance(getActivity()).getUsertype().equalsIgnoreCase(Constant.USERTYPE_PREMIUM)
                        ||UserPrefrence.getInstance(getActivity()).getUsertype().equalsIgnoreCase(Constant.USERTYPE_CORPORATE)
                ){
                    checkvalidation();
                }else{
                    Utils.customYesDialog(this,getContext());
                }
                break;

            case R.id.et_expereience:
                showPopup(view);
                break;
            case R.id.et_expereience_label:
                spinner.performClick();
                break;
        }
    }


    public void showPopup(View v) {


        PopupMenu popup = new PopupMenu(getActivity(), v);

        ArrayList<String> movies_month = new ArrayList<String>();
        movies_month.add("0-1 year");
        movies_month.add("1-2 years");
        movies_month.add("2-3 years");
        movies_month.add("3-4 years");
        movies_month.add("4-5 years");
        movies_month.add("5-6 years");
        movies_month.add("6-7 years");
        movies_month.add("7-8 years");
        movies_month.add("8-9 years");
        movies_month.add("9-10 years");
        movies_month.add("10 & More years");

        for(int p=0;p<movies_month.size();p++)
        {
            popup.getMenu().add(Menu.NONE, 1, Menu.NONE, movies_month.get(p).toString());
        }

        popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
            public boolean onMenuItemClick(MenuItem item) {
                et_expereience.setText(item.getTitle());
                year_count=firstTwo(item.getTitle().toString());
                if(year_count.contains("-"))
                {
                    year_count=year_count.replaceAll("-","");
                }
                Log.e("========",year_count+"");
                return true;
            }
        });
        popup.show();
    }

    public String firstTwo(String str) {

        if(str.length()<2){
            return str;
        }
        else{
            return str.substring(0,2);
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if(requestCode==155&&resultCode==getActivity().RESULT_OK)
        {
            isFirst = true;
            pageno = 0;
            arrayList.clear();
        }
    }
}
