package com.avoconnect.Fragments;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.graphics.Color;
import android.os.Bundle;
import androidx.annotation.NonNull;

import com.avoconnect.Constants.Constant;
import com.avoconnect.Prefrence.UserPrefrence;
import com.avoconnect.Util.ProgressBarCustom;
import com.avoconnect.activity.DetialsCardActivity;
import com.avoconnect.api.RestClient;
import com.avoconnect.responsemodel.TemplateColorModel;
import com.google.android.material.bottomsheet.BottomSheetBehavior;
import com.google.android.material.bottomsheet.BottomSheetDialogFragment;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import androidx.cardview.widget.CardView;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.avoconnect.R;
import com.avoconnect.Util.Utils;

import retrofit.RetrofitError;
import retrofit.client.Response;

public class BottomSheetFragment extends BottomSheetDialogFragment implements View.OnClickListener {

    //Bottom Sheet Callback
    Dialog dialog_gloabla;
    Callback callback;
    String themecolor;
    CardView cardview1,cardview2,cardview3,cardview4,cardview5;
    ImageView text1,text2,text3,text4,text5;
    TemplateColorModel rescolor_global;
    TextView txt_deafult,txt_yellow,txt_blue,txt_green,txt_red;
    private BottomSheetBehavior.BottomSheetCallback mBottomSheetBehaviorCallback = new BottomSheetBehavior.BottomSheetCallback() {

        @Override
        public void onStateChanged(@NonNull View bottomSheet, int newState) {
            if (newState == BottomSheetBehavior.STATE_HIDDEN) {
                dismiss();
            }

        }

        @Override
        public void onSlide(@NonNull View bottomSheet, float slideOffset) {
        }
    };

    @SuppressLint("RestrictedApi")
    @Override
    public void setupDialog(Dialog dialog, int style) {
        super.setupDialog(dialog, style);
        //Get the content View
        dialog_gloabla=dialog;
        View contentView = View.inflate(getContext(), R.layout.simplecard, null);

         txt_deafult=(TextView)contentView.findViewById(R.id.txt_deafult) ;
         txt_yellow=(TextView)contentView.findViewById(R.id.txt_yellow) ;
         txt_blue=(TextView)contentView.findViewById(R.id.txt_blue) ;
         txt_green=(TextView)contentView.findViewById(R.id.txt_green) ;
         txt_red=(TextView)contentView.findViewById(R.id.txt_red) ;

         text1=(ImageView)contentView.findViewById(R.id.text1) ;
         text2=(ImageView)contentView.findViewById(R.id.text2) ;
         text3=(ImageView)contentView.findViewById(R.id.text3) ;
         text4=(ImageView)contentView.findViewById(R.id.text4) ;
         text5=(ImageView)contentView.findViewById(R.id.text5) ;

         cardview1=(CardView)contentView.findViewById(R.id.cardview1);
         cardview2=(CardView)contentView.findViewById(R.id.cardview2);
         cardview3=(CardView)contentView.findViewById(R.id.cardview3);
         cardview4=(CardView)contentView.findViewById(R.id.cardview4);
         cardview5=(CardView)contentView.findViewById(R.id.cardview5);


         cardview1.setOnClickListener(this);
         cardview2.setOnClickListener(this);
         cardview3.setOnClickListener(this);
         cardview4.setOnClickListener(this);
         cardview5.setOnClickListener(this);

        Utils utils=new Utils(getContext());
        utils.setTextview(txt_deafult,1);
        utils.setTextview(txt_yellow,1);
        utils.setTextview(txt_blue,1);
        utils.setTextview(txt_green,1);
        utils.setTextview(txt_red,1);



        dialog.setContentView(contentView);


        Bundle bundle = this.getArguments();
        if (bundle != null) {
            themecolor = bundle.getString("themecolor", "");

            if(themecolor.contains("#"))
            {

            }else{
                themecolor="#"+themecolor;

            }

            Log.e("=======",themecolor+"");

        }

        callApiColors();



        //Set the coordinator layout behavior
        CoordinatorLayout.LayoutParams params = (CoordinatorLayout.LayoutParams) ((View) contentView.getParent()).getLayoutParams();
        CoordinatorLayout.Behavior behavior = params.getBehavior();

        //Set callback
        if (behavior != null && behavior instanceof BottomSheetBehavior) {
            ((BottomSheetBehavior) behavior).setBottomSheetCallback(mBottomSheetBehaviorCallback);
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId())
        {
            case R.id.cardview1:
                if (callback != null) {

                    if(rescolor_global!=null) {
                        callback.clickaction(rescolor_global.getResourceData().get(0).getColorCode());
                    }
                }
               dialog_gloabla.dismiss();
                break;
            case R.id.cardview2:
                if (callback != null) {
                    if(rescolor_global!=null) {
                        callback.clickaction(rescolor_global.getResourceData().get(1).getColorCode());
                    }
                }
                dialog_gloabla.dismiss();
                break;
            case R.id.cardview3:
                if (callback != null) {
                    if(rescolor_global!=null) {
                        callback.clickaction(rescolor_global.getResourceData().get(2).getColorCode());
                    }
                }
                dialog_gloabla.dismiss();
                break;
            case R.id.cardview4:
                if (callback != null) {

                    if(rescolor_global!=null) {
                        callback.clickaction(rescolor_global.getResourceData().get(3).getColorCode());
                    }
                }
                dialog_gloabla.dismiss();
                break;
            case R.id.cardview5:
                if (callback != null) {
                    if(rescolor_global!=null) {
                        callback.clickaction(rescolor_global.getResourceData().get(4).getColorCode());
                    }
                }
                dialog_gloabla.dismiss();
                break;
        }
    }

    public  void onCallBackReturn(Callback callback)
    {
        this.callback=callback;
    }

    public interface Callback{
        void clickaction(String colorcode);


    }


    private void callApiColors() {


        final ProgressBarCustom progressBarCustom=new ProgressBarCustom(getActivity());
        progressBarCustom.showProgress();

        try {
            String token = UserPrefrence.getInstance(getActivity()).getAuthtoken();
            token="bearer " + token;

            RestClient.getInstance(getActivity()).get().getAllTemplateColors(token,new retrofit.Callback<TemplateColorModel>() {

                @Override
                public void success(final TemplateColorModel res, Response response) {
                    progressBarCustom.cancelProgress();
                    if(res.getResponseMessage().equalsIgnoreCase(Constant.SUCCESS))
                    {
                        if(res.getResourceData()!=null)
                        {

                             rescolor_global=res;

                            cardview1.getBackground().setTint(Color.parseColor(res.getResourceData().get(0).getColorCode()));
                            cardview2.getBackground().setTint(Color.parseColor(res.getResourceData().get(1).getColorCode()));
                            cardview3.getBackground().setTint(Color.parseColor(res.getResourceData().get(2).getColorCode()));
                            cardview4.getBackground().setTint(Color.parseColor(res.getResourceData().get(3).getColorCode()));
                            cardview5.getBackground().setTint(Color.parseColor(res.getResourceData().get(4).getColorCode()));



                            txt_deafult.setText(res.getResourceData().get(0).getTitle());
                            txt_yellow.setText(res.getResourceData().get(1).getTitle());
                            txt_blue.setText(res.getResourceData().get(2).getTitle());
                            txt_green.setText(res.getResourceData().get(3).getTitle());
                            txt_red.setText(res.getResourceData().get(4).getTitle());

                            if(themecolor!=null&&themecolor.length()>0) {

                                if (themecolor.equalsIgnoreCase(res.getResourceData().get(0).getColorCode())) {
                                    text1.setVisibility(View.VISIBLE);
                                } else if (themecolor.equalsIgnoreCase(res.getResourceData().get(1).getColorCode())) {
                                    text2.setVisibility(View.VISIBLE);
                                } else if (themecolor.equalsIgnoreCase(res.getResourceData().get(2).getColorCode())) {
                                    text3.setVisibility(View.VISIBLE);
                                } else if (themecolor.equalsIgnoreCase(res.getResourceData().get(3).getColorCode())) {
                                    text4.setVisibility(View.VISIBLE);
                                } else if (themecolor.equalsIgnoreCase(res.getResourceData().get(4).getColorCode())) {
                                    text5.setVisibility(View.VISIBLE);
                                }
                            }

                        }

                    }else{
                        Toast.makeText(getActivity(), res.getResponseMessage(), Toast.LENGTH_SHORT).show();
                    }
                }

                @Override
                public void failure(final RetrofitError error) {
                    progressBarCustom.cancelProgress();
                    if (error != null) {
                        Utils utils=new Utils(getActivity());
                        utils.showError(error);
                    }
                }
            });

        }catch (Exception e)
        {
            progressBarCustom.cancelProgress();
            //progress.cancelProgress();
            e.printStackTrace();
        }
    }
    
}
