package com.avoconnect.Fragments;

import android.Manifest;
import android.content.BroadcastReceiver;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Color;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.avoconnect.BottomSheetFragments.CallMessageFragment;
import com.avoconnect.Util.EndlessScrollListener;
import com.avoconnect.activity.FilterScreen_Recieved;
import com.avoconnect.activity.PlanUserSubcription_Act;
import com.avoconnect.activity.SearchNearByActivity;
import com.avoconnect.beans.ReceiveCard_Baen;
import com.avoconnect.responsemodel.ContactAdd_Model;
import com.avoconnect.responsemodel.TemplateColorModel;
import com.google.android.material.bottomsheet.BottomSheetBehavior;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.Fragment;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;

import android.os.Handler;
import android.os.Looper;
import android.os.SystemClock;
import android.provider.ContactsContract;
import android.provider.Settings;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.avoconnect.BottomSheetFragments.AddInfoOtherUserFragment;
import com.avoconnect.BottomSheetFragments.AddManualScanFragment;
import com.avoconnect.Constants.Constant;
import com.avoconnect.Prefrence.UserPrefrence;
import com.avoconnect.R;
import com.avoconnect.Util.NonScrollListView;
import com.avoconnect.Util.ProgressBarCustom;
import com.avoconnect.Util.Utils;
import com.avoconnect.activity.DetialsCardActivity;
import com.avoconnect.adapter.ReceivedcardAdapter;
import com.avoconnect.api.RestClient;
import com.avoconnect.responsemodel.ReceivedCard_Model;
import com.karumi.dexter.Dexter;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.PermissionDeniedResponse;
import com.karumi.dexter.listener.PermissionGrantedResponse;
import com.karumi.dexter.listener.PermissionRequest;
import com.karumi.dexter.listener.single.PermissionListener;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;
import retrofit.mime.TypedByteArray;
import retrofit.mime.TypedInput;

import static android.content.Context.LOCATION_SERVICE;

public class ReceieveCard_Fragment extends Fragment implements View.OnClickListener{

    NonScrollListView other_card_list;
    List<Boolean> deleteCardList = new ArrayList<>();
    ReceivedcardAdapter mycardAdapter = null;
    private BottomSheetBehavior mBottomSheetBehavior;
    FloatingActionButton card_add;
    ImageView nearbyIcon,img_filter,image_serach;
    EditText et_search;
    int pagenumber=0,pagecount=10;
    JSONArray jsonArray_lead,jsonArray_label;
    List<ReceiveCard_Baen>receiveCard_baens=new ArrayList<>();
    String warm_lead="",hot_lead="",connected_lead="",label_Str="";

    RelativeLayout footerlay;
    View listfooter;
    boolean isFirst=true;
    private long mLastClickTime = 0;
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = LayoutInflater.from(getActivity()).inflate(R.layout.receivecard_fragment, null);
        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        other_card_list = (NonScrollListView) getActivity().findViewById(R.id.other_card_list);
        card_add=(FloatingActionButton)getActivity().findViewById(R.id.card_add);
        LinearLayout bottomSheet =(LinearLayout)getActivity().findViewById(R.id.bottom_sheet);

        jsonArray_lead=new JSONArray();
        jsonArray_label=new JSONArray();

        img_filter=(ImageView)getActivity().findViewById(R.id.img_filter);
        image_serach=(ImageView)getActivity().findViewById(R.id.image_serach);

        listfooter=LayoutInflater.from(getActivity()).inflate(R.layout.listviewfooter,null);
        footerlay=(RelativeLayout)listfooter.findViewById(R.id.footerlay);
        other_card_list.addFooterView(listfooter);
        footerlay.setVisibility(View.GONE);

        et_search=(EditText)getActivity().findViewById(R.id.et_search);
        img_filter.setOnClickListener(this);
        image_serach.setOnClickListener(this);

        mBottomSheetBehavior = BottomSheetBehavior.from(bottomSheet);
        mBottomSheetBehavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
        mBottomSheetBehavior.setPeekHeight(0);

        card_add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                AddManualScanFragment bottomSheetFragment = new AddManualScanFragment();
                bottomSheetFragment.show(getActivity().getSupportFragmentManager(), bottomSheetFragment.getTag());
                bottomSheetFragment.onCallBackReturn(new AddManualScanFragment.Callback() {
                    @Override
                    public void clickaction(int position) {
                        Log.e("======coming adding","====redirection");
                        pagenumber=0;
                        pagecount=10;
                        isFirst=true;
                        receiveCard_baens.clear();
                        callApireceivecard(true);

                    }
                });
            }
        });

        nearbyIcon=(ImageView)getActivity().findViewById(R.id.nearbyIcon);
        nearbyIcon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                LocationManager service = (LocationManager) getActivity().getSystemService(LOCATION_SERVICE);
                boolean enabled = service.isProviderEnabled(LocationManager.GPS_PROVIDER);
                // Check if enabled and if not send user to the GPS settings
                if (!enabled) {
                    showAlert();
                }
                else {
                    Dexter.withActivity(getActivity())
                            .withPermission(Manifest.permission.ACCESS_FINE_LOCATION)
                            .withListener(new PermissionListener() {
                                @Override
                                public void onPermissionGranted(PermissionGrantedResponse response) {

                                    Intent intent = new Intent(getActivity(), SearchNearByActivity.class);
                                    intent.putExtra("is_enable", "yes");
                                    startActivity(intent);

                                }

                                @Override
                                public void onPermissionDenied(PermissionDeniedResponse response) {

                                }

                                @Override
                                public void onPermissionRationaleShouldBeShown(PermissionRequest permission, PermissionToken token) {

                                }
                            }).check();
                }


            }
        });

        if(UserPrefrence.getInstance(getActivity()).getUsertype().equalsIgnoreCase(Constant.USERTYPE_PREMIUM)
                ||UserPrefrence.getInstance(getActivity()).getUsertype().equalsIgnoreCase(Constant.USERTYPE_CORPORATE)
        ){

        }else{
            et_search.setFocusable(false);
            et_search.setClickable(true);
            et_search.setOnClickListener(this);
        }



        et_search.addTextChangedListener(new TextWatcher() {

            @Override
            public void onTextChanged(CharSequence cs, int arg1, int arg2, int arg3) {
                // When user changed the Text
               // MainActivity.this.adapter.getFilter().filter(cs);
                if(et_search.getText().toString().length()==0)
                {
                    pagenumber=0;
                    pagecount=10;
                    receiveCard_baens.clear();
                    isFirst=true;
                    callApireceivecard(true);
                }
            }

            @Override
            public void beforeTextChanged(CharSequence arg0, int arg1, int arg2,
                                          int arg3) {
                // TODO Auto-generated method stub

            }

            @Override
            public void afterTextChanged(Editable arg0) {
                // TODO Auto-generated method stub
            }
        });

        et_search.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_DONE) {
                    if(et_search.getText().toString().length()!=0)
                    {
                        pagenumber=0;
                        pagecount=10;
                        receiveCard_baens.clear();
                        isFirst=true;
                        callApireceivecard(true);
                    }
                    return true;
                }
                return false;
            }
        });



        callApireceivecard(true);
    }

    private void showAlert() {
        final AlertDialog.Builder dialog = new AlertDialog.Builder(getActivity());
        dialog.setTitle("Enable Location")
                .setMessage("Your Locations Settings is set to 'Off'.\nPlease Enable Location to " +
                        "use this app")
                .setPositiveButton("Location Settings", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface paramDialogInterface, int paramInt) {
                        Intent myIntent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                        startActivity(myIntent);
                    }
                })
                .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface paramDialogInterface, int paramInt) {
                    }
                });
        dialog.show();
    }

    @Override
    public void onResume() {
        super.onResume();
        Log.e("=======onResume","======onresume");
        LocalBroadcastManager.getInstance(getActivity()).registerReceiver(notificationCounterBroadcast
                , new IntentFilter("notificattions"));

    }

    public BroadcastReceiver notificationCounterBroadcast = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent != null) {
                String click_action = intent.getStringExtra("click_action");
               if(click_action.equalsIgnoreCase("card_received"))
               {
                       Log.e("======refresh", click_action);
                           if (SystemClock.elapsedRealtime() - mLastClickTime < 3000) {
                               return;
                           }
                           mLastClickTime = SystemClock.elapsedRealtime();
                           pagenumber = 0;
                           pagecount = 10;
                           receiveCard_baens.clear();
                           isFirst = true;
                           callApireceivecard(true);
               }
            }
        }

    };


    @Override
    public void onDestroy() {
        LocalBroadcastManager.getInstance(getActivity()).unregisterReceiver(notificationCounterBroadcast);
        super.onDestroy();
    }


    private void callApireceivecard(boolean status) {
        try {
           final ProgressBarCustom progressBarCustom = new ProgressBarCustom(getActivity());
            if(status==true) {

                progressBarCustom.showProgress();
            }
            try {
                String token = UserPrefrence.getInstance(getActivity()).getAuthtoken();
                token = "bearer " + token;


                JSONObject jsonObject3 = new JSONObject();
                jsonObject3.put("userId",Integer.parseInt(UserPrefrence.getInstance(getActivity()).getUserid()));
                jsonObject3.put("leadType",jsonArray_lead);
                jsonObject3.put("label",jsonArray_label);
                jsonObject3.put("order","desc");
                jsonObject3.put("searchString",et_search.getText().toString());
             //   jsonObject3.put("cardId",card_id);
                TypedInput in3 = new TypedByteArray("application/json", jsonObject3.toString().getBytes("UTF-8"));
                RestClient.getInstance(getActivity()).get().receivedcards(token, in3
                        ,pagenumber,pagecount,
                        new Callback<ReceivedCard_Model>() {

                    @Override
                    public void success(final ReceivedCard_Model res, Response response) {

                        if(progressBarCustom!=null)
                        {
                            progressBarCustom.cancelProgress();
                        }
                        progressBarCustom.cancelProgress();
                        footerlay.setVisibility(View.GONE);
                        if (res.getResponseMessage().equalsIgnoreCase(Constant.SUCCESS)) {
                            if (res.getResourceData() != null) {
                                if (res.getResourceData().size() > 0) {

                                    if(isFirst==true)
                                    {
                                        receiveCard_baens.clear();
                                        other_card_list.setVisibility(View.VISIBLE);
                                        for(int a=0;a<res.getResourceData().size();a++)
                                        {
                                            ReceiveCard_Baen receiveCard_baen=new ReceiveCard_Baen();
                                            receiveCard_baen.setAutoShare(res.getResourceData().get(a).getAutoShare());
                                            receiveCard_baen.setBackgroundImage(res.getResourceData().get(a).getBackgroundImage());
                                            receiveCard_baen.setCardBackGroundColor(res.getResourceData().get(a).getCardBackGroundColor());
                                            receiveCard_baen.setCardId(res.getResourceData().get(a).getCardId());
                                            receiveCard_baen.setEmail(res.getResourceData().get(a).getEmail());
                                            receiveCard_baen.setIsPrimary(res.getResourceData().get(a).getIsPrimary());
                                            receiveCard_baen.setLeadType(res.getResourceData().get(a).getLeadType());
                                            receiveCard_baen.setLogo(res.getResourceData().get(a).getLogo());
                                            receiveCard_baen.setMobile(res.getResourceData().get(a).getMobile());
                                            receiveCard_baen.setName(res.getResourceData().get(a).getName());
                                            receiveCard_baen.setCardAddInContact(res.getResourceData().get(a).getCardAddInContact());
                                            receiveCard_baens.add(receiveCard_baen);

                                        }
                                        isFirst=false;
                                        setAdapter(receiveCard_baens);
                                    }else{
                                        if(pagenumber==0)
                                        {
                                            receiveCard_baens.clear();
                                        }
                                        new Handler(Looper.getMainLooper()).post(new Runnable() {
                                            @Override
                                            public void run() {
                                                for(int a=0;a<res.getResourceData().size();a++)
                                                {
                                                    ReceiveCard_Baen receiveCard_baen=new ReceiveCard_Baen();
                                                    receiveCard_baen.setAutoShare(res.getResourceData().get(a).getAutoShare());
                                                    receiveCard_baen.setBackgroundImage(res.getResourceData().get(a).getBackgroundImage());
                                                    receiveCard_baen.setCardBackGroundColor(res.getResourceData().get(a).getCardBackGroundColor());
                                                    receiveCard_baen.setCardId(res.getResourceData().get(a).getCardId());
                                                    receiveCard_baen.setEmail(res.getResourceData().get(a).getEmail());
                                                    receiveCard_baen.setIsPrimary(res.getResourceData().get(a).getIsPrimary());
                                                    receiveCard_baen.setLeadType(res.getResourceData().get(a).getLeadType());
                                                    receiveCard_baen.setLogo(res.getResourceData().get(a).getLogo());
                                                    receiveCard_baen.setMobile(res.getResourceData().get(a).getMobile());
                                                    receiveCard_baen.setName(res.getResourceData().get(a).getName());
                                                    receiveCard_baen.setCardAddInContact(res.getResourceData().get(a).getCardAddInContact());
                                                    receiveCard_baens.add(receiveCard_baen);

                                                }
                                                if(mycardAdapter!=null)
                                                {
                                                    mycardAdapter.notifyDataSetChanged();
                                                }
                                            }
                                        });
                                    }
                                }else{
                                    if(isFirst==true) {
                                        other_card_list.setVisibility(View.GONE);
                                    }
                                }
                            }
                        } else {
                            ProgressBarCustom.customDialog(getActivity(), res.getMessage(), false);
                        }

                    }

                    @Override
                    public void failure(final RetrofitError error) {
                        if(progressBarCustom!=null)
                        {
                            progressBarCustom.cancelProgress();
                        }
                        error.printStackTrace();
                        if (error != null) {
                            Utils utils = new Utils(getActivity());
                            utils.showError(error);
                        }
                    }
                });

            } catch (Exception e) {
                if(progressBarCustom!=null)
                {
                    progressBarCustom.cancelProgress();
                }
                e.printStackTrace();
            }
        }catch (Exception e)
        {
            e.printStackTrace();
        }
    }

    private void setAdapter( final List<ReceiveCard_Baen> res) {

        mycardAdapter = new ReceivedcardAdapter(getActivity(), res);
        other_card_list.setAdapter(mycardAdapter);


        mycardAdapter.onCallBackReturn(new ReceivedcardAdapter.Callback() {
            @Override
            public void clickaction(int position) {
                Intent intent=new Intent(getActivity(), DetialsCardActivity.class);
                intent.putExtra("additional_info","yes");
                intent.putExtra("card_id",res.get(position).getCardId()+"");
                startActivityForResult(intent, 144);
            }

            @Override
            public void share_action(int position) {
                AddInfoOtherUserFragment bottomSheetFragment = new AddInfoOtherUserFragment();
                Bundle bundle=new Bundle();
                bundle.putString("card_id",res.get(position).getCardId()+"");
                bundle.putString("card_username",res.get(position).getName()+"");
                bundle.putString("lead_type",res.get(position).getLeadType()+"");
//                if(res.getResourceData().)
//                bundle.putString("tag",res.getResourceData().get(position).getLeadType()+"");
//                bundle.putString("lable","");
//                bundle.putString("note",res.getResourceData().get(position).get());
//                bundle.putString("location",res.getResourceData().get(position).getCardId());
//                bundle.putString("link",res.getResourceData().get(position).getCardId());
                bottomSheetFragment.setArguments(bundle);
                //bottomSheetFragment.setTargetFragment(getParentFragment());
                bottomSheetFragment.show(getActivity().getSupportFragmentManager(), bottomSheetFragment.getTag());

                bottomSheetFragment.onCallBackReturn(new AddInfoOtherUserFragment.Callback() {
                    @Override
                    public void clickaction(int position) {
                        Log.e("======coming check","====redirection");

                        isFirst=true;
                        pagenumber=0;
                        pagecount=10;
                        receiveCard_baens.clear();
                        callApireceivecard(true);
                    }
                });
            }

            @Override
            public void addcontact(String name,String phonenumber,int position) {
                try {

                    String[] values=null;
                    String phonenumbers="";
                    if(phonenumber.contains(","))
                    {
                       values = phonenumber.split(",");
                        phonenumbers=values[0]+"";
                    }else{
                        phonenumbers=phonenumber;
                    }

                    CallMessageFragment bottomSheetFragment = new CallMessageFragment();
                    Bundle bundle=new Bundle();
                    bundle.putString("phonenumber",phonenumbers+"");
                    bottomSheetFragment.setArguments(bundle);
                    //bottomSheetFragment.setTargetFragment(getParentFragment());
                    bottomSheetFragment.show(getActivity().getSupportFragmentManager(), bottomSheetFragment.getTag());


//                    String[] values=null;
//
//                    if(phonenumber.contains(","))
//                    {
//                       values = phonenumber.split(",");
//                    }
//
//                    if(values!=null&&values.length>0){
//                        Intent intent = new Intent(ContactsContract.Intents.Insert.ACTION);
//                        intent.setType(ContactsContract.Contacts.CONTENT_TYPE);
//                        ArrayList<ContentValues> data = new ArrayList<ContentValues>();
//
//                        //Filling data with phone numbers
//                        for (int i = 0; i < values.length; i++) {
//                            ContentValues row = new ContentValues();
//                            row.put(ContactsContract.Contacts.Data.MIMETYPE, ContactsContract.CommonDataKinds.Phone.CONTENT_ITEM_TYPE);
//                            row.put(ContactsContract.CommonDataKinds.Phone.NUMBER, values[i].toString());
//                            row.put(ContactsContract.CommonDataKinds.Phone.TYPE, ContactsContract.RawContacts.CONTENT_TYPE);
//                            data.add(row);
//                        }
//
//                        intent.putExtra(ContactsContract.Intents.Insert.NAME, name);
//                        intent.putParcelableArrayListExtra(ContactsContract.Intents.Insert.DATA, data);
//                        startActivity(intent);
//                    }else{
//                        Intent contactIntent = new Intent(ContactsContract.Intents.Insert.ACTION);
//                        contactIntent.setType(ContactsContract.RawContacts.CONTENT_TYPE);
//                        contactIntent.putExtra(ContactsContract.Intents.Insert.NAME, name)
//                                .putExtra(ContactsContract.Intents.Insert.PHONE, phonenumber);
//                        startActivity(contactIntent);
//                    }




                }catch (Exception e)
                {
                    e.printStackTrace();
                }
            }
            @Override
            public void addcontact_number(String name, String phonenumber, int position) {


                callApiAddContact(name,phonenumber,position);

            }
            @Override
            public void sendemail(String email) {
                try {
                    Intent intent = new Intent(Intent.ACTION_SENDTO);
                    intent.setData(Uri.parse("mailto:" + email)); // only email apps should handle this
                    intent.putExtra(Intent.EXTRA_EMAIL, email);
                    intent.putExtra(Intent.EXTRA_SUBJECT, "Avoconnect");
                    if (intent.resolveActivity(getActivity().getPackageManager()) != null) {
                        startActivity(intent);
                    }
                }catch (Exception e)
                {
                    e.printStackTrace();
                }
            }
        });


        other_card_list.setOnScrollListener(new EndlessScrollListener() {
            @Override
            public boolean onLoadMore(int page, int totalItemsCount) {
                 pagenumber++;
                //pagedata=pageno+pagedata;
                footerlay.setVisibility(View.VISIBLE);
                callApireceivecard(false);
                return true;
            }
        });

    }

    private void callApiAddContact(final String name,final String phonenumber, final int position) {
        final ProgressBarCustom progressBarCustom=new ProgressBarCustom(getActivity());
        progressBarCustom.showProgress();

        try {
            String token = UserPrefrence.getInstance(getActivity()).getAuthtoken();
            token="bearer " + token;

            RestClient.getInstance(getActivity()).get().addcontact(token,receiveCard_baens.get(position).getCardId()+"",
                    true,new retrofit.Callback<ContactAdd_Model>() {

                @Override
                public void success(final ContactAdd_Model res, Response response) {
                    progressBarCustom.cancelProgress();
                    if(res.getResponseMessage().equalsIgnoreCase(Constant.SUCCESS))
                    {

                            Handler handler = new Handler();
                            handler.post(new Runnable() {
                                @Override
                                public void run() {
                                    receiveCard_baens.get(position).setCardAddInContact(true);
                                    if(mycardAdapter!=null)
                                    {
                                        mycardAdapter.notifyDataSetChanged();
                                    }
                                }
                            });

                            String[] values=null;
                            if(phonenumber.contains(","))
                            {
                                values = phonenumber.split(",");
                            }

                            if(values!=null&&values.length>0){
                                Intent intent = new Intent(ContactsContract.Intents.Insert.ACTION);
                                intent.setType(ContactsContract.Contacts.CONTENT_TYPE);
                                ArrayList<ContentValues> data = new ArrayList<ContentValues>();

                                //Filling data with phone numbers
                                for (int i = 0; i < values.length; i++) {
                                    ContentValues row = new ContentValues();
                                    row.put(ContactsContract.Contacts.Data.MIMETYPE, ContactsContract.CommonDataKinds.Phone.CONTENT_ITEM_TYPE);
                                    row.put(ContactsContract.CommonDataKinds.Phone.NUMBER, values[i].toString());
                                    row.put(ContactsContract.CommonDataKinds.Phone.TYPE, ContactsContract.RawContacts.CONTENT_TYPE);
                                    data.add(row);
                                }

                                intent.putExtra(ContactsContract.Intents.Insert.NAME, name);
                                intent.putParcelableArrayListExtra(ContactsContract.Intents.Insert.DATA, data);
                                startActivity(intent);
                            }else{
                                Intent contactIntent = new Intent(ContactsContract.Intents.Insert.ACTION);
                                contactIntent.setType(ContactsContract.RawContacts.CONTENT_TYPE);
                                contactIntent.putExtra(ContactsContract.Intents.Insert.NAME, name)
                                        .putExtra(ContactsContract.Intents.Insert.PHONE, phonenumber);
                                startActivity(contactIntent);
                            }

                    }else{
                        Toast.makeText(getActivity(), res.getResponseMessage(), Toast.LENGTH_SHORT).show();
                    }
                }

                @Override
                public void failure(final RetrofitError error) {
                    progressBarCustom.cancelProgress();
                    if (error != null) {
                        Utils utils=new Utils(getActivity());
                        utils.showError(error);
                    }
                }
            });

        }catch (Exception e)
        {
            progressBarCustom.cancelProgress();
            //progress.cancelProgress();
            e.printStackTrace();
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 144 && resultCode == getActivity().RESULT_OK) {
            Log.e("======144","====redirection");
            isFirst=true;
            pagenumber=0;
            pagecount=10;
            receiveCard_baens.clear();
            callApireceivecard(true);
        }
        else if(requestCode==155&&resultCode==getActivity().RESULT_OK){
            Log.e("======155","====redirection");
            isFirst=true;
            pagenumber=0;
            pagecount=10;

            et_search.setClickable(true);
            et_search.setFocusable(true);

            receiveCard_baens.clear();
            callApireceivecard(true);
        }
        if (requestCode == 121 && resultCode == getActivity().RESULT_OK) {
           Log.e("=====filter_Apply","=====filter");
           if(data!=null) {
               try {
                   pagenumber=0;
                   pagecount=10;
                   receiveCard_baens.clear();
                   isFirst=true;
                   jsonArray_lead = new JSONArray();
                   jsonArray_label = new JSONArray();
                   if (data.getStringExtra("warm_type") != null) {
                       if (!data.getStringExtra("warm_type").equalsIgnoreCase("0")) {
                           System.out.println("========" + "" + data.getStringExtra("warm_type") + "");
                           jsonArray_lead.put(data.getStringExtra("warm_type"));
                           warm_lead="warm_lead";
                       }else{
                           warm_lead="";
                       }
                   }
                   if (data.getStringExtra("hot_type") != null) {
                       if (!data.getStringExtra("hot_type").equalsIgnoreCase("0")) {
                           jsonArray_lead.put(data.getStringExtra("hot_type"));
                           hot_lead="hot_lead";
                       }else{
                           hot_lead="";
                       }
                   }
                   if (data.getStringExtra("connected") != null) {
                       if (!data.getStringExtra("connected").equalsIgnoreCase("0")) {
                           jsonArray_lead.put(data.getStringExtra("connected"));
                           connected_lead="connected_lead";
                       }else{
                           connected_lead="";
                       }
                   }
                   if (data.getStringExtra("label") != null) {
                       if (!data.getStringExtra("label").equalsIgnoreCase("")) {
                           jsonArray_label.put(data.getStringExtra("label"));
                           label_Str=data.getStringExtra("label");
                       }
                       else{
                           label_Str="";
                       }
                   }
                   receiveCard_baens.clear();
                   callApireceivecard(true);
               }catch (Exception e)
               {
                   e.printStackTrace();
               }
           }
        }
    }


    @Override
    public void onClick(View view) {
        switch (view.getId())
        {
            case R.id.img_filter:
                if(UserPrefrence.getInstance(getActivity()).getUsertype().equalsIgnoreCase(Constant.USERTYPE_PREMIUM)
                        ||UserPrefrence.getInstance(getActivity()).getUsertype().equalsIgnoreCase(Constant.USERTYPE_CORPORATE)
                ){

                    Intent intent=new Intent(getActivity(), FilterScreen_Recieved.class);
                    intent.putExtra("warm_lead",warm_lead);
                    intent.putExtra("hot_lead",hot_lead);
                    intent.putExtra("cold_lead",connected_lead);
                    intent.putExtra("label",label_Str);

                    startActivityForResult(intent,121);
                     //   openfilter(view);
                }else{
                    Utils.customYesDialog(this,getContext());
                }
                break;
            case R.id.image_serach:
                if(UserPrefrence.getInstance(getActivity()).getUsertype().equalsIgnoreCase(Constant.USERTYPE_PREMIUM)
                        ||UserPrefrence.getInstance(getActivity()).getUsertype().equalsIgnoreCase(Constant.USERTYPE_CORPORATE)
                ){
                    if(et_search.getText().toString().length()==0)
                    {
                        Toast.makeText(getActivity(), "Invalid Search", Toast.LENGTH_SHORT).show();
                    }else{
                         pagenumber=0;
                         pagecount=10;
                        receiveCard_baens.clear();
                        isFirst=true;
                        callApireceivecard(true);
                    }
                }else{
                    Utils.customYesDialog(this,getContext());
                }
                break;

            case R.id.txt_upgrade:
                if(Utils.yesNoDialog!=null&&Utils.yesNoDialog.isShowing()==true)
                {
                    Intent intent11=new Intent(getActivity(), PlanUserSubcription_Act.class);
                    startActivityForResult(intent11,155);
                    Utils.yesNoDialog.dismiss();
                }
                break;
            case R.id.et_search:
                if(UserPrefrence.getInstance(getActivity()).getUsertype().equalsIgnoreCase(Constant.USERTYPE_PREMIUM)
                        ||UserPrefrence.getInstance(getActivity()).getUsertype().equalsIgnoreCase(Constant.USERTYPE_CORPORATE)
                ){

                }else{
                    Utils.customYesDialog(this,getContext());
                }
                break;

        }
    }




}
