package com.avoconnect.Fragments;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.os.Handler;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.avoconnect.BottomSheetFragments.ShareCardFragment;
import com.avoconnect.Constants.Constant;
import com.avoconnect.Prefrence.UserPrefrence;
import com.avoconnect.R;
import com.avoconnect.Util.NonScrollListView;

import com.avoconnect.Util.ProgressBarCustom;
import com.avoconnect.Util.Utils;
import com.avoconnect.activity.EditProfileAcitivity;
import com.avoconnect.activity.HomeActivity;
import com.avoconnect.activity.UpdateProfile_Actitivty;
import com.avoconnect.adapter.AdditionalLink_Adapter;
import com.avoconnect.adapter.MyExpereinceAdapter;
import com.avoconnect.adapter.MySkillaAdpater;
import com.avoconnect.api.RestClient;

import com.avoconnect.responsemodel.DeleteExpereince_Model;
import com.avoconnect.responsemodel.DeleteSkills_Model;
import com.avoconnect.responsemodel.GetUserProfile_Model;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.google.android.gms.vision.text.Line;
import com.twitter.sdk.android.core.models.Image;
import com.twitter.sdk.android.core.models.User;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

import static android.app.Activity.RESULT_OK;

public class Profile_Fragment extends Fragment implements View.OnClickListener{

    NonScrollListView listview_myskills,listview_myexperience;
    LinearLayout tab_myskills,tab_media;
    private ImageView edit_card,card_view_image,image_profile,navigation_icon_img,notification_icon_img;
    TextView txt_username,txt_email,txt_uerid,txt_qualification,txt_designnation,txt_alter_mobile;
    GetUserProfile_Model res_global;
    MySkillaAdpater mySkillaAdpater;
    MyExpereinceAdapter myExpereinceAdapter;
    Utils utils;
    TextView txt_user_placeholder,txt_mobile;
    TextView txt_badge_icon;
    Profilecallback  profilecallback;
    LinearLayout mobile_lay,mobile_alter_lay;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = LayoutInflater.from(getActivity()).inflate(R.layout.profile_fragment, null);
        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        listview_myskills=(NonScrollListView)getActivity().findViewById(R.id.listview_myskills);
        listview_myexperience=(NonScrollListView)getActivity().findViewById(R.id.listview_myexperience);
        tab_myskills=(LinearLayout)getActivity().findViewById(R.id.tab_myskills);
        mobile_alter_lay=(LinearLayout)getActivity().findViewById(R.id.mobile_alter_lay);
        tab_media=(LinearLayout)getActivity().findViewById(R.id.tab_media);
        edit_card=(ImageView)getActivity().findViewById(R.id.edit_card);
        txt_mobile=(TextView)getActivity().findViewById(R.id.txt_mobile);
        mobile_lay=(LinearLayout) getActivity().findViewById(R.id.mobile_lay);
        txt_qualification=(TextView)getActivity().findViewById(R.id.txt_qualification);
        txt_alter_mobile=(TextView)getActivity().findViewById(R.id.txt_alter_mobile);
        txt_designnation=(TextView)getActivity().findViewById(R.id.txt_designnation);
        txt_badge_icon=(TextView)getActivity().findViewById(R.id.txt_badge_icon);
        card_view_image=(ImageView)getActivity().findViewById(R.id.card_view_image);
        image_profile=(ImageView)getActivity().findViewById(R.id.image_profile);
        txt_user_placeholder= (TextView) getActivity().findViewById(R.id.txt_user_placeholder);
        navigation_icon_img=(ImageView)getActivity().findViewById(R.id.navigation_icon_img);
        notification_icon_img=(ImageView)getActivity().findViewById(R.id.notification_icon_img);
        txt_username=(TextView)getActivity().findViewById(R.id.txt_username);
        txt_email=(TextView)getActivity().findViewById(R.id.txt_email);
        txt_uerid=(TextView)getActivity().findViewById(R.id.txt_uerid);


        utils=new Utils(getActivity());
        edit_card.setOnClickListener(this);
        tab_myskills.setOnClickListener(this);
        tab_media.setOnClickListener(this);
        navigation_icon_img.setOnClickListener(this);
        notification_icon_img.setOnClickListener(this);
        profilecallback=(Profilecallback)getActivity();
        setBadge();

        callApi();

    }

    @Override
    public void onResume() {
        super.onResume();
        LocalBroadcastManager.getInstance(getActivity()).registerReceiver(notificationCounterBroadcast
                , new IntentFilter("notificattions"));
    }

    private void setBadge() {
        if(UserPrefrence.getInstance(getActivity()).getNotificationUnread().length()>0)
        {
            txt_badge_icon.setVisibility(View.VISIBLE);
        }else{
            txt_badge_icon.setVisibility(View.GONE);
        }
    }



    public BroadcastReceiver notificationCounterBroadcast = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent != null) {
                String click_action = intent.getStringExtra("click_action");
                Log.e("=======checlik",click_action+"");
                UserPrefrence.getInstance(getActivity()).setNotificationUnread(click_action);
                setBadge();
            }
        }

    };


    @Override
    public void onDestroy() {
        LocalBroadcastManager.getInstance(getActivity()).unregisterReceiver(notificationCounterBroadcast);
        super.onDestroy();

    }

    private void callApi() {

        final ProgressBarCustom progressBarCustom=new ProgressBarCustom(getActivity());
        progressBarCustom.showProgress();
        try {
            String token = UserPrefrence.getInstance(getActivity()).getAuthtoken();
            token="bearer " + token;
            RestClient.getInstance(getActivity()).get().getuserprofile(token,UserPrefrence.getInstance(getActivity()).getUserid(), new Callback<GetUserProfile_Model>() {

                @Override
                public void success(final GetUserProfile_Model res, Response response) {
                    progressBarCustom.cancelProgress();

                        if(res.getResponseMessage().equalsIgnoreCase(Constant.SUCCESS))
                        {
                            res_global=res;
                            setData(res);
                        }else{
                            ProgressBarCustom.customDialog(getActivity(),res.getMessage(),false);
                        }

                }

                @Override
                public void failure(final RetrofitError error) {
                    progressBarCustom.cancelProgress();
                    error.printStackTrace();
                    if (error != null) {
                        Utils utils=new Utils(getActivity());
                        utils.showError(error);
                    }
                }
            });

        }catch (Exception e)
        {
            progressBarCustom.cancelProgress();
            e.printStackTrace();
        }


    }

    private void setData( GetUserProfile_Model res) {

        if(Utils.checkNull(res.getFirstName()))
        {
            if(Utils.checkNull(res.getLastName()))
            {
                txt_username.setText(res.getFirstName()+" "+res.getLastName());
            }else{
                txt_username.setText(res.getFirstName());
            }

        }

        txt_email.setText(res.getEmail());
        txt_uerid.setText("User Id: "+res.getAvoId());

        if(res.getDesignation()!=null)
        {
            txt_designnation.setText(res.getDesignation());
        }

        if(res.getQualification()!=null)
        {
            txt_qualification.setText(res.getQualification());
        }else{
            txt_qualification.setText("");
        }

        if(res.getAlternateMobile()!=null&&res.getAlternateMobile().length()>0)
        {
            mobile_alter_lay.setVisibility(View.VISIBLE);
            txt_alter_mobile.setText(res.getAlternateMobile());
        }else{
            mobile_alter_lay.setVisibility(View.GONE);
        }

        mobile_lay.setVisibility(View.VISIBLE);
        txt_mobile.setVisibility(View.VISIBLE);

        if(res.getMobile()!=null&&res.getMobile().length()>0)
        {
            txt_mobile.setText(res.getMobile()+"");
        }


        if(res.getUserProfileImage()!=null &&res.getUserProfileImage().length()>0) {
            image_profile.setVisibility(View.VISIBLE);
           txt_user_placeholder.setVisibility(View.GONE);
            Glide.with(this)
                    .load(res.getUserProfileImage())
                    .apply(RequestOptions.circleCropTransform())
                    .into(image_profile);
        }else if(Utils.checkNull(res.getFirstName())) {
            image_profile.setVisibility(View.GONE);
           txt_user_placeholder.setVisibility(View.VISIBLE);
           String name="";
           if(Utils.checkNull(res.getFirstName())){
               if(Utils.checkNull(res.getLastName())){
                   name=res.getFirstName()+" "+res.getLastName();
               }else {
                   name=res.getFirstName();
               }
               txt_user_placeholder.setText(Utils.nameInital(name).trim() + "");
           }

        }

        if(res.getUserProfileBanner()!=null
                &&res.getUserProfileBanner().length()>0)
        {
            Glide.with(this)
                    .load(res.getUserProfileBanner())
                    .into(card_view_image);
        }else{
            card_view_image.setImageResource(R.drawable.bridege);
        }
        setAdapter(res);
    }

    private void setAdapter(final GetUserProfile_Model res) {

         mySkillaAdpater=new MySkillaAdpater(getActivity(),res, "");
        listview_myskills.setAdapter(mySkillaAdpater);

        mySkillaAdpater.onCallBackReturn(new MySkillaAdpater.Callback() {
            @Override
            public void clickaction(int position) {
               DeleteSkillsApi(res,res.getUserSkillResponse().get(position).getSkillId()+"",position);
            }
        });

         myExpereinceAdapter=new MyExpereinceAdapter(getActivity(),res, "");
        listview_myexperience.setAdapter(myExpereinceAdapter);

        myExpereinceAdapter.onCallBackReturn(new MyExpereinceAdapter.Callback() {
            @Override
            public void clickaction(int position) {
                DeleteExperinceApi(res,res.getUserExperienceResponse().get(position).getUserExperienceId()+"",position);
            }
        });
    }

    private void DeleteSkillsApi(final GetUserProfile_Model res_gloabl,String ids,final int position) {
        final ProgressBarCustom progressBarCustom=new ProgressBarCustom(getActivity());
        progressBarCustom.showProgress();
        try {
            String token = UserPrefrence.getInstance(getActivity()).getAuthtoken();
            token="bearer " + token;
            RestClient.getInstance(getActivity()).get().deleteskills(token,UserPrefrence.getInstance(getActivity()).getUserid(),ids, new Callback<DeleteSkills_Model>() {

                @Override
                public void success(final DeleteSkills_Model res, Response response) {
                    progressBarCustom.cancelProgress();

                    if(res.getResponseMessage().equalsIgnoreCase(Constant.SUCCESS))
                    {

                        Handler handler = new Handler();
                        handler.post(new Runnable() {
                            @Override
                            public void run() {
                                res_gloabl.getUserSkillResponse().remove(position);
                                mySkillaAdpater.notifyDataSetChanged();
                            }
                        });

                    }else{
                        ProgressBarCustom.customDialog(getActivity(),res.getMessage(),false);
                    }

                }

                @Override
                public void failure(final RetrofitError error) {
                    progressBarCustom.cancelProgress();
                    error.printStackTrace();
                    if (error != null) {
                        Utils utils=new Utils(getActivity());
                        utils.showError(error);
                    }
                }
            });

        }catch (Exception e)
        {
            progressBarCustom.cancelProgress();
            e.printStackTrace();
        }
    }
    private void DeleteExperinceApi(final GetUserProfile_Model res_gloabl, String ids, final int position) {
        final ProgressBarCustom progressBarCustom=new ProgressBarCustom(getActivity());
        progressBarCustom.showProgress();
        try {
            String token = UserPrefrence.getInstance(getActivity()).getAuthtoken();
            token="bearer " + token;
            RestClient.getInstance(getActivity()).get().deleteexpeirence(token,UserPrefrence.getInstance(getActivity()).getUserid(), ids,new Callback<DeleteExpereince_Model>() {

                @Override
                public void success(final DeleteExpereince_Model res, Response response) {
                    progressBarCustom.cancelProgress();
                    if(res.getResponseMessage().equalsIgnoreCase(Constant.SUCCESS))
                    {
                        Handler handler = new Handler();
                        handler.post(new Runnable() {
                            @Override
                            public void run() {
                                res_gloabl.getUserExperienceResponse().remove(position);
                                myExpereinceAdapter.notifyDataSetChanged();
                            }
                        });
                    }else{
                        ProgressBarCustom.customDialog(getActivity(),res.getMessage(),false);
                    }


                }

                @Override
                public void failure(final RetrofitError error) {
                    progressBarCustom.cancelProgress();
                    error.printStackTrace();
                    if (error != null) {
                        Utils utils=new Utils(getActivity());
                        utils.showError(error);
                    }
                }
            });

        }catch (Exception e)
        {
            progressBarCustom.cancelProgress();
            e.printStackTrace();
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId())
        {
            case R.id.tab_media:
                Intent intent=new Intent(getActivity(), UpdateProfile_Actitivty.class);
                intent.putExtra("update_type","Add Experience");
                startActivityForResult(intent,111);

                break;
            case R.id.tab_myskills:
                Intent intent01=new Intent(getActivity(), UpdateProfile_Actitivty.class);
                intent01.putExtra("update_type","Add Skill");
                startActivityForResult(intent01,111);
                break;

            case R.id.navigation_icon_img:


                if (profilecallback != null) {
                    profilecallback.clickaction(0);
                }
                break;
            case R.id.notification_icon_img:
                if (profilecallback != null) {
                    UserPrefrence.getInstance(getActivity()).setNotificationUnread("");
                    txt_badge_icon.setVisibility(View.GONE);
                    profilecallback.clickaction(1);
                }
                break;




            case R.id.edit_card:
                if(utils.isNetworkAvailable(getActivity())) {
                    if(res_global!=null) {
                        String name="";
                        Intent intent02 = new Intent(getActivity(), EditProfileAcitivity.class);
                        intent02.putExtra("update_type", "My Profile");

                        if(Utils.checkNull(res_global.getFirstName())) {
                            if (Utils.checkNull(res_global.getLastName())) {
                                name = res_global.getFirstName() + " " + res_global.getLastName();
                            } else {
                                name = res_global.getFirstName();
                            }
                           // intent02.putExtra("name", res_global.getFirstName() + " " + res_global.getLastName());
                            intent02.putExtra("name", name);
                        }

                        intent02.putExtra("email", res_global.getEmail());
                        intent02.putExtra("userid", "User Id:" + res_global.getAvoId() + "");
                        intent02.putExtra("desingation",res_global.getDesignation());
                        intent02.putExtra("userImage",res_global.getUserProfileImage());
                        intent02.putExtra("bannerImage",res_global.getUserProfileBanner());
                        if(res_global.getQualification()!=null)
                        {
                            intent02.putExtra("qualification",res_global.getQualification());
                            intent02.putExtra("qualification_id",res_global.getQualificationId()+"");
                        }

                        if(res_global.getAlternateMobile()!=null)
                        {
                            intent02.putExtra("alternate",res_global.getAlternateMobile());
                        }
                        if(res_global.getMobile()!=null)
                        {
                            Log.e("=====mobile===",res_global.getMobile());
                            intent02.putExtra("mobile",res_global.getMobile());
                        }

                        startActivityForResult(intent02, 111);
                    }
                }else{
                    ProgressBarCustom.customDialog(getActivity(),getResources().getString(R.string.internet_connection),false);
                }
                break;
        }
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if(requestCode==111&&resultCode==RESULT_OK) {

            callApi();

            if (!UserPrefrence.getInstance(getActivity()).getUsertype().equalsIgnoreCase(Constant.USERTYPE_FREE)) {
                res_global.setMobile(UserPrefrence.getInstance(getActivity()).getMobile());
                res_global.setAlternateMobile(UserPrefrence.getInstance(getActivity()).getAlternatemobile());
            }

        }
    }

    public interface Profilecallback {
        void clickaction(int position);
    }
}
