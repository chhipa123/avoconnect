package com.avoconnect.Fragments;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.google.android.material.bottomsheet.BottomSheetBehavior;
import com.google.android.material.bottomsheet.BottomSheetDialogFragment;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import android.view.View;
import android.webkit.URLUtil;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.avoconnect.R;

public class SoacialLinkTitleFragment  extends BottomSheetDialogFragment implements View.OnClickListener{

    boolean isclick=false;
    TextView linktitle_lay;
     LinearLayout dropdown_lay;
     ImageView selected_logo;
    EditText et_link;
    private BottomSheetBehavior.BottomSheetCallback mBottomSheetBehaviorCallback = new BottomSheetBehavior.BottomSheetCallback() {

        @Override
        public void onStateChanged(@NonNull View bottomSheet, int newState) {
//            if (newState == BottomSheetBehavior.STATE_HIDDEN) {
//                dismiss();
//            }

        }

        @Override
        public void onSlide(@NonNull View bottomSheet, float slideOffset) {

        }
    };

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }

    @SuppressLint("RestrictedApi")
    @Override
    public void setupDialog(Dialog dialog, int style) {
        super.setupDialog(dialog, style);
        //Get the content View
        View contentView = View.inflate(getContext(), R.layout.addlink_layout, null);
       dropdown_lay=(LinearLayout)contentView.findViewById(R.id.dropdown_lay);
         linktitle_lay=(TextView) contentView.findViewById(R.id.linktitle_lay);
        ImageView hintEmailIcon=(ImageView)contentView.findViewById(R.id.hintEmailIcon);
        ImageView cross_img=(ImageView)contentView.findViewById(R.id.cross_img);
        selected_logo=(ImageView)contentView.findViewById(R.id.selected_logo);

         et_link=(EditText)contentView.findViewById(R.id.et_link);
        LinearLayout facebook_lay=(LinearLayout)contentView.findViewById(R.id.facebook_lay);
        LinearLayout twitter_lay=(LinearLayout)contentView.findViewById(R.id.twitter_lay);
        LinearLayout linkein_lay=(LinearLayout)contentView.findViewById(R.id.linkein_lay);
        LinearLayout intagram_lay=(LinearLayout)contentView.findViewById(R.id.intagram_lay);
        LinearLayout pinterest_lay=(LinearLayout)contentView.findViewById(R.id.pinterest_lay);

        TextView txt_submit=(TextView)contentView.findViewById(R.id.txt_submit);

        facebook_lay.setOnClickListener(this);
        twitter_lay.setOnClickListener(this);
        linkein_lay.setOnClickListener(this);
        intagram_lay.setOnClickListener(this);
        pinterest_lay.setOnClickListener(this);

        txt_submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                validation();
            }
        });

        try {
            hintEmailIcon.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (isclick == false) {
                        isclick = true;
                        dropdown_lay.setVisibility(View.VISIBLE);
                    } else {
                        isclick = false;
                        dropdown_lay.setVisibility(View.GONE);
                    }
                }
            });
        linktitle_lay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                    if (isclick == false) {
                        isclick = true;
                        dropdown_lay.setVisibility(View.VISIBLE);
                    } else {
                        isclick = false;
                        dropdown_lay.setVisibility(View.GONE);
                    }
            }
        });
        }catch (Exception e)
        {
            e.printStackTrace();
        }
        dialog.setContentView(contentView);
        cross_img.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent intent=new Intent();
                getActivity().setResult(Activity.RESULT_CANCELED,intent);
                getActivity().finish();
            }
        });
        et_link.setEnabled(false);
        //Set the coordinator layout behavior
        CoordinatorLayout.LayoutParams params = (CoordinatorLayout.LayoutParams) ((View) contentView.getParent()).getLayoutParams();
        CoordinatorLayout.Behavior behavior = params.getBehavior();

        //Set callback
        if (behavior != null && behavior instanceof BottomSheetBehavior) {
            ((BottomSheetBehavior) behavior).setBottomSheetCallback(mBottomSheetBehaviorCallback);
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.facebook_lay:
                et_link.setEnabled(true);
                et_link.setText("");
                linktitle_lay.setText(getResources().getString(R.string.facebook_txt));
                selected_logo.setVisibility(View.VISIBLE);
                selected_logo.setImageDrawable(getResources().getDrawable(R.drawable.fb));
                dropdown_lay.setVisibility(View.GONE);
                break;
            case R.id.twitter_lay:
                et_link.setEnabled(true);
                et_link.setText("");
                linktitle_lay.setText(getResources().getString(R.string.twitter_txt));
                selected_logo.setVisibility(View.VISIBLE);
                selected_logo.setImageDrawable(getResources().getDrawable(R.drawable.twitter_img));
                dropdown_lay.setVisibility(View.GONE);
                break;
            case R.id.linkein_lay:
                et_link.setEnabled(true);
                et_link.setText("");
                linktitle_lay.setText(getResources().getString(R.string.linkein_txt));
                selected_logo.setVisibility(View.VISIBLE);
                selected_logo.setImageDrawable(getResources().getDrawable(R.drawable.linkedin_img));
                dropdown_lay.setVisibility(View.GONE);
                break;
            case R.id.pinterest_lay:
                et_link.setEnabled(true);
                et_link.setText("");
                linktitle_lay.setText(getResources().getString(R.string.pinterest_txt));
                selected_logo.setVisibility(View.VISIBLE);
                selected_logo.setImageDrawable(getResources().getDrawable(R.drawable.pintrest));
                dropdown_lay.setVisibility(View.GONE);
                break;
            case R.id.intagram_lay:
                et_link.setEnabled(true);
                et_link.setText("");
                linktitle_lay.setText(getResources().getString(R.string.intagram_txt));
                selected_logo.setVisibility(View.VISIBLE);
                selected_logo.setImageDrawable(getResources().getDrawable(R.drawable.intagram));
                dropdown_lay.setVisibility(View.GONE);
                break;
        }
    }

    private void validation() {
        String getText=linktitle_lay.getText().toString();
        String getlink=et_link.getText().toString();
        if(getText.length()==0)
        {
            Toast.makeText(getActivity(), getResources().getString(R.string.select_link_title), Toast.LENGTH_SHORT).show();
        }else if(getlink.length()==0)
        {
            Toast.makeText(getActivity(), getResources().getString(R.string.enter_link), Toast.LENGTH_SHORT).show();
        }
//        else if(URLUtil.isValidUrl(getlink)==false)
//        {
//            Toast.makeText(getActivity(), getResources().getString(R.string.enter_valid_url), Toast.LENGTH_SHORT).show();
//        }
        else{
            Intent intent=new Intent();
            intent.putExtra("value",getText);
            intent.putExtra("link",getlink);
            intent.putExtra("type","social");
            intent.putExtra("image_type",checkcase());
            getActivity().setResult(Activity.RESULT_OK,intent);
            getActivity().finish();
        }

    }

    private String checkcase() {
        String image_type="";
        String getText=linktitle_lay.getText().toString();
        if(getText.equalsIgnoreCase(getResources().getString(R.string.facebook_txt)))
        {
            image_type=getResources().getString(R.string.facebook_txt);
        }
       else if(getText.equalsIgnoreCase(getResources().getString(R.string.twitter_txt)))
        {
            image_type=getResources().getString(R.string.twitter_txt);
        }
        else  if(getText.equalsIgnoreCase(getResources().getString(R.string.linkein_txt)))
        {
            image_type=getResources().getString(R.string.linkein_txt);
        }
        else if(getText.equalsIgnoreCase(getResources().getString(R.string.intagram_txt)))
        {
            image_type=getResources().getString(R.string.intagram_txt);
        }
        else  if(getText.equalsIgnoreCase(getResources().getString(R.string.pinterest_txt)))
        {
            image_type=getResources().getString(R.string.pinterest_txt);
        }
       return image_type;
    }
}